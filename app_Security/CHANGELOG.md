## [1.0.0.0](<http://demo.ibizlab.cn/vue_v2/vue_r6_res/tree/v1.0.0.0>) 

*2019-12-14*

#### bug修复

- app-popover
  - 火狐浏览器兼容性修复（[#10551a8e](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/10551a8ef270feb7f853d54661547c2abb4c56a4) by @lxm）
- srfkey为空时打开编辑视图
  - 打开编辑视图前增加srfkey非空校验（[#fe5f74e2](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/fe5f74e29d92eaa8c46d3b280036d220eb3b16ee) by @lxm）
- 非路由打开的父视图内，子视图的路由跳转
  - 模板：视图增加isModal属性，前台调用，新增，编辑路由跳转前判断isModal（[#23a94ed0](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/23a94ed08028f2a339e3edcec6000b8aca303e6e) by @lxm）
  - 基础文件：app-popover、app-drawer、app-modal打开视图时isModal设为true（[#dc6016e2](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/dc6016e21bebfd1e5455e964ae909553a04c1eb4) by @lxm）
- 文本框输入数值为小数，会出现异常
  - input-box中对CurrentVal值处理进行修改  ([#fa681e4e](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/fa681e4e2f4cafc9042b9024d7752349dc1f41b5) by @sww)
- 编辑器是数据选择和地址框，值变更，不会触发表单值校验
  - 模板：表单部件增加校验单个表单项方法（[#77acfead](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/77acfeade92ee0c96f297ba78846d3505364b483) by @zpc）
  - 基础文件：增加表单校验插件async-validator，数据选择编辑器抛值（[#632dab6b](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/632dab6bee460e2fb8100dd02ce1bb80a2b29164) by @zpc）
- app-mpicker
  - 修复app-mpicker当进行数据请求过滤时，返回数据为空时仍赋值（[#7c349275](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/7c349275d5e75a1df25e747ba03c69423b295041) by @sww）
- app-picker
  - 修复app-picker中ac输入关键字发送请求时进行数据过滤([#39428fd8a8](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/39428fd8a873be3b8d2851333cb58b48af773cf0) by @sww)
- 首页配置菜单打开重定向视图
  - 首页配置菜单打开重定向视图时提示报错信息（[#c18492cd](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/c18492cd4a0c358d21eda8c93df6a1a86f643fc9) by @sww）
- 修复数据关系分页部件参数传递问题
  - 修复数据关系分页部件参数传递问题（[#34e24c21](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/34e24c213210e12036570cca5ee4b8c577ed43fe) by @zpc）
- app-user
  - 修复右上角用户名不显示的问题（[#51b2442b](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/51b2442be611c2a5df59c643069aa5b9c6e3388b) by @lxm  [#3b359368](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/3b359368ffec625b73b70914d54d1f38f983d5be) by @sww）
- 表单和搜索表单 iframe 成员模板编写问题
  - 修复表单和搜索表单 iframe 成员模板编写问题（ [#8a498924](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/8a49892428e5e724a15690123168ef1fd911e4aa) by @sww）
- 表格行编辑获取编辑器报错问题
  - 表格行编辑获取编辑器报错修复（[#6b5ed17a](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/6b5ed17a3481616213ec96405d101ee7591a6568)  by @sww）
- 首页菜单配置菜单方向**不显示**支持
  - 首页菜单配置菜单方向**不显示**修复（[#db443699](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/db443699fb45e7c6cf83b24fe83538a17509d011) by @sww）
- 向导页面非模态完成,不会关闭页面问题
  - 修复向导页面 非模态 完成 不会关闭页面（[#4ef50093](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/4ef500933a58b79bcc46c3b14d0b0a75bdeb1c12)  by @zpc）
- 修复表格处理数据错误提示问题
  - 修复表格处理数据错误提示问题（[#994b0769](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/994b0769a0ec892db30ff79ab090df4c43ca8c8a)  by @zpc）
- 修复数据选择ac的数据上下文问题
  - 修复数据选择ac的数据上下文问题（[#ab2ed217 ](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/ab2ed2170a3f6caaeeb50a917c36ad026768e9ec)  by @zpc）
- 修复实体数据选择视图双滚动条问题
  - 修复实体数据选择视图双滚动条问题（[#ad596757](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/ad5967577f7fbcbe8fa427a02595fa9a3a6d80c7)  by @zpc）
- app-picker
  - 修复输入数据值会保存在页面问题（[#be4bc2b8](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/be4bc2b800223b86fbf7b04fe5e280c3d333fed4)  by @zk）
- 自定义路由
  - 修复自定义路由问题（[#d1861298](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/d18612983a4218ee4e5de96f99e66df5e80e4ba7) @zpc）

#### 新特性

- 前端增加单点登录功能
  - 模板：环境变量中添加是否支持单点登录和单点登录地址2个变量（[#7c296c20](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/7c296c20435d2eece517e7790811980e45d0432d) by @zpc）
  - 基础文件：在http拦截器中增加单点登录路由跳转的逻辑（[#2d65dc4d](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/2d65dc4df6421bfaffe6f65998f210b92e500cb8) by @zpc）

- 增加批量添加功能
  - 模板：新建逻辑增加批量添加功能（[#4b73f4ff](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/4b73f4ff2838001d68bb53d6273ab95d377d7048) by @zpc）

- 增加独立程序弹出功能
  - 增加独立程序弹出功能（[#89b9f66d](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/89b9f66dedfa34aa555d723e58d674c16b3b0b8f) by @zpc）

- 增加出现错误是否跳转到错误页面功能,默认不跳转
  - 模板：增加出现错误是否跳转到错误页面配置[#4a150d54](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/4a150d544ee28ed7cc7319f9060e5310609ec2cf) by @zpc）
  - 基础文件：增加出现错误是否跳转到错误页面判断[#3d02ede5](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/3d02ede5b50923e3f6fac54a6d14f0c37f88f5c7) by @zpc）

#### 优化

- 添加开发用户维护自定义路由
  - 模板：首页路由文件中将用户自定义文件导入并且合并（[#71f40275](http://demo.ibizlab.cn/vue_v2/vue_r6/commit/71f402751f000f4d734ebe36654dad25f0e2c2d9) by@sww）
  - 基础文件：在src 下新增router文件夹作为用户自定义路由（ [#7a2dcef3](http://demo.ibizlab.cn/vue_v2/vue_r6_res/commit/7a2dcef3d4a03ab40f81121d9ff4c9ecf49ad3e7) by @sww）