import nonoentity_en_US from '@locale/lanres/nonoentity/nonoentity_en_US';
import user_lqy_en_US from '@locale/lanres/user_lqy/user_lqy_en_US';
import cliententity_en_US from '@locale/lanres/cliententity/cliententity_en_US';
import oneentity_en_US from '@locale/lanres/oneentity/oneentity_en_US';
import orgdeptentity_en_US from '@locale/lanres/orgdeptentity/orgdeptentity_en_US';
import testentity2_en_US from '@locale/lanres/testentity2/testentity2_en_US';
import orgdept_en_US from '@locale/lanres/orgdept/orgdept_en_US';
import manyentity_en_US from '@locale/lanres/manyentity/manyentity_en_US';
import org_en_US from '@locale/lanres/org/org_en_US';
import orgnoentity_en_US from '@locale/lanres/orgnoentity/orgnoentity_en_US';
import mainet_en_US from '@locale/lanres/mainet/mainet_en_US';
import testentity1_en_US from '@locale/lanres/testentity1/testentity1_en_US';
import security1_en_US from '@locale/lanres/security1/security1_en_US';

export default {
    app: {
        gridpage: {
            choicecolumns: 'Choice columns',
            refresh: 'refresh',
            show: 'Show',
            records: 'records',
            totle: 'totle',
        },
        tabpage: {
            sureclosetip: {
                title: 'Close warning',
                content: 'Form data Changed, are sure close?',
            },
            closeall: 'Close all',
            closeother: 'Close other',
        },
        fileUpload: {
            caption: 'Upload',
        },
        searchButton: {
            search: 'Search',
            reset: 'Reset',
        },
        // 非实体视图
        views: {
            index: {
                caption: '首页',
            },
        },
        menus: {
            menu1: {
                menuitem3: '权限测试（单实体）',
                menuitem1: '权限测试（主从）',
                menuitem2: '无预置属性',
                menuitem4: '预置组织标识',
                menuitem5: '预置组织部门标识',
            },
        },
    },
    nonoentity: nonoentity_en_US,
    user_lqy: user_lqy_en_US,
    cliententity: cliententity_en_US,
    oneentity: oneentity_en_US,
    orgdeptentity: orgdeptentity_en_US,
    testentity2: testentity2_en_US,
    orgdept: orgdept_en_US,
    manyentity: manyentity_en_US,
    org: org_en_US,
    orgnoentity: orgnoentity_en_US,
    mainet: mainet_en_US,
    testentity1: testentity1_en_US,
    security1: security1_en_US,
};