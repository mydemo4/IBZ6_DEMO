import nonoentity_zh_CN from '@locale/lanres/nonoentity/nonoentity_zh_CN';
import user_lqy_zh_CN from '@locale/lanres/user_lqy/user_lqy_zh_CN';
import cliententity_zh_CN from '@locale/lanres/cliententity/cliententity_zh_CN';
import oneentity_zh_CN from '@locale/lanres/oneentity/oneentity_zh_CN';
import orgdeptentity_zh_CN from '@locale/lanres/orgdeptentity/orgdeptentity_zh_CN';
import testentity2_zh_CN from '@locale/lanres/testentity2/testentity2_zh_CN';
import orgdept_zh_CN from '@locale/lanres/orgdept/orgdept_zh_CN';
import manyentity_zh_CN from '@locale/lanres/manyentity/manyentity_zh_CN';
import org_zh_CN from '@locale/lanres/org/org_zh_CN';
import orgnoentity_zh_CN from '@locale/lanres/orgnoentity/orgnoentity_zh_CN';
import mainet_zh_CN from '@locale/lanres/mainet/mainet_zh_CN';
import testentity1_zh_CN from '@locale/lanres/testentity1/testentity1_zh_CN';
import security1_zh_CN from '@locale/lanres/security1/security1_zh_CN';

export default {
    app: {
        gridpage: {
            choicecolumns: '选择列',
            refresh: '刷新',
            show: '显示',
            records: '条',
            totle: '共',
        },
        tabpage: {
            sureclosetip: {
                title: '关闭提醒',
                content: '表单数据已经修改，确定要关闭？',
            },
            closeall: '关闭所有',
            closeother: '关闭其他',
        },
        fileUpload: {
            caption: '上传',
        },
        searchButton: {
            search: '搜索',
            reset: '重置',
        },
        // 非实体视图
        views: {
            index: {
                caption: '首页',
            },
        },
        menus: {
            menu1: {
                menuitem3: '权限测试（单实体）',
                menuitem1: '权限测试（主从）',
                menuitem2: '无预置属性',
                menuitem4: '预置组织标识',
                menuitem5: '预置组织部门标识',
            },
        },
    },
    nonoentity: nonoentity_zh_CN,
    user_lqy: user_lqy_zh_CN,
    cliententity: cliententity_zh_CN,
    oneentity: oneentity_zh_CN,
    orgdeptentity: orgdeptentity_zh_CN,
    testentity2: testentity2_zh_CN,
    orgdept: orgdept_zh_CN,
    manyentity: manyentity_zh_CN,
    org: org_zh_CN,
    orgnoentity: orgnoentity_zh_CN,
    mainet: mainet_zh_CN,
    testentity1: testentity1_zh_CN,
    security1: security1_zh_CN,
};