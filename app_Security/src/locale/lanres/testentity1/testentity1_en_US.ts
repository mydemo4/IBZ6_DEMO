
export default {
	views: {
		gridview: {
			caption: '权限测试实体1',
		},
		pickupgridview: {
			caption: '权限测试实体1',
		},
		editview: {
			caption: '权限测试实体1',
		},
	},
	main_form: {
		details: {
			group1: '权限测试实体1基本信息', 
			formpage1: '基本信息', 
			group2: '操作信息', 
			formpage2: '其它', 
			srfupdatedate: '更新时间', 
			srforikey: '', 
			srfkey: '权限测试实体1标识', 
			srfmajortext: '权限测试实体1名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			testentity1name: '权限测试实体1名称', 
			datascopetest01: '数据范围测试01', 
			datascopetest02: '数据范围测试02', 
			security1id: '权限测试实体1标识', 
			security1name: 'AC', 
			createman: '建立人', 
			createdate: '建立时间', 
			updateman: '更新人', 
			updatedate: '更新时间', 
			testentity1id: '权限测试实体1标识', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			testentity1name: '权限测试实体1名称',
			updateman: '更新人',
			updatedate: '更新时间',
			datascopetest01: '数据范围测试01',
			datascopetest02: '数据范围测试02',
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
			n_security1id_eq: '权限测试实体1标识(=)', 
			n_testentity1name_like: '权限测试实体1名称(%)', 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: 'New',
			tip: 'tbitem3',
		},
		tbitem4: {
			caption: 'Edit',
			tip: 'tbitem4',
		},
		tbitem6: {
			caption: 'Copy',
			tip: 'tbitem6',
		},
		tbitem7: {
			caption: '-',
			tip: 'tbitem7',
		},
		tbitem8: {
			caption: 'Remove',
			tip: 'tbitem8',
		},
		tbitem9: {
			caption: '-',
			tip: 'tbitem9',
		},
		tbitem13: {
			caption: 'Export',
			tip: 'tbitem13',
		},
		tbitem10: {
			caption: '-',
			tip: 'tbitem10',
		},
		tbitem16: {
			caption: '其它',
			tip: 'tbitem16',
		},
		tbitem21: {
			caption: 'Export Data Model',
			tip: 'tbitem21',
		},
		tbitem23: {
			caption: '数据导入',
			tip: 'tbitem23',
		},
		tbitem17: {
			caption: '-',
			tip: 'tbitem17',
		},
		tbitem19: {
			caption: 'Filter',
			tip: 'tbitem19',
		},
		tbitem18: {
			caption: 'Help',
			tip: 'tbitem18',
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: 'Save',
			tip: 'tbitem3',
		},
		tbitem4: {
			caption: 'Save And New',
			tip: 'tbitem4',
		},
		tbitem5: {
			caption: 'Save And Close',
			tip: 'tbitem5',
		},
		tbitem6: {
			caption: '-',
			tip: 'tbitem6',
		},
		tbitem7: {
			caption: 'Remove And Close',
			tip: 'tbitem7',
		},
		tbitem8: {
			caption: '-',
			tip: 'tbitem8',
		},
		tbitem12: {
			caption: 'New',
			tip: 'tbitem12',
		},
		tbitem13: {
			caption: '-',
			tip: 'tbitem13',
		},
		tbitem14: {
			caption: 'Copy',
			tip: 'tbitem14',
		},
		tbitem16: {
			caption: '-',
			tip: 'tbitem16',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: 'tbitem23',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: 'tbitem24',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: 'tbitem25',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: 'tbitem26',
		},
		tbitem21: {
			caption: '-',
			tip: 'tbitem21',
		},
		tbitem22: {
			caption: 'Help',
			tip: 'tbitem22',
		},
	},
};