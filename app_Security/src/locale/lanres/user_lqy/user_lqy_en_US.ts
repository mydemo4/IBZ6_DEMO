
export default {
	views: {
		pickupgridview: {
			caption: '用户2',
		},
		editview: {
			caption: '用户2',
		},
		redirectview: {
			caption: '用户2',
		},
		gridview: {
			caption: '用户2',
		},
		pickupview: {
			caption: '用户2',
		},
		editview2: {
			caption: '用户2',
		},
		mpickupview: {
			caption: '用户2',
		},
	},
	main_form: {
		details: {
			group1: '用户2基本信息', 
			formpage1: '基本信息', 
			group2: '操作信息', 
			formpage2: '其它', 
			srfupdatedate: '更新时间', 
			srforikey: '', 
			srfkey: '用户2标识', 
			srfmajortext: '用户2名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			user_lqyname: '用户2名称', 
			orgdeptid: '部门标识', 
			orgname: '组织', 
			orgdeptname: '部门', 
			orgid: '组织标识2', 
			createman: '建立人', 
			createdate: '建立时间', 
			updateman: '更新人', 
			updatedate: '更新时间', 
			user_lqyid: '用户2标识', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			user_lqyname: '用户2名称',
			updateman: '更新人',
			updatedate: '更新时间',
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: 'Save',
			tip: 'tbitem3',
		},
		tbitem4: {
			caption: 'Save And New',
			tip: 'tbitem4',
		},
		tbitem5: {
			caption: 'Save And Close',
			tip: 'tbitem5',
		},
		tbitem6: {
			caption: '-',
			tip: 'tbitem6',
		},
		tbitem7: {
			caption: 'Remove And Close',
			tip: 'tbitem7',
		},
		tbitem8: {
			caption: '-',
			tip: 'tbitem8',
		},
		tbitem12: {
			caption: 'New',
			tip: 'tbitem12',
		},
		tbitem13: {
			caption: '-',
			tip: 'tbitem13',
		},
		tbitem14: {
			caption: 'Copy',
			tip: 'tbitem14',
		},
		tbitem16: {
			caption: '-',
			tip: 'tbitem16',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: 'tbitem23',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: 'tbitem24',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: 'tbitem25',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: 'tbitem26',
		},
		tbitem21: {
			caption: '-',
			tip: 'tbitem21',
		},
		tbitem22: {
			caption: 'Help',
			tip: 'tbitem22',
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: 'New',
			tip: 'tbitem3',
		},
		tbitem4: {
			caption: 'Edit',
			tip: 'tbitem4',
		},
		tbitem6: {
			caption: 'Copy',
			tip: 'tbitem6',
		},
		tbitem7: {
			caption: '-',
			tip: 'tbitem7',
		},
		tbitem8: {
			caption: 'Remove',
			tip: 'tbitem8',
		},
		tbitem9: {
			caption: '-',
			tip: 'tbitem9',
		},
		tbitem13: {
			caption: 'Export',
			tip: 'tbitem13',
		},
		tbitem10: {
			caption: '-',
			tip: 'tbitem10',
		},
		tbitem16: {
			caption: '其它',
			tip: 'tbitem16',
		},
		tbitem21: {
			caption: 'Export Data Model',
			tip: 'tbitem21',
		},
		tbitem23: {
			caption: '数据导入',
			tip: 'tbitem23',
		},
		tbitem17: {
			caption: '-',
			tip: 'tbitem17',
		},
		tbitem19: {
			caption: 'Filter',
			tip: 'tbitem19',
		},
		tbitem18: {
			caption: 'Help',
			tip: 'tbitem18',
		},
	},
	editview2toolbar_toolbar: {
		tbitem3: {
			caption: 'Save',
			tip: 'tbitem3',
		},
		tbitem4: {
			caption: 'Save And New',
			tip: 'tbitem4',
		},
		tbitem5: {
			caption: 'Save And Close',
			tip: 'tbitem5',
		},
		tbitem6: {
			caption: '-',
			tip: 'tbitem6',
		},
		tbitem7: {
			caption: 'Remove And Close',
			tip: 'tbitem7',
		},
		tbitem8: {
			caption: '-',
			tip: 'tbitem8',
		},
		tbitem12: {
			caption: 'New',
			tip: 'tbitem12',
		},
		tbitem13: {
			caption: '-',
			tip: 'tbitem13',
		},
		tbitem14: {
			caption: 'Copy',
			tip: 'tbitem14',
		},
		tbitem16: {
			caption: '-',
			tip: 'tbitem16',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: 'tbitem23',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: 'tbitem24',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: 'tbitem25',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: 'tbitem26',
		},
		tbitem21: {
			caption: '-',
			tip: 'tbitem21',
		},
		tbitem22: {
			caption: 'Help',
			tip: 'tbitem22',
		},
	},
};