import { Vue, Component } from 'vue-property-decorator';
import './404.less';

@Component({})
export default class Error404 extends Vue {

    /**
     * 跳转首页
     *
     * @memberof Error404
     */
    public gotoIndexView() {
        this.$router.push('/');
    }

    /**
     * 绘制内容
     *
     * @returns
     * @memberof Error404
     */
    public render() {
        return (
            <div class="app-error-view">
                <div class="app-error-container">
                    <img src="/assets/img/404.png"></img>
                    <div class="error-text">
                        <div class="error-text1">抱歉，您访问的页面不存在！</div>
                        <div class="error-text2">您要找的页面存在，请返回 <a on-click={this.gotoIndexView}>首页</a> 继续浏览</div>
                    </div>
                </div>
            </div>
        );
    }
}