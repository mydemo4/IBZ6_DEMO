/**
 * 实体-一方实体
 *
 * @export
 * @class ONEENTITY
 */
export class ONEENTITY {
    /**
     * 一方实体标识
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    oneentityid?: any;
    /**
     * 一方实体名称
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    oneentityname?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    createdate?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    createman?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    updateman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    updatedate?: any;
    /**
     * 属性
     *
     * @type {*}
     * @memberof ONEENTITY
     */
    fieldadd?: any;
}