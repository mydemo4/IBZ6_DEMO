/**
 * 实体-权限测试实体2
 *
 * @export
 * @class TestEntity2
 */
export class TestEntity2 {
    /**
     * 权限测试实体2标识
     *
     * @type {*}
     * @memberof TestEntity2
     */
    testentity2id?: any;
    /**
     * 权限测试实体2名称
     *
     * @type {*}
     * @memberof TestEntity2
     */
    testentity2name?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof TestEntity2
     */
    createman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof TestEntity2
     */
    updatedate?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof TestEntity2
     */
    createdate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof TestEntity2
     */
    updateman?: any;
    /**
     * 组织属性
     *
     * @type {*}
     * @memberof TestEntity2
     */
    org?: any;
    /**
     * 部门属性
     *
     * @type {*}
     * @memberof TestEntity2
     */
    orgsec?: any;
    /**
     * AC属性
     *
     * @type {*}
     * @memberof TestEntity2
     */
    ac?: any;
}