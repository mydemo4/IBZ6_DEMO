/**
 * 实体-预置属性测试实体
 *
 * @export
 * @class ORGDEPTENTITY
 */
export class ORGDEPTENTITY {
    /**
     * 建立人
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    createman?: any;
    /**
     * 预置属性测试实体名称
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    orgdeptentityname?: any;
    /**
     * 预置属性测试实体标识
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    orgdeptentityid?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    createdate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    updateman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    updatedate?: any;
    /**
     * 组织标识预置字段
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    orgid?: any;
    /**
     * 部门标识预置字段
     *
     * @type {*}
     * @memberof ORGDEPTENTITY
     */
    deptid?: any;
}