/**
 * 实体-全无实体
 *
 * @export
 * @class NoNoEntity
 */
export class NoNoEntity {
    /**
     * 全无实体名称
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    nonoentityname?: any;
    /**
     * 全无实体标识
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    nonoentityid?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    updatedate?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    createdate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    updateman?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof NoNoEntity
     */
    createman?: any;
}