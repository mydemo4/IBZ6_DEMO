/**
 * 实体-权限测试实体1
 *
 * @export
 * @class Testentity1
 */
export class Testentity1 {
    /**
     * 权限测试实体1名称
     *
     * @type {*}
     * @memberof Testentity1
     */
    testentity1name?: any;
    /**
     * 权限测试实体1标识
     *
     * @type {*}
     * @memberof Testentity1
     */
    testentity1id?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof Testentity1
     */
    updatedate?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof Testentity1
     */
    createdate?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof Testentity1
     */
    createman?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof Testentity1
     */
    updateman?: any;
    /**
     * 数据组织(归属）
     *
     * @type {*}
     * @memberof Testentity1
     */
    orgid?: any;
    /**
     * 数据部门（归属）
     *
     * @type {*}
     * @memberof Testentity1
     */
    orgdeptid?: any;
    /**
     * 属性
     *
     * @type {*}
     * @memberof Testentity1
     */
    field?: any;
    /**
     * 主状态属性
     *
     * @type {*}
     * @memberof Testentity1
     */
    field1?: any;
    /**
     * 权限测试实体1标识
     *
     * @type {*}
     * @memberof Testentity1
     */
    security1id?: any;
    /**
     * 数据范围测试01
     *
     * @type {*}
     * @memberof Testentity1
     */
    datascopetest01?: any;
    /**
     * 数据范围测试02
     *
     * @type {*}
     * @memberof Testentity1
     */
    datascopetest02?: any;
    /**
     * AC
     *
     * @type {*}
     * @memberof Testentity1
     */
    security1name?: any;
    /**
     * 组织
     *
     * @type {*}
     * @memberof Testentity1
     */
    orgname?: any;
}