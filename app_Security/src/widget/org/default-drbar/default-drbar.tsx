import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { CreateElement } from 'vue';
import { Subject, Subscription } from 'rxjs';
import { ControlInterface } from '@/interface/control';
import { UICounter } from '@/utils';
import './default-drbar.less';




@Component({
    components: {
         
    }
})
export default class Default extends Vue implements ControlInterface {

    /**
     * 名称
     *
     * @type {string}
     * @memberof Default
     */
    @Prop() public name?: string;

    /**
     * 视图通讯对象
     *
     * @type {Subject<ViewState>}
     * @memberof Default
     */
    @Prop() public viewState!: Subject<ViewState>;

    /**
     * 视图状态事件
     *
     * @protected
     * @type {(Subscription | undefined)}
     * @memberof Default
     */
    protected viewStateEvent: Subscription | undefined;
    


    /**
     * 序列号
     *
     * @private
     * @type {number}
     * @memberof Default
     */
    private serialNumber: number = this.$util.createSerialNumber();

    /**
     * 请求行为序列号数组
     *
     * @private
     * @type {any[]}
     * @memberof Default
     */
    private serialsNumber: any[] = [];

    /**
     * 添加序列号
     *
     * @private
     * @param {*} action
     * @param {number} serialnumber
     * @memberof Default
     */
    private addSerialNumber(action: any, serialnumber: number): void {
        const index = this.serialsNumber.findIndex((serial: any) => Object.is(serial.action, action));
        if (index === -1) {
            this.serialsNumber.push({ action: action, serialnumber: serialnumber })
        } else {
            this.serialsNumber[index].serialnumber = serialnumber;
        }
    }

    /**
     * 删除序列号
     *
     * @private
     * @param {*} action
     * @returns {number}
     * @memberof Default
     */
    private getSerialNumber(action: any): number {
        const index = this.serialsNumber.findIndex((serial: any) => Object.is(serial.action, action));
        return this.serialsNumber[index].serialnumber;
    }

    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof Default
     */
    public closeView(args: any[]): void {
        let _this: any = this;
        _this.$emit('closeview', args);
    }


    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof Default
     */
    public getDatas(): any[] {
        return this.items;
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof Default
     */
    public getData(): any {
        return this.selection;
    }

    /**
     * 加载行为
     *
     * @type {string}
     * @memberof Default
     */
    @Prop() public loadAction?: string;

    /**
     * url
     *
     * @private
     * @type {string}
     * @memberof Default
     */
    private url: string = 'security/testb/org/defaultdrbar/';

    /**
     * 数据选中项
     *
     * @type {*}
     * @memberof Default
     */
    public selection: any = {};

    /**
     * 关系栏数据项
     *
     * @type {any[]}
     * @memberof Default
     */
    public items: any[] = [];

    /**
     * 默认打开项
     *
     * @type {string[]}
     * @memberof Default
     */
    public defaultOpeneds: string[] = [];

    /**
     * 父数据
     *
     * @private
     * @type {*}
     * @memberof Default
     */
    private parentData: any = {};

    /**
     * 宽度
     *
     * @type {number}
     * @memberof Default
     */
    public width: number = 240;

    /**
     * 生命周期
     *
     * @memberof Default
     */
    public created(): void {
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('load', action)) {
                    if (data.srfkey && !Object.is(data.srfkey, '')) {
                        Object.assign(this.parentData, { srfparentkey: data.srfkey });
                    }
                    this.load(data);
                }
                if (Object.is('state', action)) {
                    if (data.srfkey && !Object.is(data.srfkey, '')) {
                        Object.assign(this.parentData, { srfparentkey: data.srfkey });
                    }
                    const state = !this.parentData.srfparentkey || (this.parentData.srfparentkey && Object.is(this.parentData.srfparentkey, '')) ? true : false;
                    this.setItemDisabled(this.items, state);
                }
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof Default
     */
    public destroyed() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 获取关系项
     *
     * @private
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof Default
     */
    private getDRBarItem(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        return undefined;
    }

    /**
     * 加载关系栏数据
     *
     * @param {*} [arg={}]
     * @memberof Default
     */
    public load(arg: any = {}): void {
        const params: any = {};
        Object.assign(params, arg);
        const get: Promise<any> = this.$http.get(this.url + this.loadAction, params);
        get.then((response: any) => {
            if (response && response.status === 200) {
                const data = response.data;
                this.dataProcess(data.items);
                this.items = [...data.items];
                this.$emit('load', this.items);
                if (this.items.length > 0) {
                    this.onSelect(this.items[0].id)
                }
            }
        }).catch((response: any) => {
            if (response && response.status === 401) {
                return;
            }
            this.$Notice.error({ title: '错误', desc: response.info });
        });
    }

    /**
     * 处理数据
     *
     * @private
     * @param {any[]} items
     * @memberof Default
     */
    private dataProcess(items: any[]): void {
        items.forEach((_item: any) => {
            if (_item.expanded) {
                this.defaultOpeneds.push(_item.id);
            }

            _item.disabled = false;
            if (_item.items && Array.isArray(_item.items) && _item.items.length > 0) {
                this.dataProcess(_item.items);
            }
        });
    }

    /**
     * 获取子项
     *
     * @param {any[]} items
     * @param {string} drviewid
     * @returns {*}
     * @memberof Default
     */
    public getItem(items: any[], id: string): any {
        const item: any = {};
        items.some((_item: any) => {
            if (Object.is(_item.id, id)) {
                Object.assign(item, _item);
                return true;
            }
            if (_item.items && _item.items.length > 0) {
                const subItem = this.getItem(_item.items, id);
                if (Object.keys(subItem).length > 0) {
                    Object.assign(item, subItem);
                    return true;
                }
            }
            return false;
        });
        return item;
    }

    /**
     * 节点选中
     *
     * @param {*} $event
     * @memberof Default
     */
    public onSelect($event: any): void {
        const item = this.getItem(this.items, $event);
        if (Object.is(item.id, this.selection.id)) {
            return;
        }

        this.$emit('selectionchange', [item]);

        const refview = this.getDRBarItem({ nodetype: item.drviewid });
        this.selection = {};
        const params: any = { ...JSON.parse(JSON.stringify(this.parentData)) };
        if (refview && refview.parentdatajo) {
            Object.assign(params, refview.parentdatajo);
            Object.assign(this.selection, { view: { viewname: refview.viewname }, data: params });
        }
        Object.assign(this.selection, item);
    }

    /**
     * 子节点打开
     *
     * @param {*} $event
     * @memberof Default
     */
    public onOpen($event: any): void {
        const item = this.getItem(this.items, $event);
        if (Object.is(item.id, this.selection.id)) {
            return;
        }
        this.selection = {};
        Object.assign(this.selection, item);
        if (Object.is(item.id, 'form') || (item.viewname && !Object.is(item.viewname, ''))) {
            this.$emit('selectionchange', [this.selection]);
        }
    }

    /**
     * 子节点关闭
     *
     * @param {*} $event
     * @memberof Default
     */
    public onClose($event: any): void {
        const item = this.getItem(this.items, $event);
        if (Object.is(item.id, this.selection.id)) {
            return;
        }
        this.selection = {};
        Object.assign(this.selection, item);
        if (Object.is(item.id, 'form') || (item.viewname && !Object.is(item.viewname, ''))) {
            this.$emit('selectionchange', [this.selection]);
        }
    }

    /**
     * 设置关系项状态
     *
     * @param {any[]} items
     * @param {boolean} state
     * @memberof Default
     */
    public setItemDisabled(items: any[], state: boolean) {
        items.forEach((item: any) => {
            if (!Object.is(item.id, 'form')) {
                item.disabled = state;
            }
            if (item.items && Array.isArray(item.items)) {
                this.setItemDisabled(item.items, state);
            }
        });
    }

    /**
     * 绘制图标
     *
     * @param {*} item
     * @returns
     * @memberof Default
     */
    public renderIcon(item: any) {
        return (
            item.icon && !Object.is(item.icon, '') ?
                <img src={item.icon} class='app-menu-icon' />
                :
                item.iconcls && !Object.is(item.iconcls, '') ?
                    <i class={item.iconcls + ' app-menu-icon'}></i>
                    : ''
        );
    }

    /**
     * 关系项
     *
     * @param {*} [item={}]
     * @returns
     * @memberof Default
     */
    public renderItem(item: any = {}) {
        return (
            <el-menu-item index={item.id} disabled={item.disabled}>
                {this.renderIcon(item)}
                <span slot='title'>{item.text}</span>
            </el-menu-item>
        );
    }

    /**
     * 绘制关系子菜单项
     *
     * @param {*} item
     * @returns
     * @memberof Default
     */
    public renderSubMenu(item: any) {
        return (
            <el-submenu index={item.id} disabled={item.disabled}>
                <template slot='title'>
                    {this.renderIcon(item)}
                    <span>{item.text}</span>
                </template>
                {
                    item.items.map((_item: any) => {
                        if (_item.items && Array.isArray(_item.items) && _item.items.length > 0) {
                            return this.renderSubMenu(_item);
                        } else {
                            return this.renderItem(_item);
                        }
                    })
                }
            </el-submenu>
        );
    }

    /**
     * 内容绘制
     *
     * @returns
     * @memberof Default
     */
    public render() {
        return (
            <layout class='app-dr-bar'>
                <sider width={this.width}>
                    <el-menu
                        default-openeds={this.defaultOpeneds}
                        on-select={($event: any) => this.onSelect($event)}
                        on-open={($event: any) => this.onOpen($event)}
                        on-close={($event: any) => this.onClose($event)}>
                        {
                            this.items.map((item: any) => {
                                if (item.items && Array.isArray(item.items) && item.items.length > 0) {
                                    return this.renderSubMenu(item);
                                } else {
                                    return this.renderItem(item);
                                }
                            })
                        }
                    </el-menu>
                </sider>
                <content style={{ width: `calc(100% - ${this.width + 1}px)` }}>
                    <div class='main-data' v-show={Object.is(this.selection.id, 'form')}>
                        {this.$slots.default}
                    </div>
                    {
                        (
                            !Object.is(this.selection.id, 'form') &&
                            this.selection.view &&
                            !Object.is(this.selection.view.viewname, '')
                        )  ?
                            this.$createElement(this.selection.view.viewname, {
                                class: {
                                    viewcontainer2: true,
                                },
                                props: {
                                    viewdata: JSON.stringify({ srfparentdata: this.selection.data }),
                                },
                                key: this.$util.createUUID(),
                            })
                            : ''
                    }
                </content>
            </layout>
        );
    }


}