/**
 * 实体-组织
 *
 * @export
 * @class Org
 */
export class Org {
    /**
     * 组织标识
     *
     * @type {*}
     * @memberof Org
     */
    orgid?: any;
    /**
     * 组织名称
     *
     * @type {*}
     * @memberof Org
     */
    orgname?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof Org
     */
    updatedate?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof Org
     */
    createman?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof Org
     */
    updateman?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof Org
     */
    createdate?: any;
}