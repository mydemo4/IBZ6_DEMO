/**
 * 实体-权限测试实体1
 *
 * @export
 * @class Security1
 */
export class Security1 {
    /**
     * 权限测试实体1标识
     *
     * @type {*}
     * @memberof Security1
     */
    security1id?: any;
    /**
     * 权限测试实体1名称
     *
     * @type {*}
     * @memberof Security1
     */
    security1name?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof Security1
     */
    createdate?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof Security1
     */
    createman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof Security1
     */
    updatedate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof Security1
     */
    updateman?: any;
    /**
     * 组织
     *
     * @type {*}
     * @memberof Security1
     */
    org?: any;
    /**
     * 部门
     *
     * @type {*}
     * @memberof Security1
     */
    orgsec?: any;
}