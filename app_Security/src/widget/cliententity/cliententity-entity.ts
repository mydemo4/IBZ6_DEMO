/**
 * 实体-远程调用实体
 *
 * @export
 * @class CLIENTENTITY
 */
export class CLIENTENTITY {
    /**
     * 远程调用实体名称
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    cliententityname?: any;
    /**
     * 远程调用实体标识
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    cliententityid?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    createman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    updatedate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    updateman?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof CLIENTENTITY
     */
    createdate?: any;
}