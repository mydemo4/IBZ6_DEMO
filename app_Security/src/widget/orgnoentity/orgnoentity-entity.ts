/**
 * 实体-有组织无部门实体
 *
 * @export
 * @class ORGNOENTITY
 */
export class ORGNOENTITY {
    /**
     * 更新人
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    updateman?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    createdate?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    createman?: any;
    /**
     * 有组织无部门实体标识
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    orgnoentityid?: any;
    /**
     * 有组织无部门实体名称
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    orgnoentityname?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    updatedate?: any;
    /**
     * 组织标识
     *
     * @type {*}
     * @memberof ORGNOENTITY
     */
    orgid?: any;
}