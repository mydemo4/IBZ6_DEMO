/**
 * 实体-多方实体
 *
 * @export
 * @class MANYENTITY
 */
export class MANYENTITY {
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    createdate?: any;
    /**
     * 多方实体名称
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    manyentityname?: any;
    /**
     * 多方实体标识
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    manyentityid?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    updatedate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    updateman?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    createman?: any;
    /**
     * 一方实体标识
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    oneentityid?: any;
    /**
     * 一方实体名称
     *
     * @type {*}
     * @memberof MANYENTITY
     */
    oneentityname?: any;
}