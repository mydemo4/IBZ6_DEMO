/**
 * 实体-部门
 *
 * @export
 * @class OrgDept
 */
export class OrgDept {
    /**
     * 部门标识
     *
     * @type {*}
     * @memberof OrgDept
     */
    orgdeptid?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof OrgDept
     */
    createman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof OrgDept
     */
    updatedate?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof OrgDept
     */
    createdate?: any;
    /**
     * 部门名称
     *
     * @type {*}
     * @memberof OrgDept
     */
    orgdeptname?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof OrgDept
     */
    updateman?: any;
    /**
     * 组织标识
     *
     * @type {*}
     * @memberof OrgDept
     */
    orgid?: any;
    /**
     * 组织名称
     *
     * @type {*}
     * @memberof OrgDept
     */
    orgname?: any;
}