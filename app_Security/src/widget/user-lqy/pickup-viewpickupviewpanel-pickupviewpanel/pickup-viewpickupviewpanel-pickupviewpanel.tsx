import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { CreateElement } from 'vue';
import { Subject, Subscription } from 'rxjs';
import { ControlInterface } from '@/interface/control';
import { UICounter } from '@/utils';
import './pickup-viewpickupviewpanel-pickupviewpanel.less';



@Component({
    components: {
         
    }
})
export default class PickupViewpickupviewpanel extends Vue implements ControlInterface {

    /**
     * 名称
     *
     * @type {string}
     * @memberof PickupViewpickupviewpanel
     */
    @Prop() public name?: string;

    /**
     * 视图通讯对象
     *
     * @type {Subject<ViewState>}
     * @memberof PickupViewpickupviewpanel
     */
    @Prop() public viewState!: Subject<ViewState>;

    /**
     * 视图状态事件
     *
     * @protected
     * @type {(Subscription | undefined)}
     * @memberof PickupViewpickupviewpanel
     */
    protected viewStateEvent: Subscription | undefined;
    


    /**
     * 序列号
     *
     * @private
     * @type {number}
     * @memberof PickupViewpickupviewpanel
     */
    private serialNumber: number = this.$util.createSerialNumber();

    /**
     * 请求行为序列号数组
     *
     * @private
     * @type {any[]}
     * @memberof PickupViewpickupviewpanel
     */
    private serialsNumber: any[] = [];

    /**
     * 添加序列号
     *
     * @private
     * @param {*} action
     * @param {number} serialnumber
     * @memberof PickupViewpickupviewpanel
     */
    private addSerialNumber(action: any, serialnumber: number): void {
        const index = this.serialsNumber.findIndex((serial: any) => Object.is(serial.action, action));
        if (index === -1) {
            this.serialsNumber.push({ action: action, serialnumber: serialnumber })
        } else {
            this.serialsNumber[index].serialnumber = serialnumber;
        }
    }

    /**
     * 删除序列号
     *
     * @private
     * @param {*} action
     * @returns {number}
     * @memberof PickupViewpickupviewpanel
     */
    private getSerialNumber(action: any): number {
        const index = this.serialsNumber.findIndex((serial: any) => Object.is(serial.action, action));
        return this.serialsNumber[index].serialnumber;
    }

    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof PickupViewpickupviewpanel
     */
    public closeView(args: any[]): void {
        let _this: any = this;
        _this.$emit('closeview', args);
    }



    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof PickupViewpickupviewpanel
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof PickupViewpickupviewpanel
     */
    public getData(): any {
        return {};
    }

    /**
     * 视图名称
     *
     * @type {*}
     * @memberof PickupViewpickupviewpanel
     */
    public view: any = {
        viewname: 'user-lqypickup-grid-view',
        data: {},
    }

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof PickupViewpickupviewpanel
     */
    @Prop() public isSingleSelect?: boolean;

    /**
     * 初始化完成
     *
     * @type {boolean}
     * @memberof PickupViewpickupviewpanel
     */
    public inited: boolean = false;

    /**
     * 视图数据变化
     *
     * @param {*} $event
     * @memberof PickupViewpickupviewpanel
     */
    public onViewDatasChange($event: any): void {
        this.$emit('selectionchange', $event);
    }

    /**
     * 视图数据被激活
     *
     * @param {*} $event
     * @memberof PickupViewpickupviewpanel
     */
    public viewDatasActivated($event: any): void {
        this.$emit('activated', $event);
    }

    /**
     * 视图加载完成
     *
     * @param {*} $event
     * @memberof PickupViewpickupviewpanel
     */
    public onViewLoad($event: any): void {
        this.$emit('load', $event);
    }

    /**
     * vue 生命周期
     *
     * @memberof PickupViewpickupviewpanel
     */
    public created() {
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                if (Object.is('load', action)) {
                    Object.assign(this.view, { data: data });
                    this.inited = true;
                }
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof PickupViewpickupviewpanel
     */
    public destroyed() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 内容绘制
     *
     * @returns
     * @memberof Pickupviewpanel
     */
    public render() {
        return (
            <div class='pickupviewpanel'>
                {
                    this.inited && this.view.viewname && !Object.is(this.view.viewname, '') ?
                        this.$createElement(this.view.viewname, {
                            class: {
                                viewcontainer3: true,
                            },
                            props: {
                                viewdata: JSON.stringify(this.view.data),
                                isSingleSelect: this.isSingleSelect
                            },
                            on: {
                                viewdataschange: ($event: any) => {
                                    this.onViewDatasChange($event);
                                },
                                viewdatasactivated: ($event: any) => {
                                    this.viewDatasActivated($event)
                                },
                                viewload: ($event: any) => {
                                    this.onViewLoad($event);
                                },
                            },
                            key: this.$util.createUUID(),
                        }) : ''
                }
            </div>
        );
    }

}