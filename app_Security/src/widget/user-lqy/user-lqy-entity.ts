/**
 * 实体-用户2
 *
 * @export
 * @class USER_LQY
 */
export class USER_LQY {
    /**
     * 用户2标识
     *
     * @type {*}
     * @memberof USER_LQY
     */
    user_lqyid?: any;
    /**
     * 用户2名称
     *
     * @type {*}
     * @memberof USER_LQY
     */
    user_lqyname?: any;
    /**
     * 建立人
     *
     * @type {*}
     * @memberof USER_LQY
     */
    createman?: any;
    /**
     * 更新时间
     *
     * @type {*}
     * @memberof USER_LQY
     */
    updatedate?: any;
    /**
     * 更新人
     *
     * @type {*}
     * @memberof USER_LQY
     */
    updateman?: any;
    /**
     * 建立时间
     *
     * @type {*}
     * @memberof USER_LQY
     */
    createdate?: any;
    /**
     * 账号
     *
     * @type {*}
     * @memberof USER_LQY
     */
    loginname?: any;
    /**
     * 密码
     *
     * @type {*}
     * @memberof USER_LQY
     */
    password?: any;
    /**
     * 系统名称
     *
     * @type {*}
     * @memberof USER_LQY
     */
    sysname?: any;
    /**
     * 远程调用实体标识
     *
     * @type {*}
     * @memberof USER_LQY
     */
    cliententityid?: any;
    /**
     * 组织标识2
     *
     * @type {*}
     * @memberof USER_LQY
     */
    orgid?: any;
    /**
     * 部门标识
     *
     * @type {*}
     * @memberof USER_LQY
     */
    orgdeptid?: any;
    /**
     * 组织名称
     *
     * @type {*}
     * @memberof USER_LQY
     */
    orgname?: any;
    /**
     * 部门名称
     *
     * @type {*}
     * @memberof USER_LQY
     */
    orgdeptname?: any;
}