import { CreateElement } from 'vue';
import store from '../../store';
import { Util } from '../util/util';

/**
 * 代码表
 *
 * @export
 * @class CodeList
 */
export class CodeList {

    /**
     * 获取 CodeList 单例对象
     *
     * @static
     * @returns {CodeList}
     * @memberof CodeList
     */
    public static getInstance(): CodeList {
        if (!CodeList.codeList) {
            CodeList.codeList = new CodeList();
        }
        return this.codeList;
    }

    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {CodeList}
     * @memberof CodeList
     */
    private static codeList: CodeList;

    /**
     * Creates an instance of CodeList.
     * 私有构造，拒绝通过 new 创建对象
     * 
     * @memberof CodeList
     */
    private constructor() {

    }

    /**
     * 获取代码项
     *
     * @private
     * @param {any[]} items
     * @param {*} value
     * @returns {*}
     * @memberof CodeList
     */
    private getItem(items: any[], value: any): any {
        let result: any = {};
        const arr: Array<any> = items.filter(item => Object.is(item.value, value));
        if (arr.length !== 1) {
            return undefined;
        }

        result = { ...arr[0] };
        return result;
    }

    /**
     * 绘制代码内容
     *
     * @private
     * @param {CreateElement} h
     * @param {*} [item={}]
     * @returns
     * @memberof CodeList
     */
    private renderCodeItemText(h: CreateElement, item: any = {}) {
        const color = item.color;
        const textCls = item.textcls;
        const iconCls: any = item.iconcls;
        const realText = item.text;
        const staticStyle: any = color ? { color: color } : {};
        return (
            <span>
                {iconCls ? <i class={iconCls}></i> + '&nbsp;' : ''}
                {(textCls || color) ? <span class={textCls} style={staticStyle}>{realText}</span> : realText}
            </span>
        );
    }

    /**
     * 常规内容绘制
     *
     * @param {CreateElement} h
     * @param {{ items: any[]; value: string; emtpytext: string; }} { items, value, emtpytext }
     * @returns
     * @memberof CodeList
     */
    public render(h: CreateElement, { srfkey, value, emtpytext }: { srfkey: string; value: string; emtpytext: string; }) {
        const items = store.getters.getCodeListItems(srfkey);
        if (Object.is(Util.typeOf(value), 'undefined') || Object.is(Util.typeOf(value), 'null')) {
            return (
                <span>{emtpytext}</span>
            );
        }

        if (items) {
            let values: any[] = [];
            if (Object.is(Util.typeOf(value), 'number')) {
                values.push(value);
            } else {
                values = [...value.split(';')]
            }
            return (
                <span>
                    {
                        values.map((value: any, index: number) => {
                            const item = this.getItem(items, value);
                            if (item) {
                                return (
                                    <span>
                                        {index === 0 ? '' : '、'}
                                        {this.renderCodeItemText(h, item)}
                                    </span>
                                );
                            }
                        })
                    }
                </span>
            );
        }
        return (
            <span></span>
        );
    }

    /**
     * 数字或处理
     *
     * @param {CreateElement} h
     * @param {{ srfkey: ''; value: string; emtpytext: string; textSeparator: string; }} { srfkey, value, emtpytext, textSeparator }
     * @returns
     * @memberof CodeList
     */
    public renderNumOr(h: CreateElement, { srfkey, value, emtpytext, textSeparator }: { srfkey: ''; value: string; emtpytext: string; textSeparator: string; }) {
        const items = store.getters.getCodeListItems(srfkey);
        if (!textSeparator || Object.is(textSeparator, '')) {
            textSeparator = '、';
        }
        if (Object.is(Util.typeOf(value), 'undefined') || Object.is(Util.typeOf(value), 'null')) {
            return (
                <span>{emtpytext}</span>
            );
        }
        const nValue = parseInt(value, 10);
        if (items) {
            return (
                <span>
                    {
                        items.map((_item: any, index: number) => {
                            const codevalue = _item.value;
                            if ((parseInt(codevalue, 10) & nValue) > 0) {
                                return (
                                    <span>
                                        {index > 0 ? textSeparator : ''}
                                        {this.renderCodeItemText(h, _item)}
                                    </span>
                                );
                            }
                        })
                    }
                </span>
            );
        }

        return (
            <span></span>
        );
    }

    /**
     * 文本或处理
     *
     * @param {CreateElement} h
     * @param {{ srfkey: string; value: any; emtpytext: any; textSeparator: any; valueSeparator: any; }} { srfkey, value, emtpytext, textSeparator, valueSeparator }
     * @returns
     * @memberof CodeList
     */
    public renderStrOr(h: CreateElement, { srfkey, value, emtpytext, textSeparator, valueSeparator }: { srfkey: string; value: any; emtpytext: any; textSeparator: any; valueSeparator: any; }) {
        if (!textSeparator || Object.is(textSeparator, '')) {
            textSeparator = '、';
        }
        if (Object.is(Util.typeOf(value), 'undefined') || Object.is(Util.typeOf(value), 'null')) {
            return (
                <span>{emtpytext}</span>
            );
        }

        const arrayValue: Array<any> = value.split(valueSeparator);
        return (
            <span>
                {
                    arrayValue.map((value: any, index: number) => {
                        return (
                            <span>
                                {index > 0 ? textSeparator : ''}
                                {this.render(h, { srfkey, value, emtpytext })}
                            </span>
                        );
                    })
                }
            </span>
        );
    }
}