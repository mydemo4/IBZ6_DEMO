import axios from 'axios';
import { Environment } from '@/environments/environment';


/**
 * 拦截器
 *
 * @export
 * @class Interceptors
 */
export class Interceptors {

    /**
     * 路由对象
     *
     * @private
     * @type {*}
     * @memberof Interceptors
     */
    private router: any

    /**
     * 拦截器实现接口
     *
     * @private
     * @memberof Interceptors
     */
    private intercept(): void {
        axios.interceptors.request.use((config: any) => {
            const rootapp: any = this.router.app;
            const appdata: string = rootapp.$store.getters.getAppData();
            if (!Object.is(appdata, '')) {
                config.headers.srfappdata = appdata;
            }
            if (window.localStorage.getItem('token')) {
                const token = window.localStorage.getItem('token');
                config.headers.Authorization = `Bearer ${token}`;
            }
            if (!config.url.startsWith('https://') && !config.url.startsWith('http://')) {
                config.url = Environment.BaseUrl + config.url;
            }
            return config;
        }, (error: any) => {
            return Promise.reject(error);
        });

        axios.interceptors.response.use((response: any) => {
            return response;
        }, (error: any) => {
            error = error ? error : { response: {} };
            // tslint:disable-next-line:prefer-const
            let { response: res } = error;
            let { data: _data } = res;

            if (_data && _data.status === 401) {
                this.doNoLogin(_data.data);
            }
            if (res.status === 404) {
                if(Environment.isGoErrorPage){
                    this.router.push({ path: '/404' });
                }
            } else if (res.status === 500) {
                if(Environment.isGoErrorPage){
                    this.router.push({ path: '/500' });
                }
            }

            return Promise.reject(res);
        });
    }

    /**
     * 处理未登录异常情况
     *
     * @private
     * @param {*} [data={}]
     * @memberof Interceptors
     */
    private doNoLogin(data: any = {}): void {
        if (data.loginurl && !Object.is(data.loginurl, '') && data.originurl && !Object.is(data.originurl, '')) {
            let _url = encodeURIComponent(encodeURIComponent(window.location.href));
            let loginurl: string = data.loginurl;
            const originurl: string = data.originurl;

            if (originurl.indexOf('?') === -1) {
                _url = `${encodeURIComponent('?RU=')}${_url}`;
            } else {
                _url = `${encodeURIComponent('&RU=')}${_url}`;
            }
            loginurl = `${loginurl}${_url}`;

            window.location.href = loginurl;
        } else {
            if(Environment.isSsoLoginModel){
                // 单点登录
                window.location.href = Environment.ssoLoginAddr+'?service='+window.location.href;
            }else{
                // 普通应用登录  
                if (Object.is(this.router.currentRoute.name, 'login')) {
                    return;
                }
                this.router.push({ name: 'login', query: { redirect: this.router.currentRoute.fullPath } });
            }
        }
    }


    /**
     * 构建对象
     * 
     * @memberof Interceptors
     */
    private constructor(router: any) {
        this.router = router;
        this.intercept();
    }

    /**
     * 初始化变量
     *
     * @private
     * @static
     * @type {Interceptors}
     * @memberof Interceptors
     */
    private static interceptors: Interceptors;

    /**
     * 获取单例对象
     *
     * @static
     * @returns {Interceptors}
     * @memberof Interceptors
     */
    public static getInstance(router: any): Interceptors {
        if (!Interceptors.interceptors) {
            Interceptors.interceptors = new Interceptors(router);
        }
        return this.interceptors;
    }
}

