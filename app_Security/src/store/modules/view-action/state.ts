/**
 * 所有应用视图
 */
export const viewstate: any = {
    appviews: [
        {
            viewtag: '05dce0db83893e0855adc554385ded8d',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '99d6a85bec56bf4b247123abf1c1f2d4',
            ],
        },
        {
            viewtag: '184c5bc4abcbd56104930f13739d82ee',
            viewmodule: 'testb',
            viewname: 'OrgDeptGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '2ac78b35ed59bccb5b3b158f22b726e9',
            ],
        },
        {
            viewtag: '235b80db0e084824a86e247f5480e549',
            viewmodule: 'testb',
            viewname: 'USER_LQYPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '24d807de9ae9eccc83751cdcccccd0f5',
            viewmodule: 'testb',
            viewname: 'OrgEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '24dc61c752e821d9aec4f228f5566b27',
            viewmodule: 'testb',
            viewname: 'ORGDEPTENTITYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '26575c0f92b395aa7f37da8df29dae95',
            viewmodule: 'testb',
            viewname: 'OrgPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '26ebd43e3e0e4f655fc9c1d25f330d65',
            viewmodule: 'V6Model',
            viewname: 'MANYENTITYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '2ac78b35ed59bccb5b3b158f22b726e9',
            viewmodule: 'testb',
            viewname: 'OrgDeptEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '36e33fcc44895abaa9fc8aef9b0ca2c6',
            viewmodule: 'module1',
            viewname: 'testentity1GridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd2953d413646eee5be9e0312a8a533b7',
            ],
        },
        {
            viewtag: '3bad219fb46c0fbac429d3cb55892e08',
            viewmodule: 'testb',
            viewname: 'USER_LQYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '26575c0f92b395aa7f37da8df29dae95',
                'd22cc5437c9461ef87597b6ea8fee957',
            ],
        },
        {
            viewtag: '456e521a7f0f2b9bd7f5d37f2bfc2e54',
            viewmodule: 'testb',
            viewname: 'OrgDeptMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd22cc5437c9461ef87597b6ea8fee957',
            ],
        },
        {
            viewtag: '53EF3347-C134-4680-92A4-5F932E2DA2F7',
            viewmodule: 'module1',
            viewname: 'index',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c99298a48927e0697e56d93cb045afec',
                'e8ae1b3a71f4ee5ea1e46f33ead5ae19',
                '5b8dc4f8a35b42535a445ff704dd977a',
                '5e1af2dee0dc74841a7be503aedc09a8',
                'cfc2d8a3b50523fca39fade3d8a60ef3',
            ],
        },
        {
            viewtag: '56f05a4a8adb80ff09cc38b5dcdd0c1c',
            viewmodule: 'module1',
            viewname: 'TestEntity2EditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '5b8dc4f8a35b42535a445ff704dd977a',
            viewmodule: 'module1',
            viewname: 'TestEntity2GridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '56f05a4a8adb80ff09cc38b5dcdd0c1c',
            ],
        },
        {
            viewtag: '5e1af2dee0dc74841a7be503aedc09a8',
            viewmodule: 'testb',
            viewname: 'ORGDEPTENTITYGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '24dc61c752e821d9aec4f228f5566b27',
            ],
        },
        {
            viewtag: '67b91b14fbbf203a4bee9c110045dbb4',
            viewmodule: 'module1',
            viewname: 'NoNoEntityEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '6e76579558fc7a240e3a5e03ce9395cc',
            viewmodule: 'testb',
            viewname: 'OrgDeptRedirectView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '2ac78b35ed59bccb5b3b158f22b726e9',
            ],
        },
        {
            viewtag: '71265060c599368462941da3c3f210ca',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '73b2fdacdceb7fb4b5ce76682bc7945a',
            viewmodule: 'testb',
            viewname: 'USER_LQYRedirectView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '3bad219fb46c0fbac429d3cb55892e08',
            ],
        },
        {
            viewtag: '7e0eb82101dbb12d1bbeda6e5906f336',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYEditView2',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '99d6a85bec56bf4b247123abf1c1f2d4',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '9b80da8f1897ba0c93150b66f9d83389',
            viewmodule: 'V6Model',
            viewname: 'ONEENTITYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '9c25cadf52104320598d19d0c5d3ca6a',
            viewmodule: 'security',
            viewname: 'testentity1PickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a7c2586d73344e85b3a46b7a9f2d9bee',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '71265060c599368462941da3c3f210ca',
            ],
        },
        {
            viewtag: 'a92e5e993a61d2e058d70f3bc3be24e1',
            viewmodule: 'testb',
            viewname: 'USER_LQYGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '3bad219fb46c0fbac429d3cb55892e08',
            ],
        },
        {
            viewtag: 'a9cf6881ac1c0b6716cd8ddb2477b86a',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '71265060c599368462941da3c3f210ca',
            ],
        },
        {
            viewtag: 'bcd7a699200bb24afa42b1ec6a91e886',
            viewmodule: 'module1',
            viewname: 'ORGNOENTITYEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'bde734caf49a30323c078dba8412286e',
            viewmodule: 'testb',
            viewname: 'USER_LQYPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '235b80db0e084824a86e247f5480e549',
            ],
        },
        {
            viewtag: 'c08306cb289dea0b7857e6598c0754fc',
            viewmodule: 'testb',
            viewname: 'CLIENTENTITYRedirectView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '99d6a85bec56bf4b247123abf1c1f2d4',
            ],
        },
        {
            viewtag: 'c99298a48927e0697e56d93cb045afec',
            viewmodule: 'testb',
            viewname: 'ORGNOENTITYGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'bcd7a699200bb24afa42b1ec6a91e886',
            ],
        },
        {
            viewtag: 'cd2772713351cc43a80bbc4ca082f75e',
            viewmodule: 'testb',
            viewname: 'OrgPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '26575c0f92b395aa7f37da8df29dae95',
            ],
        },
        {
            viewtag: 'cfc2d8a3b50523fca39fade3d8a60ef3',
            viewmodule: 'module1',
            viewname: 'Security1GridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f26130189d7371d6b41f06078f65fb7b',
            ],
        },
        {
            viewtag: 'cfc623a1aed9e0bdbd49aaccf19b84b5',
            viewmodule: 'testb',
            viewname: 'OrgEditView2',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'd22cc5437c9461ef87597b6ea8fee957',
            viewmodule: 'testb',
            viewname: 'OrgDeptPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'd2953d413646eee5be9e0312a8a533b7',
            viewmodule: 'module1',
            viewname: 'testentity1EditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e48f600cecae187ee11904b0661a51cc',
            viewmodule: 'testb',
            viewname: 'OrgDeptEditView2',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e689aea7d245de119e1e1f34ffbc5b0a',
            viewmodule: 'testb',
            viewname: 'OrgDeptPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd22cc5437c9461ef87597b6ea8fee957',
            ],
        },
        {
            viewtag: 'e8ae1b3a71f4ee5ea1e46f33ead5ae19',
            viewmodule: 'testb',
            viewname: 'NoNoEntityGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '67b91b14fbbf203a4bee9c110045dbb4',
            ],
        },
        {
            viewtag: 'ea2a527736aa2882cd4559961fdbb2f9',
            viewmodule: 'testb',
            viewname: 'OrgGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '24d807de9ae9eccc83751cdcccccd0f5',
            ],
        },
        {
            viewtag: 'eccfe4b55a49418b435df8a23c77b64d',
            viewmodule: 'testb',
            viewname: 'USER_LQYEditView2',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '26575c0f92b395aa7f37da8df29dae95',
                'd22cc5437c9461ef87597b6ea8fee957',
            ],
        },
        {
            viewtag: 'f26130189d7371d6b41f06078f65fb7b',
            viewmodule: 'module1',
            viewname: 'Security1EditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '36e33fcc44895abaa9fc8aef9b0ca2c6',
                '9c25cadf52104320598d19d0c5d3ca6a',
            ],
        },
        {
            viewtag: 'fa943edcdd734571f8ea03073fe02f4b',
            viewmodule: 'testb',
            viewname: 'OrgRedirectView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '24d807de9ae9eccc83751cdcccccd0f5',
            ],
        },
        {
            viewtag: 'fb5a4ab6462ce388ced3167f7b642382',
            viewmodule: 'testb',
            viewname: 'USER_LQYMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '235b80db0e084824a86e247f5480e549',
            ],
        },
        {
            viewtag: 'fccf3a06f4ac6c38a2c45f96a8a05016',
            viewmodule: 'testb',
            viewname: 'OrgMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '26575c0f92b395aa7f37da8df29dae95',
            ],
        },
    ],
    createdviews: [],
}