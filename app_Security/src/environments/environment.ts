export const Environment = {
    // 原型示例数模式
    SampleMode: false,
    // 应用名称
    AppName: 'Security',
    // 应用 title
    AppTitle: '权限前端应用',
    // 应用基础路径
    BaseUrl: '../',
    // 系统名称
    SysName: 'IBZ6_DEMO',
    // 远程登录地址，本地开发调试使用
    RemoteLogin: 'ibizutil/login',
    // 文件导出
    ExportFile: 'ibizutil/download',
    // 文件上传
    UploadFile: 'ibizutil/upload',
    // 是否支持单点登录
    isSsoLoginModel:false,
    //单点登录地址，demo地址
    ssoLoginAddr:"https://www.baidu.com",
    //后台返回错误是否跳转错误页面，默认为false
    isGoErrorPage:false
};