import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';
import { CreateElement } from 'vue';
import './user-lqypickup-view.less';

import PickupViewEngine from '@engine/view/pickup-view-engine';

import view_pickupviewpanel from '@widget/user-lqy/pickup-viewpickupviewpanel-pickupviewpanel/pickup-viewpickupviewpanel-pickupviewpanel';

@Component({
    components: {
        view_pickupviewpanel, 
    },
    beforeRouteEnter: (to: any, from: any, next: any) => {
        next((vm: any) => {
            vm.$store.commit('addCurPageViewtag', { fullPath: to.fullPath, viewtag: vm.viewtag });
        });
    },
})
export default class USER_LQYPickupView extends Vue {

    /**
     * 数据变化
     *
     * @param {*} val
     * @returns {*}
     * @memberof USER_LQYPickupView
     */
    @Emit() 
    public viewDatasChange(val: any):any {
        return val;
    }

    /**
     * 数据视图
     *
     * @type {string}
     * @memberof USER_LQYPickupView
     */
    @Prop() public viewdata!: string;

    /**
     * 该视图是否为模态方式打开
     *
     * @type {boolean}
     * @memberof USER_LQYPickupView
     */
    @Prop({default:false}) public isModal?:boolean;

	/**
	 * 视图标识
	 *
	 * @type {string}
	 * @memberof AppDashboardView
	 */
	public viewtag: string = 'bde734caf49a30323c078dba8412286e';

    /**
     * 父数据对象
     *
     * @protected
     * @type {*}
     * @memberof USER_LQYPickupView
     */
    protected srfparentdata: any = {};

    /**
     * 视图模型数据
     *
     * @type {*}
     * @memberof USER_LQYPickupView
     */
    public model: any = {
        srfTitle: '用户2数据选择视图',
        srfCaption: 'user_lqy.views.pickupview.caption',
        srfSubCaption: '',
        dataInfo: ''
    }

    /**
     * 处理值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof USER_LQYPickupView
     */
    @Watch('viewdata')
    onViewData(newVal: any, oldVal: any) {
        const _this: any = this;
        if (!Object.is(newVal, oldVal) && _this.engine) {
            _this.engine.setViewData(newVal);
            _this.engine.load();
        }
    }

    /**
     * 容器模型
     *
     * @type {*}
     * @memberof USER_LQYPickupView
     */
    public containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };

    /**
     * 视图状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof USER_LQYPickupView
     */
    public viewState: Subject<ViewState> = new Subject();


    /**
     * 视图引擎
     *
     * @private
     * @type {Engine}
     * @memberof USER_LQYPickupView
     */
    private engine: PickupViewEngine = new PickupViewEngine();

    /**
     * 引擎初始化
     *
     * @private
     * @memberof USER_LQYPickupView
     */
    private engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            isLoadDefault: true,
        });
    }

    /**
     * Vue声明周期
     *
     * @memberof USER_LQYPickupView
     */
    public created() {
        const secondtag = this.$util.createUUID();
        this.$store.commit('viewaction/createdView', { viewtag: this.viewtag, secondtag: secondtag });
        this.viewtag = secondtag;
        
    }

    /**
     * 销毁之前
     *
     * @memberof USER_LQYPickupView
     */
    public beforeDestroy() {
        this.$store.commit('viewaction/removeView', this.viewtag);
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof USER_LQYPickupView
     */
    public mounted() {
        const _this: any = this;
        _this.engineInit();
        if (_this.loadModel && _this.loadModel instanceof Function) {
            _this.loadModel();
        }
        
    }


    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupView
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any) {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }


    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupView
     */
    public pickupviewpanel_activated($event: any, $event2?: any) {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }


    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupView
     */
    public pickupviewpanel_load($event: any, $event2?: any) {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }




    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof USER_LQYPickupView
     */
    public closeView(args: any[]): void {
        let _view: any = this;
        if (_view.viewdata) {
            _view.$emit('viewdataschange', args);
            _view.$emit('close');
        } else if (_view.$tabPageExp) {
            _view.$tabPageExp.onClose(_view.$route.fullPath);
        }
    }

    /**
     * 视图选中数据
     *
     * @type {any[]}
     * @memberof USER_LQYPickupView
     */
    public viewSelections:any[] = [];

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof USER_LQYPickupView
     */
    public isSingleSelect: boolean = true;

    /**
     * 确定
     *
     * @memberof USER_LQYPickupView
     */
    public onClickOk(): void {
        this.$emit('viewdataschange', this.viewSelections);
        this.$emit('close', null);
    }

    /**
     * 取消
     *
     * @memberof USER_LQYPickupView
     */
    public onClickCancel(): void {
        this.$emit('close', null);
    }


    /**
     * 绘制视图消息 （上方）
     *
     * @returns
     * @memberof USER_LQYPickupView
     */
    public renderPosTopMsgs() {
        return (
            <div class='view-top-messages'>
            </div>
        );
    }

    /**
     * 绘制视图消息 （下方）
     *
     * @returns
     * @memberof USER_LQYPickupView
     */
    public renderPosBottomMsgs() {
        return (
            <div class='view-bottom-messages'>
            </div>
        );
    }
    
    /**
     * 绘制内容
     *
     * @param {CreateElement} h
     * @returns
     * @memberof USER_LQYPickupView
     */
    public render(h: CreateElement) {
        return (
        <div class="view-container user-lqypickup-view">
            <card class='view-card view-no-caption' dis-hover={true} padding={0} bordered={false}>
                <div class="content-container pickup-view">
                    <view_pickupviewpanel 
            viewState={this.viewState} 
                isSingleSelect={this.isSingleSelect}
         
            name='pickupviewpanel' 
            ref='pickupviewpanel' 
            on-selectionchange={($event: any) => this.pickupviewpanel_selectionchange($event)} 
            on-activated={($event: any) => this.pickupviewpanel_activated($event)} 
            on-load={($event: any) => this.pickupviewpanel_load($event)} 
            on-closeview={($event: any) => this.closeView($event)}>
        </view_pickupviewpanel>
                    <card dis-hover={true} bordered={false} class="footer">
                        <row style={{ textAlign: 'right' }}>
                            <i-button type="primary"  disabled={this.viewSelections.length > 0 ? false : true} 
                                    on-click={() => this.onClickOk()}>{this.containerModel.view_okbtn.text}</i-button>
                                    &nbsp;&nbsp;
                            <i-button on-click={() => this.onClickCancel()}>{this.containerModel.view_cancelbtn.text}</i-button>
                        </row>
                    </card>
                </div>
            </card>
        </div>
        );
    }

}