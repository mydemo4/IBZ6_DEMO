import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';
import { CreateElement } from 'vue';
import './user-lqypickup-grid-view.less';

import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';

import view_grid from '@widget/user-lqy/main-grid/main-grid';
import view_searchform from '@widget/user-lqy/default-searchform/default-searchform';

@Component({
    components: {
        view_grid, 
        view_searchform, 
    },
    beforeRouteEnter: (to: any, from: any, next: any) => {
        next((vm: any) => {
            vm.$store.commit('addCurPageViewtag', { fullPath: to.fullPath, viewtag: vm.viewtag });
        });
    },
})
export default class USER_LQYPickupGridView extends Vue {

    /**
     * 数据变化
     *
     * @param {*} val
     * @returns {*}
     * @memberof USER_LQYPickupGridView
     */
    @Emit() 
    public viewDatasChange(val: any):any {
        return val;
    }

    /**
     * 数据视图
     *
     * @type {string}
     * @memberof USER_LQYPickupGridView
     */
    @Prop() public viewdata!: string;

    /**
     * 该视图是否为模态方式打开
     *
     * @type {boolean}
     * @memberof USER_LQYPickupGridView
     */
    @Prop({default:false}) public isModal?:boolean;

	/**
	 * 视图标识
	 *
	 * @type {string}
	 * @memberof AppDashboardView
	 */
	public viewtag: string = '235b80db0e084824a86e247f5480e549';

    /**
     * 父数据对象
     *
     * @protected
     * @type {*}
     * @memberof USER_LQYPickupGridView
     */
    protected srfparentdata: any = {};

    /**
     * 视图模型数据
     *
     * @type {*}
     * @memberof USER_LQYPickupGridView
     */
    public model: any = {
        srfTitle: '用户2选择表格视图',
        srfCaption: 'user_lqy.views.pickupgridview.caption',
        srfSubCaption: '',
        dataInfo: ''
    }

    /**
     * 处理值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof USER_LQYPickupGridView
     */
    @Watch('viewdata')
    onViewData(newVal: any, oldVal: any) {
        const _this: any = this;
        if (!Object.is(newVal, oldVal) && _this.engine) {
            _this.engine.setViewData(newVal);
            _this.engine.load();
        }
    }

    /**
     * 容器模型
     *
     * @type {*}
     * @memberof USER_LQYPickupGridView
     */
    public containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };

    /**
     * 视图状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof USER_LQYPickupGridView
     */
    public viewState: Subject<ViewState> = new Subject();


    /**
     * 视图引擎
     *
     * @private
     * @type {Engine}
     * @memberof USER_LQYPickupGridView
     */
    private engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @private
     * @memberof USER_LQYPickupGridView
     */
    private engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            isLoadDefault: true,
        });
    }

    /**
     * Vue声明周期
     *
     * @memberof USER_LQYPickupGridView
     */
    public created() {
        const secondtag = this.$util.createUUID();
        this.$store.commit('viewaction/createdView', { viewtag: this.viewtag, secondtag: secondtag });
        this.viewtag = secondtag;
        
    }

    /**
     * 销毁之前
     *
     * @memberof USER_LQYPickupGridView
     */
    public beforeDestroy() {
        this.$store.commit('viewaction/removeView', this.viewtag);
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof USER_LQYPickupGridView
     */
    public mounted() {
        const _this: any = this;
        _this.engineInit();
        if (_this.loadModel && _this.loadModel instanceof Function) {
            _this.loadModel();
        }
        
    }


    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public grid_selectionchange($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }


    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public grid_beforeload($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }


    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public grid_rowdblclick($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }


    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public grid_load($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }


    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public searchform_save($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }


    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public searchform_search($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }


    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public searchform_load($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }




    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof USER_LQYPickupGridView
     */
    public closeView(args: any[]): void {
        let _view: any = this;
        if (_view.viewdata) {
            _view.$emit('viewdataschange', args);
            _view.$emit('close');
        } else if (_view.$tabPageExp) {
            _view.$tabPageExp.onClose(_view.$route.fullPath);
        }
    }

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof USER_LQYPickupGridView
     */
    @Prop() public isSingleSelect?: boolean;

    /**
     * 搜索值
     *
     * @type {string}
     * @memberof USER_LQYPickupGridView
     */
    public query: string = '';

    /**
     * 是否展开搜索表单
     *
     * @type {boolean}
     * @memberof USER_LQYPickupGridView
     */
    public isExpandSearchForm: boolean = true;

    /**
     * 表格行数据默认激活模式
     * 0 不激活
     * 1 单击激活
     * 2 双击激活
     *
     * @type {(number | 0 | 1 | 2)}
     * @memberof USER_LQYPickupGridView
     */
    public gridRowActiveMode: number | 0 | 1 | 2 = 2;

    /**
     * 快速搜索
     *
     * @param {*} $event
     * @memberof USER_LQYPickupGridView
     */
    public onSearch($event: any): void {
        const refs: any = this.$refs;
        if (refs.grid) {
            refs.grid.load({});
        }
    }


    /**
     * 绘制视图消息 （上方）
     *
     * @returns
     * @memberof USER_LQYPickupGridView
     */
    public renderPosTopMsgs() {
        return (
            <div class='view-top-messages'>
            </div>
        );
    }

    /**
     * 绘制视图消息 （下方）
     *
     * @returns
     * @memberof USER_LQYPickupGridView
     */
    public renderPosBottomMsgs() {
        return (
            <div class='view-bottom-messages'>
            </div>
        );
    }
    
    /**
     * 绘制内容
     *
     * @param {CreateElement} h
     * @returns
     * @memberof USER_LQYPickupGridView
     */
    public render(h: CreateElement) {
        return (
        <div class='view-container user-lqypickup-grid-view'>
            <card class='view-card view-no-caption'  dis-hover={true} bordered={false}>
                <div class='content-container'>
                    <view_searchform 
                        viewState={this.viewState} 
                            loaddraftAction='loaddraft' 
                        showBusyIndicator={true} 
                        v-show={this.isExpandSearchForm} 
                     
                        name='searchform' 
                        ref='searchform' 
                        on-save={($event: any) => this.searchform_save($event)} 
                        on-search={($event: any) => this.searchform_search($event)} 
                        on-load={($event: any) => this.searchform_load($event)} 
                        on-closeview={($event: any) => this.closeView($event)}>
                    </view_searchform>
                    <view_grid 
                        viewState={this.viewState} 
                            isSingleSelect={this.isSingleSelect} 
                        showBusyIndicator={true} 
                        searchAction='searchdefault' 
                        updateAction='update' 
                        removeAction='remove' 
                        loadAction='get' 
                        loaddraftAction='getdraft' 
                        createAction='create' 
                     
                        name='grid' 
                        ref='grid' 
                        on-selectionchange={($event: any) => this.grid_selectionchange($event)} 
                        on-beforeload={($event: any) => this.grid_beforeload($event)} 
                        on-rowdblclick={($event: any) => this.grid_rowdblclick($event)} 
                        on-load={($event: any) => this.grid_load($event)} 
                        on-closeview={($event: any) => this.closeView($event)}>
                    </view_grid>
                </div>
            </card>
        </div>
        );
    }

}