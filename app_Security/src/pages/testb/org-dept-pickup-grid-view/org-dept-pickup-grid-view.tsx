import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';
import { CreateElement } from 'vue';
import './org-dept-pickup-grid-view.less';

import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';

import view_grid from '@widget/org-dept/main-grid/main-grid';
import view_searchform from '@widget/org-dept/default-searchform/default-searchform';

@Component({
    components: {
        view_grid, 
        view_searchform, 
    },
    beforeRouteEnter: (to: any, from: any, next: any) => {
        next((vm: any) => {
            vm.$store.commit('addCurPageViewtag', { fullPath: to.fullPath, viewtag: vm.viewtag });
        });
    },
})
export default class OrgDeptPickupGridView extends Vue {

    /**
     * 数据变化
     *
     * @param {*} val
     * @returns {*}
     * @memberof OrgDeptPickupGridView
     */
    @Emit() 
    public viewDatasChange(val: any):any {
        return val;
    }

    /**
     * 数据视图
     *
     * @type {string}
     * @memberof OrgDeptPickupGridView
     */
    @Prop() public viewdata!: string;

    /**
     * 该视图是否为模态方式打开
     *
     * @type {boolean}
     * @memberof OrgDeptPickupGridView
     */
    @Prop({default:false}) public isModal?:boolean;

	/**
	 * 视图标识
	 *
	 * @type {string}
	 * @memberof AppDashboardView
	 */
	public viewtag: string = 'd22cc5437c9461ef87597b6ea8fee957';

    /**
     * 父数据对象
     *
     * @protected
     * @type {*}
     * @memberof OrgDeptPickupGridView
     */
    protected srfparentdata: any = {};

    /**
     * 视图模型数据
     *
     * @type {*}
     * @memberof OrgDeptPickupGridView
     */
    public model: any = {
        srfTitle: '部门选择表格视图',
        srfCaption: 'orgdept.views.pickupgridview.caption',
        srfSubCaption: '',
        dataInfo: ''
    }

    /**
     * 处理值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof OrgDeptPickupGridView
     */
    @Watch('viewdata')
    onViewData(newVal: any, oldVal: any) {
        const _this: any = this;
        if (!Object.is(newVal, oldVal) && _this.engine) {
            _this.engine.setViewData(newVal);
            _this.engine.load();
        }
    }

    /**
     * 容器模型
     *
     * @type {*}
     * @memberof OrgDeptPickupGridView
     */
    public containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };

    /**
     * 视图状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof OrgDeptPickupGridView
     */
    public viewState: Subject<ViewState> = new Subject();


    /**
     * 视图引擎
     *
     * @private
     * @type {Engine}
     * @memberof OrgDeptPickupGridView
     */
    private engine: PickupGridViewEngine = new PickupGridViewEngine();

    /**
     * 引擎初始化
     *
     * @private
     * @memberof OrgDeptPickupGridView
     */
    private engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            isLoadDefault: true,
        });
    }

    /**
     * Vue声明周期
     *
     * @memberof OrgDeptPickupGridView
     */
    public created() {
        const secondtag = this.$util.createUUID();
        this.$store.commit('viewaction/createdView', { viewtag: this.viewtag, secondtag: secondtag });
        this.viewtag = secondtag;
        
    }

    /**
     * 销毁之前
     *
     * @memberof OrgDeptPickupGridView
     */
    public beforeDestroy() {
        this.$store.commit('viewaction/removeView', this.viewtag);
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof OrgDeptPickupGridView
     */
    public mounted() {
        const _this: any = this;
        _this.engineInit();
        if (_this.loadModel && _this.loadModel instanceof Function) {
            _this.loadModel();
        }
        
    }


    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public grid_selectionchange($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }


    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public grid_beforeload($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }


    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public grid_rowdblclick($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }


    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public grid_load($event: any, $event2?: any) {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }


    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public searchform_save($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }


    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public searchform_search($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }


    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public searchform_load($event: any, $event2?: any) {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }




    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof OrgDeptPickupGridView
     */
    public closeView(args: any[]): void {
        let _view: any = this;
        if (_view.viewdata) {
            _view.$emit('viewdataschange', args);
            _view.$emit('close');
        } else if (_view.$tabPageExp) {
            _view.$tabPageExp.onClose(_view.$route.fullPath);
        }
    }

    /**
     * 是否单选
     *
     * @type {boolean}
     * @memberof OrgDeptPickupGridView
     */
    @Prop() public isSingleSelect?: boolean;

    /**
     * 搜索值
     *
     * @type {string}
     * @memberof OrgDeptPickupGridView
     */
    public query: string = '';

    /**
     * 是否展开搜索表单
     *
     * @type {boolean}
     * @memberof OrgDeptPickupGridView
     */
    public isExpandSearchForm: boolean = true;

    /**
     * 表格行数据默认激活模式
     * 0 不激活
     * 1 单击激活
     * 2 双击激活
     *
     * @type {(number | 0 | 1 | 2)}
     * @memberof OrgDeptPickupGridView
     */
    public gridRowActiveMode: number | 0 | 1 | 2 = 2;

    /**
     * 快速搜索
     *
     * @param {*} $event
     * @memberof OrgDeptPickupGridView
     */
    public onSearch($event: any): void {
        const refs: any = this.$refs;
        if (refs.grid) {
            refs.grid.load({});
        }
    }


    /**
     * 绘制视图消息 （上方）
     *
     * @returns
     * @memberof OrgDeptPickupGridView
     */
    public renderPosTopMsgs() {
        return (
            <div class='view-top-messages'>
            </div>
        );
    }

    /**
     * 绘制视图消息 （下方）
     *
     * @returns
     * @memberof OrgDeptPickupGridView
     */
    public renderPosBottomMsgs() {
        return (
            <div class='view-bottom-messages'>
            </div>
        );
    }
    
    /**
     * 绘制内容
     *
     * @param {CreateElement} h
     * @returns
     * @memberof OrgDeptPickupGridView
     */
    public render(h: CreateElement) {
        return (
        <div class='view-container org-dept-pickup-grid-view'>
            <card class='view-card view-no-caption'  dis-hover={true} bordered={false}>
                <div class='content-container'>
                    <view_searchform 
                        viewState={this.viewState} 
                            loaddraftAction='loaddraft' 
                        showBusyIndicator={true} 
                        v-show={this.isExpandSearchForm} 
                     
                        name='searchform' 
                        ref='searchform' 
                        on-save={($event: any) => this.searchform_save($event)} 
                        on-search={($event: any) => this.searchform_search($event)} 
                        on-load={($event: any) => this.searchform_load($event)} 
                        on-closeview={($event: any) => this.closeView($event)}>
                    </view_searchform>
                    <view_grid 
                        viewState={this.viewState} 
                            isSingleSelect={this.isSingleSelect} 
                        showBusyIndicator={true} 
                        searchAction='searchdefault' 
                        updateAction='update' 
                        removeAction='remove' 
                        loadAction='get' 
                        loaddraftAction='getdraft' 
                        createAction='create' 
                     
                        name='grid' 
                        ref='grid' 
                        on-selectionchange={($event: any) => this.grid_selectionchange($event)} 
                        on-beforeload={($event: any) => this.grid_beforeload($event)} 
                        on-rowdblclick={($event: any) => this.grid_rowdblclick($event)} 
                        on-load={($event: any) => this.grid_load($event)} 
                        on-closeview={($event: any) => this.closeView($event)}>
                    </view_grid>
                </div>
            </card>
        </div>
        );
    }

}