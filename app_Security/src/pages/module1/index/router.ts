import Vue from 'vue';
import Router from 'vue-router';
import { Util, AuthGuard } from '@/utils';
import routes from '@/router';
Vue.use(Router);

const router = new Router({
    routes: [
                {
            path: '/module1_index/:module1_index?',
            name: 'module1_index',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = 'module1_index';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, Util.formatMatrixParse(to.params[routerParamsName]));
                }
                const url: string = 'security/app/security/getappdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, router);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {  
                caption: '首页',
                viewType: 'APPINDEX',
                requireAuth: true,
            },
            component: () => import('@pages/module1/index/index'),
            children: [
                {
                    path: 'module1_security1editview/:module1_security1editview?',
                    name: 'module1_security1editview',
                    meta: {  
                        caption: 'security1.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/security1-edit-view/security1-edit-view'),
                },
                {
                    path: 'module1_testentity1editview/:module1_testentity1editview?',
                    name: 'module1_testentity1editview',
                    meta: {  
                        caption: 'testentity1.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/testentity1-edit-view/testentity1-edit-view'),
                },
                {
                    path: 'testb_orgdeptentityeditview/:testb_orgdeptentityeditview?',
                    name: 'testb_orgdeptentityeditview',
                    meta: {  
                        caption: 'orgdeptentity.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/testb/orgdeptentityedit-view/orgdeptentityedit-view'),
                },
                {
                    path: 'module1_nonoentityeditview/:module1_nonoentityeditview?',
                    name: 'module1_nonoentityeditview',
                    meta: {  
                        caption: 'nonoentity.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/no-no-entity-edit-view/no-no-entity-edit-view'),
                },
                {
                    path: 'security_testentity1pickupgridview/:security_testentity1pickupgridview?',
                    name: 'security_testentity1pickupgridview',
                    meta: {  
                        caption: 'testentity1.views.pickupgridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/security/testentity1-pickup-grid-view/testentity1-pickup-grid-view'),
                },
                {
                    path: 'testb_orgdeptentitygridview/:testb_orgdeptentitygridview?',
                    name: 'testb_orgdeptentitygridview',
                    meta: {  
                        caption: 'orgdeptentity.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/testb/orgdeptentitygrid-view/orgdeptentitygrid-view'),
                },
                {
                    path: 'module1_testentity1gridview/:module1_testentity1gridview?',
                    name: 'module1_testentity1gridview',
                    meta: {  
                        caption: 'testentity1.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/testentity1-grid-view/testentity1-grid-view'),
                },
                {
                    path: 'module1_testentity2editview/:module1_testentity2editview?',
                    name: 'module1_testentity2editview',
                    meta: {  
                        caption: 'testentity2.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/test-entity2-edit-view/test-entity2-edit-view'),
                },
                {
                    path: 'testb_orgnoentitygridview/:testb_orgnoentitygridview?',
                    name: 'testb_orgnoentitygridview',
                    meta: {  
                        caption: 'orgnoentity.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/testb/orgnoentitygrid-view/orgnoentitygrid-view'),
                },
                {
                    path: 'testb_nonoentitygridview/:testb_nonoentitygridview?',
                    name: 'testb_nonoentitygridview',
                    meta: {  
                        caption: 'nonoentity.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/testb/no-no-entity-grid-view/no-no-entity-grid-view'),
                },
                {
                    path: 'module1_orgnoentityeditview/:module1_orgnoentityeditview?',
                    name: 'module1_orgnoentityeditview',
                    meta: {  
                        caption: 'orgnoentity.views.editview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/orgnoentityedit-view/orgnoentityedit-view'),
                },
                {
                    path: 'module1_testentity2gridview/:module1_testentity2gridview?',
                    name: 'module1_testentity2gridview',
                    meta: {  
                        caption: 'testentity2.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/test-entity2-grid-view/test-entity2-grid-view'),
                },
                {
                    path: 'module1_security1gridview/:module1_security1gridview?',
                    name: 'module1_security1gridview',
                    meta: {  
                        caption: 'security1.views.gridview.caption',
                        requireAuth: true,
                    },
                    component: () => import('@pages/module1/security1-grid-view/security1-grid-view'),
                },
                ...routes
            ]
        },
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                router.app.$store.commit('resetRootStateData');
                next();
            },
            component: () => import('@components/login/login'),
        },
        {
            path: '/404',
            component: () => import('@components/404/404')
        },
        {
            path: '/500',
            component: () => import('@components/500/500')
        },
        {
            path: '*',
            redirect: 'module1_index'
        },
    ],
});

export default router;
