export const PageComponents = {
    install(Vue: any, opt: any) {
                Vue.component('security1-edit-view', () => import('@pages/module1/security1-edit-view/security1-edit-view'));
        Vue.component('testentity1-edit-view', () => import('@pages/module1/testentity1-edit-view/testentity1-edit-view'));
        Vue.component('orgdeptentityedit-view', () => import('@pages/testb/orgdeptentityedit-view/orgdeptentityedit-view'));
        Vue.component('no-no-entity-edit-view', () => import('@pages/module1/no-no-entity-edit-view/no-no-entity-edit-view'));
        Vue.component('testentity1-pickup-grid-view', () => import('@pages/security/testentity1-pickup-grid-view/testentity1-pickup-grid-view'));
        Vue.component('orgdeptentitygrid-view', () => import('@pages/testb/orgdeptentitygrid-view/orgdeptentitygrid-view'));
        Vue.component('testentity1-grid-view', () => import('@pages/module1/testentity1-grid-view/testentity1-grid-view'));
        Vue.component('test-entity2-edit-view', () => import('@pages/module1/test-entity2-edit-view/test-entity2-edit-view'));
        Vue.component('orgnoentitygrid-view', () => import('@pages/testb/orgnoentitygrid-view/orgnoentitygrid-view'));
        Vue.component('no-no-entity-grid-view', () => import('@pages/testb/no-no-entity-grid-view/no-no-entity-grid-view'));
        Vue.component('orgnoentityedit-view', () => import('@pages/module1/orgnoentityedit-view/orgnoentityedit-view'));
        Vue.component('test-entity2-grid-view', () => import('@pages/module1/test-entity2-grid-view/test-entity2-grid-view'));
        Vue.component('security1-grid-view', () => import('@pages/module1/security1-grid-view/security1-grid-view'));
    }
};