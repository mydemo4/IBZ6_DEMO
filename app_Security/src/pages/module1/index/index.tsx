import { Vue, Component, Prop, Provide, Emit, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';
import { CreateElement } from 'vue';
import './index.less';


import view_appmenu from '@widget/app/menu1-appmenu/menu1-appmenu';

@Component({
    components: {
        view_appmenu, 
    },
    beforeRouteEnter: (to: any, from: any, next: any) => {
        next((vm: any) => {
            vm.$store.commit('addCurPageViewtag', { fullPath: to.fullPath, viewtag: vm.viewtag });
        });
    },
})
export default class Index extends Vue {

    /**
     * 数据变化
     *
     * @param {*} val
     * @returns {*}
     * @memberof Index
     */
    @Emit() 
    public viewDatasChange(val: any):any {
        return val;
    }

    /**
     * 数据视图
     *
     * @type {string}
     * @memberof Index
     */
    @Prop() public viewdata!: string;

    /**
     * 该视图是否为模态方式打开
     *
     * @type {boolean}
     * @memberof Index
     */
    @Prop({default:false}) public isModal?:boolean;

	/**
	 * 视图标识
	 *
	 * @type {string}
	 * @memberof AppDashboardView
	 */
	public viewtag: string = '53EF3347-C134-4680-92A4-5F932E2DA2F7';

    /**
     * 父数据对象
     *
     * @protected
     * @type {*}
     * @memberof Index
     */
    protected srfparentdata: any = {};

    /**
     * 视图模型数据
     *
     * @type {*}
     * @memberof Index
     */
    public model: any = {
        srfTitle: '',
        srfCaption: 'app.views.index.caption',
        srfSubCaption: '',
        dataInfo: ''
    }

    /**
     * 处理值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof Index
     */
    @Watch('viewdata')
    onViewData(newVal: any, oldVal: any) {
        const _this: any = this;
        if (!Object.is(newVal, oldVal) && _this.engine) {
            _this.engine.setViewData(newVal);
            _this.engine.load();
        }
    }

    /**
     * 容器模型
     *
     * @type {*}
     * @memberof Index
     */
    public containerModel: any = {
        view_appmenu: { name: 'appmenu', type: 'APPMENU' },
    };

    /**
     * 视图状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof Index
     */
    public viewState: Subject<ViewState> = new Subject();




    /**
     * 引擎初始化
     *
     * @private
     * @memberof Index
     */
    private engineInit(): void {
    }

    /**
     * Vue声明周期
     *
     * @memberof Index
     */
    public created() {
        const secondtag = this.$util.createUUID();
        this.$store.commit('viewaction/createdView', { viewtag: this.viewtag, secondtag: secondtag });
        this.viewtag = secondtag;
        
    }

    /**
     * 销毁之前
     *
     * @memberof Index
     */
    public beforeDestroy() {
        this.$store.commit('viewaction/removeView', this.viewtag);
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof Index
     */
    public mounted() {
        const _this: any = this;
        _this.engineInit();
        if (_this.loadModel && _this.loadModel instanceof Function) {
            _this.loadModel();
        }
                this.viewState.next({ tag: 'appmenu', action: 'load', data: {} });

    }




    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof Index
     */
    public closeView(args: any[]): void {
        let _view: any = this;
        if (_view.viewdata) {
            _view.$emit('viewdataschange', args);
            _view.$emit('close');
        } else if (_view.$tabPageExp) {
            _view.$tabPageExp.onClose(_view.$route.fullPath);
        }
    }

    /**
     * api地址
     *
     * @private
     * @type {string}
     * @memberof Index
     */
    private url: string = 'Security/module1/index.do';

    /**
     * api地址
     *
     * @private
     * @type {string}
     * @memberof Index
     */
    private mode: string ='vertical';

    /**
     * 当前主题
     *
     * @readonly
     * @memberof Index
     */
    get selectTheme() {
        if (this.$router.app.$store.state.selectTheme) {
            return this.$router.app.$store.state.selectTheme;
        } else if (localStorage.getItem('theme-class')) {
            return localStorage.getItem('theme-class');
        } else {
            return 'app-default-theme';
        }
    }

    /**
     * 当前字体
     *
     * @readonly
     * @memberof Index
     */
    get selectFont() {
        if (this.$router.app.$store.state.selectFont) {
            return this.$router.app.$store.state.selectFont;
        } else if (localStorage.getItem('font-family')) {
            return localStorage.getItem('font-family');
        } else {
            return 'Microsoft YaHei';
        }
    }

    /**
     * 菜单收缩变化
     */
    public collapseChange: boolean = false;

    /**
     * 菜单收缩点击
     */
    public handleClick() {
        this.collapseChange = !this.collapseChange;
    }

    /**
     * 默认打开的视图
     *
     * @type {*}
     * @memberof Index
     */
    public defPSAppView: any = {
    };

    /**
     * 应用起始页面
     *
     * @type {boolean}
     * @memberof Index
     */
    public isDefaultPage: boolean = true;


    /**
     * 绘制视图消息 （上方）
     *
     * @returns
     * @memberof Index
     */
    public renderPosTopMsgs() {
        return (
            <div class='view-top-messages'>
            </div>
        );
    }

    /**
     * 绘制视图消息 （下方）
     *
     * @returns
     * @memberof Index
     */
    public renderPosBottomMsgs() {
        return (
            <div class='view-bottom-messages'>
            </div>
        );
    }
    
    /**
     * 绘制内容
     *
     * @param {CreateElement} h
     * @returns
     * @memberof Index
     */
    public render(h: CreateElement) {
        return (
        <div class="index_view index">
                        <layout class={{ 'app_theme_blue': 'app_theme_blue' == this.selectTheme ? true : false, 'app-default-theme': 'app-default-theme' == this.selectTheme ? true : false, 'app_theme_darkblue': 'app_theme_darkblue' == this.selectTheme ? true : false }} style={{ 'font-family': this.selectFont, 'height': '100vh' }}>
                            <header class="index_header">
                                <div class="page-logo">
                                        <img src="./assets/img/logo2.png" height="22" />
                                    <span style="display: inline-block;margin-left: 10px;font-size: 22px;">首页</span>
                                </div>
                                <div class="header-right" style="display: flex;align-items: center;justify-content: space-between;">
                                    <div>
                                        <icon type="md-notifications-outline" style="font-size: 25px;margin: 0 10px;" />
                                    </div>
                                    <div>
                                        <icon type="ios-mail-open-outline" style="font-size: 25px;margin: 0 10px;" />
                                    </div>
                                    <app-lang style='font-size: 15px;padding: 0 10px;'></app-lang>
                                     <app-user></app-user>
                                    <app-theme style="width:45px;display: flex;justify-content: center;"></app-theme>
                                </div>
                            </header>
                            <layout>
                                <sider width={this.collapseChange ? 64 : 200} hide-trigger v-model={this.collapseChange}>
                                    <div class="sider-top">
                                        <i class="ivu-icon ivu-icon-md-menu" on-click={this.handleClick}></i>
                                    </div>
                                   <view_appmenu 
            viewState={this.viewState} 
                showBusyIndicator={true} 
            collapsechange={this.collapseChange} 
            mode={this.mode} 
            selectTheme={this.selectTheme} 
            isDefaultPage={this.isDefaultPage} 
            defPSAppView={this.defPSAppView}
         
            name='appmenu' 
            ref='appmenu' 
            on-closeview={($event: any) => this.closeView($event)}>
        </view_appmenu>
                                </sider>
                                <content class="index_content"  style={{'width':this.collapseChange ? 'calc(100vw - 64px)' : 'calc(100vw - 200px)'}}>
                                    <tab-page-exp></tab-page-exp>
                                    <app-keep-alive routerList={this.$store.state.historyPathList}>
                                        <router-view key={this.$route.fullPath}></router-view>
                                    </app-keep-alive>
                                </content>
                            </layout>
                        </layout>
                    </div>
        );
    }

}
