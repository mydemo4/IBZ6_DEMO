# Vue_R6 解决方案

## 简介

[Vue_R6](<https://doc.ibizlab.cn/templ/vue_r6/>) 是 IBiz 基于 Vue 的新一代开源 PC 端 Web App 解决方案，解决方案包含 iBiz 模型体系、模板文件体系和 iBiz 内置发布引擎体系等组成。本文档将介绍解决方案模板和成果的结构组成、开发模式等，为开发人员提供一定指导支持。

解决方案入门为中、高级难度，适合掌握 `FreeMarker`、`Vue`、`TypeScript` 等技术的开发人员。

## 特性

- 基于 iBiz 多版本企业级中后台产品迭代升级而来
- 使用 FreeMarker 模板引擎发布成果文件
- 集成 iVew、Element 等高质量 UI 组件
- 使用 TypeScript 中 tsx 、Less 构建成果文件，保证样式与逻辑分离

## 支持环境

现代浏览器（不支持 IE ）。

## 技术体系

![技术体系](./img/technology-system.png)

解决方案，由多种不同的技术在不同内容场景上组成。

模板部分如下：

- iBiz V2：核心发布引擎
- FreeMarker：解析业务模型数据

前端素材部分如下：

- 编写规范： TypeScript、TSX、Less  前端语言
- UI 框架：iView、Element
- Http：axios 
- 其他：Rxjs、Echarts等

## 更新日志

每个版本的详细更改都记录在[发行说明](CHANGELOG.md)中。

## 链接

- [FreeMarker](https://freemarker.apache.org/)

- [Node.js](https://nodejs.org)

- [Yarn](https://yarnpkg.com)

- [Vue](https://vuejs.org/index.html)

- [Vue Cli](https://cli.vuejs.org/)

- [TypeScript](https://www.typescriptlang.org/)

## 如何贡献

如果你希望参与贡献，欢迎 [Pull Request](<http://demo.ibizlab.cn/vue_v2/vue_r6/issues/new?issue>)，或通过自助服务群给我们报告 Bug。

强烈推荐阅读 [《提问的智慧》](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way)(本指南不提供此项目的实际支持服务！)、[《如何向开源社区提问题》](https://github.com/seajs/seajs/issues/545) 和 [《如何有效地报告 Bug》](https://www.chiark.greenend.org.uk/~sgtatham/bugs-cn.html)、[《如何向开源项目提交无法解答的问题》](https://zhuanlan.zhihu.com/p/25795393)，更好的问题更容易获得帮助。

## 社区互助

1.[iBizLab论坛](https://bbs.ibizlab.cn/)

2.加入钉钉 Vue_R6自助服务群（中文）

![1574922763438](./img/vue_r6_group.png)