
:: 脚本使用须知：该脚本用于在进行数据库更新时，针对本次更新的脚本，对应输出的回滚脚本（注意：执行该脚本前请将diff出来的changelog拷贝到源目录对应的classes目录下）
:: 指令1： liquibase : tag   说明：用于在【databaseChangelog】表中标记更新前最后一次的更新点
:: 指令2： liquibase : futureRollbackSQL     说明：输出最后一次更新点以后的更新脚本对应的回滚脚本

echo begin

call mvn  org.liquibase:liquibase-maven-plugin:3.6.3:tag -Dliquibase.tag=rollbacktag  -Dliquibase.driver=org.h2.Driver   -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';" -DdefaultSchemaName=a0239cb076   -Dliquibase.username=a0239cb076

call mvn org.liquibase:liquibase-maven-plugin:3.6.3:futureRollbackSQL   -Dliquibase.rollbackTag=201906161758 -Dliquibase.driver=org.h2.Driver   -Dliquibase.changeLogFile=src/main/resources/liquibase/master.xml  -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';"  -DdefaultSchemaName=a0239cb076  -Dliquibase.username=a0239cb076    -Dliquibase.outputFile=src/main/resources/liquibase/rollback/rollback_future.sql

echo done

pause

