@echo off

rem 该脚本作用于通过liquibase将ibz开发环境中最新的表结构与生产环境中的表结构进行差异比较，输出差异内容及对应的回滚脚本，执行脚本后，会在resources/liquibase/changelog目录下输出差异内容；resources/liquibase/rollback目录下输出回滚脚本
rem 在SpringBoot项目启动时，自动加载changelog目录中的文件，将生产环境同步到最新。
rem 脚本解析
rem liquibase update 通过分别加载ibz的changelog跟生产环境的changelog，在本地构造ibz的h2数据库与生产环境的h2数据库
rem liquibase diff   通过liquibase比较ibz的h2数据库与生产环境h2数据库之间的差异，获得差异文件
rem xcopy            将差异文件拷贝到classes目录下
rem liquibase tag    在生产环境的h2数据库的databasechangelog中标记最后一次更新点
rem liquibase futureRollbackSQL  获取回滚到最后一次更新点的脚本

set "year=%date:~0,4%"
set "month=%date:~5,2%"
set "day=%date:~8,2%"
set "hour_ten=%time:~0,1%"
set "hour_one=%time:~1,1%"
set "minute=%time:~3,2%"
set "second=%time:~6,2%"
set cur_date="";
if "%hour_ten%" == " " (
   set cur_date=%year%%month%%day%0%hour_one%%minute%%second%
) else (
  set cur_date=%year%%month%%day%%hour_ten%%hour_one%%minute%%second%
)

if "%1" == "rollback" (
    if not "%2" == "" (
        set cur_date=%2
    )
    goto rollback
)

:update
call mvn org.liquibase:liquibase-maven-plugin:3.6.3:update -Dliquibase.driver=org.h2.Driver   -Dliquibase.changeLogFile=src/main/resources/liquibase/h2_table.xml  -Dliquibase.url="jdbc:h2:~/h2/ibzdbmirror/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';"  -DdefaultSchemaName=a0239cb076  -Dliquibase.username=a0239cb076  -Dliquibase.dropFirst=true

call mvn  org.liquibase:liquibase-maven-plugin:3.6.3:update -Dliquibase.driver=org.h2.Driver   -Dliquibase.changeLogFile=src/main/resources/liquibase/master_table.xml  -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';" -DdefaultSchemaName=a0239cb076   -Dliquibase.username=a0239cb076  -Dliquibase.dropFirst=true

call mvn  org.liquibase:liquibase-maven-plugin:3.6.3:diff -Dliquibase.driver=org.h2.Driver  -Dliquibase.changeLogFile=src/main/resources/liquibase/empty.xml  -Dliquibase.diffChangeLogFile="src/main/resources/liquibase/changelog/%cur_date%.xml"  -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';"     -Dliquibase.username=a0239cb076   -Dliquibase.referenceUrl="jdbc:h2:~/h2/ibzdbmirror/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';"    -Dliquibase.referenceUsername=a0239cb076    -Dliquibase.referenceDefaultSchemaName=a0239cb076     -Dliquibase.defaultSchemaName=a0239cb076  -Dliquibase.diffExcludeObjects="Index:.*,table:IBZFILE,IBZUSER,IBZDATAAUDIT"

xcopy  src\main\resources\liquibase\changelog  target\classes\liquibase\changelog\   /y

if "%1" == "update" goto eof

:rollback
call mvn  org.liquibase:liquibase-maven-plugin:3.6.3:tag -Dliquibase.tag=rollbacktag  -Dliquibase.driver=org.h2.Driver   -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';" -DdefaultSchemaName=a0239cb076   -Dliquibase.username=a0239cb076

call mvn org.liquibase:liquibase-maven-plugin:3.6.3:futureRollbackSQL   -Dliquibase.rollbackTag=rollbacktag -Dliquibase.driver=org.h2.Driver   -Dliquibase.changeLogFile=src/main/resources/liquibase/master_table.xml  -Dliquibase.url="jdbc:h2:~/h2/last_db/a0239cb076;MODE=ORACLE;INIT=runscript from 'src/main/resources/liquibase/config/init_h2.sql';"  -DdefaultSchemaName=a0239cb076  -Dliquibase.username=a0239cb076    -Dliquibase.outputFile=src/main/resources/liquibase/rollback/%cur_date%.sql

:eof

pause

