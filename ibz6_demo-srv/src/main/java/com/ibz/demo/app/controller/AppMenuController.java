package com.ibz.demo.app.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.helper.MenuAppMenuHelper;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class AppMenuController{
    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验
    @Autowired
    private MenuAppMenuHelper menuAppMenuHelper;

	@GetMapping(value="/security/ctrl/menu1appmenu/get")
	public ResponseEntity<JsonNode> securityMenu1Get() throws Exception{
		log.debug("尝试获取菜单信息。");
		//是否是超级管理员
		Integer isSuperUser = AuthenticationUser.getAuthenticationUser().getSuperuser();
		InputStream in = this.getClass().getResourceAsStream("/appmenu/security_menu1.json");
		JsonNode jsonNode = new ObjectMapper().readTree(in);
		//如果开启权限验证 && 当前用户不是超级管理员,过滤权限相关菜单。
		if (enableRemoteValid && (1 != isSuperUser)) {
			jsonNode = menuAppMenuHelper.menuFilter(jsonNode);
		}
		log.debug("返回的菜单信息："+jsonNode);
		return ResponseEntity.ok().body(jsonNode);
	}
	/**
	 * 系统预置服务接口
	 * @return
	 */
	@RequestMapping(value="/security/app/security/getappdata")
    public ResponseEntity<AppData> securityGetAppData(@Validated HttpServletRequest request){
		AppData appData = new AppData();
    	return ResponseEntity.ok().body(appData);
    }
	@Data
	static protected class AppData{
		private String remotetag = null;
		private ObjectNode localdata = null;
	}
}
