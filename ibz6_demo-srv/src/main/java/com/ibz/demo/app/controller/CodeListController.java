package com.ibz.demo.app.controller;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;

@RestController
public class CodeListController{

	@Resource(name="IBZ6_DEMO_SysOperatorCodeList")
	private com.ibz.demo.ysmk.codelist.SysOperatorCodeList iBZ6_DEMO_security_SysOperatorCodeList;
	@RequestMapping("/security/app/codelist/ibz6_demo/sysoperator")
    public ResponseEntity<CodeList> securitygetIBZ6_DEMO_SysOperatorCodeList(){
    	return ResponseEntity.ok().body(iBZ6_DEMO_security_SysOperatorCodeList.getCodeList());
	}
	@RequestMapping("/security/app/codelist/getall")
    public ResponseEntity<List<CodeList>> securitygetAll(){
     List<CodeList> list = new ArrayList<CodeList>();
    list.add(iBZ6_DEMO_security_SysOperatorCodeList.getCodeList());
    	return ResponseEntity.ok().body(list);
	}
}
