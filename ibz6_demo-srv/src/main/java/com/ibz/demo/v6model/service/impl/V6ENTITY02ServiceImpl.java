
package com.ibz.demo.v6model.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.v6model.mapper.V6ENTITY02Mapper;
import com.ibz.demo.v6model.domain.V6ENTITY02;
import com.ibz.demo.v6model.service.V6ENTITY02Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.v6model.service.dto.V6ENTITY02SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[V6ENTITY02] 服务对象接口实现
 */
@Service(value="V6ENTITY02ServiceImpl")
public class V6ENTITY02ServiceImpl extends ServiceImplBase
<V6ENTITY02Mapper, V6ENTITY02> implements V6ENTITY02Service{


    @Resource
    private V6ENTITY02Mapper v6entity02Mapper;

    @Override
    public boolean create(V6ENTITY02 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(V6ENTITY02 et){
        this.beforeUpdate(et);
		this.v6entity02Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(V6ENTITY02 et){
        this.v6entity02Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(V6ENTITY02 et){
		return et.getV6entity02id();
    }
    @Override
    public boolean getDraft(V6ENTITY02 et)  {
            return true;
    }







    @Override
	public List<V6ENTITY02> listDefault(V6ENTITY02SearchFilter filter) {
		return v6entity02Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<V6ENTITY02> searchDefault(V6ENTITY02SearchFilter filter) {
		return v6entity02Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return v6entity02Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return v6entity02Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return v6entity02Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return v6entity02Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<V6ENTITY02> selectPermission(QueryWrapper<V6ENTITY02> selectCond) {
        return v6entity02Mapper.selectPermission(new V6ENTITY02SearchFilter().getPage(),selectCond);
    }

 }


