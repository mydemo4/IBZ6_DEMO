package com.ibz.demo.v6model.service;

import com.ibz.demo.v6model.domain.MANYENTITY;
import com.ibz.demo.v6model.service.dto.MANYENTITYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[MANYENTITY] 服务对象接口
 */
public interface MANYENTITYService extends IService<MANYENTITY>{

	boolean remove(MANYENTITY et);
	boolean update(MANYENTITY et);
	MANYENTITY get(MANYENTITY et);
	boolean getDraft(MANYENTITY et);
	boolean checkKey(MANYENTITY et);
	boolean create(MANYENTITY et);
    List<MANYENTITY> listDefault(MANYENTITYSearchFilter filter) ;
	Page<MANYENTITY> searchDefault(MANYENTITYSearchFilter filter);
	/**	 实体[MANYENTITY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
