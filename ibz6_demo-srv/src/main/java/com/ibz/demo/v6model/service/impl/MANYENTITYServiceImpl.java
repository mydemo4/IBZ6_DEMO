
package com.ibz.demo.v6model.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.v6model.mapper.MANYENTITYMapper;
import com.ibz.demo.v6model.domain.MANYENTITY;
import com.ibz.demo.v6model.service.MANYENTITYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.v6model.service.dto.MANYENTITYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.v6model.service.ONEENTITYService;

/**
 * 实体[MANYENTITY] 服务对象接口实现
 */
@Service(value="MANYENTITYServiceImpl")
public class MANYENTITYServiceImpl extends ServiceImplBase
<MANYENTITYMapper, MANYENTITY> implements MANYENTITYService{


    @Autowired
    private ONEENTITYService der1n_manyentity_oneentity_oneentityidService;
    @Resource
    private MANYENTITYMapper manyentityMapper;

    @Override
    public boolean create(MANYENTITY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(MANYENTITY et){
        this.beforeUpdate(et);
		this.manyentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(MANYENTITY et){
        this.manyentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(MANYENTITY et){
		return et.getManyentityid();
    }
    @Override
    public boolean getDraft(MANYENTITY et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getOneentityid())) && et.getOneentityname() == null){
                try {
                    et.setOneentityname(der1n_manyentity_oneentity_oneentityidService.getById(et.getOneentityid()).getOneentityname());
                }catch (Exception ex){}
            }
            return true;
    }







    @Override
	public List<MANYENTITY> listDefault(MANYENTITYSearchFilter filter) {
		return manyentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<MANYENTITY> searchDefault(MANYENTITYSearchFilter filter) {
		return manyentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return manyentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return manyentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return manyentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return manyentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<MANYENTITY> selectPermission(QueryWrapper<MANYENTITY> selectCond) {
        return manyentityMapper.selectPermission(new MANYENTITYSearchFilter().getPage(),selectCond);
    }

 }


