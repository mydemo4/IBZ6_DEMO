package com.ibz.demo.v6model.service;

import com.ibz.demo.v6model.domain.ONEENTITY;
import com.ibz.demo.v6model.service.dto.ONEENTITYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ONEENTITY] 服务对象接口
 */
public interface ONEENTITYService extends IService<ONEENTITY>{

	boolean getDraft(ONEENTITY et);
	boolean create(ONEENTITY et);
	boolean checkKey(ONEENTITY et);
	ONEENTITY get(ONEENTITY et);
	boolean update(ONEENTITY et);
	boolean remove(ONEENTITY et);
    List<ONEENTITY> listTest(ONEENTITYSearchFilter filter) ;
	Page<ONEENTITY> searchTest(ONEENTITYSearchFilter filter);
    List<ONEENTITY> listDefault(ONEENTITYSearchFilter filter) ;
	Page<ONEENTITY> searchDefault(ONEENTITYSearchFilter filter);
	/**	 实体[ONEENTITY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
