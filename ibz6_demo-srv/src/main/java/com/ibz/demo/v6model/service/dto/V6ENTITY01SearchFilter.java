package com.ibz.demo.v6model.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.v6model.domain.V6ENTITY01;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class V6ENTITY01SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<V6ENTITY01> selectCond;
	public V6ENTITY01SearchFilter(){
		this.page =new Page<V6ENTITY01>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<V6ENTITY01>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("v6entity01name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_v6entity01name_like;//[v6实体名称]
	public void setN_v6entity01name_like(String n_v6entity01name_like) {
        this.n_v6entity01name_like = n_v6entity01name_like;
        if(!StringUtils.isEmpty(this.n_v6entity01name_like)){
            this.selectCond.like("v6entity01name", n_v6entity01name_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
