
package com.ibz.demo.v6model.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.v6model.mapper.ONEENTITYMapper;
import com.ibz.demo.v6model.domain.ONEENTITY;
import com.ibz.demo.v6model.service.ONEENTITYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.v6model.service.dto.ONEENTITYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ONEENTITY] 服务对象接口实现
 */
@Service(value="ONEENTITYServiceImpl")
public class ONEENTITYServiceImpl extends ServiceImplBase
<ONEENTITYMapper, ONEENTITY> implements ONEENTITYService{


    @Resource
    private ONEENTITYMapper oneentityMapper;

    @Override
    public boolean create(ONEENTITY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(ONEENTITY et){
        this.beforeUpdate(et);
		this.oneentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ONEENTITY et){
        this.oneentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ONEENTITY et){
		return et.getOneentityid();
    }
    @Override
    public boolean getDraft(ONEENTITY et)  {
            return true;
    }







    @Override
	public List<ONEENTITY> listTest(ONEENTITYSearchFilter filter) {
		return oneentityMapper.searchTest(filter,filter.getSelectCond());
	}
    @Override
	public Page<ONEENTITY> searchTest(ONEENTITYSearchFilter filter) {
		return oneentityMapper.searchTest(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<ONEENTITY> listDefault(ONEENTITYSearchFilter filter) {
		return oneentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ONEENTITY> searchDefault(ONEENTITYSearchFilter filter) {
		return oneentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return oneentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return oneentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return oneentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return oneentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ONEENTITY> selectPermission(QueryWrapper<ONEENTITY> selectCond) {
        return oneentityMapper.selectPermission(new ONEENTITYSearchFilter().getPage(),selectCond);
    }

 }


