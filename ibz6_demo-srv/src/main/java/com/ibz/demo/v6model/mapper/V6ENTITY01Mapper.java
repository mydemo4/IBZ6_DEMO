package com.ibz.demo.v6model.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.v6model.domain.V6ENTITY01;
import com.ibz.demo.v6model.service.dto.V6ENTITY01SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface V6ENTITY01Mapper extends BaseMapper<V6ENTITY01>{

    List<V6ENTITY01> searchDefault(@Param("srf") V6ENTITY01SearchFilter searchfilter,@Param("ew") Wrapper<V6ENTITY01> wrapper) ;
    Page<V6ENTITY01> searchDefault(IPage page, @Param("srf") V6ENTITY01SearchFilter searchfilter, @Param("ew") Wrapper<V6ENTITY01> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'V6ENTITY01:'+#p0")
    V6ENTITY01 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'V6ENTITY01:'+#p0.v6entity01id")
    int insert(V6ENTITY01 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'V6ENTITY01:'+#p0.v6entity01id")
    int updateById(@Param(Constants.ENTITY) V6ENTITY01 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'V6ENTITY01:'+#p0")
    int deleteById(Serializable id);
    Page<V6ENTITY01> selectPermission(IPage page,@Param("pw") Wrapper<V6ENTITY01> wrapper) ;

    //直接sql查询。
    List<V6ENTITY01> executeRawSql(@Param("sql")String sql);
}
