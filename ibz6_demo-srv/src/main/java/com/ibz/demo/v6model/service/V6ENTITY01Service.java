package com.ibz.demo.v6model.service;

import com.ibz.demo.v6model.domain.V6ENTITY01;
import com.ibz.demo.v6model.service.dto.V6ENTITY01SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[V6ENTITY01] 服务对象接口
 */
public interface V6ENTITY01Service extends IService<V6ENTITY01>{

	boolean remove(V6ENTITY01 et);
	boolean getDraft(V6ENTITY01 et);
	V6ENTITY01 get(V6ENTITY01 et);
	boolean create(V6ENTITY01 et);
	boolean update(V6ENTITY01 et);
	boolean checkKey(V6ENTITY01 et);
    List<V6ENTITY01> listDefault(V6ENTITY01SearchFilter filter) ;
	Page<V6ENTITY01> searchDefault(V6ENTITY01SearchFilter filter);
	/**	 实体[V6ENTITY01] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
