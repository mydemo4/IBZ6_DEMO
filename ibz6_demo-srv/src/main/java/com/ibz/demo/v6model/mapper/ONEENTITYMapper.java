package com.ibz.demo.v6model.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.v6model.domain.ONEENTITY;
import com.ibz.demo.v6model.service.dto.ONEENTITYSearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface ONEENTITYMapper extends BaseMapper<ONEENTITY>{

    List<ONEENTITY> searchTest(@Param("srf") ONEENTITYSearchFilter searchfilter,@Param("ew") Wrapper<ONEENTITY> wrapper) ;
    Page<ONEENTITY> searchTest(IPage page, @Param("srf") ONEENTITYSearchFilter searchfilter, @Param("ew") Wrapper<ONEENTITY> wrapper) ;
    List<ONEENTITY> searchDefault(@Param("srf") ONEENTITYSearchFilter searchfilter,@Param("ew") Wrapper<ONEENTITY> wrapper) ;
    Page<ONEENTITY> searchDefault(IPage page, @Param("srf") ONEENTITYSearchFilter searchfilter, @Param("ew") Wrapper<ONEENTITY> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'ONEENTITY:'+#p0")
    ONEENTITY selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'ONEENTITY:'+#p0.oneentityid")
    int insert(ONEENTITY entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'ONEENTITY:'+#p0.oneentityid")
    int updateById(@Param(Constants.ENTITY) ONEENTITY entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'ONEENTITY:'+#p0")
    int deleteById(Serializable id);
    Page<ONEENTITY> selectPermission(IPage page,@Param("pw") Wrapper<ONEENTITY> wrapper) ;

    //直接sql查询。
    List<ONEENTITY> executeRawSql(@Param("sql")String sql);
}
