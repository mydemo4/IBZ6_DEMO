package com.ibz.demo.v6model.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.v6model.domain.MANYENTITY;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MANYENTITYSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<MANYENTITY> selectCond;
	public MANYENTITYSearchFilter(){
		this.page =new Page<MANYENTITY>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<MANYENTITY>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_MANYENTITY_ONEENTITY_ONEENTITYID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_oneentityid_eq("NA");
            } else {
                this.setN_oneentityid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("manyentityname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_manyentityname_like;//[多方实体名称]
	public void setN_manyentityname_like(String n_manyentityname_like) {
        this.n_manyentityname_like = n_manyentityname_like;
        if(!StringUtils.isEmpty(this.n_manyentityname_like)){
            this.selectCond.like("manyentityname", n_manyentityname_like);
        }
    }
	private String n_oneentityid_eq;//[一方实体标识]
	public void setN_oneentityid_eq(String n_oneentityid_eq) {
        this.n_oneentityid_eq = n_oneentityid_eq;
        if(!StringUtils.isEmpty(this.n_oneentityid_eq)){
            this.selectCond.eq("oneentityid", n_oneentityid_eq);
        }
    }
	private String n_oneentityname_eq;//[一方实体名称]
	public void setN_oneentityname_eq(String n_oneentityname_eq) {
        this.n_oneentityname_eq = n_oneentityname_eq;
        if(!StringUtils.isEmpty(this.n_oneentityname_eq)){
            this.selectCond.eq("oneentityname", n_oneentityname_eq);
        }
    }
	private String n_oneentityname_like;//[一方实体名称]
	public void setN_oneentityname_like(String n_oneentityname_like) {
        this.n_oneentityname_like = n_oneentityname_like;
        if(!StringUtils.isEmpty(this.n_oneentityname_like)){
            this.selectCond.like("oneentityname", n_oneentityname_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
