
package com.ibz.demo.v6model.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.v6model.mapper.V6ENTITY01Mapper;
import com.ibz.demo.v6model.domain.V6ENTITY01;
import com.ibz.demo.v6model.service.V6ENTITY01Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.v6model.service.dto.V6ENTITY01SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[V6ENTITY01] 服务对象接口实现
 */
@Service(value="V6ENTITY01ServiceImpl")
public class V6ENTITY01ServiceImpl extends ServiceImplBase
<V6ENTITY01Mapper, V6ENTITY01> implements V6ENTITY01Service{


    @Resource
    private V6ENTITY01Mapper v6entity01Mapper;

    @Override
    public boolean create(V6ENTITY01 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(V6ENTITY01 et){
        this.beforeUpdate(et);
		this.v6entity01Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(V6ENTITY01 et){
        this.v6entity01Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(V6ENTITY01 et){
		return et.getV6entity01id();
    }
    @Override
    public boolean getDraft(V6ENTITY01 et)  {
            return true;
    }







    @Override
	public List<V6ENTITY01> listDefault(V6ENTITY01SearchFilter filter) {
		return v6entity01Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<V6ENTITY01> searchDefault(V6ENTITY01SearchFilter filter) {
		return v6entity01Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return v6entity01Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return v6entity01Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return v6entity01Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return v6entity01Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<V6ENTITY01> selectPermission(QueryWrapper<V6ENTITY01> selectCond) {
        return v6entity01Mapper.selectPermission(new V6ENTITY01SearchFilter().getPage(),selectCond);
    }

 }


