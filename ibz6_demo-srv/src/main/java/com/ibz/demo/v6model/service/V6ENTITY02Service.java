package com.ibz.demo.v6model.service;

import com.ibz.demo.v6model.domain.V6ENTITY02;
import com.ibz.demo.v6model.service.dto.V6ENTITY02SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[V6ENTITY02] 服务对象接口
 */
public interface V6ENTITY02Service extends IService<V6ENTITY02>{

	boolean remove(V6ENTITY02 et);
	boolean checkKey(V6ENTITY02 et);
	boolean update(V6ENTITY02 et);
	boolean getDraft(V6ENTITY02 et);
	boolean create(V6ENTITY02 et);
	V6ENTITY02 get(V6ENTITY02 et);
    List<V6ENTITY02> listDefault(V6ENTITY02SearchFilter filter) ;
	Page<V6ENTITY02> searchDefault(V6ENTITY02SearchFilter filter);
	/**	 实体[V6ENTITY02] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
