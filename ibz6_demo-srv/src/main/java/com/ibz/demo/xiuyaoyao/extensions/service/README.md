平台发布service类重写请放入此文件夹


#重写service类
注：IDEA中可以打开基类service文件，鼠标点中基类service类名，按Alt+Enter，弹出的操作菜单中选择Create Subclass，选中此目录完成快速重写

声明@Primary和@Service

```java
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Primary
@Service
public class TestEntityServiceImplImpl extends TestEntityServiceImpl
{
	@Override
	public boolean create(TA_JS_QZCS et)
	{
		//此处放入create之前代码

		boolean rt=super.create(et);

		//此处放入create之后代码

		return rt;
	}
}

```