package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.BJQTEST;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BJQTEST_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String bjqtestid;
    @JsonIgnore
    private String bjqtestname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String input;
    @JsonIgnore
    private String mmk;
    @JsonIgnore
    private String dhwb;
    @JsonIgnore
    private String dxlb;
    @JsonIgnore
    private String dsjxz;
    @JsonIgnore
    private String sjxz;
    @JsonIgnore
    private String xllbd;
    @JsonIgnore
    private String xllbdx;
    @JsonIgnore
    private String sjxzq;
    @JsonIgnore
    private String bujinqi;
    @JsonIgnore
    private String kgbj;
    @JsonIgnore
    private String tpxzd;
    @JsonIgnore
    private String tpxzdx;
    @JsonIgnore
    private String wjsc;
    @JsonIgnore
    private String wjscd;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[r7编辑器标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return bjqtestid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String bjqtestid){
        this.bjqtestid = bjqtestid;
    }
    @Size(min = 0, max = 200, message = "[r7编辑器名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return bjqtestname;
    }
    @Size(min = 0, max = 100, message = "[输入框]长度必须在[100]以内!")
    @JsonProperty(value = "input")
    public String getInput(){
        return input;
    }
    @JsonProperty(value = "input")
    public void setInput(String input){
        this.input = input;
    }
    @Size(min = 0, max = 100, message = "[密码框]长度必须在[100]以内!")
    @JsonProperty(value = "mmk")
    public String getMmk(){
        return mmk;
    }
    @JsonProperty(value = "mmk")
    public void setMmk(String mmk){
        this.mmk = mmk;
    }
    @Size(min = 0, max = 100, message = "[多行文本]长度必须在[100]以内!")
    @JsonProperty(value = "dhwb")
    public String getDhwb(){
        return dhwb;
    }
    @JsonProperty(value = "dhwb")
    public void setDhwb(String dhwb){
        this.dhwb = dhwb;
    }
    @Size(min = 0, max = 100, message = "[单选列表]长度必须在[100]以内!")
    @JsonProperty(value = "dxlb")
    public String getDxlb(){
        return dxlb;
    }
    @JsonProperty(value = "dxlb")
    public void setDxlb(String dxlb){
        this.dxlb = dxlb;
    }
    @Size(min = 0, max = 100, message = "[多数据选择]长度必须在[100]以内!")
    @JsonProperty(value = "dsjxz")
    public String getDsjxz(){
        return dsjxz;
    }
    @JsonProperty(value = "dsjxz")
    public void setDsjxz(String dsjxz){
        this.dsjxz = dsjxz;
    }
    @Size(min = 0, max = 100, message = "[数据选择]长度必须在[100]以内!")
    @JsonProperty(value = "sjxz")
    public String getSjxz(){
        return sjxz;
    }
    @JsonProperty(value = "sjxz")
    public void setSjxz(String sjxz){
        this.sjxz = sjxz;
    }
    @NotBlank(message = "[下拉列表（单选）]不允许为空!")
    @Size(min = 0, max = 100, message = "[下拉列表（单选）]长度必须在[100]以内!")
    @JsonProperty(value = "xllbd")
    public String getXllbd(){
        return xllbd;
    }
    @JsonProperty(value = "xllbd")
    public void setXllbd(String xllbd){
        this.xllbd = xllbd;
    }
    @NotBlank(message = "[下拉列表（多选）]不允许为空!")
    @Size(min = 0, max = 100, message = "[下拉列表（多选）]长度必须在[100]以内!")
    @JsonProperty(value = "xllbdx")
    public String getXllbdx(){
        return xllbdx;
    }
    @JsonProperty(value = "xllbdx")
    public void setXllbdx(String xllbdx){
        this.xllbdx = xllbdx;
    }
    @Size(min = 0, max = 100, message = "[时间选择器]长度必须在[100]以内!")
    @JsonProperty(value = "sjxzq")
    public String getSjxzq(){
        return sjxzq;
    }
    @JsonProperty(value = "sjxzq")
    public void setSjxzq(String sjxzq){
        this.sjxzq = sjxzq;
    }
    @Size(min = 0, max = 100, message = "[步进器]长度必须在[100]以内!")
    @JsonProperty(value = "bujinqi")
    public String getBujinqi(){
        return bujinqi;
    }
    @JsonProperty(value = "bujinqi")
    public void setBujinqi(String bujinqi){
        this.bujinqi = bujinqi;
    }
    @Size(min = 0, max = 100, message = "[开关部件]长度必须在[100]以内!")
    @JsonProperty(value = "kgbj")
    public String getKgbj(){
        return kgbj;
    }
    @JsonProperty(value = "kgbj")
    public void setKgbj(String kgbj){
        this.kgbj = kgbj;
    }
    @Size(min = 0, max = 100, message = "[图片选择（单选）]长度必须在[100]以内!")
    @JsonProperty(value = "tpxzd")
    public String getTpxzd(){
        return tpxzd;
    }
    @JsonProperty(value = "tpxzd")
    public void setTpxzd(String tpxzd){
        this.tpxzd = tpxzd;
    }
    @Size(min = 0, max = 100, message = "[图片选择（多选）]长度必须在[100]以内!")
    @JsonProperty(value = "tpxzdx")
    public String getTpxzdx(){
        return tpxzdx;
    }
    @JsonProperty(value = "tpxzdx")
    public void setTpxzdx(String tpxzdx){
        this.tpxzdx = tpxzdx;
    }
    @Size(min = 0, max = 100, message = "[文件上传]长度必须在[100]以内!")
    @JsonProperty(value = "wjsc")
    public String getWjsc(){
        return wjsc;
    }
    @JsonProperty(value = "wjsc")
    public void setWjsc(String wjsc){
        this.wjsc = wjsc;
    }
    @Size(min = 0, max = 100, message = "[文件上传（多选）]长度必须在[100]以内!")
    @JsonProperty(value = "wjscd")
    public String getWjscd(){
        return wjscd;
    }
    @JsonProperty(value = "wjscd")
    public void setWjscd(String wjscd){
        this.wjscd = wjscd;
    }
    @Size(min = 0, max = 100, message = "[r7编辑器标识]长度必须在[100]以内!")
    @JsonProperty(value = "bjqtestid")
    public String getBjqtestid(){
        return bjqtestid;
    }
    @JsonProperty(value = "bjqtestid")
    public void setBjqtestid(String bjqtestid){
        this.bjqtestid = bjqtestid;
    }
    public  void fromBJQTEST(BJQTEST sourceEntity)  {
        this.fromBJQTEST(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromBJQTEST(BJQTEST sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(BJQTEST.class, BJQTEST_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  BJQTEST toBJQTEST()  {
        BJQTEST targetEntity =new BJQTEST();
        BeanCopier copier=BeanCopier.create(BJQTEST_EditForm_Main.class, BJQTEST.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
    /**
     * 使用@JsonFilter输出表单项更新中配置的表单项
     * 过滤掉Vo中其余无效表单项
     */
    private static final String DYNC_INCLUDE = "DYNC_INCLUDE";
    @JsonFilter(DYNC_INCLUDE)
    interface DynamicInclude{}
    /**
     * 表单项更新过滤器 [test01]
     * 只输出表单项更新的明细项
     * @return
     */
    public JSONObject test01FormItemUpdateFilter() throws JsonProcessingException{
        JSONObject formItemDetailObj =new JSONObject();
        ObjectMapper mapper = new ObjectMapper();
        Set<String> properties =new HashSet<>();
		properties.add("bujinqi");
		properties.add("input");
        mapper.setFilterProvider(new SimpleFilterProvider().addFilter(DYNC_INCLUDE, SimpleBeanPropertyFilter.filterOutAllExcept(properties)));
        mapper.addMixIn(this.getClass(), DynamicInclude.class);
        String strResult=mapper.writeValueAsString(this);
        if(!StringUtils.isEmpty(strResult)){
            formItemDetailObj=JSONObject.parseObject(strResult);
        }
        return formItemDetailObj;
    }
}
