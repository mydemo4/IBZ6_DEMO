package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.MEETING;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MEETING_MOBMDCTRL_Mob{
    private String srfkey;
    private String srfmajortext;
     public  void fromMEETING(MEETING sourceEntity)  {
	    this.setSrfkey(String.format("%s",sourceEntity.getMeetingid()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getMeetingname()));
	}
	public static Page<MEETING_MOBMDCTRL_Mob> fromMEETING(Page<MEETING> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<MEETING_MOBMDCTRL_Mob> targetpage=new Page<MEETING_MOBMDCTRL_Mob>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<MEETING_MOBMDCTRL_Mob> records=new ArrayList<MEETING_MOBMDCTRL_Mob>();
        for(MEETING source:sourcePage.getRecords()) {
    MEETING_MOBMDCTRL_Mob target=new MEETING_MOBMDCTRL_Mob();
            target.fromMEETING(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
