package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[业务状态代码表演示]
 */
@Component("IBZ6_DEMO_WorkStateDemoCodeList")
public class WorkStateDemoCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_WorkStateDemo");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("10");
		item0.setId("10");
		item0.setText("未审核");
		item0.setLabel("未审核");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("10",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("20");
		item1.setId("20");
		item1.setText("审核中");
		item1.setLabel("审核中");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("20",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("30");
		item2.setId("30");
		item2.setText("已审核");
		item2.setLabel("已审核");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("30",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("40");
		item3.setId("40");
		item3.setText("作废");
		item3.setLabel("作废");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("40",item3);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}