package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.LBT;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LBT_List_LB{
    private String image;
    private String srfkey;
    private String srfmajortext;
     public  void fromLBT(LBT sourceEntity)  {
	    this.setImage(String.format("%s",sourceEntity.getImage()));
	    this.setSrfkey(String.format("%s",sourceEntity.getLbtid()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getLbtname()));
	}
	public static Page<LBT_List_LB> fromLBT(Page<LBT> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<LBT_List_LB> targetpage=new Page<LBT_List_LB>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<LBT_List_LB> records=new ArrayList<LBT_List_LB>();
        for(LBT source:sourcePage.getRecords()) {
    LBT_List_LB target=new LBT_List_LB();
            target.fromLBT(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
