
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.IBZSAM02Mapper;
import com.ibz.demo.ysmk.domain.IBZSAM02;
import com.ibz.demo.ysmk.service.IBZSAM02Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.IBZSAM02SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.ysmk.service.IBZSam01Service;

/**
 * 实体[IBZSAM02] 服务对象接口实现
 */
@Service(value="IBZSAM02ServiceImpl")
public class IBZSAM02ServiceImpl extends ServiceImplBase
<IBZSAM02Mapper, IBZSAM02> implements IBZSAM02Service{


    @Autowired
    private IBZSam01Service ibzsam01Service;
    @Resource
    private IBZSAM02Mapper ibzsam02Mapper;

    @Override
    public boolean create(IBZSAM02 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(IBZSAM02 et){
        this.beforeUpdate(et);
		this.ibzsam02Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(IBZSAM02 et){
        this.ibzsam02Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(IBZSAM02 et){
		return et.getIbzsam02id();
    }
    @Override
    public boolean getDraft(IBZSAM02 et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getIbzsam01id())) && et.getIbzsam01name() == null){
                try {
                    et.setIbzsam01name(ibzsam01Service.getById(et.getIbzsam01id()).getIbzsam01name());
                }catch (Exception ex){}
            }
            return true;
    }


    @Autowired
    com.ibz.demo.ysmk.logic.IBZSAM02Logic ibzsam02Logic;
    @Override
    public IBZSAM02 updateJE(IBZSAM02 et)  {
        ibzsam02Logic.updateJE(et);
		return et;
    }






    @Override
	public List<IBZSAM02> listDefault(IBZSAM02SearchFilter filter) {
		return ibzsam02Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<IBZSAM02> searchDefault(IBZSAM02SearchFilter filter) {
		return ibzsam02Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return ibzsam02Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return ibzsam02Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return ibzsam02Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return ibzsam02Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<IBZSAM02> selectPermission(QueryWrapper<IBZSAM02> selectCond) {
        return ibzsam02Mapper.selectPermission(new IBZSAM02SearchFilter().getPage(),selectCond);
    }

 }


