package com.ibz.demo.ysmk.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.ysmk.domain.LBT;
import com.ibz.demo.ysmk.service.dto.LBTSearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface LBTMapper extends BaseMapper<LBT>{

    List<LBT> searchSC(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchSC(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchLToR(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchLToR(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchScroll(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchScroll(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchTitleImg(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchTitleImg(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchMDLB(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchMDLB(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchLToR2(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchLToR2(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchImgLB(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchImgLB(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchLB(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchLB(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchSSGroup(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchSSGroup(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
    List<LBT> searchDefault(@Param("srf") LBTSearchFilter searchfilter,@Param("ew") Wrapper<LBT> wrapper) ;
    Page<LBT> searchDefault(IPage page, @Param("srf") LBTSearchFilter searchfilter, @Param("ew") Wrapper<LBT> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'LBT:'+#p0")
    LBT selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'LBT:'+#p0.lbtid")
    int insert(LBT entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'LBT:'+#p0.lbtid")
    int updateById(@Param(Constants.ENTITY) LBT entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'LBT:'+#p0")
    int deleteById(Serializable id);
    Page<LBT> selectPermission(IPage page,@Param("pw") Wrapper<LBT> wrapper) ;

    //直接sql查询。
    List<LBT> executeRawSql(@Param("sql")String sql);
}
