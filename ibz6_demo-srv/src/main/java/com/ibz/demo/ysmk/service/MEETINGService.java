package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.MEETING;
import com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[MEETING] 服务对象接口
 */
public interface MEETINGService extends IService<MEETING>{

	boolean checkKey(MEETING et);
	MEETING get(MEETING et);
	boolean create(MEETING et);
	boolean remove(MEETING et);
	boolean getDraft(MEETING et);
	boolean update(MEETING et);
    List<MEETING> listDefault(MEETINGSearchFilter filter) ;
	Page<MEETING> searchDefault(MEETINGSearchFilter filter);
	/**	 实体[MEETING] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
