package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.TjfxSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tjfx_SearchForm_Default{

    private DataObj srfparentdata;

    private String n_tjfxname_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromTjfxSearchFilter(TjfxSearchFilter sourceSearchFilter)  {
        this.setN_tjfxname_like(sourceSearchFilter.getN_tjfxname_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  TjfxSearchFilter toTjfxSearchFilter()  {
        TjfxSearchFilter targetSearchFilter=new TjfxSearchFilter();
        if(targetSearchFilter.getN_tjfxname_like()==null)
            targetSearchFilter.setN_tjfxname_like(this.getN_tjfxname_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
