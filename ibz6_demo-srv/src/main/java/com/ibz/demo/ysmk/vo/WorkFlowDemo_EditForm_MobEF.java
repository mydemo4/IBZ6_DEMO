package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.WorkFlowDemo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkFlowDemo_EditForm_MobEF{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String workflowdemoid;
    @JsonIgnore
    private String workflowdemoname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String shyj;
    @JsonIgnore
    private String nextpr;
    @JsonIgnore
    private String nextpr1;
    @JsonIgnore
    private String testfield;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[工作流演示标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return workflowdemoid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String workflowdemoid){
        this.workflowdemoid = workflowdemoid;
    }
    @Size(min = 0, max = 200, message = "[工作流演示名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return workflowdemoname;
    }
    @Size(min = 0, max = 200, message = "[工作流演示名称]长度必须在[200]以内!")
    @JsonProperty(value = "workflowdemoname")
    public String getWorkflowdemoname(){
        return workflowdemoname;
    }
    @JsonProperty(value = "workflowdemoname")
    public void setWorkflowdemoname(String workflowdemoname){
        this.workflowdemoname = workflowdemoname;
    }
    @Size(min = 0, max = 100, message = "[审核意见]长度必须在[100]以内!")
    @JsonProperty(value = "shyj")
    public String getShyj(){
        return shyj;
    }
    @JsonProperty(value = "shyj")
    public void setShyj(String shyj){
        this.shyj = shyj;
    }
    @Size(min = 0, max = 100, message = "[节点处理人]长度必须在[100]以内!")
    @JsonProperty(value = "nextpr")
    public String getNextpr(){
        return nextpr;
    }
    @JsonProperty(value = "nextpr")
    public void setNextpr(String nextpr){
        this.nextpr = nextpr;
    }
    @Size(min = 0, max = 100, message = "[节点处理人1]长度必须在[100]以内!")
    @JsonProperty(value = "nextpr1")
    public String getNextpr1(){
        return nextpr1;
    }
    @JsonProperty(value = "nextpr1")
    public void setNextpr1(String nextpr1){
        this.nextpr1 = nextpr1;
    }
    @Size(min = 0, max = 100, message = "[测试属性]长度必须在[100]以内!")
    @JsonProperty(value = "testfield")
    public String getTestfield(){
        return testfield;
    }
    @JsonProperty(value = "testfield")
    public void setTestfield(String testfield){
        this.testfield = testfield;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[工作流演示标识]长度必须在[100]以内!")
    @JsonProperty(value = "workflowdemoid")
    public String getWorkflowdemoid(){
        return workflowdemoid;
    }
    @JsonProperty(value = "workflowdemoid")
    public void setWorkflowdemoid(String workflowdemoid){
        this.workflowdemoid = workflowdemoid;
    }
    public  void fromWorkFlowDemo(WorkFlowDemo sourceEntity)  {
        this.fromWorkFlowDemo(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromWorkFlowDemo(WorkFlowDemo sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(WorkFlowDemo.class, WorkFlowDemo_EditForm_MobEF.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  WorkFlowDemo toWorkFlowDemo()  {
        WorkFlowDemo targetEntity =new WorkFlowDemo();
        BeanCopier copier=BeanCopier.create(WorkFlowDemo_EditForm_MobEF.class, WorkFlowDemo.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
