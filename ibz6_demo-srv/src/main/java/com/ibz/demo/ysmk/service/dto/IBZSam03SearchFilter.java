package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.IBZSam03;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSam03SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<IBZSam03> selectCond;
	public IBZSam03SearchFilter(){
		this.page =new Page<IBZSam03>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<IBZSam03>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_IBZSAM03_IBZSAM02_IBZSAM02ID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_ibzsam02id_eq("NA");
            } else {
                this.setN_ibzsam02id_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("ibzsam03name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_ibzsam03name_like;//[实例实体03名称]
	public void setN_ibzsam03name_like(String n_ibzsam03name_like) {
        this.n_ibzsam03name_like = n_ibzsam03name_like;
        if(!StringUtils.isEmpty(this.n_ibzsam03name_like)){
            this.selectCond.like("ibzsam03name", n_ibzsam03name_like);
        }
    }
	private String n_ibzsam02id_eq;//[示例实体02]
	public void setN_ibzsam02id_eq(String n_ibzsam02id_eq) {
        this.n_ibzsam02id_eq = n_ibzsam02id_eq;
        if(!StringUtils.isEmpty(this.n_ibzsam02id_eq)){
            this.selectCond.eq("ibzsam02id", n_ibzsam02id_eq);
        }
    }
	private String n_ibzsam02name_eq;//[示例实体02名称]
	public void setN_ibzsam02name_eq(String n_ibzsam02name_eq) {
        this.n_ibzsam02name_eq = n_ibzsam02name_eq;
        if(!StringUtils.isEmpty(this.n_ibzsam02name_eq)){
            this.selectCond.eq("ibzsam02name", n_ibzsam02name_eq);
        }
    }
	private String n_ibzsam02name_like;//[示例实体02名称]
	public void setN_ibzsam02name_like(String n_ibzsam02name_like) {
        this.n_ibzsam02name_like = n_ibzsam02name_like;
        if(!StringUtils.isEmpty(this.n_ibzsam02name_like)){
            this.selectCond.like("ibzsam02name", n_ibzsam02name_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
