package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.HumanSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Human_SearchForm_Mobsearchform{

    private DataObj srfparentdata;

    private String n_humanname_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromHumanSearchFilter(HumanSearchFilter sourceSearchFilter)  {
        this.setN_humanname_like(sourceSearchFilter.getN_humanname_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  HumanSearchFilter toHumanSearchFilter()  {
        HumanSearchFilter targetSearchFilter=new HumanSearchFilter();
        if(targetSearchFilter.getN_humanname_like()==null)
            targetSearchFilter.setN_humanname_like(this.getN_humanname_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
