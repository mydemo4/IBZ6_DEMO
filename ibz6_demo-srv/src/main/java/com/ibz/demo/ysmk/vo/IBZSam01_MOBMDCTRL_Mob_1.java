package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.IBZSam01;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSam01_MOBMDCTRL_Mob_1{
    private String url;
    private String srfmajortext;
    private String count;
    private String content;
    private String srfkey;
    private String srfmstag;
     public  void fromIBZSam01(IBZSam01 sourceEntity)  {
	    this.setUrl(String.format("%s",sourceEntity.getImage()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getIbzsam01name()));
	    this.setCount(String.format("%s",sourceEntity.getSl()));
	    this.setContent(String.format("%s",sourceEntity.getDxtext()));
	    this.setSrfkey(String.format("%s",sourceEntity.getIbzsam01id()));
	}
	public static Page<IBZSam01_MOBMDCTRL_Mob_1> fromIBZSam01(Page<IBZSam01> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<IBZSam01_MOBMDCTRL_Mob_1> targetpage=new Page<IBZSam01_MOBMDCTRL_Mob_1>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<IBZSam01_MOBMDCTRL_Mob_1> records=new ArrayList<IBZSam01_MOBMDCTRL_Mob_1>();
        for(IBZSam01 source:sourcePage.getRecords()) {
    IBZSam01_MOBMDCTRL_Mob_1 target=new IBZSam01_MOBMDCTRL_Mob_1();
            target.fromIBZSam01(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
