package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.ENTITY2;
import com.ibz.demo.ysmk.service.dto.ENTITY2SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ENTITY2] 服务对象接口
 */
public interface ENTITY2Service extends IService<ENTITY2>{

	ENTITY2 get(ENTITY2 et);
	boolean update(ENTITY2 et);
	boolean remove(ENTITY2 et);
	boolean checkKey(ENTITY2 et);
	boolean getDraft(ENTITY2 et);
	boolean create(ENTITY2 et);
    List<ENTITY2> listDefault(ENTITY2SearchFilter filter) ;
	Page<ENTITY2> searchDefault(ENTITY2SearchFilter filter);
	/**	 实体[ENTITY2] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
