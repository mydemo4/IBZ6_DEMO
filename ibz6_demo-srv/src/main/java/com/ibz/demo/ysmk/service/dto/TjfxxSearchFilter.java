package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.Tjfxx;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TjfxxSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<Tjfxx> selectCond;
	public TjfxxSearchFilter(){
		this.page =new Page<Tjfxx>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<Tjfxx>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_TJFXX_TJFX_TJFXID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_tjfxid_eq("NA");
            } else {
                this.setN_tjfxid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("tjfxxname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_tjfxxname_like;//[统计分析项名称]
	public void setN_tjfxxname_like(String n_tjfxxname_like) {
        this.n_tjfxxname_like = n_tjfxxname_like;
        if(!StringUtils.isEmpty(this.n_tjfxxname_like)){
            this.selectCond.like("tjfxxname", n_tjfxxname_like);
        }
    }
	private String n_tjfxid_eq;//[统计分析标识]
	public void setN_tjfxid_eq(String n_tjfxid_eq) {
        this.n_tjfxid_eq = n_tjfxid_eq;
        if(!StringUtils.isEmpty(this.n_tjfxid_eq)){
            this.selectCond.eq("tjfxid", n_tjfxid_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
