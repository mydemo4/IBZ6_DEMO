
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.WorkFlowDemoMapper;
import com.ibz.demo.ysmk.domain.WorkFlowDemo;
import com.ibz.demo.ysmk.service.WorkFlowDemoService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.WorkFlowDemoSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[WorkFlowDemo] 服务对象接口实现
 */
@Service(value="WorkFlowDemoServiceImpl")
public class WorkFlowDemoServiceImpl extends ServiceImplBase
<WorkFlowDemoMapper, WorkFlowDemo> implements WorkFlowDemoService{


    @Resource
    private WorkFlowDemoMapper workflowdemoMapper;

    @Override
    public boolean create(WorkFlowDemo et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(WorkFlowDemo et){
        this.beforeUpdate(et);
		this.workflowdemoMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(WorkFlowDemo et){
        this.workflowdemoMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(WorkFlowDemo et){
		return et.getWorkflowdemoid();
    }
    @Override
    public boolean getDraft(WorkFlowDemo et)  {
            return true;
    }







    @Override
	public List<WorkFlowDemo> listDefault(WorkFlowDemoSearchFilter filter) {
		return workflowdemoMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<WorkFlowDemo> searchDefault(WorkFlowDemoSearchFilter filter) {
		return workflowdemoMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return workflowdemoMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return workflowdemoMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return workflowdemoMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return workflowdemoMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<WorkFlowDemo> selectPermission(QueryWrapper<WorkFlowDemo> selectCond) {
        return workflowdemoMapper.selectPermission(new WorkFlowDemoSearchFilter().getPage(),selectCond);
    }

 }


