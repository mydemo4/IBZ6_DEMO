package com.ibz.demo.ysmk.logic.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ysmk.mapper.TjfxxMapper;
import com.ibz.demo.ysmk.domain.Tjfxx;
import com.ibz.demo.ysmk.service.TjfxxService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ysmk.service.dto.TjfxxSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import org.kie.api.runtime.KieSession;
import com.ibz.demo.ysmk.logic.TjfxxLogic;
import java.util.HashMap;
import org.kie.api.runtime.KieContainer;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.aspectj.lang.annotation.Aspect;
/**
 * 实体[Tjfxx] 处理逻辑接口实现
 */
@Service
public class TjfxxLogicImpl  implements  TjfxxLogic{


    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private com.ibz.demo.ysmk.service.TjfxxService iBzSysDefaultService;

	public boolean tianjiatishi(Tjfxx et){
           KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("tjfxxtianjiatishidefault",et);
           kieSession.setGlobal("iBzSysTjfxxDefaultService",iBzSysDefaultService);
           kieSession.startProcess("com.ibz.demo.ysmk.logic.tjfxxlogic.tianjiatishi");

        }catch(Exception e){
            throw new BadRequestAlertException("执行[tianjiatishi]处理逻辑发生异常"+e,"","");
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
            return true;
    }

}