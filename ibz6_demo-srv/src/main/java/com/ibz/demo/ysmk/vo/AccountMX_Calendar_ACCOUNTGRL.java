package com.ibz.demo.ysmk.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ibz.demo.ysmk.domain.AccountMX;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMX_Calendar_ACCOUNTGRL{

    //前台传入的开始时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(access=Access.WRITE_ONLY)
    private Timestamp srfbegintime;
    //前台传入的结束时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(access=Access.WRITE_ONLY)
    private Timestamp srfendtime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp begintime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp endtime;
    private String icon;
    private String hrefTarget;
    private String text;
    private String cls;
    private String type;
    private String srfkey;
    private String srfmajortext;
    private String iconCls;
    private String bkcolor;
    private String id;
    private String content;
    private String color;
    private String href;
    private String qtip;
    private boolean disabled;

    /**
     * 日历部件[报销]设置数据源查询条件
     * @param searchFilter
     * @return
     */
    public com.ibz.demo.ysmk.service.dto.AccountSearchFilter getAccountSearchFilter(AccountMX_Calendar_ACCOUNTGRL searchFilter) {
        com.ibz.demo.ysmk.service.dto.AccountSearchFilter filter=new com.ibz.demo.ysmk.service.dto.AccountSearchFilter();
        QueryWrapper<com.ibz.demo.ysmk.domain.Account> qw =filter.getSelectCond();
        qw.ge("createdate",srfbegintime);
        qw.le("updatedate",srfendtime);
        return filter;
    }
    /**
    * 日历部件[报销]将数据源结果填入list集合中
    * @param
    * @param records
    */
    public void addAccountToRecords(Page<com.ibz.demo.ysmk.domain.Account> accountSearchResult, List<AccountMX_Calendar_ACCOUNTGRL> records) {
        for(com.ibz.demo.ysmk.domain.Account source:accountSearchResult.getRecords()) {
        AccountMX_Calendar_ACCOUNTGRL target=new AccountMX_Calendar_ACCOUNTGRL();
        target.fromAccount(source);
        records.add(target);
        }
     }
    /**
    * 日历部件[报销]数据源结果vo对象转换
    * @param sourceEntity
    */
    public  void fromAccount(com.ibz.demo.ysmk.domain.Account sourceEntity)  {
        this.setEndtime(sourceEntity.getUpdatedate());
        this.setIcon("");
        this.setHrefTarget("");
        this.setText("");
        this.setCls("");
        this.setBegintime(sourceEntity.getCreatedate());
        this.setType("account");
        this.setSrfkey(sourceEntity.getAccountid());
        this.setSrfmajortext(sourceEntity.getAccountname());
        this.setIconCls("");
        this.setBkcolor("");
        this.setId(sourceEntity.getAccountid());
        this.setContent(sourceEntity.getAccountname());
        this.setColor("");
        this.setHref("");
        this.setQtip("");
        this.setDisabled(false);
    }
    /**
     * 日历部件[报销明细]设置数据源查询条件
     * @param searchFilter
     * @return
     */
    public com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter getAccountMXSearchFilter(AccountMX_Calendar_ACCOUNTGRL searchFilter) {
        com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter filter=new com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter();
        QueryWrapper<com.ibz.demo.ysmk.domain.AccountMX> qw =filter.getSelectCond();
        qw.ge("createdate",srfbegintime);
        qw.le("updatedate",srfendtime);
        return filter;
    }
    /**
    * 日历部件[报销明细]将数据源结果填入list集合中
    * @param
    * @param records
    */
    public void addAccountMXToRecords(Page<com.ibz.demo.ysmk.domain.AccountMX> accountmxSearchResult, List<AccountMX_Calendar_ACCOUNTGRL> records) {
        for(com.ibz.demo.ysmk.domain.AccountMX source:accountmxSearchResult.getRecords()) {
        AccountMX_Calendar_ACCOUNTGRL target=new AccountMX_Calendar_ACCOUNTGRL();
        target.fromAccountMX(source);
        records.add(target);
        }
     }
    /**
    * 日历部件[报销明细]数据源结果vo对象转换
    * @param sourceEntity
    */
    public  void fromAccountMX(com.ibz.demo.ysmk.domain.AccountMX sourceEntity)  {
        this.setEndtime(sourceEntity.getUpdatedate());
        this.setIcon("");
        this.setHrefTarget("");
        this.setText("");
        this.setCls("");
        this.setBegintime(sourceEntity.getCreatedate());
        this.setType("ACCOUNTMX");
        this.setSrfkey(sourceEntity.getAccountmxid());
        this.setSrfmajortext(sourceEntity.getAccountmxname());
        this.setIconCls("");
        this.setBkcolor("");
        this.setId(sourceEntity.getAccountmxid());
        this.setContent(sourceEntity.getAccountmxname());
        this.setColor("");
        this.setHref("");
        this.setQtip("");
        this.setDisabled(false);
    }
    /**
     * 日历部件[部门]设置数据源查询条件
     * @param searchFilter
     * @return
     */
    public com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter getDepartmentSearchFilter(AccountMX_Calendar_ACCOUNTGRL searchFilter) {
        com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter filter=new com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter();
        QueryWrapper<com.ibz.demo.ysmk.domain.Department> qw =filter.getSelectCond();
        qw.ge("createdate",srfbegintime);
        qw.le("updatedate",srfendtime);
        return filter;
    }
    /**
    * 日历部件[部门]将数据源结果填入list集合中
    * @param
    * @param records
    */
    public void addDepartmentToRecords(Page<com.ibz.demo.ysmk.domain.Department> departmentSearchResult, List<AccountMX_Calendar_ACCOUNTGRL> records) {
        for(com.ibz.demo.ysmk.domain.Department source:departmentSearchResult.getRecords()) {
        AccountMX_Calendar_ACCOUNTGRL target=new AccountMX_Calendar_ACCOUNTGRL();
        target.fromDepartment(source);
        records.add(target);
        }
     }
    /**
    * 日历部件[部门]数据源结果vo对象转换
    * @param sourceEntity
    */
    public  void fromDepartment(com.ibz.demo.ysmk.domain.Department sourceEntity)  {
        this.setEndtime(sourceEntity.getUpdatedate());
        this.setIcon("");
        this.setHrefTarget("");
        this.setText("");
        this.setCls("");
        this.setBegintime(sourceEntity.getCreatedate());
        this.setType("DEPARTMENT");
        this.setSrfkey(sourceEntity.getDepartmentid());
        this.setSrfmajortext(sourceEntity.getDepartmentname());
        this.setIconCls("");
        this.setBkcolor("");
        this.setId(sourceEntity.getDepartmentid());
        this.setContent(sourceEntity.getDepartmentname());
        this.setColor("");
        this.setHref("");
        this.setQtip("");
        this.setDisabled(false);
    }
    /**
    * 将全部数据源结果集封装成page对象
    * @param records
    * @return
    */
    public Page<AccountMX_Calendar_ACCOUNTGRL> addRecordsToPage(List<AccountMX_Calendar_ACCOUNTGRL> records) {
        Page<AccountMX_Calendar_ACCOUNTGRL> targetpage=new Page<AccountMX_Calendar_ACCOUNTGRL>(0,0,records.size(),false);
            targetpage.setAsc(null);
            targetpage.setDesc(null);
            targetpage.setOptimizeCountSql(false);
            targetpage.setRecords(records);
            return targetpage;
        }
}
