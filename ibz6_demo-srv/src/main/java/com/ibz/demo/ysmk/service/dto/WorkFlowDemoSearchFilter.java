package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.WorkFlowDemo;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkFlowDemoSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<WorkFlowDemo> selectCond;
	public WorkFlowDemoSearchFilter(){
		this.page =new Page<WorkFlowDemo>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<WorkFlowDemo>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("workflowdemoname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_workflowdemoname_like;//[工作流演示名称]
	public void setN_workflowdemoname_like(String n_workflowdemoname_like) {
        this.n_workflowdemoname_like = n_workflowdemoname_like;
        if(!StringUtils.isEmpty(this.n_workflowdemoname_like)){
            this.selectCond.like("workflowdemoname", n_workflowdemoname_like);
        }
    }
	private String n_lcstate_eq;//[业务流程状态]
	public void setN_lcstate_eq(String n_lcstate_eq) {
        this.n_lcstate_eq = n_lcstate_eq;
        if(!StringUtils.isEmpty(this.n_lcstate_eq)){
            this.selectCond.eq("lcstate", n_lcstate_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!StringUtils.isEmpty(this.n_wfstep_eq)){
            this.selectCond.eq("wfstep", n_wfstep_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
