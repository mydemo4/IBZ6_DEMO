package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMX_SearchForm_Default{

    private DataObj srfparentdata;

    private String n_accountmxname_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromAccountMXSearchFilter(AccountMXSearchFilter sourceSearchFilter)  {
        this.setN_accountmxname_like(sourceSearchFilter.getN_accountmxname_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  AccountMXSearchFilter toAccountMXSearchFilter()  {
        AccountMXSearchFilter targetSearchFilter=new AccountMXSearchFilter();
        if(targetSearchFilter.getN_accountmxname_like()==null)
            targetSearchFilter.setN_accountmxname_like(this.getN_accountmxname_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
