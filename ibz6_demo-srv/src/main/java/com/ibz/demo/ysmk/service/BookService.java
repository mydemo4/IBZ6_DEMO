package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Book;
import com.ibz.demo.ysmk.service.dto.BookSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Book] 服务对象接口
 */
public interface BookService extends IService<Book>{

	boolean getDraft(Book et);
	boolean update(Book et);
	Book get(Book et);
	boolean remove(Book et);
	boolean checkKey(Book et);
	boolean create(Book et);
    List<Book> listXYJ(BookSearchFilter filter) ;
	Page<Book> searchXYJ(BookSearchFilter filter);
    List<Book> listDefault(BookSearchFilter filter) ;
	Page<Book> searchDefault(BookSearchFilter filter);
	/**	 实体[Book] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
