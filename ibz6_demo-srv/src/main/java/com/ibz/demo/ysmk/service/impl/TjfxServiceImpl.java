
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.TjfxMapper;
import com.ibz.demo.ysmk.domain.Tjfx;
import com.ibz.demo.ysmk.service.TjfxService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.TjfxSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Tjfx] 服务对象接口实现
 */
@Service(value="TjfxServiceImpl")
public class TjfxServiceImpl extends ServiceImplBase
<TjfxMapper, Tjfx> implements TjfxService{


    @Resource
    private TjfxMapper tjfxMapper;

    @Override
    public boolean create(Tjfx et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Tjfx et){
        this.beforeUpdate(et);
		this.tjfxMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Tjfx et){
        this.tjfxMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Tjfx et){
		return et.getTjfxid();
    }
    @Override
    public boolean getDraft(Tjfx et)  {
            return true;
    }







    @Override
	public List<Tjfx> listDefault(TjfxSearchFilter filter) {
		return tjfxMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Tjfx> searchDefault(TjfxSearchFilter filter) {
		return tjfxMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return tjfxMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return tjfxMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return tjfxMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return tjfxMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Tjfx> selectPermission(QueryWrapper<Tjfx> selectCond) {
        return tjfxMapper.selectPermission(new TjfxSearchFilter().getPage(),selectCond);
    }

 }


