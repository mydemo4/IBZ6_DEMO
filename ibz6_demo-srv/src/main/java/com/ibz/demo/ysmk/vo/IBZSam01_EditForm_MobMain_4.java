package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.IBZSam01;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSam01_EditForm_MobMain_4{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String ibzsam01id;
    @JsonIgnore
    private String ibzsam01name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private Timestamp sj;
    @JsonIgnore
    private Integer sl;
    @JsonIgnore
    private Double sz;
    @JsonIgnore
    private Integer sf;
    @JsonIgnore
    private Double je;
    @JsonIgnore
    private String bz;
    @JsonIgnore
    private Integer mdxsz;
    @JsonIgnore
    private Integer dxsz;
    @JsonIgnore
    private String mdxtext;
    @JsonIgnore
    private String dxtext;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonIgnore
    private String pwd;
    @JsonIgnore
    private String mrowtext;
    @JsonIgnore
    private String dxxlb;
    @JsonIgnore
    private String exldx;
    @JsonIgnore
    private String mxldx;
    @JsonIgnore
    private String sjxz;
    @JsonIgnore
    private String dsjxz;
    @JsonIgnore
    private String timexz;
    @JsonIgnore
    private String kg;
    @JsonIgnore
    private String image;
    @JsonIgnore
    private String mimage;
    @JsonIgnore
    private String wjsc;
    @JsonIgnore
    private String mwjsc;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[实例实体01标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return ibzsam01id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String ibzsam01id){
        this.ibzsam01id = ibzsam01id;
    }
    @Size(min = 0, max = 200, message = "[实例实体01名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return ibzsam01name;
    }
    @Size(min = 0, max = 200, message = "[实例实体01名称]长度必须在[200]以内!")
    @JsonProperty(value = "ibzsam01name")
    public String getIbzsam01name(){
        return ibzsam01name;
    }
    @JsonProperty(value = "ibzsam01name")
    public void setIbzsam01name(String ibzsam01name){
        this.ibzsam01name = ibzsam01name;
    }
    @NotNull(message = "[时间]不允许为空!")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "sj")
    public Timestamp getSj(){
        return sj;
    }
    @JsonProperty(value = "sj")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSj(Timestamp sj){
        this.sj = sj;
    }
    @JsonProperty(value = "sl")
    public Integer getSl(){
        return sl;
    }
    @JsonProperty(value = "sl")
    public void setSl(Integer sl){
        this.sl = sl;
    }
    @JsonProperty(value = "sz")
    public Double getSz(){
        return sz;
    }
    @JsonProperty(value = "sz")
    public void setSz(Double sz){
        this.sz = sz;
    }
    @JsonProperty(value = "sf")
    public Integer getSf(){
        return sf;
    }
    @JsonProperty(value = "sf")
    public void setSf(Integer sf){
        this.sf = sf;
    }
    @JsonProperty(value = "je")
    public Double getJe(){
        return je;
    }
    @JsonProperty(value = "je")
    public void setJe(Double je){
        this.je = je;
    }
    @Size(min = 0, max = 1048576, message = "[备注]长度必须在[1048576]以内!")
    @JsonProperty(value = "bz")
    public String getBz(){
        if(StringUtils.isEmpty(bz)){
        if(isCreate()){
            this.bz=AuthenticationUser.getAuthenticationUser().getSessionParams().get("srfloginname");
        }
        }
        return bz;
    }
    @JsonProperty(value = "bz")
    public void setBz(String bz){
        this.bz = bz;
    }
    @JsonProperty(value = "mdxsz")
    public Integer getMdxsz(){
        return mdxsz;
    }
    @JsonProperty(value = "mdxsz")
    public void setMdxsz(Integer mdxsz){
        this.mdxsz = mdxsz;
    }
    @JsonProperty(value = "dxsz")
    public Integer getDxsz(){
        return dxsz;
    }
    @JsonProperty(value = "dxsz")
    public void setDxsz(Integer dxsz){
        this.dxsz = dxsz;
    }
    @Size(min = 0, max = 1000, message = "[多选文本]长度必须在[1000]以内!")
    @JsonProperty(value = "mdxtext")
    public String getMdxtext(){
        return mdxtext;
    }
    @JsonProperty(value = "mdxtext")
    public void setMdxtext(String mdxtext){
        this.mdxtext = mdxtext;
    }
    @Size(min = 0, max = 60, message = "[单选文本]长度必须在[60]以内!")
    @JsonProperty(value = "dxtext")
    public String getDxtext(){
        return dxtext;
    }
    @JsonProperty(value = "dxtext")
    public void setDxtext(String dxtext){
        this.dxtext = dxtext;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[编辑器密码框]长度必须在[100]以内!")
    @JsonProperty(value = "pwd")
    public String getPwd(){
        return pwd;
    }
    @JsonProperty(value = "pwd")
    public void setPwd(String pwd){
        this.pwd = pwd;
    }
    @Size(min = 0, max = 400, message = "[编辑器多行文本]长度必须在[400]以内!")
    @JsonProperty(value = "mrowtext")
    public String getMrowtext(){
        return mrowtext;
    }
    @JsonProperty(value = "mrowtext")
    public void setMrowtext(String mrowtext){
        this.mrowtext = mrowtext;
    }
    @Size(min = 0, max = 200, message = "[单选项列表]长度必须在[200]以内!")
    @JsonProperty(value = "dxxlb")
    public String getDxxlb(){
        return dxxlb;
    }
    @JsonProperty(value = "dxxlb")
    public void setDxxlb(String dxxlb){
        this.dxxlb = dxxlb;
    }
    @Size(min = 0, max = 100, message = "[编辑器下拉单选]长度必须在[100]以内!")
    @JsonProperty(value = "exldx")
    public String getExldx(){
        return exldx;
    }
    @JsonProperty(value = "exldx")
    public void setExldx(String exldx){
        this.exldx = exldx;
    }
    @Size(min = 0, max = 1000, message = "[编辑器下拉多选]长度必须在[1000]以内!")
    @JsonProperty(value = "mxldx")
    public String getMxldx(){
        return mxldx;
    }
    @JsonProperty(value = "mxldx")
    public void setMxldx(String mxldx){
        this.mxldx = mxldx;
    }
    @Size(min = 0, max = 100, message = "[编辑器数据选择]长度必须在[100]以内!")
    @JsonProperty(value = "sjxz")
    public String getSjxz(){
        return sjxz;
    }
    @JsonProperty(value = "sjxz")
    public void setSjxz(String sjxz){
        this.sjxz = sjxz;
    }
    @Size(min = 0, max = 500, message = "[编辑器多数据选择]长度必须在[500]以内!")
    @JsonProperty(value = "dsjxz")
    public String getDsjxz(){
        return dsjxz;
    }
    @JsonProperty(value = "dsjxz")
    public void setDsjxz(String dsjxz){
        this.dsjxz = dsjxz;
    }
    @Size(min = 0, max = 100, message = "[编辑器时间选择]长度必须在[100]以内!")
    @JsonProperty(value = "timexz")
    public String getTimexz(){
        return timexz;
    }
    @JsonProperty(value = "timexz")
    public void setTimexz(String timexz){
        this.timexz = timexz;
    }
    @Size(min = 0, max = 100, message = "[开关]长度必须在[100]以内!")
    @JsonProperty(value = "kg")
    public String getKg(){
        return kg;
    }
    @JsonProperty(value = "kg")
    public void setKg(String kg){
        this.kg = kg;
    }
    @Size(min = 0, max = 1000, message = "[编辑器图片]长度必须在[1000]以内!")
    @JsonProperty(value = "image")
    public String getImage(){
        return image;
    }
    @JsonProperty(value = "image")
    public void setImage(String image){
        this.image = image;
    }
    @Size(min = 0, max = 2000, message = "[编辑器图片多选]长度必须在[2000]以内!")
    @JsonProperty(value = "mimage")
    public String getMimage(){
        return mimage;
    }
    @JsonProperty(value = "mimage")
    public void setMimage(String mimage){
        this.mimage = mimage;
    }
    @Size(min = 0, max = 2000, message = "[编辑器文件上传]长度必须在[2000]以内!")
    @JsonProperty(value = "wjsc")
    public String getWjsc(){
        return wjsc;
    }
    @JsonProperty(value = "wjsc")
    public void setWjsc(String wjsc){
        this.wjsc = wjsc;
    }
    @Size(min = 0, max = 1048576, message = "[编辑器文件上传多选]长度必须在[1048576]以内!")
    @JsonProperty(value = "mwjsc")
    public String getMwjsc(){
        return mwjsc;
    }
    @JsonProperty(value = "mwjsc")
    public void setMwjsc(String mwjsc){
        this.mwjsc = mwjsc;
    }
    @Size(min = 0, max = 100, message = "[实例实体01标识]长度必须在[100]以内!")
    @JsonProperty(value = "ibzsam01id")
    public String getIbzsam01id(){
        return ibzsam01id;
    }
    @JsonProperty(value = "ibzsam01id")
    public void setIbzsam01id(String ibzsam01id){
        this.ibzsam01id = ibzsam01id;
    }
    public  void fromIBZSam01(IBZSam01 sourceEntity)  {
        this.fromIBZSam01(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromIBZSam01(IBZSam01 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(IBZSam01.class, IBZSam01_EditForm_MobMain_4.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  IBZSam01 toIBZSam01()  {
        IBZSam01 targetEntity =new IBZSam01();
        BeanCopier copier=BeanCopier.create(IBZSam01_EditForm_MobMain_4.class, IBZSam01.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
    /**
	 * 新建
	 * @param
	 * @return
	 */
    private boolean isCreate()
    {
    	if((StringUtils.isEmpty(this.srfuf))||this.srfuf.equals("0"))
	    	return true;
    	else
    	    return false;
    }
}
