package com.ibz.demo.ysmk.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ibz.demo.ysmk.domain.MEETING;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.alibaba.fastjson.JSONObject;
import java.util.Map;
import java.util.HashMap;
import org.springframework.cglib.beans.BeanMap;
import com.ibz.demo.ibizutil.chart.model.ChartAxisModel;
import com.ibz.demo.ibizutil.chart.model.ChartSeriesModel;
import com.ibz.demo.ibizutil.chart.model.ChartModelBase;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MEETING_Chart_Meetingline extends ChartModelBase{

    /**
     * 准备图表坐标轴模型
     * @throws Exception
     */
    protected void prepareChartAxisModels() {
        ChartAxisModel chartAxis0 = new ChartAxisModel();
        chartAxis0.setName("会议时间");
        chartAxis0.setCaption("");
        chartAxis0.setAxisType("category");
        chartAxis0.setAxisPos("bottom");
        this.registerChartAxisModel(chartAxis0);
        ChartAxisModel chartAxis1 = new ChartAxisModel();
        chartAxis1.setName("会议次数");
        chartAxis1.setCaption("");
        chartAxis1.setAxisType("numeric");
        chartAxis1.setAxisPos("left");
        this.registerChartAxisModel(chartAxis1);
    }
    /**
     * 准备图表序列模型
     * @throws Exception
     */
    protected void prepareChartSeriesModels() {
        ChartSeriesModel chartSeries0 = new ChartSeriesModel();
        chartSeries0.setName("line");
        chartSeries0.setCaption("会议柱状图");
        chartSeries0.setSeriesType("line");
        chartSeries0.setCatalogField("starttime");
        chartSeries0.setValueField("cs");
        chartSeries0.setTimeGroupMode("MONTH");
        this.registerChartSeriesModel(chartSeries0);
    }
    public Page<JSONObject> addRecordsToPage(JSONObject opt)  {
        Page<JSONObject> targetpage=new Page<JSONObject>();
        List<JSONObject> records=new ArrayList<JSONObject>();
        records.add(opt);
        targetpage.setAsc(null);
        targetpage.setDesc(null);
        targetpage.setOptimizeCountSql(false);
        targetpage.setRecords(records);
        return targetpage;
    }
    /**
    * 将entity转成map
    * @param searchResult
    * @return
    */
    public List<Map<String, Object>> pageToListDatas(Page<MEETING> searchResult) {
        if(searchResult==null)
            return null;
        List<Map<String, Object>> records=new ArrayList<>();
        for(MEETING source:searchResult.getRecords()) {
            MEETING_Chart_Meetingline target=new MEETING_Chart_Meetingline();
            records.add(target.beanToMap(source));
        }
        return records;
    }
    /**
    *将entity转成map
    * @param bean
    * @param <T>
    * @return
    */
    public <T> Map<String,Object> beanToMap(T bean){
        Map map =new HashMap<String,Object>();
        if(bean!=null){
            BeanMap beanMap=BeanMap.create(bean);
            for(Object obj:beanMap.keySet()){
                map.put(obj+"",beanMap.get(obj));
            }
        }
        return map;
    }
}
