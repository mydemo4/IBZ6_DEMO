package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[报表分类]
 */
@Component("IBZ6_DEMO_CodeList12CodeList")
public class CodeList12CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList12");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("REPORTFOLDER_1");
		item0.setId("REPORTFOLDER_1");
		item0.setText("经营性报表");
		item0.setLabel("经营性报表");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("REPORTFOLDER_1",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("REPORTFOLDER_2");
		item1.setId("REPORTFOLDER_2");
		item1.setText("财务报表");
		item1.setLabel("财务报表");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("REPORTFOLDER_2",item1);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}