package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.Book;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book_List_Mob{
    private String subtitle;
    private String num;
    private String content;
    private String srfmajortext;
    private String group;
    private String time;
    private String srfkey;
     public  void fromBook(Book sourceEntity)  {
	    this.setSubtitle(String.format("%s",sourceEntity.getCreateman()));
	    this.setNum(String.format("%s",sourceEntity.getNumber()));
	    this.setContent(String.format("%s",sourceEntity.getCreateman()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getBookname()));
	    this.setGroup(String.format("%s",sourceEntity.getBuy()));
	    this.setTime(String.format("%s",sourceEntity.getCreatedate()));
	    this.setSrfkey(String.format("%s",sourceEntity.getBookid()));
	}
	public static Page<Book_List_Mob> fromBook(Page<Book> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<Book_List_Mob> targetpage=new Page<Book_List_Mob>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<Book_List_Mob> records=new ArrayList<Book_List_Mob>();
        for(Book source:sourcePage.getRecords()) {
    Book_List_Mob target=new Book_List_Mob();
            target.fromBook(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
