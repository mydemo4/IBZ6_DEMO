package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[年份（2010～2020）]
 */
@Component("IBZ6_DEMO_CodeList81CodeList")
public class CodeList81CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList81");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("2010");
		item0.setId("2010");
		item0.setText("2010年");
		item0.setLabel("2010年");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("2010",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("2011");
		item1.setId("2011");
		item1.setText("2011年");
		item1.setLabel("2011年");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("2011",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("2012");
		item2.setId("2012");
		item2.setText("2012年");
		item2.setLabel("2012年");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("2012",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("2013");
		item3.setId("2013");
		item3.setText("2013年");
		item3.setLabel("2013年");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("2013",item3);
		CodeItem item4 = new CodeItem();
		item4.setValue("2014");
		item4.setId("2014");
		item4.setText("2014年");
		item4.setLabel("2014年");
		codeItemList.add(item4);
		codeList.getCodeItemModelMap().put("2014",item4);
		CodeItem item5 = new CodeItem();
		item5.setValue("2015");
		item5.setId("2015");
		item5.setText("2015年");
		item5.setLabel("2015年");
		codeItemList.add(item5);
		codeList.getCodeItemModelMap().put("2015",item5);
		CodeItem item6 = new CodeItem();
		item6.setValue("2016");
		item6.setId("2016");
		item6.setText("2016年");
		item6.setLabel("2016年");
		codeItemList.add(item6);
		codeList.getCodeItemModelMap().put("2016",item6);
		CodeItem item7 = new CodeItem();
		item7.setValue("2017");
		item7.setId("2017");
		item7.setText("2017年");
		item7.setLabel("2017年");
		codeItemList.add(item7);
		codeList.getCodeItemModelMap().put("2017",item7);
		CodeItem item8 = new CodeItem();
		item8.setValue("2018");
		item8.setId("2018");
		item8.setText("2018年");
		item8.setLabel("2018年");
		codeItemList.add(item8);
		codeList.getCodeItemModelMap().put("2018",item8);
		CodeItem item9 = new CodeItem();
		item9.setValue("2019");
		item9.setId("2019");
		item9.setText("2019年");
		item9.setLabel("2019年");
		codeItemList.add(item9);
		codeList.getCodeItemModelMap().put("2019",item9);
		CodeItem item10 = new CodeItem();
		item10.setValue("2020");
		item10.setId("2020");
		item10.setText("2020年");
		item10.setLabel("2020年");
		codeItemList.add(item10);
		codeList.getCodeItemModelMap().put("2020",item10);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}