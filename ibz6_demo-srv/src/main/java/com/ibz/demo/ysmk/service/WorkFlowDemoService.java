package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.WorkFlowDemo;
import com.ibz.demo.ysmk.service.dto.WorkFlowDemoSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[WorkFlowDemo] 服务对象接口
 */
public interface WorkFlowDemoService extends IService<WorkFlowDemo>{

	boolean getDraft(WorkFlowDemo et);
	boolean create(WorkFlowDemo et);
	WorkFlowDemo get(WorkFlowDemo et);
	boolean update(WorkFlowDemo et);
	boolean checkKey(WorkFlowDemo et);
	boolean remove(WorkFlowDemo et);
    List<WorkFlowDemo> listDefault(WorkFlowDemoSearchFilter filter) ;
	Page<WorkFlowDemo> searchDefault(WorkFlowDemoSearchFilter filter);
	/**	 实体[WorkFlowDemo] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
