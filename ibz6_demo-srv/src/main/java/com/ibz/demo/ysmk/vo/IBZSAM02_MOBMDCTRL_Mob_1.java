package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.IBZSAM02;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSAM02_MOBMDCTRL_Mob_1{
    private String num;
    private String subtitle;
    private String time;
    private String content;
    private String srfkey;
    private String srfmajortext;
    private String ibzsam01id;
     public  void fromIBZSAM02(IBZSAM02 sourceEntity)  {
	    this.setNum(String.format("%s",sourceEntity.getJe()));
	    this.setSubtitle(String.format("%s",sourceEntity.getCreateman()));
	    this.setTime(String.format("%s",sourceEntity.getCreatedate()));
	    this.setContent(String.format("%s",sourceEntity.getIbzsam01name()));
	    this.setSrfkey(String.format("%s",sourceEntity.getIbzsam02id()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getIbzsam02name()));
	    this.setIbzsam01id(String.format("%s",sourceEntity.getIbzsam01id()));
	}
	public static Page<IBZSAM02_MOBMDCTRL_Mob_1> fromIBZSAM02(Page<IBZSAM02> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<IBZSAM02_MOBMDCTRL_Mob_1> targetpage=new Page<IBZSAM02_MOBMDCTRL_Mob_1>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<IBZSAM02_MOBMDCTRL_Mob_1> records=new ArrayList<IBZSAM02_MOBMDCTRL_Mob_1>();
        for(IBZSAM02 source:sourcePage.getRecords()) {
    IBZSAM02_MOBMDCTRL_Mob_1 target=new IBZSAM02_MOBMDCTRL_Mob_1();
            target.fromIBZSAM02(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
