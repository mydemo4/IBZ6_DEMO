package com.ibz.demo.ysmk.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface AccountMXMapper extends BaseMapper<AccountMX>{

    List<AccountMX> searchDefault(@Param("srf") AccountMXSearchFilter searchfilter,@Param("ew") Wrapper<AccountMX> wrapper) ;
    Page<AccountMX> searchDefault(IPage page, @Param("srf") AccountMXSearchFilter searchfilter, @Param("ew") Wrapper<AccountMX> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'AccountMX:'+#p0")
    AccountMX selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'AccountMX:'+#p0.accountmxid")
    int insert(AccountMX entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'AccountMX:'+#p0.accountmxid")
    int updateById(@Param(Constants.ENTITY) AccountMX entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'AccountMX:'+#p0")
    int deleteById(Serializable id);
    Page<AccountMX> selectPermission(IPage page,@Param("pw") Wrapper<AccountMX> wrapper) ;

    //直接sql查询。
    List<AccountMX> executeRawSql(@Param("sql")String sql);
}
