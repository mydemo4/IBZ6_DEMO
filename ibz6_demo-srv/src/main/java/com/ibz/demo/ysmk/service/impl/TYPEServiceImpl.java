
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.TYPEMapper;
import com.ibz.demo.ysmk.domain.TYPE;
import com.ibz.demo.ysmk.service.TYPEService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.TYPESearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[TYPE] 服务对象接口实现
 */
@Service(value="TYPEServiceImpl")
public class TYPEServiceImpl extends ServiceImplBase
<TYPEMapper, TYPE> implements TYPEService{


    @Resource
    private TYPEMapper typeMapper;

    @Override
    public boolean create(TYPE et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(TYPE et){
        this.beforeUpdate(et);
		this.typeMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(TYPE et){
        this.typeMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(TYPE et){
		return et.getTypeid();
    }
    @Override
    public boolean getDraft(TYPE et)  {
            return true;
    }







    @Override
	public List<TYPE> listDefault(TYPESearchFilter filter) {
		return typeMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<TYPE> searchDefault(TYPESearchFilter filter) {
		return typeMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return typeMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return typeMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return typeMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return typeMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<TYPE> selectPermission(QueryWrapper<TYPE> selectCond) {
        return typeMapper.selectPermission(new TYPESearchFilter().getPage(),selectCond);
    }

 }


