
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.BookMapper;
import com.ibz.demo.ysmk.domain.Book;
import com.ibz.demo.ysmk.service.BookService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.BookSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Book] 服务对象接口实现
 */
@Service(value="BookServiceImpl")
public class BookServiceImpl extends ServiceImplBase
<BookMapper, Book> implements BookService{


    @Resource
    private BookMapper bookMapper;

    @Override
    public boolean create(Book et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Book et){
        this.beforeUpdate(et);
		this.bookMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Book et){
        this.bookMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Book et){
		return et.getBookid();
    }
    @Override
    public boolean getDraft(Book et)  {
            return true;
    }







    @Override
	public List<Book> listXYJ(BookSearchFilter filter) {
		return bookMapper.searchXYJ(filter,filter.getSelectCond());
	}
    @Override
	public Page<Book> searchXYJ(BookSearchFilter filter) {
		return bookMapper.searchXYJ(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Book> listDefault(BookSearchFilter filter) {
		return bookMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Book> searchDefault(BookSearchFilter filter) {
		return bookMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return bookMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return bookMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return bookMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return bookMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Book> selectPermission(QueryWrapper<Book> selectCond) {
        return bookMapper.selectPermission(new BookSearchFilter().getPage(),selectCond);
    }

 }


