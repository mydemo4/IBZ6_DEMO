
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.AccountMXMapper;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.ibz.demo.ysmk.service.AccountMXService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.ysmk.service.AccountService;

/**
 * 实体[AccountMX] 服务对象接口实现
 */
@Service(value="AccountMXServiceImpl")
public class AccountMXServiceImpl extends ServiceImplBase
<AccountMXMapper, AccountMX> implements AccountMXService{


    @Autowired
    private AccountService accountService;
    @Resource
    private AccountMXMapper accountmxMapper;

    @Override
    public boolean create(AccountMX et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(AccountMX et){
        this.beforeUpdate(et);
		this.accountmxMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(AccountMX et){
        this.accountmxMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(AccountMX et){
		return et.getAccountmxid();
    }
    @Override
    public boolean getDraft(AccountMX et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getAccountid())) && et.getAccountname() == null){
                try {
                    et.setAccountname(accountService.getById(et.getAccountid()).getAccountname());
                }catch (Exception ex){}
            }
            return true;
    }







    @Override
	public List<AccountMX> listDefault(AccountMXSearchFilter filter) {
		return accountmxMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<AccountMX> searchDefault(AccountMXSearchFilter filter) {
		return accountmxMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return accountmxMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return accountmxMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return accountmxMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return accountmxMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<AccountMX> selectPermission(QueryWrapper<AccountMX> selectCond) {
        return accountmxMapper.selectPermission(new AccountMXSearchFilter().getPage(),selectCond);
    }

 }


