
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.AccountMapper;
import com.ibz.demo.ysmk.domain.Account;
import com.ibz.demo.ysmk.service.AccountService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.AccountSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Account] 服务对象接口实现
 */
@Service(value="AccountServiceImpl")
public class AccountServiceImpl extends ServiceImplBase
<AccountMapper, Account> implements AccountService{


    @Resource
    private AccountMapper accountMapper;

    @Override
    public boolean create(Account et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Account et){
        this.beforeUpdate(et);
		this.accountMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Account et){
        this.accountMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Account et){
		return et.getAccountid();
    }
    @Override
    public boolean getDraft(Account et)  {
            return true;
    }







    @Override
	public List<Account> listDefault(AccountSearchFilter filter) {
		return accountMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Account> searchDefault(AccountSearchFilter filter) {
		return accountMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return accountMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return accountMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return accountMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return accountMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Account> selectPermission(QueryWrapper<Account> selectCond) {
        return accountMapper.selectPermission(new AccountSearchFilter().getPage(),selectCond);
    }

 }


