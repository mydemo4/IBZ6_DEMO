package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[月周（1～5）]
 */
@Component("IBZ6_DEMO_CodeList86CodeList")
public class CodeList86CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList86");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue(1);
		item0.setId("1");
		item0.setText("1周");
		item0.setLabel("1周");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("1",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue(2);
		item1.setId("2");
		item1.setText("2周");
		item1.setLabel("2周");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("2",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue(4);
		item2.setId("4");
		item2.setText("3周");
		item2.setLabel("3周");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("4",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue(8);
		item3.setId("8");
		item3.setText("4周");
		item3.setLabel("4周");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("8",item3);
		CodeItem item4 = new CodeItem();
		item4.setValue(16);
		item4.setId("16");
		item4.setText("5周");
		item4.setLabel("5周");
		codeItemList.add(item4);
		codeList.getCodeItemModelMap().put("16",item4);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}