package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.LBT;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LBT_EditForm_MobMain{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String lbtid;
    @JsonIgnore
    private String lbtname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String title;
    @JsonIgnore
    private String type;
    @JsonIgnore
    private String priceaera;
    @JsonIgnore
    private String dq;
    @JsonIgnore
    private String message;
    @JsonIgnore
    private String image;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[图库标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return lbtid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String lbtid){
        this.lbtid = lbtid;
    }
    @Size(min = 0, max = 200, message = "[图库名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return lbtname;
    }
    @Size(min = 0, max = 200, message = "[图库名称]长度必须在[200]以内!")
    @JsonProperty(value = "lbtname")
    public String getLbtname(){
        return lbtname;
    }
    @JsonProperty(value = "lbtname")
    public void setLbtname(String lbtname){
        this.lbtname = lbtname;
    }
    @Size(min = 0, max = 100, message = "[标题]长度必须在[100]以内!")
    @JsonProperty(value = "title")
    public String getTitle(){
        return title;
    }
    @JsonProperty(value = "title")
    public void setTitle(String title){
        this.title = title;
    }
    @Size(min = 0, max = 60, message = "[类型]长度必须在[60]以内!")
    @JsonProperty(value = "type")
    public String getType(){
        return type;
    }
    @JsonProperty(value = "type")
    public void setType(String type){
        this.type = type;
    }
    @Size(min = 0, max = 200, message = "[价格区]长度必须在[200]以内!")
    @JsonProperty(value = "priceaera")
    public String getPriceaera(){
        return priceaera;
    }
    @JsonProperty(value = "priceaera")
    public void setPriceaera(String priceaera){
        this.priceaera = priceaera;
    }
    @Size(min = 0, max = 100, message = "[地区]长度必须在[100]以内!")
    @JsonProperty(value = "dq")
    public String getDq(){
        return dq;
    }
    @JsonProperty(value = "dq")
    public void setDq(String dq){
        this.dq = dq;
    }
    @Size(min = 0, max = 400, message = "[信息]长度必须在[400]以内!")
    @JsonProperty(value = "message")
    public String getMessage(){
        return message;
    }
    @JsonProperty(value = "message")
    public void setMessage(String message){
        this.message = message;
    }
    @Size(min = 0, max = 2000, message = "[图片]长度必须在[2000]以内!")
    @JsonProperty(value = "image")
    public String getImage(){
        return image;
    }
    @JsonProperty(value = "image")
    public void setImage(String image){
        this.image = image;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[图库标识]长度必须在[100]以内!")
    @JsonProperty(value = "lbtid")
    public String getLbtid(){
        return lbtid;
    }
    @JsonProperty(value = "lbtid")
    public void setLbtid(String lbtid){
        this.lbtid = lbtid;
    }
    public  void fromLBT(LBT sourceEntity)  {
        this.fromLBT(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromLBT(LBT sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(LBT.class, LBT_EditForm_MobMain.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  LBT toLBT()  {
        LBT targetEntity =new LBT();
        BeanCopier copier=BeanCopier.create(LBT_EditForm_MobMain.class, LBT.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
