package com.ibz.demo.ysmk.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.domain.TreeNode;
import com.ibz.demo.ibizutil.domain.TreeNodeFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
@RequestMapping("/ionic4/ctrl/ibzsam01treeviewtreeview")
public class IONIC4IBZSAM01TreeViewTreeViewController{

	/**
	 * 节点分隔符号
	 */
	public static final String TREENODE_SEPARATOR = ";";
	public static final String TREENODE_ROOT = "ROOT";
	public static final String TREENODE_SLSTBJ = "slstbj";
	public static final String TREENODE_PICTURE = "picture";
	public static final String TREENODE_RL = "rl";
	public static final String TREENODE_TEST2 = "test2";


	@GetMapping(value="getnodes")
	public ResponseEntity<List<TreeNode>> getNodes(@Validated String srfparentkey,String srfcat,String srfnodeid,String srfnodefilter){
			List<TreeNode> list = new ArrayList<TreeNode>();
			TreeNodeFilter filter = new TreeNodeFilter();
			if (!StringUtils.hasLength(srfnodeid) || srfnodeid.equals("#")) {
				srfnodeid = TREENODE_ROOT;
			}
			String strTreeNodeId = srfnodeid;
			String strRealNodeId = "";
			boolean bRootSelect = false;
			String strNodeType = null;
			// String strRootSelectNode = this.getWebContext().getPostValue("srfnodeselect");
			String strRootSelectNode = "";// this.getWebContext().getPostValue("srfnodeselect");
			if (strTreeNodeId.equals(TREENODE_ROOT)) {
				strNodeType = TREENODE_ROOT;
				//treeNode = this.getTreeModel().getRootTreeNodeModel();
				//strRealNodeId = treeNodeFetchContext.getCatalog();
				//bRootSelect = this.getTreeModel().isEnableRootSelect();
			} else {
				int nPos = strTreeNodeId.indexOf(TREENODE_SEPARATOR);
				if (nPos == -1) {
					throw new BadRequestAlertException("树节点[%1$s]标识无效","","");
				}

				strNodeType = strTreeNodeId.substring(0, nPos);
				strRealNodeId = strTreeNodeId.substring(nPos + 1);
			}
			filter.setSrfParentKey(srfparentkey);
			filter.setSrfCat(srfcat);
			filter.setSrfNodeFilter(srfnodefilter);
			filter.setRealNodeId(strRealNodeId);
			filter.setSrfNodeId(srfnodeid);
			filter.setNodeType(strNodeType);
			/**
			 * 分解节点标识
			 */
			String[] nodeid = strRealNodeId.split(TREENODE_SEPARATOR);
			for (int i = 0; i < nodeid.length; i++) {
				switch(i){
				case 0:
					filter.setNodeId(nodeid[0]);
					break;
				case 1:
					filter.setNodeId2(nodeid[1]);
					break;
				case 2:
					filter.setNodeId3(nodeid[2]);
					break;
				case 3:
					filter.setNodeId4(nodeid[3]);
					break;
				default:
					break;
				}
			}
			if(strNodeType.equals(TREENODE_ROOT)){
				fillRootNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_SLSTBJ)){
				fillSlstbjNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_PICTURE)){
				fillPictureNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_RL)){
				fillRlNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_TEST2)){
				fillTest2NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
				return ResponseEntity.ok().body(list);
	}

	/**
	* 填充 树视图节点[默认根节点]
	*/
	protected void fillRootNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("默认根节点");
		treeNode.setSrfmajortext("默认根节点");
		String strNodeId = "ROOT";
		treeNode.setSrfkey("root");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "root";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[默认根节点]子节点
	*/
	protected void fillRootNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充测试实体2
		fillTest2Nodes(filter,list);
		//填充日历
		fillRlNodes(filter,list);
		//填充图库
		fillPictureNodes(filter,list);
		//填充实例01编辑
		fillSlstbjNodes(filter,list);
	}

	/**
	* 填充 树视图节点[实例01编辑]
	*/
	protected void fillSlstbjNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("实例01编辑");
		treeNode.setSrfmajortext("实例01编辑");
		String strNodeId = "slstbj";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[实例01编辑]子节点
	*/
	protected void fillSlstbjNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[图库]
	*/
	protected void fillPictureNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("图库");
		treeNode.setSrfmajortext("图库");
		String strNodeId = "picture";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[图库]子节点
	*/
	protected void fillPictureNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[日历]
	*/
	protected void fillRlNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("日历");
		treeNode.setSrfmajortext("日历");
		String strNodeId = "rl";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[日历]子节点
	*/
	protected void fillRlNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[测试实体2]
	*/
	protected void fillTest2Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("测试实体2");
		treeNode.setSrfmajortext("测试实体2");
		String strNodeId = "test2";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[测试实体2]子节点
	*/
	protected void fillTest2NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

}
