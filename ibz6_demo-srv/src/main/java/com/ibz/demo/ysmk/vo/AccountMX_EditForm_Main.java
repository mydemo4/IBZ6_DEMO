package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMX_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String accountmxid;
    @JsonIgnore
    private String accountmxname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String accountname;
    @JsonIgnore
    private String accountid;
    @JsonIgnore
    private Timestamp bxsj;
    @JsonIgnore
    private Double je;
    @JsonIgnore
    private String use;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[报销明细标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return accountmxid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String accountmxid){
        this.accountmxid = accountmxid;
    }
    @Size(min = 0, max = 200, message = "[报销明细名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return accountmxname;
    }
    @Size(min = 0, max = 200, message = "[报销明细名称]长度必须在[200]以内!")
    @JsonProperty(value = "accountmxname")
    public String getAccountmxname(){
        return accountmxname;
    }
    @JsonProperty(value = "accountmxname")
    public void setAccountmxname(String accountmxname){
        this.accountmxname = accountmxname;
    }
    @Size(min = 0, max = 200, message = "[报销名称]长度必须在[200]以内!")
    @JsonProperty(value = "accountname")
    public String getAccountname(){
        return accountname;
    }
    @JsonProperty(value = "accountname")
    public void setAccountname(String accountname){
        this.accountname = accountname;
    }
    @Size(min = 0, max = 100, message = "[报销标识]长度必须在[100]以内!")
    @JsonProperty(value = "accountid")
    public String getAccountid(){
        //输出获取外键值方法-用于通过parentdata关系填充外键值id
        if(StringUtils.isEmpty(accountid) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_ACCOUNTMX_ACCOUNT_ACCOUNTID"))
                this.accountid=this.getSrfparentdata().getStringValue("srfparentkey");
        }
        return accountid;
    }
    @JsonProperty(value = "accountid")
    public void setAccountid(String accountid){
        this.accountid = accountid;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "bxsj")
    public Timestamp getBxsj(){
        return bxsj;
    }
    @JsonProperty(value = "bxsj")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setBxsj(Timestamp bxsj){
        this.bxsj = bxsj;
    }
    @JsonProperty(value = "je")
    public Double getJe(){
        return je;
    }
    @JsonProperty(value = "je")
    public void setJe(Double je){
        this.je = je;
    }
    @Size(min = 0, max = 2000, message = "[报销明细用途]长度必须在[2000]以内!")
    @JsonProperty(value = "use")
    public String getUse(){
        return use;
    }
    @JsonProperty(value = "use")
    public void setUse(String use){
        this.use = use;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[报销明细标识]长度必须在[100]以内!")
    @JsonProperty(value = "accountmxid")
    public String getAccountmxid(){
        return accountmxid;
    }
    @JsonProperty(value = "accountmxid")
    public void setAccountmxid(String accountmxid){
        this.accountmxid = accountmxid;
    }
    public  void fromAccountMX(AccountMX sourceEntity)  {
        this.fromAccountMX(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromAccountMX(AccountMX sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(AccountMX.class, AccountMX_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  AccountMX toAccountMX()  {
        AccountMX targetEntity =new AccountMX();
        BeanCopier copier=BeanCopier.create(AccountMX_EditForm_Main.class, AccountMX.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
