
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.DepartmentMapper;
import com.ibz.demo.ysmk.domain.Department;
import com.ibz.demo.ysmk.service.DepartmentService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Department] 服务对象接口实现
 */
@Service(value="DepartmentServiceImpl")
public class DepartmentServiceImpl extends ServiceImplBase
<DepartmentMapper, Department> implements DepartmentService{


    @Resource
    private DepartmentMapper departmentMapper;

    @Override
    public boolean create(Department et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Department et){
        this.beforeUpdate(et);
		this.departmentMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Department et){
        this.departmentMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Department et){
		return et.getDepartmentid();
    }
    @Override
    public boolean getDraft(Department et)  {
            return true;
    }







    @Override
	public List<Department> listEngineering(DepartmentSearchFilter filter) {
		return departmentMapper.searchEngineering(filter,filter.getSelectCond());
	}
    @Override
	public Page<Department> searchEngineering(DepartmentSearchFilter filter) {
		return departmentMapper.searchEngineering(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Department> listManagement(DepartmentSearchFilter filter) {
		return departmentMapper.searchManagement(filter,filter.getSelectCond());
	}
    @Override
	public Page<Department> searchManagement(DepartmentSearchFilter filter) {
		return departmentMapper.searchManagement(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Department> listDefault(DepartmentSearchFilter filter) {
		return departmentMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Department> searchDefault(DepartmentSearchFilter filter) {
		return departmentMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Department> listBusiness(DepartmentSearchFilter filter) {
		return departmentMapper.searchBusiness(filter,filter.getSelectCond());
	}
    @Override
	public Page<Department> searchBusiness(DepartmentSearchFilter filter) {
		return departmentMapper.searchBusiness(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return departmentMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return departmentMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return departmentMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return departmentMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Department> selectPermission(QueryWrapper<Department> selectCond) {
        return departmentMapper.selectPermission(new DepartmentSearchFilter().getPage(),selectCond);
    }

 }


