package com.ibz.demo.ysmk.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ibz.demo.ysmk.domain.MEETING;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MEETING_Calendar_MEETINGRL{

    //前台传入的开始时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(access=Access.WRITE_ONLY)
    private Timestamp srfbegintime;
    //前台传入的结束时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(access=Access.WRITE_ONLY)
    private Timestamp srfendtime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp begintime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp endtime;
    private String icon;
    private String hrefTarget;
    private String text;
    private String cls;
    private String type;
    private String srfkey;
    private String srfmajortext;
    private String iconCls;
    private String bkcolor;
    private String id;
    private String content;
    private String color;
    private String href;
    private String qtip;
    private boolean disabled;

    /**
     * 日历部件[会议]设置数据源查询条件
     * @param searchFilter
     * @return
     */
    public com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter getMEETINGSearchFilter(MEETING_Calendar_MEETINGRL searchFilter) {
        com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter filter=new com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter();
        QueryWrapper<com.ibz.demo.ysmk.domain.MEETING> qw =filter.getSelectCond();
        qw.ge("starttime",srfbegintime);
        qw.le("endtime",srfendtime);
        return filter;
    }
    /**
    * 日历部件[会议]将数据源结果填入list集合中
    * @param
    * @param records
    */
    public void addMEETINGToRecords(Page<com.ibz.demo.ysmk.domain.MEETING> meetingSearchResult, List<MEETING_Calendar_MEETINGRL> records) {
        for(com.ibz.demo.ysmk.domain.MEETING source:meetingSearchResult.getRecords()) {
        MEETING_Calendar_MEETINGRL target=new MEETING_Calendar_MEETINGRL();
        target.fromMEETING(source);
        records.add(target);
        }
     }
    /**
    * 日历部件[会议]数据源结果vo对象转换
    * @param sourceEntity
    */
    public  void fromMEETING(com.ibz.demo.ysmk.domain.MEETING sourceEntity)  {
        this.setEndtime(sourceEntity.getEndtime());
        this.setIcon("");
        this.setHrefTarget("");
        this.setText("");
        this.setCls("");
        this.setBegintime(sourceEntity.getStarttime());
        this.setType("MEETING");
        this.setSrfkey(sourceEntity.getMeetingid());
        this.setSrfmajortext(sourceEntity.getMeetingname());
        this.setIconCls("");
        this.setBkcolor("Azure");
        this.setId(sourceEntity.getMeetingid());
        this.setContent(sourceEntity.getMeetingname());
        this.setColor("Aqua");
        this.setHref("");
        this.setQtip("");
        this.setDisabled(false);
    }
    /**
     * 日历部件[图库]设置数据源查询条件
     * @param searchFilter
     * @return
     */
    public com.ibz.demo.ysmk.service.dto.LBTSearchFilter getLBTSearchFilter(MEETING_Calendar_MEETINGRL searchFilter) {
        com.ibz.demo.ysmk.service.dto.LBTSearchFilter filter=new com.ibz.demo.ysmk.service.dto.LBTSearchFilter();
        QueryWrapper<com.ibz.demo.ysmk.domain.LBT> qw =filter.getSelectCond();
        qw.ge("createdate",srfbegintime);
        qw.le("updatedate",srfendtime);
        return filter;
    }
    /**
    * 日历部件[图库]将数据源结果填入list集合中
    * @param
    * @param records
    */
    public void addLBTToRecords(Page<com.ibz.demo.ysmk.domain.LBT> lbtSearchResult, List<MEETING_Calendar_MEETINGRL> records) {
        for(com.ibz.demo.ysmk.domain.LBT source:lbtSearchResult.getRecords()) {
        MEETING_Calendar_MEETINGRL target=new MEETING_Calendar_MEETINGRL();
        target.fromLBT(source);
        records.add(target);
        }
     }
    /**
    * 日历部件[图库]数据源结果vo对象转换
    * @param sourceEntity
    */
    public  void fromLBT(com.ibz.demo.ysmk.domain.LBT sourceEntity)  {
        this.setEndtime(sourceEntity.getUpdatedate());
        this.setIcon("");
        this.setHrefTarget("");
        this.setText("");
        this.setCls("");
        this.setBegintime(sourceEntity.getCreatedate());
        this.setType("LBT");
        this.setSrfkey(sourceEntity.getLbtid());
        this.setSrfmajortext(sourceEntity.getLbtname());
        this.setIconCls("");
        this.setBkcolor("FireBrick");
        this.setId(sourceEntity.getLbtid());
        this.setContent(sourceEntity.getLbtname());
        this.setColor("Bisque");
        this.setHref("");
        this.setQtip("");
        this.setDisabled(false);
    }
    /**
    * 将全部数据源结果集封装成page对象
    * @param records
    * @return
    */
    public Page<MEETING_Calendar_MEETINGRL> addRecordsToPage(List<MEETING_Calendar_MEETINGRL> records) {
        Page<MEETING_Calendar_MEETINGRL> targetpage=new Page<MEETING_Calendar_MEETINGRL>(0,0,records.size(),false);
            targetpage.setAsc(null);
            targetpage.setDesc(null);
            targetpage.setOptimizeCountSql(false);
            targetpage.setRecords(records);
            return targetpage;
        }
}
