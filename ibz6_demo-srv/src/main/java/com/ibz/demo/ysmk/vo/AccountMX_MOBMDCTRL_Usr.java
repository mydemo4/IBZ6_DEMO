package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMX_MOBMDCTRL_Usr{
     public  void fromAccountMX(AccountMX sourceEntity)  {
	}
	public static Page<AccountMX_MOBMDCTRL_Usr> fromAccountMX(Page<AccountMX> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<AccountMX_MOBMDCTRL_Usr> targetpage=new Page<AccountMX_MOBMDCTRL_Usr>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<AccountMX_MOBMDCTRL_Usr> records=new ArrayList<AccountMX_MOBMDCTRL_Usr>();
        for(AccountMX source:sourcePage.getRecords()) {
    AccountMX_MOBMDCTRL_Usr target=new AccountMX_MOBMDCTRL_Usr();
            target.fromAccountMX(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
