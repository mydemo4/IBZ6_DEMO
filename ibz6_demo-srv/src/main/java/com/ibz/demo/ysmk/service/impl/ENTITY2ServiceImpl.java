
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.ENTITY2Mapper;
import com.ibz.demo.ysmk.domain.ENTITY2;
import com.ibz.demo.ysmk.service.ENTITY2Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.ENTITY2SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ENTITY2] 服务对象接口实现
 */
@Service(value="ENTITY2ServiceImpl")
public class ENTITY2ServiceImpl extends ServiceImplBase
<ENTITY2Mapper, ENTITY2> implements ENTITY2Service{


    @Resource
    private ENTITY2Mapper entity2Mapper;

    @Override
    public boolean create(ENTITY2 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(ENTITY2 et){
        this.beforeUpdate(et);
		this.entity2Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ENTITY2 et){
        this.entity2Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ENTITY2 et){
		return et.getEntity2id();
    }
    @Override
    public boolean getDraft(ENTITY2 et)  {
            return true;
    }







    @Override
	public List<ENTITY2> listDefault(ENTITY2SearchFilter filter) {
		return entity2Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ENTITY2> searchDefault(ENTITY2SearchFilter filter) {
		return entity2Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return entity2Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return entity2Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return entity2Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return entity2Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ENTITY2> selectPermission(QueryWrapper<ENTITY2> selectCond) {
        return entity2Mapper.selectPermission(new ENTITY2SearchFilter().getPage(),selectCond);
    }

 }


