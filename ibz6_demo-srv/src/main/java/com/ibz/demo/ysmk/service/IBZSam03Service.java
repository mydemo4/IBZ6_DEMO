package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.IBZSam03;
import com.ibz.demo.ysmk.service.dto.IBZSam03SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[IBZSam03] 服务对象接口
 */
public interface IBZSam03Service extends IService<IBZSam03>{

	boolean remove(IBZSam03 et);
	IBZSam03 get(IBZSam03 et);
	boolean getDraft(IBZSam03 et);
	boolean checkKey(IBZSam03 et);
	boolean update(IBZSam03 et);
	boolean create(IBZSam03 et);
    List<IBZSam03> listDefault(IBZSam03SearchFilter filter) ;
	Page<IBZSam03> searchDefault(IBZSam03SearchFilter filter);
	/**	 实体[IBZSam03] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
