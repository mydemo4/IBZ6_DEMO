package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

import com.ibz.demo.ysmk.service.MEETINGService;
/**
 * 代码表[会议]
 */
@Component("IBZ6_DEMO_MeetCodeList")
public class MeetCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Autowired
	private MEETINGService meetingService;

	protected MEETINGService getMEETINGService(){
		return meetingService;
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_Meet");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
	com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter();
        List<com.ibz.demo.ysmk.domain.MEETING> pageResult = this.meetingService.listDefault(searchFilter);
		for(com.ibz.demo.ysmk.domain.MEETING entity : pageResult) {
			CodeItem item = new CodeItem();
			String strId = entity.getMeetingid();
			item.setValue(strId);
			item.setId(strId);
			String strText = entity.getMeetingname();
			item.setText(strText);
			item.setLabel(strText);
			codeItemList.add(item);
			codeList.getCodeItemModelMap().put(strId,item);
		}
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}