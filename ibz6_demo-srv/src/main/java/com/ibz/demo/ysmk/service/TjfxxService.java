package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Tjfxx;
import com.ibz.demo.ysmk.service.dto.TjfxxSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Tjfxx] 服务对象接口
 */
public interface TjfxxService extends IService<Tjfxx>{

	boolean checkKey(Tjfxx et);
	boolean create(Tjfxx et);
	Tjfxx get(Tjfxx et);
	boolean update(Tjfxx et);
	boolean remove(Tjfxx et);
	boolean getDraft(Tjfxx et);
    List<Tjfxx> listDefault(TjfxxSearchFilter filter) ;
	Page<Tjfxx> searchDefault(TjfxxSearchFilter filter);
	/**	 实体[Tjfxx] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
