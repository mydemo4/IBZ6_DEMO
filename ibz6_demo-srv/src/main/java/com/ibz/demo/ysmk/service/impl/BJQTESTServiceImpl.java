
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.BJQTESTMapper;
import com.ibz.demo.ysmk.domain.BJQTEST;
import com.ibz.demo.ysmk.service.BJQTESTService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.BJQTESTSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[BJQTEST] 服务对象接口实现
 */
@Service(value="BJQTESTServiceImpl")
public class BJQTESTServiceImpl extends ServiceImplBase
<BJQTESTMapper, BJQTEST> implements BJQTESTService{


    @Resource
    private BJQTESTMapper bjqtestMapper;

    @Override
    public boolean create(BJQTEST et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(BJQTEST et){
        this.beforeUpdate(et);
		this.bjqtestMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(BJQTEST et){
        this.bjqtestMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(BJQTEST et){
		return et.getBjqtestid();
    }
    @Override
    public boolean getDraft(BJQTEST et)  {
            return true;
    }







    @Override
	public List<BJQTEST> listDefault(BJQTESTSearchFilter filter) {
		return bjqtestMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<BJQTEST> searchDefault(BJQTESTSearchFilter filter) {
		return bjqtestMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return bjqtestMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return bjqtestMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return bjqtestMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return bjqtestMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<BJQTEST> selectPermission(QueryWrapper<BJQTEST> selectCond) {
        return bjqtestMapper.selectPermission(new BJQTESTSearchFilter().getPage(),selectCond);
    }

 }


