package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.BJQTEST;
import com.ibz.demo.ysmk.service.dto.BJQTESTSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[BJQTEST] 服务对象接口
 */
public interface BJQTESTService extends IService<BJQTEST>{

	boolean remove(BJQTEST et);
	boolean create(BJQTEST et);
	boolean update(BJQTEST et);
	BJQTEST get(BJQTEST et);
	boolean checkKey(BJQTEST et);
	boolean getDraft(BJQTEST et);
    List<BJQTEST> listDefault(BJQTESTSearchFilter filter) ;
	Page<BJQTEST> searchDefault(BJQTESTSearchFilter filter);
	/**	 实体[BJQTEST] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
