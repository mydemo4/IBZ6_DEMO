package com.ibz.demo.ysmk.logic;

import com.ibz.demo.ysmk.domain.IBZSAM02;

/**
 * 实体[IBZSAM02] 处理逻辑接口
 */
public interface IBZSAM02Logic {

	boolean updateJE(IBZSAM02 et);

 }