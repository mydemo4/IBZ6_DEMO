package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MEETING_SearchForm_MobDef{

    private DataObj srfparentdata;

    private String n_meetingname_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromMEETINGSearchFilter(MEETINGSearchFilter sourceSearchFilter)  {
        this.setN_meetingname_like(sourceSearchFilter.getN_meetingname_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  MEETINGSearchFilter toMEETINGSearchFilter()  {
        MEETINGSearchFilter targetSearchFilter=new MEETINGSearchFilter();
        if(targetSearchFilter.getN_meetingname_like()==null)
            targetSearchFilter.setN_meetingname_like(this.getN_meetingname_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
