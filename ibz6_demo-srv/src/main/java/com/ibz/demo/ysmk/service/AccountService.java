package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Account;
import com.ibz.demo.ysmk.service.dto.AccountSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Account] 服务对象接口
 */
public interface AccountService extends IService<Account>{

	boolean checkKey(Account et);
	boolean remove(Account et);
	boolean getDraft(Account et);
	boolean create(Account et);
	Account get(Account et);
	boolean update(Account et);
    List<Account> listDefault(AccountSearchFilter filter) ;
	Page<Account> searchDefault(AccountSearchFilter filter);
	/**	 实体[Account] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
