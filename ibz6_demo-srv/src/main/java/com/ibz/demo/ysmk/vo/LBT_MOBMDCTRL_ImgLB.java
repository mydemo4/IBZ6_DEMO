package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.LBT;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LBT_MOBMDCTRL_ImgLB{
    private String image;
    private String srfmajortext;
    private String buttoncaption;
    private String content;
    private String srfkey;
     public  void fromLBT(LBT sourceEntity)  {
	    this.setImage(String.format("%s",sourceEntity.getImage()));
	    this.setButtoncaption(String.format("%s",sourceEntity.getTitle()));
	    this.setSrfkey(String.format("%s",sourceEntity.getLbtid()));
	}
	public static Page<LBT_MOBMDCTRL_ImgLB> fromLBT(Page<LBT> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<LBT_MOBMDCTRL_ImgLB> targetpage=new Page<LBT_MOBMDCTRL_ImgLB>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<LBT_MOBMDCTRL_ImgLB> records=new ArrayList<LBT_MOBMDCTRL_ImgLB>();
        for(LBT source:sourcePage.getRecords()) {
    LBT_MOBMDCTRL_ImgLB target=new LBT_MOBMDCTRL_ImgLB();
            target.fromLBT(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
