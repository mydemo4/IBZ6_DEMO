package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.AccountMX;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountMXSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<AccountMX> selectCond;
	public AccountMXSearchFilter(){
		this.page =new Page<AccountMX>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<AccountMX>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_ACCOUNTMX_ACCOUNT_ACCOUNTID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_accountid_eq("NA");
            } else {
                this.setN_accountid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("accountmxname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_accountmxname_like;//[报销明细名称]
	public void setN_accountmxname_like(String n_accountmxname_like) {
        this.n_accountmxname_like = n_accountmxname_like;
        if(!StringUtils.isEmpty(this.n_accountmxname_like)){
            this.selectCond.like("accountmxname", n_accountmxname_like);
        }
    }
	private String n_accountid_eq;//[报销标识]
	public void setN_accountid_eq(String n_accountid_eq) {
        this.n_accountid_eq = n_accountid_eq;
        if(!StringUtils.isEmpty(this.n_accountid_eq)){
            this.selectCond.eq("accountid", n_accountid_eq);
        }
    }
	private String n_accountname_eq;//[报销名称]
	public void setN_accountname_eq(String n_accountname_eq) {
        this.n_accountname_eq = n_accountname_eq;
        if(!StringUtils.isEmpty(this.n_accountname_eq)){
            this.selectCond.eq("accountname", n_accountname_eq);
        }
    }
	private String n_accountname_like;//[报销名称]
	public void setN_accountname_like(String n_accountname_like) {
        this.n_accountname_like = n_accountname_like;
        if(!StringUtils.isEmpty(this.n_accountname_like)){
            this.selectCond.like("accountname", n_accountname_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
