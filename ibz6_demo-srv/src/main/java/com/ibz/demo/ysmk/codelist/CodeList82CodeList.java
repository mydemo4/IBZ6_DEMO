package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[月份（1～12）]
 */
@Component("IBZ6_DEMO_CodeList82CodeList")
public class CodeList82CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList82");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("01");
		item0.setId("01");
		item0.setText("1月");
		item0.setLabel("1月");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("01",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("02");
		item1.setId("02");
		item1.setText("2月");
		item1.setLabel("2月");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("02",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("03");
		item2.setId("03");
		item2.setText("3月");
		item2.setLabel("3月");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("03",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("04");
		item3.setId("04");
		item3.setText("4月");
		item3.setLabel("4月");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("04",item3);
		CodeItem item4 = new CodeItem();
		item4.setValue("05");
		item4.setId("05");
		item4.setText("5月");
		item4.setLabel("5月");
		codeItemList.add(item4);
		codeList.getCodeItemModelMap().put("05",item4);
		CodeItem item5 = new CodeItem();
		item5.setValue("06");
		item5.setId("06");
		item5.setText("6月");
		item5.setLabel("6月");
		codeItemList.add(item5);
		codeList.getCodeItemModelMap().put("06",item5);
		CodeItem item6 = new CodeItem();
		item6.setValue("07");
		item6.setId("07");
		item6.setText("7月");
		item6.setLabel("7月");
		codeItemList.add(item6);
		codeList.getCodeItemModelMap().put("07",item6);
		CodeItem item7 = new CodeItem();
		item7.setValue("08");
		item7.setId("08");
		item7.setText("8月");
		item7.setLabel("8月");
		codeItemList.add(item7);
		codeList.getCodeItemModelMap().put("08",item7);
		CodeItem item8 = new CodeItem();
		item8.setValue("09");
		item8.setId("09");
		item8.setText("9月");
		item8.setLabel("9月");
		codeItemList.add(item8);
		codeList.getCodeItemModelMap().put("09",item8);
		CodeItem item9 = new CodeItem();
		item9.setValue("10");
		item9.setId("10");
		item9.setText("10月");
		item9.setLabel("10月");
		codeItemList.add(item9);
		codeList.getCodeItemModelMap().put("10",item9);
		CodeItem item10 = new CodeItem();
		item10.setValue("11");
		item10.setId("11");
		item10.setText("11月");
		item10.setLabel("11月");
		codeItemList.add(item10);
		codeList.getCodeItemModelMap().put("11",item10);
		CodeItem item11 = new CodeItem();
		item11.setValue("12");
		item11.setId("12");
		item11.setText("12月");
		item11.setLabel("12月");
		codeItemList.add(item11);
		codeList.getCodeItemModelMap().put("12",item11);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}