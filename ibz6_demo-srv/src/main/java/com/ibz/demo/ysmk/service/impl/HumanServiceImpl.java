
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.HumanMapper;
import com.ibz.demo.ysmk.domain.Human;
import com.ibz.demo.ysmk.service.HumanService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.HumanSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Human] 服务对象接口实现
 */
@Service(value="HumanServiceImpl")
public class HumanServiceImpl extends ServiceImplBase
<HumanMapper, Human> implements HumanService{


    @Resource
    private HumanMapper humanMapper;

    @Override
    public boolean create(Human et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Human et){
        this.beforeUpdate(et);
		this.humanMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Human et){
        this.humanMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Human et){
		return et.getHumanid();
    }
    @Override
    public boolean getDraft(Human et)  {
            return true;
    }







    @Override
	public List<Human> listDefault(HumanSearchFilter filter) {
		return humanMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Human> searchDefault(HumanSearchFilter filter) {
		return humanMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return humanMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return humanMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return humanMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return humanMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Human> selectPermission(QueryWrapper<Human> selectCond) {
        return humanMapper.selectPermission(new HumanSearchFilter().getPage(),selectCond);
    }

 }


