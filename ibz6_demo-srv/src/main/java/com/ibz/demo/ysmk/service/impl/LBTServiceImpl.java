
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.LBTMapper;
import com.ibz.demo.ysmk.domain.LBT;
import com.ibz.demo.ysmk.service.LBTService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.LBTSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[LBT] 服务对象接口实现
 */
@Service(value="LBTServiceImpl")
public class LBTServiceImpl extends ServiceImplBase
<LBTMapper, LBT> implements LBTService{


    @Resource
    private LBTMapper lbtMapper;

    @Override
    public boolean create(LBT et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(LBT et){
        this.beforeUpdate(et);
		this.lbtMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(LBT et){
        this.lbtMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(LBT et){
		return et.getLbtid();
    }
    @Override
    public boolean getDraft(LBT et)  {
            return true;
    }







    @Override
	public List<LBT> listSC(LBTSearchFilter filter) {
		return lbtMapper.searchSC(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchSC(LBTSearchFilter filter) {
		return lbtMapper.searchSC(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listLToR(LBTSearchFilter filter) {
		return lbtMapper.searchLToR(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchLToR(LBTSearchFilter filter) {
		return lbtMapper.searchLToR(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listScroll(LBTSearchFilter filter) {
		return lbtMapper.searchScroll(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchScroll(LBTSearchFilter filter) {
		return lbtMapper.searchScroll(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listTitleImg(LBTSearchFilter filter) {
		return lbtMapper.searchTitleImg(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchTitleImg(LBTSearchFilter filter) {
		return lbtMapper.searchTitleImg(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listMDLB(LBTSearchFilter filter) {
		return lbtMapper.searchMDLB(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchMDLB(LBTSearchFilter filter) {
		return lbtMapper.searchMDLB(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listLToR2(LBTSearchFilter filter) {
		return lbtMapper.searchLToR2(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchLToR2(LBTSearchFilter filter) {
		return lbtMapper.searchLToR2(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listImgLB(LBTSearchFilter filter) {
		return lbtMapper.searchImgLB(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchImgLB(LBTSearchFilter filter) {
		return lbtMapper.searchImgLB(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listLB(LBTSearchFilter filter) {
		return lbtMapper.searchLB(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchLB(LBTSearchFilter filter) {
		return lbtMapper.searchLB(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listSSGroup(LBTSearchFilter filter) {
		return lbtMapper.searchSSGroup(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchSSGroup(LBTSearchFilter filter) {
		return lbtMapper.searchSSGroup(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<LBT> listDefault(LBTSearchFilter filter) {
		return lbtMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<LBT> searchDefault(LBTSearchFilter filter) {
		return lbtMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return lbtMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return lbtMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return lbtMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return lbtMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<LBT> selectPermission(QueryWrapper<LBT> selectCond) {
        return lbtMapper.selectPermission(new LBTSearchFilter().getPage(),selectCond);
    }

 }


