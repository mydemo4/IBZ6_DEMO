
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.TjMapper;
import com.ibz.demo.ysmk.domain.Tj;
import com.ibz.demo.ysmk.service.TjService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.TjSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Tj] 服务对象接口实现
 */
@Service(value="TjServiceImpl")
public class TjServiceImpl extends ServiceImplBase
<TjMapper, Tj> implements TjService{


    @Resource
    private TjMapper tjMapper;

    @Override
    public boolean create(Tj et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Tj et){
        this.beforeUpdate(et);
		this.tjMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Tj et){
        this.tjMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Tj et){
		return et.getTjid();
    }
    @Override
    public boolean getDraft(Tj et)  {
            return true;
    }







    @Override
	public List<Tj> listDefault(TjSearchFilter filter) {
		return tjMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Tj> searchDefault(TjSearchFilter filter) {
		return tjMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return tjMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return tjMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return tjMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return tjMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Tj> selectPermission(QueryWrapper<Tj> selectCond) {
        return tjMapper.selectPermission(new TjSearchFilter().getPage(),selectCond);
    }

 }


