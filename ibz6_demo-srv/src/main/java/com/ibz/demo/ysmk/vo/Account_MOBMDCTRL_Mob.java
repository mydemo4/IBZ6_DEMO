package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.Account;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account_MOBMDCTRL_Mob{
    private String time;
    private String srfkey;
    private String srfmajortext;
     public  void fromAccount(Account sourceEntity)  {
	    this.setTime(String.format("%s",sourceEntity.getCreatedate()));
	    this.setSrfkey(String.format("%s",sourceEntity.getAccountid()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getAccountname()));
	}
	public static Page<Account_MOBMDCTRL_Mob> fromAccount(Page<Account> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<Account_MOBMDCTRL_Mob> targetpage=new Page<Account_MOBMDCTRL_Mob>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<Account_MOBMDCTRL_Mob> records=new ArrayList<Account_MOBMDCTRL_Mob>();
        for(Account source:sourcePage.getRecords()) {
    Account_MOBMDCTRL_Mob target=new Account_MOBMDCTRL_Mob();
            target.fromAccount(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
