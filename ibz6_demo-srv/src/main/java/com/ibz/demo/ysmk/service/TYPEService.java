package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.TYPE;
import com.ibz.demo.ysmk.service.dto.TYPESearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[TYPE] 服务对象接口
 */
public interface TYPEService extends IService<TYPE>{

	boolean update(TYPE et);
	boolean checkKey(TYPE et);
	TYPE get(TYPE et);
	boolean create(TYPE et);
	boolean remove(TYPE et);
	boolean getDraft(TYPE et);
    List<TYPE> listDefault(TYPESearchFilter filter) ;
	Page<TYPE> searchDefault(TYPESearchFilter filter);
	/**	 实体[TYPE] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
