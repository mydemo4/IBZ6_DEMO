
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.TjfxxMapper;
import com.ibz.demo.ysmk.domain.Tjfxx;
import com.ibz.demo.ysmk.service.TjfxxService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.TjfxxSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.ysmk.service.TjfxService;

/**
 * 实体[Tjfxx] 服务对象接口实现
 */
@Service(value="TjfxxServiceImpl")
public class TjfxxServiceImpl extends ServiceImplBase
<TjfxxMapper, Tjfxx> implements TjfxxService{


    @Autowired
    private TjfxService tjfxService;
    @Resource
    private TjfxxMapper tjfxxMapper;

    @Override
    public boolean create(Tjfxx et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Tjfxx et){
        this.beforeUpdate(et);
		this.tjfxxMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Tjfxx et){
        this.tjfxxMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Tjfxx et){
		return et.getTjfxxid();
    }
    @Override
    public boolean getDraft(Tjfxx et)  {
            return true;
    }







    @Override
	public List<Tjfxx> listDefault(TjfxxSearchFilter filter) {
		return tjfxxMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Tjfxx> searchDefault(TjfxxSearchFilter filter) {
		return tjfxxMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return tjfxxMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return tjfxxMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return tjfxxMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return tjfxxMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Tjfxx> selectPermission(QueryWrapper<Tjfxx> selectCond) {
        return tjfxxMapper.selectPermission(new TjfxxSearchFilter().getPage(),selectCond);
    }

 }


