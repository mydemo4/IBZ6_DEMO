package com.ibz.demo.ysmk.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.domain.TreeNode;
import com.ibz.demo.ibizutil.domain.TreeNodeFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
@RequestMapping("/vue_mobile_r7/ctrl/usrtreeview")
public class Vue_Mobile_R7UsrTreeViewController{

	/**
	 * 节点分隔符号
	 */
	public static final String TREENODE_SEPARATOR = ";";
	public static final String TREENODE_TREE0301 = "tree0301";
	public static final String TREENODE_TREE0201 = "tree0201";
	public static final String TREENODE_ROOT = "ROOT";
	public static final String TREENODE_TREE0302 = "tree0302";


	@GetMapping(value="getnodes")
	public ResponseEntity<List<TreeNode>> getNodes(@Validated String srfparentkey,String srfcat,String srfnodeid,String srfnodefilter){
			List<TreeNode> list = new ArrayList<TreeNode>();
			TreeNodeFilter filter = new TreeNodeFilter();
			if (!StringUtils.hasLength(srfnodeid) || srfnodeid.equals("#")) {
				fillRootNodes(filter,list);
				return ResponseEntity.ok().body(list);
			}
			String strTreeNodeId = srfnodeid;
			String strRealNodeId = "";
			boolean bRootSelect = false;
			String strNodeType = null;
			// String strRootSelectNode = this.getWebContext().getPostValue("srfnodeselect");
			String strRootSelectNode = "";// this.getWebContext().getPostValue("srfnodeselect");
			if (strTreeNodeId.equals(TREENODE_ROOT)) {
				strNodeType = TREENODE_ROOT;
				//treeNode = this.getTreeModel().getRootTreeNodeModel();
				//strRealNodeId = treeNodeFetchContext.getCatalog();
				//bRootSelect = this.getTreeModel().isEnableRootSelect();
			} else {
				int nPos = strTreeNodeId.indexOf(TREENODE_SEPARATOR);
				if (nPos == -1) {
					throw new BadRequestAlertException("树节点[%1$s]标识无效","","");
				}

				strNodeType = strTreeNodeId.substring(0, nPos);
				strRealNodeId = strTreeNodeId.substring(nPos + 1);
			}
			filter.setSrfParentKey(srfparentkey);
			filter.setSrfCat(srfcat);
			filter.setSrfNodeFilter(srfnodefilter);
			filter.setRealNodeId(strRealNodeId);
			filter.setSrfNodeId(srfnodeid);
			filter.setNodeType(strNodeType);
			/**
			 * 分解节点标识
			 */
			String[] nodeid = strRealNodeId.split(TREENODE_SEPARATOR);
			for (int i = 0; i < nodeid.length; i++) {
				switch(i){
				case 0:
					filter.setNodeId(nodeid[0]);
					break;
				case 1:
					filter.setNodeId2(nodeid[1]);
					break;
				case 2:
					filter.setNodeId3(nodeid[2]);
					break;
				case 3:
					filter.setNodeId4(nodeid[3]);
					break;
				default:
					break;
				}
			}
			if(strNodeType.equals(TREENODE_TREE0301)){
				fillTree0301NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_TREE0201)){
				fillTree0201NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_ROOT)){
				fillRootNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_TREE0302)){
				fillTree0302NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
				return ResponseEntity.ok().body(list);
	}

	/**
	* 填充 树视图节点[树脖1]
	*/
	protected void fillTree0301Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("树脖1");
		treeNode.setSrfmajortext("树脖1");
		String strNodeId = "tree0301";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[树脖1]子节点
	*/
	protected void fillTree0301NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[树头_静态]
	*/
	protected void fillTree0201Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("树头_静态");
		treeNode.setSrfmajortext("树头_静态");
		String strNodeId = "tree0201";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[树头_静态]子节点
	*/
	protected void fillTree0201NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充树脖1
		fillTree0301Nodes(filter,list);
		//填充树脖02
		fillTree0302Nodes(filter,list);
	}

	/**
	* 填充 树视图节点[树顶]
	*/
	protected void fillRootNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("树顶");
		treeNode.setSrfmajortext("树顶");
		String strNodeId = "ROOT";
		treeNode.setSrfkey("root");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "root";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[树顶]子节点
	*/
	protected void fillRootNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充树头_静态
		fillTree0201Nodes(filter,list);
	}

	/**
	* 填充 树视图节点[树脖02]
	*/
	protected void fillTree0302Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("树脖02");
		treeNode.setSrfmajortext("树脖02");
		String strNodeId = "tree0302";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[树脖02]子节点
	*/
	protected void fillTree0302NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

}
