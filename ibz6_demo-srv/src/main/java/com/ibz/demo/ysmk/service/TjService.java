package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Tj;
import com.ibz.demo.ysmk.service.dto.TjSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Tj] 服务对象接口
 */
public interface TjService extends IService<Tj>{

	boolean remove(Tj et);
	boolean create(Tj et);
	boolean getDraft(Tj et);
	boolean checkKey(Tj et);
	boolean update(Tj et);
	Tj get(Tj et);
    List<Tj> listDefault(TjSearchFilter filter) ;
	Page<Tj> searchDefault(TjSearchFilter filter);
	/**	 实体[Tj] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
