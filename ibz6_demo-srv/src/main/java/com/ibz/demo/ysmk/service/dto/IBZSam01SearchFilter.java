package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.IBZSam01;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSam01SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<IBZSam01> selectCond;
	public IBZSam01SearchFilter(){
		this.page =new Page<IBZSam01>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<IBZSam01>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("ibzsam01name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_ibzsam01name_like;//[实例实体01名称]
	public void setN_ibzsam01name_like(String n_ibzsam01name_like) {
        this.n_ibzsam01name_like = n_ibzsam01name_like;
        if(!StringUtils.isEmpty(this.n_ibzsam01name_like)){
            this.selectCond.like("ibzsam01name", n_ibzsam01name_like);
        }
    }
	private String n_dxtext_eq;//[单选文本]
	public void setN_dxtext_eq(String n_dxtext_eq) {
        this.n_dxtext_eq = n_dxtext_eq;
        if(!StringUtils.isEmpty(this.n_dxtext_eq)){
            this.selectCond.eq("dxtext", n_dxtext_eq);
        }
    }
	private Integer n_dxsz_eq;//[单选数值]
	public void setN_dxsz_eq(Integer n_dxsz_eq) {
        this.n_dxsz_eq = n_dxsz_eq;
        if(!StringUtils.isEmpty(this.n_dxsz_eq)){
            this.selectCond.eq("dxsz", n_dxsz_eq);
        }
    }
	private String n_dxxlb_like;//[单选项列表]
	public void setN_dxxlb_like(String n_dxxlb_like) {
        this.n_dxxlb_like = n_dxxlb_like;
        if(!StringUtils.isEmpty(this.n_dxxlb_like)){
            this.selectCond.like("dxxlb", n_dxxlb_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
