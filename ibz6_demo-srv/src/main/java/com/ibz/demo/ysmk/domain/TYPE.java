package com.ibz.demo.ysmk.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.cglib.beans.BeanGenerator;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import java.util.Map;
import java.util.HashMap;
import org.springframework.util.StringUtils;
//import com.ibz.demo.ysmk.valuerule.anno.type.*;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import org.springframework.cglib.beans.BeanCopier;
import com.ibz.demo.ibizutil.annotation.Dict;
import com.ibz.demo.ibizutil.domain.EntityBase;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.enums.FillMode;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 实体[TYPE] 数据对象
 */
@TableName(value = "T_TYPES",resultMap = "TYPEResultMap")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TYPE extends EntityBase implements Serializable{

    @TableId(value= "typesid",type=IdType.UUID)//指定主键生成策略
    private String typeid;
    @TableField(value= "typesname")
    private String typename;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String createman;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createdate;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String updateman;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
     /**
     *    typeid :typesid
     *   当属性名称与代码名称不一致时，
     *   多发一套get属性名称方法给gridvo用
     */
    public String getTypesid(){
        return  typeid;
    }
     /**
     *    typeid :typesid
     *   关闭mp驼峰转换，当属性名称与代码名称不一致时，多发一套set用于值注入
     */
    public void setTypesid(String typesid){
        this.typeid=typesid;
    }
     /**
     *    typename :typesname
     *   当属性名称与代码名称不一致时，
     *   多发一套get属性名称方法给gridvo用
     */
    public String getTypesname(){
        return  typename;
    }
     /**
     *    typename :typesname
     *   关闭mp驼峰转换，当属性名称与代码名称不一致时，多发一套set用于值注入
     */
    public void setTypesname(String typesname){
        this.typename=typesname;
    }
     /**
     * 复制当前对象数据到目标对象(粘贴重置)
     *
     * @throws Exception
     */
    public TYPE copyTo(TYPE targetEntity)
	{
		BeanCopier copier=BeanCopier.create(TYPE.class, TYPE.class, false);
		copier.copy(this, targetEntity, null);
        targetEntity.setTypeid(null);
		return targetEntity;
	}
}
