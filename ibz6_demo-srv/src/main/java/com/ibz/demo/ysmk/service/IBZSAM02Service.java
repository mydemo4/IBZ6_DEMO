package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.IBZSAM02;
import com.ibz.demo.ysmk.service.dto.IBZSAM02SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[IBZSAM02] 服务对象接口
 */
public interface IBZSAM02Service extends IService<IBZSAM02>{

	boolean update(IBZSAM02 et);
	IBZSAM02 updateJE(IBZSAM02 et);
	boolean checkKey(IBZSAM02 et);
	boolean remove(IBZSAM02 et);
	IBZSAM02 get(IBZSAM02 et);
	boolean create(IBZSAM02 et);
	boolean getDraft(IBZSAM02 et);
    List<IBZSAM02> listDefault(IBZSAM02SearchFilter filter) ;
	Page<IBZSAM02> searchDefault(IBZSAM02SearchFilter filter);
	/**	 实体[IBZSAM02] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
