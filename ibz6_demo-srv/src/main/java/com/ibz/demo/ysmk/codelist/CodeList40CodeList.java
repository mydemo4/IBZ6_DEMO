package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[周期时间类型]
 */
@Component("IBZ6_DEMO_CodeList40CodeList")
public class CodeList40CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList40");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("MONTH");
		item0.setId("MONTH");
		item0.setText("月度");
		item0.setLabel("月度");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("MONTH",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("SEASON");
		item1.setId("SEASON");
		item1.setText("季度");
		item1.setLabel("季度");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("SEASON",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("WEEK");
		item2.setId("WEEK");
		item2.setText("周");
		item2.setLabel("周");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("WEEK",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("DAY");
		item3.setId("DAY");
		item3.setText("天");
		item3.setLabel("天");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("DAY",item3);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}