package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Tjfx;
import com.ibz.demo.ysmk.service.dto.TjfxSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Tjfx] 服务对象接口
 */
public interface TjfxService extends IService<Tjfx>{

	boolean getDraft(Tjfx et);
	boolean checkKey(Tjfx et);
	boolean update(Tjfx et);
	Tjfx get(Tjfx et);
	boolean remove(Tjfx et);
	boolean create(Tjfx et);
    List<Tjfx> listDefault(TjfxSearchFilter filter) ;
	Page<Tjfx> searchDefault(TjfxSearchFilter filter);
	/**	 实体[Tjfx] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
