package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.WorkFlowDemo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkFlowDemo_MOBMDCTRL_Mob{
    private String srfkey;
    private String srfmajortext;
    private String wfstep;
    private String lcstate;
    private String srfwfstep;
     public  void fromWorkFlowDemo(WorkFlowDemo sourceEntity)  {
	    this.setSrfkey(String.format("%s",sourceEntity.getWorkflowdemoid()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getWorkflowdemoname()));
	    this.setWfstep(String.format("%s",sourceEntity.getWfstep()));
	    this.setLcstate(String.format("%s",sourceEntity.getLcstate()));
	    this.setSrfwfstep(String.format("%s",sourceEntity.getWfstep()));
	}
	public static Page<WorkFlowDemo_MOBMDCTRL_Mob> fromWorkFlowDemo(Page<WorkFlowDemo> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<WorkFlowDemo_MOBMDCTRL_Mob> targetpage=new Page<WorkFlowDemo_MOBMDCTRL_Mob>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<WorkFlowDemo_MOBMDCTRL_Mob> records=new ArrayList<WorkFlowDemo_MOBMDCTRL_Mob>();
        for(WorkFlowDemo source:sourcePage.getRecords()) {
    WorkFlowDemo_MOBMDCTRL_Mob target=new WorkFlowDemo_MOBMDCTRL_Mob();
            target.fromWorkFlowDemo(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
