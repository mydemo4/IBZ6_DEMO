package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.AccountMX;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[AccountMX] 服务对象接口
 */
public interface AccountMXService extends IService<AccountMX>{

	AccountMX get(AccountMX et);
	boolean update(AccountMX et);
	boolean create(AccountMX et);
	boolean getDraft(AccountMX et);
	boolean remove(AccountMX et);
	boolean checkKey(AccountMX et);
    List<AccountMX> listDefault(AccountMXSearchFilter filter) ;
	Page<AccountMX> searchDefault(AccountMXSearchFilter filter);
	/**	 实体[AccountMX] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
