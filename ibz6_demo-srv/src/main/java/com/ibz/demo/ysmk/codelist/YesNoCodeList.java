package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[是否]
 */
@Component("IBZ6_DEMO_YesNoCodeList")
public class YesNoCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_YesNo");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("1");
		item0.setId("1");
		item0.setText("是");
		item0.setLabel("是");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("1",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("0");
		item1.setId("0");
		item1.setText("否");
		item1.setLabel("否");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("0",item1);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}