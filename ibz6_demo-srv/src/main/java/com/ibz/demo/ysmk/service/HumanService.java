package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Human;
import com.ibz.demo.ysmk.service.dto.HumanSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Human] 服务对象接口
 */
public interface HumanService extends IService<Human>{

	boolean checkKey(Human et);
	boolean update(Human et);
	boolean create(Human et);
	Human get(Human et);
	boolean remove(Human et);
	boolean getDraft(Human et);
    List<Human> listDefault(HumanSearchFilter filter) ;
	Page<Human> searchDefault(HumanSearchFilter filter);
	/**	 实体[Human] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
