package com.ibz.demo.ysmk.logic.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ysmk.mapper.IBZSAM02Mapper;
import com.ibz.demo.ysmk.domain.IBZSAM02;
import com.ibz.demo.ysmk.service.IBZSAM02Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ysmk.service.dto.IBZSAM02SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import org.kie.api.runtime.KieSession;
import com.ibz.demo.ysmk.logic.IBZSAM02Logic;
import java.util.HashMap;
import org.kie.api.runtime.KieContainer;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.aspectj.lang.annotation.Aspect;
/**
 * 实体[IBZSAM02] 处理逻辑接口实现
 */
@Service
public class IBZSAM02LogicImpl  implements  IBZSAM02Logic{


    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private com.ibz.demo.ysmk.service.IBZSam01Service ibzsam01service;
    @Autowired
    private com.ibz.demo.ysmk.service.IBZSAM02Service iBzSysDefaultService;

	public boolean updateJE(IBZSAM02 et){
           KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("ibzsam02updatejedefault",et);
           com.ibz.demo.ysmk.domain.IBZSam01  ibzsam02updatejeibzsam01 =new com.ibz.demo.ysmk.domain.IBZSam01();
           kieSession.insert(ibzsam02updatejeibzsam01); 
           kieSession.setGlobal("ibzsam02updatejeibzsam01",ibzsam02updatejeibzsam01);
           kieSession.setGlobal("ibzsam01service",ibzsam01service);
           kieSession.setGlobal("iBzSysIbzsam02DefaultService",iBzSysDefaultService);
           kieSession.startProcess("com.ibz.demo.ysmk.logic.ibzsam02logic.updateje");

        }catch(Exception e){
            throw new BadRequestAlertException("执行[更新Sam01金额]处理逻辑发生异常"+e,"","");
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
            return true;
    }

}