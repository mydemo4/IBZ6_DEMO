package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[图库类型]
 */
@Component("IBZ6_DEMO_ImageTypeCodeList")
public class ImageTypeCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_ImageType");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("5");
		item0.setId("5");
		item0.setText("轮播图");
		item0.setLabel("轮播图");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("5",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("10");
		item1.setId("10");
		item1.setText("左右滑动");
		item1.setLabel("左右滑动");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("10",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("15");
		item2.setId("15");
		item2.setText("左右滑动2");
		item2.setLabel("左右滑动2");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("15",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("20");
		item3.setId("20");
		item3.setText("商城样式列表");
		item3.setLabel("商城样式列表");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("20",item3);
		CodeItem item4 = new CodeItem();
		item4.setValue("30");
		item4.setId("30");
		item4.setText("含标题图片");
		item4.setLabel("含标题图片");
		codeItemList.add(item4);
		codeList.getCodeItemModelMap().put("30",item4);
		CodeItem item5 = new CodeItem();
		item5.setValue("40");
		item5.setId("40");
		item5.setText("分组收缩");
		item5.setLabel("分组收缩");
		codeItemList.add(item5);
		codeList.getCodeItemModelMap().put("40",item5);
		CodeItem item6 = new CodeItem();
		item6.setValue("50");
		item6.setId("50");
		item6.setText("锚点列表");
		item6.setLabel("锚点列表");
		codeItemList.add(item6);
		codeList.getCodeItemModelMap().put("50",item6);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}