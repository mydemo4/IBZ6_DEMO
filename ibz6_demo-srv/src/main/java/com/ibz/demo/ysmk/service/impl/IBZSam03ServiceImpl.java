
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.IBZSam03Mapper;
import com.ibz.demo.ysmk.domain.IBZSam03;
import com.ibz.demo.ysmk.service.IBZSam03Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.IBZSam03SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.ysmk.service.IBZSAM02Service;

/**
 * 实体[IBZSam03] 服务对象接口实现
 */
@Service(value="IBZSam03ServiceImpl")
public class IBZSam03ServiceImpl extends ServiceImplBase
<IBZSam03Mapper, IBZSam03> implements IBZSam03Service{


    @Autowired
    private IBZSAM02Service ibzsam02Service;
    @Resource
    private IBZSam03Mapper ibzsam03Mapper;

    @Override
    public boolean create(IBZSam03 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(IBZSam03 et){
        this.beforeUpdate(et);
		this.ibzsam03Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(IBZSam03 et){
        this.ibzsam03Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(IBZSam03 et){
		return et.getIbzsam03id();
    }
    @Override
    public boolean getDraft(IBZSam03 et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getIbzsam02id())) && et.getIbzsam02name() == null){
                try {
                    et.setIbzsam02name(ibzsam02Service.getById(et.getIbzsam02id()).getIbzsam02name());
                }catch (Exception ex){}
            }
            return true;
    }







    @Override
	public List<IBZSam03> listDefault(IBZSam03SearchFilter filter) {
		return ibzsam03Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<IBZSam03> searchDefault(IBZSam03SearchFilter filter) {
		return ibzsam03Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return ibzsam03Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return ibzsam03Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return ibzsam03Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return ibzsam03Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<IBZSam03> selectPermission(QueryWrapper<IBZSam03> selectCond) {
        return ibzsam03Mapper.selectPermission(new IBZSam03SearchFilter().getPage(),selectCond);
    }

 }


