package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[统一资源类型]
 */
@Component("IBZ6_DEMO_CodeList16CodeList")
public class CodeList16CodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeList16");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("PAGE");
		item0.setId("PAGE");
		item0.setText("内置页面");
		item0.setLabel("内置页面");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("PAGE",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("REPORT");
		item1.setId("REPORT");
		item1.setText("报表");
		item1.setLabel("报表");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("REPORT",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("CUSTOM");
		item2.setId("CUSTOM");
		item2.setText("自定义");
		item2.setLabel("自定义");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("CUSTOM",item2);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}