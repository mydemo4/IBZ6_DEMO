package com.ibz.demo.ysmk.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.domain.TreeNode;
import com.ibz.demo.ibizutil.domain.TreeNodeFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;

import com.ibz.demo.ysmk.service.LBTService;

@RestController
@RequestMapping("/ionic4/ctrl/imgtreetreeview")
public class IONIC4ImgTreeTreeViewController{

	/**
	 * 节点分隔符号
	 */
	public static final String TREENODE_SEPARATOR = ";";
	public static final String TREENODE_LTOR = "LToR";
	public static final String TREENODE_SC = "SC";
	public static final String TREENODE_ROOT = "ROOT";
	public static final String TREENODE_SCDATA = "SCData";
	public static final String TREENODE_LBTDATA = "LBTData";
	public static final String TREENODE_LBT = "LBT";
	public static final String TREENODE_LTORDATA = "LToRData";

	@Autowired
	private LBTService lbtService;

	protected LBTService getLBTService(){
		return lbtService;
	}

	@GetMapping(value="getnodes")
	public ResponseEntity<List<TreeNode>> getNodes(@Validated String srfparentkey,String srfcat,String srfnodeid,String srfnodefilter){
			List<TreeNode> list = new ArrayList<TreeNode>();
			TreeNodeFilter filter = new TreeNodeFilter();
			if (!StringUtils.hasLength(srfnodeid) || srfnodeid.equals("#")) {
				srfnodeid = TREENODE_ROOT;
			}
			String strTreeNodeId = srfnodeid;
			String strRealNodeId = "";
			boolean bRootSelect = false;
			String strNodeType = null;
			// String strRootSelectNode = this.getWebContext().getPostValue("srfnodeselect");
			String strRootSelectNode = "";// this.getWebContext().getPostValue("srfnodeselect");
			if (strTreeNodeId.equals(TREENODE_ROOT)) {
				strNodeType = TREENODE_ROOT;
				//treeNode = this.getTreeModel().getRootTreeNodeModel();
				//strRealNodeId = treeNodeFetchContext.getCatalog();
				//bRootSelect = this.getTreeModel().isEnableRootSelect();
			} else {
				int nPos = strTreeNodeId.indexOf(TREENODE_SEPARATOR);
				if (nPos == -1) {
					throw new BadRequestAlertException("树节点[%1$s]标识无效","","");
				}

				strNodeType = strTreeNodeId.substring(0, nPos);
				strRealNodeId = strTreeNodeId.substring(nPos + 1);
			}
			filter.setSrfParentKey(srfparentkey);
			filter.setSrfCat(srfcat);
			filter.setSrfNodeFilter(srfnodefilter);
			filter.setRealNodeId(strRealNodeId);
			filter.setSrfNodeId(srfnodeid);
			filter.setNodeType(strNodeType);
			/**
			 * 分解节点标识
			 */
			String[] nodeid = strRealNodeId.split(TREENODE_SEPARATOR);
			for (int i = 0; i < nodeid.length; i++) {
				switch(i){
				case 0:
					filter.setNodeId(nodeid[0]);
					break;
				case 1:
					filter.setNodeId2(nodeid[1]);
					break;
				case 2:
					filter.setNodeId3(nodeid[2]);
					break;
				case 3:
					filter.setNodeId4(nodeid[3]);
					break;
				default:
					break;
				}
			}
			if(strNodeType.equals(TREENODE_LTOR)){
				fillLtorNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_SC)){
				fillScNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_ROOT)){
				fillRootNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_SCDATA)){
				fillScdataNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_LBTDATA)){
				fillLbtdataNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_LBT)){
				fillLbtNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_LTORDATA)){
				fillLtordataNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
				return ResponseEntity.ok().body(list);
	}

	/**
	* 填充 树视图节点[左右滑动]
	*/
	protected void fillLtorNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("左右滑动");
		treeNode.setSrfmajortext("左右滑动");
		String strNodeId = "LToR";
		treeNode.setSrfkey("LToR");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "LToR";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[左右滑动]子节点
	*/
	protected void fillLtorNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充左右滑动数据
		fillLtordataNodes(filter,list);
	}

	/**
	* 填充 树视图节点[商城]
	*/
	protected void fillScNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("商城");
		treeNode.setSrfmajortext("商城");
		String strNodeId = "SC";
		treeNode.setSrfkey("SC");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "SC";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[商城]子节点
	*/
	protected void fillScNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充商城数据
		fillScdataNodes(filter,list);
	}

	/**
	* 填充 树视图节点[默认根节点]
	*/
	protected void fillRootNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("默认根节点");
		treeNode.setSrfmajortext("默认根节点");
		String strNodeId = "ROOT";
		treeNode.setSrfkey("root");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "root";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[默认根节点]子节点
	*/
	protected void fillRootNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充轮播图
		fillLbtNodes(filter,list);
		//填充左右滑动
		fillLtorNodes(filter,list);
		//填充商城
		fillScNodes(filter,list);
	}

	/**
	* 填充 树视图节点[商城数据]
	*/
	protected void fillScdataNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.LBTSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.LBTSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.LBT> pageResult = this.getLBTService().listSC(searchFilter);
		for(com.ibz.demo.ysmk.domain.LBT entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getLbtid();
			String strText = entity.getLbtname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "SCData";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[商城数据]子节点
	*/
	protected void fillScdataNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[轮播图数据]
	*/
	protected void fillLbtdataNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.LBTSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.LBTSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.LBT> pageResult = this.getLBTService().listLB(searchFilter);
		for(com.ibz.demo.ysmk.domain.LBT entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getLbtid();
			String strText = entity.getLbtname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "LBTData";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[轮播图数据]子节点
	*/
	protected void fillLbtdataNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[轮播图]
	*/
	protected void fillLbtNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("轮播图");
		treeNode.setSrfmajortext("轮播图");
		String strNodeId = "LBT";
		treeNode.setSrfkey("LBT");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "LBT";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[轮播图]子节点
	*/
	protected void fillLbtNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充轮播图数据
		fillLbtdataNodes(filter,list);
	}

	/**
	* 填充 树视图节点[左右滑动数据]
	*/
	protected void fillLtordataNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.LBTSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.LBTSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.LBT> pageResult = this.getLBTService().listLToR(searchFilter);
		for(com.ibz.demo.ysmk.domain.LBT entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getLbtid();
			String strText = entity.getLbtname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "LToRData";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[左右滑动数据]子节点
	*/
	protected void fillLtordataNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

}
