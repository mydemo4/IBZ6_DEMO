package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.User_zk;
import com.ibz.demo.ysmk.service.dto.User_zkSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[User_zk] 服务对象接口
 */
public interface User_zkService extends IService<User_zk>{

	boolean checkKey(User_zk et);
	User_zk get(User_zk et);
	boolean remove(User_zk et);
	boolean update(User_zk et);
	boolean create(User_zk et);
	boolean getDraft(User_zk et);
    List<User_zk> listDefault(User_zkSearchFilter filter) ;
	Page<User_zk> searchDefault(User_zkSearchFilter filter);
	/**	 实体[User_zk] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
