package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.Department;
import com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Department] 服务对象接口
 */
public interface DepartmentService extends IService<Department>{

	Department get(Department et);
	boolean update(Department et);
	boolean remove(Department et);
	boolean create(Department et);
	boolean getDraft(Department et);
	boolean checkKey(Department et);
    List<Department> listEngineering(DepartmentSearchFilter filter) ;
	Page<Department> searchEngineering(DepartmentSearchFilter filter);
    List<Department> listManagement(DepartmentSearchFilter filter) ;
	Page<Department> searchManagement(DepartmentSearchFilter filter);
    List<Department> listDefault(DepartmentSearchFilter filter) ;
	Page<Department> searchDefault(DepartmentSearchFilter filter);
    List<Department> listBusiness(DepartmentSearchFilter filter) ;
	Page<Department> searchBusiness(DepartmentSearchFilter filter);
	/**	 实体[Department] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
