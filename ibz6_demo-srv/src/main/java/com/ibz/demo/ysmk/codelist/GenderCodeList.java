package com.ibz.demo.ysmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[性别]
 */
@Component("IBZ6_DEMO_GenderCodeList")
public class GenderCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_Gender");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("3");
		item0.setId("3");
		item0.setText("不详");
		item0.setLabel("不详");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("3",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("2");
		item1.setId("2");
		item1.setText("女性");
		item1.setLabel("女性");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("2",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("1");
		item2.setId("1");
		item2.setText("男性");
		item2.setLabel("男性");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("1",item2);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}