package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.IBZSAM02;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSAM02_EditForm_MobMain{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String ibzsam02id;
    @JsonIgnore
    private String ibzsam02name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private Integer sfxs;
    @JsonIgnore
    private String xskz;
    @JsonIgnore
    private Integer sfqy;
    @JsonIgnore
    private String qykz;
    @JsonIgnore
    private Integer kfwk;
    @JsonIgnore
    private String wkkz;
    @JsonIgnore
    private String metting;
    @JsonIgnore
    private Timestamp sj;
    @JsonIgnore
    private Double je;
    @JsonIgnore
    private String ibzsam01name;
    @JsonIgnore
    private String ibzsam01id;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[示例实体02标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return ibzsam02id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String ibzsam02id){
        this.ibzsam02id = ibzsam02id;
    }
    @Size(min = 0, max = 200, message = "[示例实体02名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return ibzsam02name;
    }
    @Size(min = 0, max = 200, message = "[示例实体02名称]长度必须在[200]以内!")
    @JsonProperty(value = "ibzsam02name")
    public String getIbzsam02name(){
        return ibzsam02name;
    }
    @JsonProperty(value = "ibzsam02name")
    public void setIbzsam02name(String ibzsam02name){
        this.ibzsam02name = ibzsam02name;
    }
    @JsonProperty(value = "sfxs")
    public Integer getSfxs(){
        return sfxs;
    }
    @JsonProperty(value = "sfxs")
    public void setSfxs(Integer sfxs){
        this.sfxs = sfxs;
    }
    @Size(min = 0, max = 100, message = "[显示控制属性]长度必须在[100]以内!")
    @JsonProperty(value = "xskz")
    public String getXskz(){
        return xskz;
    }
    @JsonProperty(value = "xskz")
    public void setXskz(String xskz){
        this.xskz = xskz;
    }
    @JsonProperty(value = "sfqy")
    public Integer getSfqy(){
        return sfqy;
    }
    @JsonProperty(value = "sfqy")
    public void setSfqy(Integer sfqy){
        this.sfqy = sfqy;
    }
    @Size(min = 0, max = 100, message = "[启用控制属性]长度必须在[100]以内!")
    @JsonProperty(value = "qykz")
    public String getQykz(){
        return qykz;
    }
    @JsonProperty(value = "qykz")
    public void setQykz(String qykz){
        this.qykz = qykz;
    }
    @JsonProperty(value = "kfwk")
    public Integer getKfwk(){
        return kfwk;
    }
    @JsonProperty(value = "kfwk")
    public void setKfwk(Integer kfwk){
        this.kfwk = kfwk;
    }
    @Size(min = 0, max = 100, message = "[动态为空控制属性]长度必须在[100]以内!")
    @JsonProperty(value = "wkkz")
    public String getWkkz(){
        return wkkz;
    }
    @JsonProperty(value = "wkkz")
    public void setWkkz(String wkkz){
        this.wkkz = wkkz;
    }
    @Size(min = 0, max = 60, message = "[会议]长度必须在[60]以内!")
    @JsonProperty(value = "metting")
    public String getMetting(){
        return metting;
    }
    @JsonProperty(value = "metting")
    public void setMetting(String metting){
        this.metting = metting;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "sj")
    public Timestamp getSj(){
        return sj;
    }
    @JsonProperty(value = "sj")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSj(Timestamp sj){
        this.sj = sj;
    }
    @JsonProperty(value = "je")
    public Double getJe(){
        return je;
    }
    @JsonProperty(value = "je")
    public void setJe(Double je){
        this.je = je;
    }
    @Size(min = 0, max = 200, message = "[实例实体01名称]长度必须在[200]以内!")
    @JsonProperty(value = "ibzsam01name")
    public String getIbzsam01name(){
        return ibzsam01name;
    }
    @JsonProperty(value = "ibzsam01name")
    public void setIbzsam01name(String ibzsam01name){
        this.ibzsam01name = ibzsam01name;
    }
    @Size(min = 0, max = 100, message = "[示例实体01]长度必须在[100]以内!")
    @JsonProperty(value = "ibzsam01id")
    public String getIbzsam01id(){
        //输出获取外键值方法-用于通过parentdata关系填充外键值id
        if(StringUtils.isEmpty(ibzsam01id) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_IBZSAM02_IBZSAM01_IBZSAM01ID"))
                this.ibzsam01id=this.getSrfparentdata().getStringValue("srfparentkey");
        }
        return ibzsam01id;
    }
    @JsonProperty(value = "ibzsam01id")
    public void setIbzsam01id(String ibzsam01id){
        this.ibzsam01id = ibzsam01id;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[示例实体02标识]长度必须在[100]以内!")
    @JsonProperty(value = "ibzsam02id")
    public String getIbzsam02id(){
        return ibzsam02id;
    }
    @JsonProperty(value = "ibzsam02id")
    public void setIbzsam02id(String ibzsam02id){
        this.ibzsam02id = ibzsam02id;
    }
    public  void fromIBZSAM02(IBZSAM02 sourceEntity)  {
        this.fromIBZSAM02(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromIBZSAM02(IBZSAM02 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(IBZSAM02.class, IBZSAM02_EditForm_MobMain.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  IBZSAM02 toIBZSAM02()  {
        IBZSAM02 targetEntity =new IBZSAM02();
        BeanCopier copier=BeanCopier.create(IBZSAM02_EditForm_MobMain.class, IBZSAM02.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
