
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.IBZSam01Mapper;
import com.ibz.demo.ysmk.domain.IBZSam01;
import com.ibz.demo.ysmk.service.IBZSam01Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.IBZSam01SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[IBZSam01] 服务对象接口实现
 */
@Service(value="IBZSam01ServiceImpl")
public class IBZSam01ServiceImpl extends ServiceImplBase
<IBZSam01Mapper, IBZSam01> implements IBZSam01Service{


    @Resource
    private IBZSam01Mapper ibzsam01Mapper;

    @Override
    public boolean create(IBZSam01 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(IBZSam01 et){
        this.beforeUpdate(et);
		this.ibzsam01Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(IBZSam01 et){
        this.ibzsam01Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(IBZSam01 et){
		return et.getIbzsam01id();
    }
    @Override
    public boolean getDraft(IBZSam01 et)  {
            return true;
    }







    @Override
	public List<IBZSam01> listDefault(IBZSam01SearchFilter filter) {
		return ibzsam01Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<IBZSam01> searchDefault(IBZSam01SearchFilter filter) {
		return ibzsam01Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return ibzsam01Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return ibzsam01Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return ibzsam01Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return ibzsam01Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    /**
     * 获取实体主状态
     * @param et 业务数据
     * @return
     */
    public String getMajState(IBZSam01 et)
    {
        String strMajState="";
        TreeMap
        <String,String> matchMap = new TreeMap<>();
        //输出实体主状态
         if(isMatchCond("20",et))
             matchMap.put(getWildcardCnt("20"),"20");
         if(isMatchCond("10",et))
             matchMap.put(getWildcardCnt("10"),"10");
        if(matchMap.size()>0){
            strMajState=matchMap.get(matchMap.firstKey());
        }
        return strMajState;
    }
    /**
     * 判断是否满足实体主状态
     * @param strMSTag
     * @param et
     * @return
     */
    public boolean isMatchCond(String strMSTag ,IBZSam01 et)
    {
        boolean flag=false;
        //list遍历出主状态属性
        String dataState0=et.getIbzsam01name();
        if(!StringUtils.isEmpty(strMSTag)){
           String deState[]=strMSTag.split("__");
            if(deState.length==1){
                if(dataState0.equals(deState[0]) || (deState[0].equals("*") ) ){
                    flag=true;
                }
            }
        }
         return flag;
    }
     /**
     * 通配符数量
     * @param strMSTag
     * @return
     */
    public String getWildcardCnt(String strMSTag){
        Pattern p = Pattern.compile("\\*");
        Matcher m = p.matcher(strMSTag);
        int i = 0;
        while(m.find()){
            i++;
        }
        return String.valueOf(i);
    }
    @Override
    public Page<IBZSam01> selectPermission(QueryWrapper<IBZSam01> selectCond) {
        return ibzsam01Mapper.selectPermission(new IBZSam01SearchFilter().getPage(),selectCond);
    }

 }


