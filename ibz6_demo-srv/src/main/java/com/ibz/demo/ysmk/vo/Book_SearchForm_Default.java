package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.BookSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book_SearchForm_Default{

    private DataObj srfparentdata;

    private String n_bookname_like;
    @JsonProperty(access=Access.READ_ONLY)
    private String n_createman_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromBookSearchFilter(BookSearchFilter sourceSearchFilter)  {
        this.setN_bookname_like(sourceSearchFilter.getN_bookname_like());
        this.setN_createman_like(sourceSearchFilter.getN_createman_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  BookSearchFilter toBookSearchFilter()  {
        BookSearchFilter targetSearchFilter=new BookSearchFilter();
        if(targetSearchFilter.getN_bookname_like()==null)
            targetSearchFilter.setN_bookname_like(this.getN_bookname_like());
        if(targetSearchFilter.getN_createman_like()==null)
            targetSearchFilter.setN_createman_like(this.getN_createman_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
