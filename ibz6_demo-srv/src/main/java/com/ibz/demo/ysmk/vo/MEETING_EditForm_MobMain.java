package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.MEETING;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MEETING_EditForm_MobMain{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String meetingid;
    @JsonIgnore
    private String meetingname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private Timestamp starttime;
    @JsonIgnore
    private Timestamp endtime;
    @JsonIgnore
    private String hynr;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[会议标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return meetingid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String meetingid){
        this.meetingid = meetingid;
    }
    @Size(min = 0, max = 200, message = "[会议名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return meetingname;
    }
    @Size(min = 0, max = 200, message = "[会议名称]长度必须在[200]以内!")
    @JsonProperty(value = "meetingname")
    public String getMeetingname(){
        return meetingname;
    }
    @JsonProperty(value = "meetingname")
    public void setMeetingname(String meetingname){
        this.meetingname = meetingname;
    }
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "starttime")
    public Timestamp getStarttime(){
        if(StringUtils.isEmpty(starttime)){
        if(isCreate()){
                // DefaultValue(value=starttime,type=CONTEXT)  暂未支持从[CONTEXT]中取值
        }
        }
        return starttime;
    }
    @JsonProperty(value = "starttime")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    public void setStarttime(Timestamp starttime){
        this.starttime = starttime;
    }
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "endtime")
    public Timestamp getEndtime(){
        return endtime;
    }
    @JsonProperty(value = "endtime")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    public void setEndtime(Timestamp endtime){
        this.endtime = endtime;
    }
    @Size(min = 0, max = 2000, message = "[会议内容]长度必须在[2000]以内!")
    @JsonProperty(value = "hynr")
    public String getHynr(){
        return hynr;
    }
    @JsonProperty(value = "hynr")
    public void setHynr(String hynr){
        this.hynr = hynr;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[会议标识]长度必须在[100]以内!")
    @JsonProperty(value = "meetingid")
    public String getMeetingid(){
        return meetingid;
    }
    @JsonProperty(value = "meetingid")
    public void setMeetingid(String meetingid){
        this.meetingid = meetingid;
    }
    public  void fromMEETING(MEETING sourceEntity)  {
        this.fromMEETING(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromMEETING(MEETING sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(MEETING.class, MEETING_EditForm_MobMain.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  MEETING toMEETING()  {
        MEETING targetEntity =new MEETING();
        BeanCopier copier=BeanCopier.create(MEETING_EditForm_MobMain.class, MEETING.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
    /**
	 * 新建
	 * @param
	 * @return
	 */
    private boolean isCreate()
    {
    	if((StringUtils.isEmpty(this.srfuf))||this.srfuf.equals("0"))
	    	return true;
    	else
    	    return false;
    }
}
