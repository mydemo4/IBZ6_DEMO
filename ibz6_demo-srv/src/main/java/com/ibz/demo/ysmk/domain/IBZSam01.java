package com.ibz.demo.ysmk.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.cglib.beans.BeanGenerator;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import java.util.Map;
import java.util.HashMap;
import org.springframework.util.StringUtils;
//import com.ibz.demo.ysmk.valuerule.anno.ibzsam01.*;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import org.springframework.cglib.beans.BeanCopier;
import com.ibz.demo.ibizutil.annotation.Dict;
import com.ibz.demo.ibizutil.domain.EntityBase;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.enums.FillMode;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 实体[IBZSam01] 数据对象
 */
@TableName(value = "T_IBZSAM01",resultMap = "IBZSam01ResultMap")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IBZSam01 extends EntityBase implements Serializable{

    @TableId(value= "ibzsam01id",type=IdType.UUID)//指定主键生成策略
    private String ibzsam01id;
    private String ibzsam01name;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String createman;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createdate;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp sj;
    private String image;
    private Integer sl;
    private Double je;
    private Double sz;
    private String bz;
    @Dict(dictName = "IBZ6_DEMO_YesNoCodeList")
    private Integer sf;
    @Dict(dictName = "IBZ6_DEMO_CodeList81CodeList")
    private String dxtext;
    @Dict(dictName = "IBZ6_DEMO_CodeList81CodeList")
    private String mdxtext;
    @Dict(dictName = "IBZ6_DEMO_CodeList86CodeList")
    private Integer dxsz;
    @Dict(dictName = "IBZ6_DEMO_CodeList86CodeList")
    private Integer mdxsz;
    private String wjsc;
    private String mimage;
    private String mwjsc;
    private String mrowtext;
    @Dict(dictName = "IBZ6_DEMO_YesNoCodeList")
    private String kg;
    private String dxxlb;
    private String exldx;
    private String mxldx;
    private String sjxz;
    private String dsjxz;
    private String timexz;
    private String pwd;
     /**
     * 复制当前对象数据到目标对象(粘贴重置)
     *
     * @throws Exception
     */
    public IBZSam01 copyTo(IBZSam01 targetEntity)
	{
		BeanCopier copier=BeanCopier.create(IBZSam01.class, IBZSam01.class, false);
		copier.copy(this, targetEntity, null);
        targetEntity.setIbzsam01id(null);
		return targetEntity;
	}
}
