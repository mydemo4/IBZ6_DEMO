
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.MEETINGMapper;
import com.ibz.demo.ysmk.domain.MEETING;
import com.ibz.demo.ysmk.service.MEETINGService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.MEETINGSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[MEETING] 服务对象接口实现
 */
@Service(value="MEETINGServiceImpl")
public class MEETINGServiceImpl extends ServiceImplBase
<MEETINGMapper, MEETING> implements MEETINGService{


    @Resource
    private MEETINGMapper meetingMapper;

    @Override
    public boolean create(MEETING et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(MEETING et){
        this.beforeUpdate(et);
		this.meetingMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(MEETING et){
        this.meetingMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(MEETING et){
		return et.getMeetingid();
    }
    @Override
    public boolean getDraft(MEETING et)  {
            return true;
    }







    @Override
	public List<MEETING> listDefault(MEETINGSearchFilter filter) {
		return meetingMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<MEETING> searchDefault(MEETINGSearchFilter filter) {
		return meetingMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return meetingMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return meetingMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return meetingMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return meetingMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<MEETING> selectPermission(QueryWrapper<MEETING> selectCond) {
        return meetingMapper.selectPermission(new MEETINGSearchFilter().getPage(),selectCond);
    }

 }


