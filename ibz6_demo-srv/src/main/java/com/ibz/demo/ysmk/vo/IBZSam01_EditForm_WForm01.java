package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ysmk.domain.IBZSam01;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IBZSam01_EditForm_WForm01{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String ibzsam01id;
    @JsonIgnore
    private String ibzsam01name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String dxtext;
    @JsonIgnore
    private Integer dxsz;
    @JsonIgnore
    private String dxxlb;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[实例实体01标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return ibzsam01id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String ibzsam01id){
        this.ibzsam01id = ibzsam01id;
    }
    @Size(min = 0, max = 200, message = "[实例实体01名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return ibzsam01name;
    }
    @Size(min = 0, max = 200, message = "[实例实体01名称]长度必须在[200]以内!")
    @JsonProperty(value = "ibzsam01name")
    public String getIbzsam01name(){
        return ibzsam01name;
    }
    @JsonProperty(value = "ibzsam01name")
    public void setIbzsam01name(String ibzsam01name){
        this.ibzsam01name = ibzsam01name;
    }
    @Size(min = 0, max = 60, message = "[单选文本]长度必须在[60]以内!")
    @JsonProperty(value = "dxtext")
    public String getDxtext(){
        return dxtext;
    }
    @JsonProperty(value = "dxtext")
    public void setDxtext(String dxtext){
        this.dxtext = dxtext;
    }
    @JsonProperty(value = "dxsz")
    public Integer getDxsz(){
        return dxsz;
    }
    @JsonProperty(value = "dxsz")
    public void setDxsz(Integer dxsz){
        this.dxsz = dxsz;
    }
    @Size(min = 0, max = 200, message = "[单选项列表]长度必须在[200]以内!")
    @JsonProperty(value = "dxxlb")
    public String getDxxlb(){
        return dxxlb;
    }
    @JsonProperty(value = "dxxlb")
    public void setDxxlb(String dxxlb){
        this.dxxlb = dxxlb;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[实例实体01标识]长度必须在[100]以内!")
    @JsonProperty(value = "ibzsam01id")
    public String getIbzsam01id(){
        return ibzsam01id;
    }
    @JsonProperty(value = "ibzsam01id")
    public void setIbzsam01id(String ibzsam01id){
        this.ibzsam01id = ibzsam01id;
    }
    public  void fromIBZSam01(IBZSam01 sourceEntity)  {
        this.fromIBZSam01(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromIBZSam01(IBZSam01 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(IBZSam01.class, IBZSam01_EditForm_WForm01.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  IBZSam01 toIBZSam01()  {
        IBZSam01 targetEntity =new IBZSam01();
        BeanCopier copier=BeanCopier.create(IBZSam01_EditForm_WForm01.class, IBZSam01.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
