package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.ysmk.domain.LBT;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LBT_MOBMDCTRL_Mob{
    private String content;
    private String subtitle;
    private String srfmajortext;
    private String time;
    private String srfkey;
     public  void fromLBT(LBT sourceEntity)  {
	    this.setContent(String.format("%s",sourceEntity.getType()));
	    this.setSubtitle(String.format("%s",sourceEntity.getTitle()));
	    this.setSrfmajortext(String.format("%s",sourceEntity.getLbtname()));
	    this.setTime(String.format("%s",sourceEntity.getUpdatedate()));
	    this.setSrfkey(String.format("%s",sourceEntity.getLbtid()));
	}
	public static Page<LBT_MOBMDCTRL_Mob> fromLBT(Page<LBT> sourcePage)   {
        if(sourcePage==null)
            return null;
        Page<LBT_MOBMDCTRL_Mob> targetpage=new Page<LBT_MOBMDCTRL_Mob>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<LBT_MOBMDCTRL_Mob> records=new ArrayList<LBT_MOBMDCTRL_Mob>();
        for(LBT source:sourcePage.getRecords()) {
    LBT_MOBMDCTRL_Mob target=new LBT_MOBMDCTRL_Mob();
            target.fromLBT(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
