package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.LBT;
import com.ibz.demo.ysmk.service.dto.LBTSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[LBT] 服务对象接口
 */
public interface LBTService extends IService<LBT>{

	boolean create(LBT et);
	boolean remove(LBT et);
	boolean update(LBT et);
	boolean getDraft(LBT et);
	boolean checkKey(LBT et);
	LBT get(LBT et);
    List<LBT> listSC(LBTSearchFilter filter) ;
	Page<LBT> searchSC(LBTSearchFilter filter);
    List<LBT> listLToR(LBTSearchFilter filter) ;
	Page<LBT> searchLToR(LBTSearchFilter filter);
    List<LBT> listScroll(LBTSearchFilter filter) ;
	Page<LBT> searchScroll(LBTSearchFilter filter);
    List<LBT> listTitleImg(LBTSearchFilter filter) ;
	Page<LBT> searchTitleImg(LBTSearchFilter filter);
    List<LBT> listMDLB(LBTSearchFilter filter) ;
	Page<LBT> searchMDLB(LBTSearchFilter filter);
    List<LBT> listLToR2(LBTSearchFilter filter) ;
	Page<LBT> searchLToR2(LBTSearchFilter filter);
    List<LBT> listImgLB(LBTSearchFilter filter) ;
	Page<LBT> searchImgLB(LBTSearchFilter filter);
    List<LBT> listLB(LBTSearchFilter filter) ;
	Page<LBT> searchLB(LBTSearchFilter filter);
    List<LBT> listSSGroup(LBTSearchFilter filter) ;
	Page<LBT> searchSSGroup(LBTSearchFilter filter);
    List<LBT> listDefault(LBTSearchFilter filter) ;
	Page<LBT> searchDefault(LBTSearchFilter filter);
	/**	 实体[LBT] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
