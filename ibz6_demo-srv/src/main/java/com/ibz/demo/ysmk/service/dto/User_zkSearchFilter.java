package com.ibz.demo.ysmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.ysmk.domain.User_zk;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User_zkSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<User_zk> selectCond;
	public User_zkSearchFilter(){
		this.page =new Page<User_zk>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<User_zk>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("user_zkname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_user_zkname_like;//[用户名称]
	public void setN_user_zkname_like(String n_user_zkname_like) {
        this.n_user_zkname_like = n_user_zkname_like;
        if(!StringUtils.isEmpty(this.n_user_zkname_like)){
            this.selectCond.like("user_zkname", n_user_zkname_like);
        }
    }
	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!StringUtils.isEmpty(this.n_type_eq)){
            this.selectCond.eq("type", n_type_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
