package com.ibz.demo.ysmk.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.ibizutil.domain.TreeNode;
import com.ibz.demo.ibizutil.domain.TreeNodeFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;

import com.ibz.demo.ysmk.service.DepartmentService;
import com.ibz.demo.ysmk.service.AccountService;

@RestController
@RequestMapping("/vue_mobile_r7/ctrl/departmenttreetreeview")
public class Vue_Mobile_R7DepartmentTreeTreeViewController{

	/**
	 * 节点分隔符号
	 */
	public static final String TREENODE_SEPARATOR = ";";
	public static final String TREENODE_DYNAMICNODE = "dynamicnode";
	public static final String TREENODE_CFO = "cfo";
	public static final String TREENODE_ROOT = "ROOT";
	public static final String TREENODE_DS2 = "ds2";
	public static final String TREENODE_ENGINEERING = "Engineering";
	public static final String TREENODE_SUBMANAGER = "subManager";
	public static final String TREENODE_MANAGER = "manager";
	public static final String TREENODE_DS1 = "ds1";
	public static final String TREENODE_MANAGEMENT = "Management";
	public static final String TREENODE_BUSINESS = "Business";

	@Autowired
	private DepartmentService departmentService;

	protected DepartmentService getDepartmentService(){
		return departmentService;
	}
	@Autowired
	private AccountService accountService;

	protected AccountService getAccountService(){
		return accountService;
	}

	@GetMapping(value="getnodes")
	public ResponseEntity<List<TreeNode>> getNodes(@Validated String srfparentkey,String srfcat,String srfnodeid,String srfnodefilter){
			List<TreeNode> list = new ArrayList<TreeNode>();
			TreeNodeFilter filter = new TreeNodeFilter();
			if (!StringUtils.hasLength(srfnodeid) || srfnodeid.equals("#")) {
				srfnodeid = TREENODE_ROOT;
			}
			String strTreeNodeId = srfnodeid;
			String strRealNodeId = "";
			boolean bRootSelect = false;
			String strNodeType = null;
			// String strRootSelectNode = this.getWebContext().getPostValue("srfnodeselect");
			String strRootSelectNode = "";// this.getWebContext().getPostValue("srfnodeselect");
			if (strTreeNodeId.equals(TREENODE_ROOT)) {
				strNodeType = TREENODE_ROOT;
				//treeNode = this.getTreeModel().getRootTreeNodeModel();
				//strRealNodeId = treeNodeFetchContext.getCatalog();
				//bRootSelect = this.getTreeModel().isEnableRootSelect();
			} else {
				int nPos = strTreeNodeId.indexOf(TREENODE_SEPARATOR);
				if (nPos == -1) {
					throw new BadRequestAlertException("树节点[%1$s]标识无效","","");
				}

				strNodeType = strTreeNodeId.substring(0, nPos);
				strRealNodeId = strTreeNodeId.substring(nPos + 1);
			}
			filter.setSrfParentKey(srfparentkey);
			filter.setSrfCat(srfcat);
			filter.setSrfNodeFilter(srfnodefilter);
			filter.setRealNodeId(strRealNodeId);
			filter.setSrfNodeId(srfnodeid);
			filter.setNodeType(strNodeType);
			/**
			 * 分解节点标识
			 */
			String[] nodeid = strRealNodeId.split(TREENODE_SEPARATOR);
			for (int i = 0; i < nodeid.length; i++) {
				switch(i){
				case 0:
					filter.setNodeId(nodeid[0]);
					break;
				case 1:
					filter.setNodeId2(nodeid[1]);
					break;
				case 2:
					filter.setNodeId3(nodeid[2]);
					break;
				case 3:
					filter.setNodeId4(nodeid[3]);
					break;
				default:
					break;
				}
			}
			if(strNodeType.equals(TREENODE_DYNAMICNODE)){
				fillDynamicnodeNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_CFO)){
				fillCfoNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_ROOT)){
				fillRootNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_DS2)){
				fillDs2NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_ENGINEERING)){
				fillEngineeringNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_SUBMANAGER)){
				fillSubmanagerNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_MANAGER)){
				fillManagerNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_DS1)){
				fillDs1NodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_MANAGEMENT)){
				fillManagementNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
			if(strNodeType.equals(TREENODE_BUSINESS)){
				fillBusinessNodeChilds(filter,list);
				return ResponseEntity.ok().body(list);
			}
				return ResponseEntity.ok().body(list);
	}

	/**
	* 填充 树视图节点[动态董事]
	*/
	protected void fillDynamicnodeNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[动态董事]子节点
	*/
	protected void fillDynamicnodeNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[财务副总]
	*/
	protected void fillCfoNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[财务副总]子节点
	*/
	protected void fillCfoNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[默认根节点]
	*/
	protected void fillRootNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("默认根节点");
		treeNode.setSrfmajortext("默认根节点");
		String strNodeId = "ROOT";
		treeNode.setSrfkey("root");
		strNodeId += TREENODE_SEPARATOR;
		strNodeId += "root";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[默认根节点]子节点
	*/
	protected void fillRootNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充总经理
		fillManagerNodes(filter,list);
		//填充董事1
		fillDs1Nodes(filter,list);
		//填充董事2
		fillDs2Nodes(filter,list);
	}

	/**
	* 填充 树视图节点[董事2]
	*/
	protected void fillDs2Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("董事2");
		treeNode.setSrfmajortext("董事2");
		String strNodeId = "ds2";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[董事2]子节点
	*/
	protected void fillDs2NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[工程部]
	*/
	protected void fillEngineeringNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.Department> pageResult = this.getDepartmentService().listEngineering(searchFilter);
		for(com.ibz.demo.ysmk.domain.Department entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getDepartmentid();
			String strText = entity.getDepartmentname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "Engineering";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[工程部]子节点
	*/
	protected void fillEngineeringNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[副总经理]
	*/
	protected void fillSubmanagerNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("副总经理");
		treeNode.setSrfmajortext("副总经理");
		String strNodeId = "subManager";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[副总经理]子节点
	*/
	protected void fillSubmanagerNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充业务部
		fillBusinessNodes(filter,list);
		//填充管理部
		fillManagementNodes(filter,list);
		//填充工程部
		fillEngineeringNodes(filter,list);
	}

	/**
	* 填充 树视图节点[总经理]
	*/
	protected void fillManagerNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		TreeNode treeNode = new TreeNode();
		treeNode.setText("总经理");
		treeNode.setSrfmajortext("总经理");
		String strNodeId = "manager";
		treeNode.setId(strNodeId);
		treeNode.setExpanded(filter.isAutoExpand());
		treeNode.setLeaf(false);
		list.add(treeNode);
	}

	/**
	* 填充 树视图节点[总经理]子节点
	*/
	protected void fillManagerNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充副总经理
		fillSubmanagerNodes(filter,list);
		//填充财务副总
		fillCfoNodes(filter,list);
	}

	/**
	* 填充 树视图节点[董事1]
	*/
	protected void fillDs1Nodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.AccountSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.AccountSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.Account> pageResult = this.getAccountService().listDefault(searchFilter);
		for(com.ibz.demo.ysmk.domain.Account entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getAccountid();
			String strText = entity.getAccountname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "ds1";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			treeNode.setLeaf(false);
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[董事1]子节点
	*/
	protected void fillDs1NodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		//填充动态董事
		fillDynamicnodeNodes(filter,list);
	}

	/**
	* 填充 树视图节点[管理部]
	*/
	protected void fillManagementNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.Department> pageResult = this.getDepartmentService().listManagement(searchFilter);
		for(com.ibz.demo.ysmk.domain.Department entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getDepartmentid();
			String strText = entity.getDepartmentname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "Management";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[管理部]子节点
	*/
	protected void fillManagementNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

	/**
	* 填充 树视图节点[业务部]
	*/
	protected void fillBusinessNodes(@Validated TreeNodeFilter filter,List<TreeNode> list) {
		com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter();
		//searchFilter.setActiveData(filter);
		List<com.ibz.demo.ysmk.domain.Department> pageResult = this.getDepartmentService().listBusiness(searchFilter);
		for(com.ibz.demo.ysmk.domain.Department entity : pageResult) {
			TreeNode treeNode = new TreeNode();
			String strId = entity.getDepartmentid();
			String strText = entity.getDepartmentname();
			treeNode.setSrfkey(strId);
			treeNode.setText(strText);
			treeNode.setSrfmajortext(strText);
			String strNodeId = "Business";
			strNodeId += TREENODE_SEPARATOR;
			strNodeId += strId;
			treeNode.setId(strNodeId);
			treeNode.setExpanded(filter.isAutoExpand());
			list.add(treeNode);
		}
	}

	/**
	* 填充 树视图节点[业务部]子节点
	*/
	protected void fillBusinessNodeChilds(@Validated TreeNodeFilter filter,List<TreeNode> list) {
	}

}
