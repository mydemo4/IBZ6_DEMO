package com.ibz.demo.ysmk.service;

import com.ibz.demo.ysmk.domain.IBZSam01;
import com.ibz.demo.ysmk.service.dto.IBZSam01SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[IBZSam01] 服务对象接口
 */
public interface IBZSam01Service extends IService<IBZSam01>{

	boolean create(IBZSam01 et);
	boolean update(IBZSam01 et);
	boolean remove(IBZSam01 et);
	boolean checkKey(IBZSam01 et);
	boolean getDraft(IBZSam01 et);
	IBZSam01 get(IBZSam01 et);
    List<IBZSam01> listDefault(IBZSam01SearchFilter filter) ;
	Page<IBZSam01> searchDefault(IBZSam01SearchFilter filter);
	/**	 实体[IBZSam01] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
	String getMajState(IBZSam01 et);
}
