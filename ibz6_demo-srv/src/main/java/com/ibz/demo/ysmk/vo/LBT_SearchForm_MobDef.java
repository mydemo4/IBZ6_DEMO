package com.ibz.demo.ysmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.ysmk.service.dto.LBTSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LBT_SearchForm_MobDef{

    private DataObj srfparentdata;

    private String n_lbtname_like;
    private String n_type_eq;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromLBTSearchFilter(LBTSearchFilter sourceSearchFilter)  {
        this.setN_lbtname_like(sourceSearchFilter.getN_lbtname_like());
        this.setN_type_eq(sourceSearchFilter.getN_type_eq());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  LBTSearchFilter toLBTSearchFilter()  {
        LBTSearchFilter targetSearchFilter=new LBTSearchFilter();
        if(targetSearchFilter.getN_lbtname_like()==null)
            targetSearchFilter.setN_lbtname_like(this.getN_lbtname_like());
        if(targetSearchFilter.getN_type_eq()==null)
            targetSearchFilter.setN_type_eq(this.getN_type_eq());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
