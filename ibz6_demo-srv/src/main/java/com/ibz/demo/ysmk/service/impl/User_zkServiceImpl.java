
package com.ibz.demo.ysmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ysmk.mapper.User_zkMapper;
import com.ibz.demo.ysmk.domain.User_zk;
import com.ibz.demo.ysmk.service.User_zkService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ysmk.service.dto.User_zkSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[User_zk] 服务对象接口实现
 */
@Service(value="User_zkServiceImpl")
public class User_zkServiceImpl extends ServiceImplBase
<User_zkMapper, User_zk> implements User_zkService{


    @Resource
    private User_zkMapper user_zkMapper;

    @Override
    public boolean create(User_zk et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(User_zk et){
        this.beforeUpdate(et);
		this.user_zkMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(User_zk et){
        this.user_zkMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(User_zk et){
		return et.getUser_zkid();
    }
    @Override
    public boolean getDraft(User_zk et)  {
            return true;
    }







    @Override
	public List<User_zk> listDefault(User_zkSearchFilter filter) {
		return user_zkMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<User_zk> searchDefault(User_zkSearchFilter filter) {
		return user_zkMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return user_zkMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return user_zkMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return user_zkMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return user_zkMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<User_zk> selectPermission(QueryWrapper<User_zk> selectCond) {
        return user_zkMapper.selectPermission(new User_zkSearchFilter().getPage(),selectCond);
    }

 }


