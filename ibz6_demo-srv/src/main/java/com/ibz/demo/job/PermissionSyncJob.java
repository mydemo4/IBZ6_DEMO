package com.ibz.demo.job;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.helper.HttpUtils;
import com.ibz.demo.ibizutil.center.IUser;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 权限：同步当前权限资源任务类
 */
@Component   //开启此类需要保证Main中开启了feign ：EnableFeignClients
public class PermissionSyncJob implements ApplicationRunner {
    private Log log = LogFactory.getLog(PermissionSyncJob.class);

    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验

    @Value("${ibiz.enableAdminAccountSync:false}")
    boolean enableAdminAccountSync;  //是否同步内置管理员账号

    @Value("${ibiz.systemid:2C40DFCD-0DF5-47BF-91A5-C45F810B0001}")
    private String systemid;

    @Value("${ibiz.appid:BCF19981-56A2-43B3-AE9D-363512DF0570}")
    private String appid;

    @Value("${ibiz.systemname:IBZ6_DEMO}")
    private String systemname;

    @Value("${ibiz.appname:权限前端应用}")
    private String appname;

    @Value("${ibiz.admin.loginname:demo_admin}")
    private String loginname;
    @Value("${ibiz.admin.password:123456}")
    private String password;
    @Value("${ibiz.center.security.pushjob}")
    public String securityURI;

    @Value("${ibiz.center.user.rest.sync.adminaccount}")
    public String userURI;

    @Autowired
    private IUser userCenter;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(enableRemoteValid){
            try {
                InputStream permission= this.getClass().getResourceAsStream("/decapability/security_resource.json");
                String permissionResult = IOUtils.toString(permission,"UTF-8");
                JSONObject jsonObject = JSONObject.parseObject(permissionResult);
                JSONObject jsonheader = new JSONObject();
                jsonheader.put("Content-Type","application/json");
                JSONObject param = new JSONObject();
                jsonObject.put("systemid",systemid);
                jsonObject.put("systemname",systemname);
                jsonObject.put("appid",appid);
                jsonObject.put("appname",appname);
                param.put("data",jsonObject);
                String httpResponse =  HttpUtils.post(securityURI,jsonheader,param);
                JSONObject json  = JSONObject.parseObject(httpResponse);
            }
            catch (Exception ex) {
                log.error(String.format("向安全中心同步数据发生错误，请检查服务是否正常! [%s]",ex));
            }
        }
        if(enableAdminAccountSync){
            try{
                JSONObject params = new JSONObject();
                params.put("loginaccountname",loginname);
                params.put("loginaccountid",systemid);
                params.put("pwd",password);
                JSONObject jsonheader = new JSONObject();
                jsonheader.put("Content-Type","application/json");
                userCenter.syncData(userURI,systemid,params);
                log.info("系统管理员账号["+loginname+"]推送成功");
            }catch (Exception ex) {
                log.error(String.format("向用户中心同步数据发生错误，请检查服务是否正常! [%s]",ex));
            }
        }
    }
}