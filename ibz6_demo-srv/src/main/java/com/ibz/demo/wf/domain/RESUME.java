package com.ibz.demo.wf.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.cglib.beans.BeanGenerator;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import java.util.Map;
import java.util.HashMap;
import org.springframework.util.StringUtils;
//import com.ibz.demo.wf.valuerule.anno.resume.*;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import org.springframework.cglib.beans.BeanCopier;
import com.ibz.demo.ibizutil.annotation.Dict;
import com.ibz.demo.ibizutil.domain.EntityBase;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.enums.FillMode;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 实体[RESUME] 数据对象
 */
@TableName(value = "T_RESUME",resultMap = "RESUMEResultMap")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RESUME extends EntityBase implements Serializable{

    @TableId(value= "resumeid",type=IdType.UUID)//指定主键生成策略
    private String resumeid;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String createman;
    private String resumename;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createdate;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String updateman;
    @Dict(dictName = "IBZ6_DEMO_WFStatesCodeList")
    private Integer wfstate;
    @Dict(dictName = "IBZ6_DEMO_ResumeStateCodeList")
    private String resumestate;
    @Dict(dictName = "IBZ6_DEMO_ResumeStepCodeList")
    private String wfstep;
    private String resumewfstate;
    private String wfinstance;
     /**
     * 复制当前对象数据到目标对象(粘贴重置)
     *
     * @throws Exception
     */
    public RESUME copyTo(RESUME targetEntity)
	{
		BeanCopier copier=BeanCopier.create(RESUME.class, RESUME.class, false);
		copier.copy(this, targetEntity, null);
        targetEntity.setResumeid(null);
		return targetEntity;
	}
}
