package com.ibz.demo.wf.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[简历步骤]
 */
@Component("IBZ6_DEMO_ResumeStepCodeList")
public class ResumeStepCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_ResumeStep");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("100");
		item0.setId("100");
		item0.setText("部门领导查阅");
		item0.setLabel("部门领导查阅");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("100",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("200");
		item1.setId("200");
		item1.setText("公司领导查阅");
		item1.setLabel("公司领导查阅");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("200",item1);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}