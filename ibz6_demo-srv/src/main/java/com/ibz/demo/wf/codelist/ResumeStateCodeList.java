package com.ibz.demo.wf.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

/**
 * 代码表[简历状态]
 */
@Component("IBZ6_DEMO_ResumeStateCodeList")
public class ResumeStateCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_ResumeState");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
		CodeItem item0 = new CodeItem();
		item0.setValue("10");
		item0.setId("10");
		item0.setText("未提交");
		item0.setLabel("未提交");
		codeItemList.add(item0);
		codeList.getCodeItemModelMap().put("10",item0);
		CodeItem item1 = new CodeItem();
		item1.setValue("20");
		item1.setId("20");
		item1.setText("查阅中");
		item1.setLabel("查阅中");
		codeItemList.add(item1);
		codeList.getCodeItemModelMap().put("20",item1);
		CodeItem item2 = new CodeItem();
		item2.setValue("30");
		item2.setId("30");
		item2.setText("通过");
		item2.setLabel("通过");
		codeItemList.add(item2);
		codeList.getCodeItemModelMap().put("30",item2);
		CodeItem item3 = new CodeItem();
		item3.setValue("40");
		item3.setId("40");
		item3.setText("未通过");
		item3.setLabel("未通过");
		codeItemList.add(item3);
		codeList.getCodeItemModelMap().put("40",item3);
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}