package com.ibz.demo.wf.service;

import com.ibz.demo.wf.domain.RESUME;
import com.ibz.demo.wf.service.dto.RESUMESearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[RESUME] 服务对象接口
 */
public interface RESUMEService extends IService<RESUME>{

	RESUME get(RESUME et);
	boolean checkKey(RESUME et);
	boolean create(RESUME et);
	boolean remove(RESUME et);
	boolean update(RESUME et);
	boolean getDraft(RESUME et);
    List<RESUME> listDefault(RESUMESearchFilter filter) ;
	Page<RESUME> searchDefault(RESUMESearchFilter filter);
	/**	 实体[RESUME] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
