
package com.ibz.demo.wf.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.wf.mapper.RESUMEMapper;
import com.ibz.demo.wf.domain.RESUME;
import com.ibz.demo.wf.service.RESUMEService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.wf.service.dto.RESUMESearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[RESUME] 服务对象接口实现
 */
@Service(value="RESUMEServiceImpl")
public class RESUMEServiceImpl extends ServiceImplBase
<RESUMEMapper, RESUME> implements RESUMEService{


    @Resource
    private RESUMEMapper resumeMapper;

    @Override
    public boolean create(RESUME et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(RESUME et){
        this.beforeUpdate(et);
		this.resumeMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(RESUME et){
        this.resumeMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(RESUME et){
		return et.getResumeid();
    }
    @Override
    public boolean getDraft(RESUME et)  {
            return true;
    }







    @Override
	public List<RESUME> listDefault(RESUMESearchFilter filter) {
		return resumeMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<RESUME> searchDefault(RESUMESearchFilter filter) {
		return resumeMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return resumeMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return resumeMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return resumeMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return resumeMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<RESUME> selectPermission(QueryWrapper<RESUME> selectCond) {
        return resumeMapper.selectPermission(new RESUMESearchFilter().getPage(),selectCond);
    }

 }


