package com.ibz.demo.wf.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.wf.domain.RESUME;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RESUMESearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<RESUME> selectCond;
	public RESUMESearchFilter(){
		this.page =new Page<RESUME>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<RESUME>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("resumename",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_resumename_like;//[简历名称]
	public void setN_resumename_like(String n_resumename_like) {
        this.n_resumename_like = n_resumename_like;
        if(!StringUtils.isEmpty(this.n_resumename_like)){
            this.selectCond.like("resumename", n_resumename_like);
        }
    }
	private String n_resumestate_eq;//[简历状态]
	public void setN_resumestate_eq(String n_resumestate_eq) {
        this.n_resumestate_eq = n_resumestate_eq;
        if(!StringUtils.isEmpty(this.n_resumestate_eq)){
            this.selectCond.eq("resumestate", n_resumestate_eq);
        }
    }
	private String n_wfstep_eq;//[简历工作流步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!StringUtils.isEmpty(this.n_wfstep_eq)){
            this.selectCond.eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_resumewfstate_eq;//[业务状态]
	public void setN_resumewfstate_eq(String n_resumewfstate_eq) {
        this.n_resumewfstate_eq = n_resumewfstate_eq;
        if(!StringUtils.isEmpty(this.n_resumewfstate_eq)){
            this.selectCond.eq("resumewfstate", n_resumewfstate_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
