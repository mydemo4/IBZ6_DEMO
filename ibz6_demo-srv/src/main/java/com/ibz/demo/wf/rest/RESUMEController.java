package com.ibz.demo.wf.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.wf.service.RESUMEService;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import com.ibz.demo.wf.domain.RESUME;
import com.ibz.demo.wf.service.dto.RESUMESearchFilter;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;
import java.io.IOException;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.RedirectResult;
import javax.validation.constraints.NotBlank;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.access.prepost.PreAuthorize;
import com.ibz.demo.ibizutil.service.RemoteWFServiceImpl;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.domain.WFProcMemo;
import com.alibaba.fastjson.JSONArray;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import com.ibz.demo.ibizutil.security.DataAccessUtil;

@RestController
public class RESUMEController{
    /**
    * 工作流相关
    */
    //流程定义Key
    private String strWorkflowKey = "resumeread" ;

    @Autowired
	RemoteWFServiceImpl wfServiceImpl ;

	/**
	 * 用于权限校验
	 * @return
	 */
	public RESUME getEntity(){
		return new RESUME();
	}
}


