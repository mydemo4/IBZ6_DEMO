package com.ibz.demo.wf.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.wf.domain.RESUME;
import com.ibz.demo.wf.service.dto.RESUMESearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface RESUMEMapper extends BaseMapper<RESUME>{

    List<RESUME> searchDefault(@Param("srf") RESUMESearchFilter searchfilter,@Param("ew") Wrapper<RESUME> wrapper) ;
    Page<RESUME> searchDefault(IPage page, @Param("srf") RESUMESearchFilter searchfilter, @Param("ew") Wrapper<RESUME> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'RESUME:'+#p0")
    RESUME selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'RESUME:'+#p0.resumeid")
    int insert(RESUME entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'RESUME:'+#p0.resumeid")
    int updateById(@Param(Constants.ENTITY) RESUME entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'RESUME:'+#p0")
    int deleteById(Serializable id);
    Page<RESUME> selectPermission(IPage page,@Param("pw") Wrapper<RESUME> wrapper) ;

    //直接sql查询。
    List<RESUME> executeRawSql(@Param("sql")String sql);
}
