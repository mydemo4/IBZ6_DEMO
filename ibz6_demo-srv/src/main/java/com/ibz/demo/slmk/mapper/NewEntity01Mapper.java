package com.ibz.demo.slmk.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.slmk.domain.NewEntity01;
import com.ibz.demo.slmk.service.dto.NewEntity01SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface NewEntity01Mapper extends BaseMapper<NewEntity01>{

    List<NewEntity01> searchDefault(@Param("srf") NewEntity01SearchFilter searchfilter,@Param("ew") Wrapper<NewEntity01> wrapper) ;
    Page<NewEntity01> searchDefault(IPage page, @Param("srf") NewEntity01SearchFilter searchfilter, @Param("ew") Wrapper<NewEntity01> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'NewEntity01:'+#p0")
    NewEntity01 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'NewEntity01:'+#p0.newentity01id")
    int insert(NewEntity01 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'NewEntity01:'+#p0.newentity01id")
    int updateById(@Param(Constants.ENTITY) NewEntity01 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'NewEntity01:'+#p0")
    int deleteById(Serializable id);
    Page<NewEntity01> selectPermission(IPage page,@Param("pw") Wrapper<NewEntity01> wrapper) ;

    //直接sql查询。
    List<NewEntity01> executeRawSql(@Param("sql")String sql);
}
