
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.NewEntity01Mapper;
import com.ibz.demo.slmk.domain.NewEntity01;
import com.ibz.demo.slmk.service.NewEntity01Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.NewEntity01SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[NewEntity01] 服务对象接口实现
 */
@Service(value="NewEntity01ServiceImpl")
public class NewEntity01ServiceImpl extends ServiceImplBase
<NewEntity01Mapper, NewEntity01> implements NewEntity01Service{


    @Resource
    private NewEntity01Mapper newentity01Mapper;

    @Override
    public boolean create(NewEntity01 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(NewEntity01 et){
        this.beforeUpdate(et);
		this.newentity01Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(NewEntity01 et){
        this.newentity01Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(NewEntity01 et){
		return et.getNewentity01id();
    }
    @Override
    public boolean getDraft(NewEntity01 et)  {
            return true;
    }







    @Override
	public List<NewEntity01> listDefault(NewEntity01SearchFilter filter) {
		return newentity01Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<NewEntity01> searchDefault(NewEntity01SearchFilter filter) {
		return newentity01Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return newentity01Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return newentity01Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return newentity01Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return newentity01Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<NewEntity01> selectPermission(QueryWrapper<NewEntity01> selectCond) {
        return newentity01Mapper.selectPermission(new NewEntity01SearchFilter().getPage(),selectCond);
    }

 }


