package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.Ibz03;
import com.ibz.demo.slmk.service.dto.Ibz03SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Ibz03] 服务对象接口
 */
public interface Ibz03Service extends IService<Ibz03>{

	boolean remove(Ibz03 et);
	boolean update(Ibz03 et);
	boolean create(Ibz03 et);
	boolean checkKey(Ibz03 et);
	boolean getDraft(Ibz03 et);
	Ibz03 get(Ibz03 et);
    List<Ibz03> listDefault(Ibz03SearchFilter filter) ;
	Page<Ibz03> searchDefault(Ibz03SearchFilter filter);
	/**	 实体[Ibz03] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
