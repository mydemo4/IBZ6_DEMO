package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.ZPCENTITY2;
import com.ibz.demo.slmk.service.dto.ZPCENTITY2SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ZPCENTITY2] 服务对象接口
 */
public interface ZPCENTITY2Service extends IService<ZPCENTITY2>{

	boolean checkKey(ZPCENTITY2 et);
	boolean update(ZPCENTITY2 et);
	boolean remove(ZPCENTITY2 et);
	boolean create(ZPCENTITY2 et);
	ZPCENTITY2 get(ZPCENTITY2 et);
	boolean getDraft(ZPCENTITY2 et);
    List<ZPCENTITY2> listDefault(ZPCENTITY2SearchFilter filter) ;
	Page<ZPCENTITY2> searchDefault(ZPCENTITY2SearchFilter filter);
	/**	 实体[ZPCENTITY2] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
