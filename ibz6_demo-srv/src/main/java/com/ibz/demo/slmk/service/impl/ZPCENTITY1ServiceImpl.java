
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.ZPCENTITY1Mapper;
import com.ibz.demo.slmk.domain.ZPCENTITY1;
import com.ibz.demo.slmk.service.ZPCENTITY1Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.ZPCENTITY1SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ZPCENTITY1] 服务对象接口实现
 */
@Service(value="ZPCENTITY1ServiceImpl")
public class ZPCENTITY1ServiceImpl extends ServiceImplBase
<ZPCENTITY1Mapper, ZPCENTITY1> implements ZPCENTITY1Service{


    @Resource
    private ZPCENTITY1Mapper zpcentity1Mapper;

    @Override
    public boolean create(ZPCENTITY1 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(ZPCENTITY1 et){
        this.beforeUpdate(et);
		this.zpcentity1Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ZPCENTITY1 et){
        this.zpcentity1Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ZPCENTITY1 et){
		return et.getZpcentity1id();
    }
    @Override
    public boolean getDraft(ZPCENTITY1 et)  {
            return true;
    }







    @Override
	public List<ZPCENTITY1> listDefault(ZPCENTITY1SearchFilter filter) {
		return zpcentity1Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ZPCENTITY1> searchDefault(ZPCENTITY1SearchFilter filter) {
		return zpcentity1Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return zpcentity1Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return zpcentity1Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return zpcentity1Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return zpcentity1Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ZPCENTITY1> selectPermission(QueryWrapper<ZPCENTITY1> selectCond) {
        return zpcentity1Mapper.selectPermission(new ZPCENTITY1SearchFilter().getPage(),selectCond);
    }

 }


