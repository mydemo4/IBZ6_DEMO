package com.ibz.demo.slmk.codelist;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Component;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.CodeList;
import com.ibz.demo.ibizutil.domain.CodeItem;

import com.ibz.demo.ysmk.service.DepartmentService;
/**
 * 代码表[动态代码表测试]
 */
@Component("IBZ6_DEMO_CodeListTestCodeList")
public class CodeListTestCodeList extends com.ibz.demo.ibizutil.domain.CodeListBase{
    @PostConstruct
	protected void init()
	{
	}
	@Autowired
	private DepartmentService departmentService;

	protected DepartmentService getDepartmentService(){
		return departmentService;
	}
	@Override
	public void initCodeList()  {
		CodeList codeList = new CodeList();
		codeList.setSrfkey("IBZ6_DEMO_CodeListTest");
		List<CodeItem> codeItemList = new ArrayList<CodeItem>();
	com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter searchFilter = new com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter();
        List<com.ibz.demo.ysmk.domain.Department> pageResult = this.departmentService.listManagement(searchFilter);
		for(com.ibz.demo.ysmk.domain.Department entity : pageResult) {
			CodeItem item = new CodeItem();
			String strId = entity.getDepartmentid();
			item.setValue(strId);
			item.setId(strId);
			String strText = entity.getDepartmentname();
			item.setText(strText);
			item.setLabel(strText);
			codeItemList.add(item);
			codeList.getCodeItemModelMap().put(strId,item);
		}
		codeList.setItems(codeItemList.toArray(new CodeItem[codeItemList.size()]));
        this.setCodeList(codeList);
    }
}