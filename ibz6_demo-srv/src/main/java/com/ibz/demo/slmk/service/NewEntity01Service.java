package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.NewEntity01;
import com.ibz.demo.slmk.service.dto.NewEntity01SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[NewEntity01] 服务对象接口
 */
public interface NewEntity01Service extends IService<NewEntity01>{

	boolean create(NewEntity01 et);
	NewEntity01 get(NewEntity01 et);
	boolean update(NewEntity01 et);
	boolean checkKey(NewEntity01 et);
	boolean remove(NewEntity01 et);
	boolean getDraft(NewEntity01 et);
    List<NewEntity01> listDefault(NewEntity01SearchFilter filter) ;
	Page<NewEntity01> searchDefault(NewEntity01SearchFilter filter);
	/**	 实体[NewEntity01] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
