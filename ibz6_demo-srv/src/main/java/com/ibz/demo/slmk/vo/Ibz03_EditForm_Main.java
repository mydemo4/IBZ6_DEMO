package com.ibz.demo.slmk.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.slmk.domain.Ibz03;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ibz03_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String ibz03id;
    @JsonIgnore
    private String ibz03name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String dj;
    @JsonIgnore
    private String number;
    @JsonIgnore
    private String starttime;
    @JsonIgnore
    private String createman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[示例实体03标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return ibz03id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String ibz03id){
        this.ibz03id = ibz03id;
    }
    @Size(min = 0, max = 200, message = "[示例实体03名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return ibz03name;
    }
    @NotBlank(message = "[示例实体03名称]不允许为空!")
    @Size(min = 0, max = 200, message = "[示例实体03名称]长度必须在[200]以内!")
    @JsonProperty(value = "ibz03name")
    public String getIbz03name(){
        return ibz03name;
    }
    @JsonProperty(value = "ibz03name")
    public void setIbz03name(String ibz03name){
        this.ibz03name = ibz03name;
    }
    @NotBlank(message = "[单价]不允许为空!")
    @Size(min = 0, max = 100, message = "[单价]长度必须在[100]以内!")
    @JsonProperty(value = "dj")
    public String getDj(){
        return dj;
    }
    @JsonProperty(value = "dj")
    public void setDj(String dj){
        this.dj = dj;
    }
    @Size(min = 0, max = 100, message = "[数量]长度必须在[100]以内!")
    @JsonProperty(value = "number")
    public String getNumber(){
        return number;
    }
    @JsonProperty(value = "number")
    public void setNumber(String number){
        this.number = number;
    }
    @Size(min = 0, max = 100, message = "[开始时间]长度必须在[100]以内!")
    @JsonProperty(value = "starttime")
    public String getStarttime(){
        return starttime;
    }
    @JsonProperty(value = "starttime")
    public void setStarttime(String starttime){
        this.starttime = starttime;
    }
    @NotBlank(message = "[建立人]不允许为空!")
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @Size(min = 0, max = 100, message = "[示例实体03标识]长度必须在[100]以内!")
    @JsonProperty(value = "ibz03id")
    public String getIbz03id(){
        return ibz03id;
    }
    @JsonProperty(value = "ibz03id")
    public void setIbz03id(String ibz03id){
        this.ibz03id = ibz03id;
    }
    public  void fromIbz03(Ibz03 sourceEntity)  {
        this.fromIbz03(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromIbz03(Ibz03 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(Ibz03.class, Ibz03_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  Ibz03 toIbz03()  {
        Ibz03 targetEntity =new Ibz03();
        BeanCopier copier=BeanCopier.create(Ibz03_EditForm_Main.class, Ibz03.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
