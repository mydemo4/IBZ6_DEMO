package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.IBZ_test;
import com.ibz.demo.slmk.service.dto.IBZ_testSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[IBZ_test] 服务对象接口
 */
public interface IBZ_testService extends IService<IBZ_test>{

	boolean update(IBZ_test et);
	IBZ_test get(IBZ_test et);
	boolean checkKey(IBZ_test et);
	boolean getDraft(IBZ_test et);
	boolean create(IBZ_test et);
	boolean remove(IBZ_test et);
    List<IBZ_test> listDefault(IBZ_testSearchFilter filter) ;
	Page<IBZ_test> searchDefault(IBZ_testSearchFilter filter);
	/**	 实体[IBZ_test] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
