
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.ZPCENTITY2Mapper;
import com.ibz.demo.slmk.domain.ZPCENTITY2;
import com.ibz.demo.slmk.service.ZPCENTITY2Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.ZPCENTITY2SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ZPCENTITY2] 服务对象接口实现
 */
@Service(value="ZPCENTITY2ServiceImpl")
public class ZPCENTITY2ServiceImpl extends ServiceImplBase
<ZPCENTITY2Mapper, ZPCENTITY2> implements ZPCENTITY2Service{


    @Resource
    private ZPCENTITY2Mapper zpcentity2Mapper;

    @Override
    public boolean create(ZPCENTITY2 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(ZPCENTITY2 et){
        this.beforeUpdate(et);
		this.zpcentity2Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ZPCENTITY2 et){
        this.zpcentity2Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ZPCENTITY2 et){
		return et.getZpcentity2id();
    }
    @Override
    public boolean getDraft(ZPCENTITY2 et)  {
            return true;
    }







    @Override
	public List<ZPCENTITY2> listDefault(ZPCENTITY2SearchFilter filter) {
		return zpcentity2Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ZPCENTITY2> searchDefault(ZPCENTITY2SearchFilter filter) {
		return zpcentity2Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return zpcentity2Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return zpcentity2Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return zpcentity2Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return zpcentity2Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ZPCENTITY2> selectPermission(QueryWrapper<ZPCENTITY2> selectCond) {
        return zpcentity2Mapper.selectPermission(new ZPCENTITY2SearchFilter().getPage(),selectCond);
    }

 }


