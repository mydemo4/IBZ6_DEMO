package com.ibz.demo.slmk.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.slmk.domain.Ibz03;
import com.ibz.demo.slmk.service.dto.Ibz03SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Ibz03Mapper extends BaseMapper<Ibz03>{

    List<Ibz03> searchDefault(@Param("srf") Ibz03SearchFilter searchfilter,@Param("ew") Wrapper<Ibz03> wrapper) ;
    Page<Ibz03> searchDefault(IPage page, @Param("srf") Ibz03SearchFilter searchfilter, @Param("ew") Wrapper<Ibz03> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'Ibz03:'+#p0")
    Ibz03 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Ibz03:'+#p0.ibz03id")
    int insert(Ibz03 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Ibz03:'+#p0.ibz03id")
    int updateById(@Param(Constants.ENTITY) Ibz03 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Ibz03:'+#p0")
    int deleteById(Serializable id);
    Page<Ibz03> selectPermission(IPage page,@Param("pw") Wrapper<Ibz03> wrapper) ;

    //直接sql查询。
    List<Ibz03> executeRawSql(@Param("sql")String sql);
}
