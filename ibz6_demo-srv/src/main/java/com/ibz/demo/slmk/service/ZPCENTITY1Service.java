package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.ZPCENTITY1;
import com.ibz.demo.slmk.service.dto.ZPCENTITY1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ZPCENTITY1] 服务对象接口
 */
public interface ZPCENTITY1Service extends IService<ZPCENTITY1>{

	ZPCENTITY1 get(ZPCENTITY1 et);
	boolean getDraft(ZPCENTITY1 et);
	boolean checkKey(ZPCENTITY1 et);
	boolean update(ZPCENTITY1 et);
	boolean create(ZPCENTITY1 et);
	boolean remove(ZPCENTITY1 et);
    List<ZPCENTITY1> listDefault(ZPCENTITY1SearchFilter filter) ;
	Page<ZPCENTITY1> searchDefault(ZPCENTITY1SearchFilter filter);
	/**	 实体[ZPCENTITY1] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
