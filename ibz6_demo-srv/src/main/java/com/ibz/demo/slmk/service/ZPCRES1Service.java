package com.ibz.demo.slmk.service;

import com.ibz.demo.slmk.domain.ZPCRES1;
import com.ibz.demo.slmk.service.dto.ZPCRES1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ZPCRES1] 服务对象接口
 */
public interface ZPCRES1Service extends IService<ZPCRES1>{

	boolean create(ZPCRES1 et);
	ZPCRES1 get(ZPCRES1 et);
	boolean update(ZPCRES1 et);
	boolean remove(ZPCRES1 et);
	boolean checkKey(ZPCRES1 et);
	boolean getDraft(ZPCRES1 et);
    List<ZPCRES1> listDefault(ZPCRES1SearchFilter filter) ;
	Page<ZPCRES1> searchDefault(ZPCRES1SearchFilter filter);
	/**	 实体[ZPCRES1] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
