package com.ibz.demo.slmk.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.cglib.beans.BeanGenerator;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import java.util.Map;
import java.util.HashMap;
import org.springframework.util.StringUtils;
//import com.ibz.demo.slmk.valuerule.anno.zpcentity2.*;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import org.springframework.cglib.beans.BeanCopier;
import com.ibz.demo.ibizutil.annotation.Dict;
import com.ibz.demo.ibizutil.domain.EntityBase;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.enums.FillMode;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * 实体[ZPCENTITY2] 数据对象
 */
@TableName(value = "T_ZPCENTITY2",resultMap = "ZPCENTITY2ResultMap")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ZPCENTITY2 extends EntityBase implements Serializable{

    @TableId(value= "zpcentity2id",type=IdType.UUID)//指定主键生成策略
    private String zpcentity2id;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createdate;
    @Dict(dictName = "IBZ6_DEMO_YesNoCodeList")
    @TableLogic(value= "1",delval="0")//设定逻辑有效属性
    @TableField(fill= FieldFill.INSERT)
    private Integer enable;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String updateman;
    private String zpcentity2name;
    @PreField(fill= FillMode.INSERT,preType = PredefinedType.CREATEMAN)
    @Dict(dictName = "IBZ6_DEMO_SysOperatorCodeList")
    private String createman;
    @PreField(fill= FillMode.INSERT_UPDATE,preType = PredefinedType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
     /**
     * 复制当前对象数据到目标对象(粘贴重置)
     *
     * @throws Exception
     */
    public ZPCENTITY2 copyTo(ZPCENTITY2 targetEntity)
	{
		BeanCopier copier=BeanCopier.create(ZPCENTITY2.class, ZPCENTITY2.class, false);
		copier.copy(this, targetEntity, null);
        targetEntity.setZpcentity2id(null);
		return targetEntity;
	}
}
