
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.IBZ_testMapper;
import com.ibz.demo.slmk.domain.IBZ_test;
import com.ibz.demo.slmk.service.IBZ_testService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.IBZ_testSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[IBZ_test] 服务对象接口实现
 */
@Service(value="IBZ_testServiceImpl")
public class IBZ_testServiceImpl extends ServiceImplBase
<IBZ_testMapper, IBZ_test> implements IBZ_testService{


    @Resource
    private IBZ_testMapper ibz_testMapper;

    @Override
    public boolean create(IBZ_test et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(IBZ_test et){
        this.beforeUpdate(et);
		this.ibz_testMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(IBZ_test et){
        this.ibz_testMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(IBZ_test et){
		return et.getIbz_testid();
    }
    @Override
    public boolean getDraft(IBZ_test et)  {
            return true;
    }







    @Override
	public List<IBZ_test> listDefault(IBZ_testSearchFilter filter) {
		return ibz_testMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<IBZ_test> searchDefault(IBZ_testSearchFilter filter) {
		return ibz_testMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return ibz_testMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return ibz_testMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return ibz_testMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return ibz_testMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<IBZ_test> selectPermission(QueryWrapper<IBZ_test> selectCond) {
        return ibz_testMapper.selectPermission(new IBZ_testSearchFilter().getPage(),selectCond);
    }

 }


