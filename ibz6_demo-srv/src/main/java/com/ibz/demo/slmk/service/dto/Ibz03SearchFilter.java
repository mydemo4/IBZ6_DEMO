package com.ibz.demo.slmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.slmk.domain.Ibz03;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Ibz03SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<Ibz03> selectCond;
	public Ibz03SearchFilter(){
		this.page =new Page<Ibz03>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<Ibz03>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("ibz03name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_ibz03name_like;//[示例实体03名称]
	public void setN_ibz03name_like(String n_ibz03name_like) {
        this.n_ibz03name_like = n_ibz03name_like;
        if(!StringUtils.isEmpty(this.n_ibz03name_like)){
            this.selectCond.like("ibz03name", n_ibz03name_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
