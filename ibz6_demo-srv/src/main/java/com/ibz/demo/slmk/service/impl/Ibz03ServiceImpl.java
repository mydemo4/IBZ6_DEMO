
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.Ibz03Mapper;
import com.ibz.demo.slmk.domain.Ibz03;
import com.ibz.demo.slmk.service.Ibz03Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.Ibz03SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Ibz03] 服务对象接口实现
 */
@Service(value="Ibz03ServiceImpl")
public class Ibz03ServiceImpl extends ServiceImplBase
<Ibz03Mapper, Ibz03> implements Ibz03Service{


    @Resource
    private Ibz03Mapper ibz03Mapper;

    @Override
    public boolean create(Ibz03 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Ibz03 et){
        this.beforeUpdate(et);
		this.ibz03Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Ibz03 et){
        this.ibz03Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Ibz03 et){
		return et.getIbz03id();
    }
    @Override
    public boolean getDraft(Ibz03 et)  {
            return true;
    }







    @Override
	public List<Ibz03> listDefault(Ibz03SearchFilter filter) {
		return ibz03Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Ibz03> searchDefault(Ibz03SearchFilter filter) {
		return ibz03Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return ibz03Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return ibz03Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return ibz03Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return ibz03Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Ibz03> selectPermission(QueryWrapper<Ibz03> selectCond) {
        return ibz03Mapper.selectPermission(new Ibz03SearchFilter().getPage(),selectCond);
    }

 }


