package com.ibz.demo.slmk.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.slmk.domain.ZPCRES1;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZPCRES1SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<ZPCRES1> selectCond;
	public ZPCRES1SearchFilter(){
		this.page =new Page<ZPCRES1>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<ZPCRES1>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_ZPCRES1_ZPCENTITY2_ZPCENTITY2ID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_zpcentity2id_eq("NA");
            } else {
                this.setN_zpcentity2id_eq(strParentkey);
            }
        }
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_ZPCRES1_ZPCENTITY1_ZPCENTITY1ID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_zpcentity1id_eq("NA");
            } else {
                this.setN_zpcentity1id_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("zpcres1name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_zpcres1name_like;//[zpc关系1名称]
	public void setN_zpcres1name_like(String n_zpcres1name_like) {
        this.n_zpcres1name_like = n_zpcres1name_like;
        if(!StringUtils.isEmpty(this.n_zpcres1name_like)){
            this.selectCond.like("zpcres1name", n_zpcres1name_like);
        }
    }
	private String n_zpcentity2id_eq;//[zpc实体2标识]
	public void setN_zpcentity2id_eq(String n_zpcentity2id_eq) {
        this.n_zpcentity2id_eq = n_zpcentity2id_eq;
        if(!StringUtils.isEmpty(this.n_zpcentity2id_eq)){
            this.selectCond.eq("zpcentity2id", n_zpcentity2id_eq);
        }
    }
	private String n_zpcentity1id_eq;//[zpc实体1标识]
	public void setN_zpcentity1id_eq(String n_zpcentity1id_eq) {
        this.n_zpcentity1id_eq = n_zpcentity1id_eq;
        if(!StringUtils.isEmpty(this.n_zpcentity1id_eq)){
            this.selectCond.eq("zpcentity1id", n_zpcentity1id_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
