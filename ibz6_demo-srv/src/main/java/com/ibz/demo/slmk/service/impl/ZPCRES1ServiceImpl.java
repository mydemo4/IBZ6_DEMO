
package com.ibz.demo.slmk.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.slmk.mapper.ZPCRES1Mapper;
import com.ibz.demo.slmk.domain.ZPCRES1;
import com.ibz.demo.slmk.service.ZPCRES1Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.slmk.service.dto.ZPCRES1SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.slmk.service.ZPCENTITY1Service;
import com.ibz.demo.slmk.service.ZPCENTITY2Service;

/**
 * 实体[ZPCRES1] 服务对象接口实现
 */
@Service(value="ZPCRES1ServiceImpl")
public class ZPCRES1ServiceImpl extends ServiceImplBase
<ZPCRES1Mapper, ZPCRES1> implements ZPCRES1Service{


    @Autowired
    private ZPCENTITY1Service zpcentity1Service;
    @Autowired
    private ZPCENTITY2Service zpcentity2Service;
    @Resource
    private ZPCRES1Mapper zpcres1Mapper;

    @Override
    public boolean create(ZPCRES1 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(ZPCRES1 et){
        this.beforeUpdate(et);
		this.zpcres1Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ZPCRES1 et){
        this.zpcres1Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ZPCRES1 et){
		return et.getZpcres1id();
    }
    @Override
    public boolean getDraft(ZPCRES1 et)  {
            return true;
    }







    @Override
	public List<ZPCRES1> listDefault(ZPCRES1SearchFilter filter) {
		return zpcres1Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ZPCRES1> searchDefault(ZPCRES1SearchFilter filter) {
		return zpcres1Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return zpcres1Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return zpcres1Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return zpcres1Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return zpcres1Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ZPCRES1> selectPermission(QueryWrapper<ZPCRES1> selectCond) {
        return zpcres1Mapper.selectPermission(new ZPCRES1SearchFilter().getPage(),selectCond);
    }

 }


