package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.Employee;
import com.ibz.demo.testb.service.dto.EmployeeSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Employee] 服务对象接口
 */
public interface EmployeeService extends IService<Employee>{

	boolean remove(Employee et);
	boolean checkKey(Employee et);
	boolean update(Employee et);
	boolean create(Employee et);
	Employee get(Employee et);
	boolean getDraft(Employee et);
    List<Employee> listDefault(EmployeeSearchFilter filter) ;
	Page<Employee> searchDefault(EmployeeSearchFilter filter);
	/**	 实体[Employee] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
