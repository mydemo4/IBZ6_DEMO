package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.DataEntity;
import com.ibz.demo.testb.service.dto.DataEntitySearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[DataEntity] 服务对象接口
 */
public interface DataEntityService extends IService<DataEntity>{

	boolean getDraft(DataEntity et);
	boolean checkKey(DataEntity et);
	boolean create(DataEntity et);
	DataEntity get(DataEntity et);
	boolean update(DataEntity et);
	boolean remove(DataEntity et);
    List<DataEntity> listDefault(DataEntitySearchFilter filter) ;
	Page<DataEntity> searchDefault(DataEntitySearchFilter filter);
    List<DataEntity> listSelectDEAll(DataEntitySearchFilter filter) ;
	Page<DataEntity> searchSelectDEAll(DataEntitySearchFilter filter);
	/**	 实体[DataEntity] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
