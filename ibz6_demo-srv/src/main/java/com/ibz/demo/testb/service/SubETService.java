package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.SubET;
import com.ibz.demo.testb.service.dto.SubETSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[SubET] 服务对象接口
 */
public interface SubETService extends IService<SubET>{

	boolean update(SubET et);
	boolean checkKey(SubET et);
	boolean remove(SubET et);
	boolean create(SubET et);
	boolean getDraft(SubET et);
	SubET get(SubET et);
    List<SubET> listDefault(SubETSearchFilter filter) ;
	Page<SubET> searchDefault(SubETSearchFilter filter);
	/**	 实体[SubET] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
