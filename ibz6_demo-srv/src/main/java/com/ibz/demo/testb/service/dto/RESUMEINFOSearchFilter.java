package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.RESUMEINFO;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RESUMEINFOSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<RESUMEINFO> selectCond;
	public RESUMEINFOSearchFilter(){
		this.page =new Page<RESUMEINFO>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<RESUMEINFO>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_RESUMEINFO_EMPLOYEE_EMPLOYEEID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_employeeid_eq("NA");
            } else {
                this.setN_employeeid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("resumeinfoname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_resumeinfoname_like;//[履历信息名称]
	public void setN_resumeinfoname_like(String n_resumeinfoname_like) {
        this.n_resumeinfoname_like = n_resumeinfoname_like;
        if(!StringUtils.isEmpty(this.n_resumeinfoname_like)){
            this.selectCond.like("resumeinfoname", n_resumeinfoname_like);
        }
    }
	private String n_employeeid_eq;//[员工信息标识]
	public void setN_employeeid_eq(String n_employeeid_eq) {
        this.n_employeeid_eq = n_employeeid_eq;
        if(!StringUtils.isEmpty(this.n_employeeid_eq)){
            this.selectCond.eq("employeeid", n_employeeid_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
