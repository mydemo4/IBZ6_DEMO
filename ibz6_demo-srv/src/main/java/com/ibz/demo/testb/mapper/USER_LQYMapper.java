package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.USER_LQY;
import com.ibz.demo.testb.service.dto.USER_LQYSearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface USER_LQYMapper extends BaseMapper<USER_LQY>{

    List<USER_LQY> searchDefault(@Param("srf") USER_LQYSearchFilter searchfilter,@Param("ew") Wrapper<USER_LQY> wrapper) ;
    Page<USER_LQY> searchDefault(IPage page, @Param("srf") USER_LQYSearchFilter searchfilter, @Param("ew") Wrapper<USER_LQY> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'USER_LQY:'+#p0")
    USER_LQY selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'USER_LQY:'+#p0.user_lqyid")
    int insert(USER_LQY entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'USER_LQY:'+#p0.user_lqyid")
    int updateById(@Param(Constants.ENTITY) USER_LQY entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'USER_LQY:'+#p0")
    int deleteById(Serializable id);
    Page<USER_LQY> selectPermission(IPage page,@Param("pw") Wrapper<USER_LQY> wrapper) ;

    //直接sql查询。
    List<USER_LQY> executeRawSql(@Param("sql")String sql);
}
