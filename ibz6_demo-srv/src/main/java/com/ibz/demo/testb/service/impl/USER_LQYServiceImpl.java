
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.USER_LQYMapper;
import com.ibz.demo.testb.domain.USER_LQY;
import com.ibz.demo.testb.service.USER_LQYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.USER_LQYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.CLIENTENTITYService;
import com.ibz.demo.testb.service.OrgDeptService;
import com.ibz.demo.testb.service.OrgService;

/**
 * 实体[USER_LQY] 服务对象接口实现
 */
@Service(value="USER_LQYServiceImpl")
public class USER_LQYServiceImpl extends ServiceImplBase
<USER_LQYMapper, USER_LQY> implements USER_LQYService{


    @Autowired
    private CLIENTENTITYService cliententityService;
    @Autowired
    private OrgDeptService orgdeptService;
    @Autowired
    private OrgService orgService;
    @Resource
    private USER_LQYMapper user_lqyMapper;

    @Override
    public boolean create(USER_LQY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(USER_LQY et){
        this.beforeUpdate(et);
		this.user_lqyMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(USER_LQY et){
        this.user_lqyMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(USER_LQY et){
		return et.getUser_lqyid();
    }
    @Override
    public boolean getDraft(USER_LQY et)  {
            return true;
    }

    @Override
    public USER_LQY userAPI(USER_LQY et)  {
		return et;
    }







    @Override
	public List<USER_LQY> listDefault(USER_LQYSearchFilter filter) {
		return user_lqyMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<USER_LQY> searchDefault(USER_LQYSearchFilter filter) {
		return user_lqyMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return user_lqyMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return user_lqyMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return user_lqyMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return user_lqyMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<USER_LQY> selectPermission(QueryWrapper<USER_LQY> selectCond) {
        return user_lqyMapper.selectPermission(new USER_LQYSearchFilter().getPage(),selectCond);
    }

 }


