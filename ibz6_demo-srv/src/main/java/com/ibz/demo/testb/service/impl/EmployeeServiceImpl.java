
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.EmployeeMapper;
import com.ibz.demo.testb.domain.Employee;
import com.ibz.demo.testb.service.EmployeeService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.EmployeeSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Employee] 服务对象接口实现
 */
@Service(value="EmployeeServiceImpl")
public class EmployeeServiceImpl extends ServiceImplBase
<EmployeeMapper, Employee> implements EmployeeService{


    @Resource
    private EmployeeMapper employeeMapper;

    @Override
    public boolean create(Employee et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Employee et){
        this.beforeUpdate(et);
		this.employeeMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Employee et){
        this.employeeMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Employee et){
		return et.getEmployeeid();
    }
    @Override
    public boolean getDraft(Employee et)  {
            return true;
    }







    @Override
	public List<Employee> listDefault(EmployeeSearchFilter filter) {
		return employeeMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Employee> searchDefault(EmployeeSearchFilter filter) {
		return employeeMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return employeeMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return employeeMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return employeeMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return employeeMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Employee> selectPermission(QueryWrapper<Employee> selectCond) {
        return employeeMapper.selectPermission(new EmployeeSearchFilter().getPage(),selectCond);
    }

 }


