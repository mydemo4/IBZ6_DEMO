package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.Testentity1;
import com.ibz.demo.testb.service.dto.Testentity1SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Testentity1Mapper extends BaseMapper<Testentity1>{

    List<Testentity1> searchScopeTest(@Param("srf") Testentity1SearchFilter searchfilter,@Param("ew") Wrapper<Testentity1> wrapper) ;
    Page<Testentity1> searchScopeTest(IPage page, @Param("srf") Testentity1SearchFilter searchfilter, @Param("ew") Wrapper<Testentity1> wrapper) ;
    List<Testentity1> searchDEFAULT2(@Param("srf") Testentity1SearchFilter searchfilter,@Param("ew") Wrapper<Testentity1> wrapper) ;
    Page<Testentity1> searchDEFAULT2(IPage page, @Param("srf") Testentity1SearchFilter searchfilter, @Param("ew") Wrapper<Testentity1> wrapper) ;
    List<Testentity1> searchDefault(@Param("srf") Testentity1SearchFilter searchfilter,@Param("ew") Wrapper<Testentity1> wrapper) ;
    Page<Testentity1> searchDefault(IPage page, @Param("srf") Testentity1SearchFilter searchfilter, @Param("ew") Wrapper<Testentity1> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'Testentity1:'+#p0")
    Testentity1 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Testentity1:'+#p0.testentity1id")
    int insert(Testentity1 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Testentity1:'+#p0.testentity1id")
    int updateById(@Param(Constants.ENTITY) Testentity1 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'Testentity1:'+#p0")
    int deleteById(Serializable id);
    Page<Testentity1> selectPermission(IPage page,@Param("pw") Wrapper<Testentity1> wrapper) ;

    //直接sql查询。
    List<Testentity1> executeRawSql(@Param("sql")String sql);
}
