package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.DBEntity3;
import com.ibz.demo.testb.service.dto.DBEntity3SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[DBEntity3] 服务对象接口
 */
public interface DBEntity3Service extends IService<DBEntity3>{

	DBEntity3 get(DBEntity3 et);
	boolean create(DBEntity3 et);
	boolean getDraft(DBEntity3 et);
	boolean remove(DBEntity3 et);
	boolean update(DBEntity3 et);
	boolean checkKey(DBEntity3 et);
    List<DBEntity3> listDefault(DBEntity3SearchFilter filter) ;
	Page<DBEntity3> searchDefault(DBEntity3SearchFilter filter);
	/**	 实体[DBEntity3] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
