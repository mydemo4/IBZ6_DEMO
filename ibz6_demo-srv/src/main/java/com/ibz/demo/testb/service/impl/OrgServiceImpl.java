package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.beans.BeanMap;
import java.util.*;
import java.io.Serializable;
import javax.annotation.Resource;
import com.ibz.demo.ibizutil.center.AbstractCenter;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import com.github.javaparser.utils.Log;
import org.springframework.http.HttpMethod;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.domain.Org;
import com.ibz.demo.testb.service.OrgService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.OrgSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Org] 服务对象接口实现
 */
@Service(value="OrgServiceImpl")
public class OrgServiceImpl extends AbstractCenter implements OrgService{

    public boolean update(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI("/org",de,et.getClass(),HttpMethod.PUT);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean checkKey(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/org/{orgid}/checkkey",et.getOrgid()),de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean save(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI("/org/save",de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public Org get(Org et){
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
        Org result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/org/{orgid}",et.getOrgid()),de,et.getClass(),HttpMethod.GET);
        if(result!=null){
            BeanCopier copier=BeanCopier.create(result.getClass(),et.getClass(), false);
            copier.copy(result,et,null);
        }
        return et;
    }
    public boolean remove(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI("/org",de,et.getClass(),HttpMethod.DELETE);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean create(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI("/org",de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean getDraft(Org et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                Org result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/org/{orgid}/getdraft",et.getOrgid()),de,et.getClass(),HttpMethod.GET);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public List<Org> listDefault(OrgSearchFilter filter){
        JSONObject jsonfilter =new JSONObject();
        BeanMap beanMap = BeanMap.create(filter);
        jsonfilter.putAll(beanMap);
        jsonfilter.remove("datacontext");
        jsonfilter.remove("permissionCond");
        jsonfilter.remove("selectCond");
                        Org[] arr = callServiceAPI("http://172.16.103.29:8082/rest/orgrestapi/org/search",jsonfilter,Org[].class,HttpMethod.POST);
        List<Org> list = Arrays.asList(arr);
		return list;
    }
	public Page<Org> searchDefault(OrgSearchFilter filter){
	    JSONObject jsonfilter =new JSONObject();
        BeanMap beanMap = BeanMap.create(filter);
        jsonfilter.putAll(beanMap);
        jsonfilter.remove("datacontext");
        jsonfilter.remove("permissionCond");
        jsonfilter.remove("selectCond");
        Page<Org> pages = new Page<>();
        Org[] arr = callServiceAPI("http://172.16.103.29:8082/rest/orgrestapi/org/search",jsonfilter,Org[].class,HttpMethod.POST);
		List<Org> list = Arrays.asList(arr);
        pages.setRecords(list);
		return pages;
	}
    public Org getById(Serializable id) throws Exception {
        if(id==null){
            Log.error("参数异常："+id);
            throw new Exception();
        }
        Org et = new Org();
        et.setOrgid(id.toString());
        return get(et);
    }

    public boolean saveBatch(Collection<Org> entityList, int batchSize){
        return false;
    }
    protected void beforeCreate(Org et){

    }
    protected void beforeUpdate(Org et){

    }
}



