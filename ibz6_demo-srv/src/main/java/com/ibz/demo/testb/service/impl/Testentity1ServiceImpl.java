
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.Testentity1Mapper;
import com.ibz.demo.testb.domain.Testentity1;
import com.ibz.demo.testb.service.Testentity1Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.Testentity1SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.Security1Service;

/**
 * 实体[Testentity1] 服务对象接口实现
 */
@Service(value="Testentity1ServiceImpl")
public class Testentity1ServiceImpl extends ServiceImplBase
<Testentity1Mapper, Testentity1> implements Testentity1Service{


    @Autowired
    private Security1Service security1Service;
    @Resource
    private Testentity1Mapper testentity1Mapper;

    @Override
    public boolean create(Testentity1 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Testentity1 et){
        this.beforeUpdate(et);
		this.testentity1Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Testentity1 et){
        this.testentity1Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Testentity1 et){
		return et.getTestentity1id();
    }
    @Override
    public boolean getDraft(Testentity1 et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getSecurity1id())) && et.getSecurity1name() == null){
                try {
                    et.setSecurity1name(security1Service.getById(et.getSecurity1id()).getSecurity1name());
                }catch (Exception ex){}
            }
            return true;
    }







    @Override
	public List<Testentity1> listScopeTest(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchScopeTest(filter,filter.getSelectCond());
	}
    @Override
	public Page<Testentity1> searchScopeTest(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchScopeTest(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Testentity1> listDEFAULT2(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchDEFAULT2(filter,filter.getSelectCond());
	}
    @Override
	public Page<Testentity1> searchDEFAULT2(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchDEFAULT2(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<Testentity1> listDefault(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Testentity1> searchDefault(Testentity1SearchFilter filter) {
		return testentity1Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return testentity1Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return testentity1Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return testentity1Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return testentity1Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    /**
     * 获取实体主状态
     * @param et 业务数据
     * @return
     */
    public String getMajState(Testentity1 et)
    {
        String strMajState="";
        TreeMap
        <String,String> matchMap = new TreeMap<>();
        //输出实体主状态
        if(matchMap.size()>0){
            strMajState=matchMap.get(matchMap.firstKey());
        }
        return strMajState;
    }
    /**
     * 判断是否满足实体主状态
     * @param strMSTag
     * @param et
     * @return
     */
    public boolean isMatchCond(String strMSTag ,Testentity1 et)
    {
        boolean flag=false;
        //list遍历出主状态属性
        String dataState0=et.getField1();
        if(!StringUtils.isEmpty(strMSTag)){
           String deState[]=strMSTag.split("__");
            if(deState.length==1){
                if(dataState0.equals(deState[0]) || (deState[0].equals("*") ) ){
                    flag=true;
                }
            }
        }
         return flag;
    }
     /**
     * 通配符数量
     * @param strMSTag
     * @return
     */
    public String getWildcardCnt(String strMSTag){
        Pattern p = Pattern.compile("\\*");
        Matcher m = p.matcher(strMSTag);
        int i = 0;
        while(m.find()){
            i++;
        }
        return String.valueOf(i);
    }
    @Override
    public Page<Testentity1> selectPermission(QueryWrapper<Testentity1> selectCond) {
        return testentity1Mapper.selectPermission(new Testentity1SearchFilter().getPage(),selectCond);
    }

 }


