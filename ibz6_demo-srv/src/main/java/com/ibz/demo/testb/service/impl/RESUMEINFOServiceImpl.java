
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.RESUMEINFOMapper;
import com.ibz.demo.testb.domain.RESUMEINFO;
import com.ibz.demo.testb.service.RESUMEINFOService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.RESUMEINFOSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.EmployeeService;

/**
 * 实体[RESUMEINFO] 服务对象接口实现
 */
@Service(value="RESUMEINFOServiceImpl")
public class RESUMEINFOServiceImpl extends ServiceImplBase
<RESUMEINFOMapper, RESUMEINFO> implements RESUMEINFOService{


    @Autowired
    private EmployeeService employeeService;
    @Resource
    private RESUMEINFOMapper resumeinfoMapper;

    @Override
    public boolean create(RESUMEINFO et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(RESUMEINFO et){
        this.beforeUpdate(et);
		this.resumeinfoMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(RESUMEINFO et){
        this.resumeinfoMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(RESUMEINFO et){
		return et.getResumeinfoid();
    }
    @Override
    public boolean getDraft(RESUMEINFO et)  {
            return true;
    }







    @Override
	public List<RESUMEINFO> listDefault(RESUMEINFOSearchFilter filter) {
		return resumeinfoMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<RESUMEINFO> searchDefault(RESUMEINFOSearchFilter filter) {
		return resumeinfoMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return resumeinfoMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return resumeinfoMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return resumeinfoMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return resumeinfoMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<RESUMEINFO> selectPermission(QueryWrapper<RESUMEINFO> selectCond) {
        return resumeinfoMapper.selectPermission(new RESUMEINFOSearchFilter().getPage(),selectCond);
    }

 }


