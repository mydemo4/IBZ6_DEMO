package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.testb.service.dto.OrgDeptSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrgDept_SearchForm_Default{

    private DataObj srfparentdata;

    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromOrgDeptSearchFilter(OrgDeptSearchFilter sourceSearchFilter)  {
        this.setPage(sourceSearchFilter.getPage());
	}
    public  OrgDeptSearchFilter toOrgDeptSearchFilter()  {
        OrgDeptSearchFilter targetSearchFilter=new OrgDeptSearchFilter();
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

}
