package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.TestEntity3;
import com.ibz.demo.testb.service.dto.TestEntity3SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[TestEntity3] 服务对象接口
 */
public interface TestEntity3Service extends IService<TestEntity3>{

	boolean update(TestEntity3 et);
	boolean remove(TestEntity3 et);
	boolean create(TestEntity3 et);
	TestEntity3 get(TestEntity3 et);
	boolean getDraft(TestEntity3 et);
	boolean checkKey(TestEntity3 et);
    List<TestEntity3> listDefault(TestEntity3SearchFilter filter) ;
	Page<TestEntity3> searchDefault(TestEntity3SearchFilter filter);
	/**	 实体[TestEntity3] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
