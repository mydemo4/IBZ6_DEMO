package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.testb.domain.TestEntity2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.springframework.cglib.beans.BeanMap;
import com.alibaba.fastjson.JSONObject;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestEntity2_Grid_Main{
    private String testentity2name;
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
    private String srfmajortext;
    private String srfdataaccaction;
    private String srfkey;
    public void fromTestEntity2(TestEntity2 sourceEntity)  {
         this.setTestentity2name(sourceEntity.getTestentity2name());
         this.setUpdateman(sourceEntity.getUpdateman());
         this.setUpdatedate(sourceEntity.getUpdatedate());
         this.setSrfmajortext(sourceEntity.getTestentity2name());
         this.setSrfdataaccaction(sourceEntity.getTestentity2id());
         this.setSrfkey(sourceEntity.getTestentity2id());
	}
	public static Page<TestEntity2_Grid_Main> fromTestEntity2(Page<TestEntity2> sourcePage)  {
        if(sourcePage==null)
            return null;
        Page<TestEntity2_Grid_Main> targetpage=new Page<TestEntity2_Grid_Main>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<TestEntity2_Grid_Main> records=new ArrayList<TestEntity2_Grid_Main>();
        for(TestEntity2 source:sourcePage.getRecords()) {
            TestEntity2_Grid_Main target=new TestEntity2_Grid_Main();
            target.fromTestEntity2(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
    /**
     * 表格模型集合
     * @return
     */
    public static List<Map<String,String>> getGridColumnModels() {
        List columnModelList =new ArrayList();
        columnModelList.add(new HashMap(){ { put("testentity2name","权限测试实体2名称"); }});
        columnModelList.add(new HashMap(){ { put("updateman","更新人"); }});
        columnModelList.add(new HashMap(){ { put("updatedate","更新时间"); }});
        return columnModelList;
    }
    /**
     *将entity转成map
     * @param bean
     * @param <T>
     * @return
     */
    public <T> Map<String,Object> beanToMap(T bean){
        Map map =new HashMap<String,Object>();
        if(bean!=null){
            BeanMap beanMap=BeanMap.create(bean);
            for(Object obj:beanMap.keySet()){
                map.put(obj+"",beanMap.get(obj));
            }
        }
        return map;
    }
    /**
     * 将entity转成map
     * @param searchResult
     * @return
     */
    public static List<Map<String, Object>> pageToListDatas(Page<TestEntity2> searchResult) {
        if(searchResult==null)
            return null;
        List<Map<String, Object>> records=new ArrayList<>();
        for(TestEntity2 source:searchResult.getRecords()) {
            TestEntity2_Grid_Main target=new TestEntity2_Grid_Main();
            records.add(target.beanToMap(source));
        }
        return records;
    }
    /**
     * 输出表格数据导出文件url
     * @param sourcePage
     * @param downloadurl
     * @return
     */
    public static Page<JSONObject> getResultPage(Page<TestEntity2> sourcePage,String downloadurl)  {
        if(sourcePage==null)
            return null;
        Page<JSONObject> targetpage=new Page<JSONObject>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<JSONObject> records=new ArrayList<JSONObject>();
        JSONObject obj =new JSONObject();
        obj.put("downloadurl",downloadurl);
        records.add(obj);
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
