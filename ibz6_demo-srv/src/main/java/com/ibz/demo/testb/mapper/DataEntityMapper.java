package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.DataEntity;
import com.ibz.demo.testb.service.dto.DataEntitySearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface DataEntityMapper extends BaseMapper<DataEntity>{

    List<DataEntity> searchDefault(@Param("srf") DataEntitySearchFilter searchfilter,@Param("ew") Wrapper<DataEntity> wrapper) ;
    Page<DataEntity> searchDefault(IPage page, @Param("srf") DataEntitySearchFilter searchfilter, @Param("ew") Wrapper<DataEntity> wrapper) ;
    List<DataEntity> searchSelectDEAll(@Param("srf") DataEntitySearchFilter searchfilter,@Param("ew") Wrapper<DataEntity> wrapper) ;
    Page<DataEntity> searchSelectDEAll(IPage page, @Param("srf") DataEntitySearchFilter searchfilter, @Param("ew") Wrapper<DataEntity> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'DataEntity:'+#p0")
    DataEntity selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DataEntity:'+#p0.dataentityid")
    int insert(DataEntity entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DataEntity:'+#p0.dataentityid")
    int updateById(@Param(Constants.ENTITY) DataEntity entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DataEntity:'+#p0")
    int deleteById(Serializable id);
    Page<DataEntity> selectPermission(IPage page,@Param("pw") Wrapper<DataEntity> wrapper) ;

    //直接sql查询。
    List<DataEntity> executeRawSql(@Param("sql")String sql);
}
