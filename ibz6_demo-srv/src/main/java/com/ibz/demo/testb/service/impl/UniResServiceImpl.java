
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.UniResMapper;
import com.ibz.demo.testb.domain.UniRes;
import com.ibz.demo.testb.service.UniResService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.UniResSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[UniRes] 服务对象接口实现
 */
@Service(value="UniResServiceImpl")
public class UniResServiceImpl extends ServiceImplBase
<UniResMapper, UniRes> implements UniResService{


    @Resource
    private UniResMapper uniresMapper;

    @Override
    public boolean create(UniRes et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(UniRes et){
        this.beforeUpdate(et);
		this.uniresMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(UniRes et){
        this.uniresMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(UniRes et){
		return et.getUniresid();
    }
    @Override
    public boolean getDraft(UniRes et)  {
            return true;
    }







    @Override
	public List<UniRes> listDefault(UniResSearchFilter filter) {
		return uniresMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<UniRes> searchDefault(UniResSearchFilter filter) {
		return uniresMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<UniRes> listSelectUniresAll(UniResSearchFilter filter) {
		return uniresMapper.searchSelectUniresAll(filter,filter.getSelectCond());
	}
    @Override
	public Page<UniRes> searchSelectUniresAll(UniResSearchFilter filter) {
		return uniresMapper.searchSelectUniresAll(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return uniresMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return uniresMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return uniresMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return uniresMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<UniRes> selectPermission(QueryWrapper<UniRes> selectCond) {
        return uniresMapper.selectPermission(new UniResSearchFilter().getPage(),selectCond);
    }

 }


