package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.ORGDEPTENTITY;
import com.ibz.demo.testb.service.dto.ORGDEPTENTITYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ORGDEPTENTITY] 服务对象接口
 */
public interface ORGDEPTENTITYService extends IService<ORGDEPTENTITY>{

	ORGDEPTENTITY get(ORGDEPTENTITY et);
	boolean getDraft(ORGDEPTENTITY et);
	boolean update(ORGDEPTENTITY et);
	boolean remove(ORGDEPTENTITY et);
	boolean create(ORGDEPTENTITY et);
	boolean checkKey(ORGDEPTENTITY et);
    List<ORGDEPTENTITY> listDefault(ORGDEPTENTITYSearchFilter filter) ;
	Page<ORGDEPTENTITY> searchDefault(ORGDEPTENTITYSearchFilter filter);
	/**	 实体[ORGDEPTENTITY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
