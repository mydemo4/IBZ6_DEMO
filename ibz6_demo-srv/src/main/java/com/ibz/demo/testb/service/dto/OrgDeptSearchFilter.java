package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.OrgDept;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrgDeptSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<OrgDept> selectCond;
	public OrgDeptSearchFilter(){
		this.page =new Page<OrgDept>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<OrgDept>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_ORGDEPT_ORG_ORGID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_orgid_eq("NA");
            } else {
                this.setN_orgid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("orgdeptname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_orgdeptname_like;//[部门名称]
	public void setN_orgdeptname_like(String n_orgdeptname_like) {
        this.n_orgdeptname_like = n_orgdeptname_like;
        if(!StringUtils.isEmpty(this.n_orgdeptname_like)){
            this.selectCond.like("orgdeptname", n_orgdeptname_like);
        }
    }
	private String n_orgid_eq;//[组织标识]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!StringUtils.isEmpty(this.n_orgid_eq)){
            this.selectCond.eq("orgid", n_orgid_eq);
        }
    }
	private String n_orgname_eq;//[组织名称]
	public void setN_orgname_eq(String n_orgname_eq) {
        this.n_orgname_eq = n_orgname_eq;
        if(!StringUtils.isEmpty(this.n_orgname_eq)){
            this.selectCond.eq("orgname", n_orgname_eq);
        }
    }
	private String n_orgname_like;//[组织名称]
	public void setN_orgname_like(String n_orgname_like) {
        this.n_orgname_like = n_orgname_like;
        if(!StringUtils.isEmpty(this.n_orgname_like)){
            this.selectCond.like("orgname", n_orgname_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
