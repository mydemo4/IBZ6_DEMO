package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.USER_LQY;
import com.ibz.demo.testb.service.dto.USER_LQYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[USER_LQY] 服务对象接口
 */
public interface USER_LQYService extends IService<USER_LQY>{

	boolean checkKey(USER_LQY et);
	USER_LQY userAPI(USER_LQY et);
	USER_LQY get(USER_LQY et);
	boolean update(USER_LQY et);
	boolean getDraft(USER_LQY et);
	boolean remove(USER_LQY et);
	boolean create(USER_LQY et);
    List<USER_LQY> listDefault(USER_LQYSearchFilter filter) ;
	Page<USER_LQY> searchDefault(USER_LQYSearchFilter filter);
	/**	 实体[USER_LQY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
