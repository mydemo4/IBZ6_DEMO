package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.DBEntity4;
import com.ibz.demo.testb.service.dto.DBEntity4SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface DBEntity4Mapper extends BaseMapper<DBEntity4>{

    List<DBEntity4> searchDefault(@Param("srf") DBEntity4SearchFilter searchfilter,@Param("ew") Wrapper<DBEntity4> wrapper) ;
    Page<DBEntity4> searchDefault(IPage page, @Param("srf") DBEntity4SearchFilter searchfilter, @Param("ew") Wrapper<DBEntity4> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'DBEntity4:'+#p0")
    DBEntity4 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity4:'+#p0.dbentity4id")
    int insert(DBEntity4 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity4:'+#p0.dbentity4id")
    int updateById(@Param(Constants.ENTITY) DBEntity4 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity4:'+#p0")
    int deleteById(Serializable id);
    Page<DBEntity4> selectPermission(IPage page,@Param("pw") Wrapper<DBEntity4> wrapper) ;

    //直接sql查询。
    List<DBEntity4> executeRawSql(@Param("sql")String sql);
}
