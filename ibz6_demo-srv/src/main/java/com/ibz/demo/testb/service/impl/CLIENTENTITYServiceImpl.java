
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.CLIENTENTITYMapper;
import com.ibz.demo.testb.domain.CLIENTENTITY;
import com.ibz.demo.testb.service.CLIENTENTITYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.CLIENTENTITYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[CLIENTENTITY] 服务对象接口实现
 */
@Service(value="CLIENTENTITYServiceImpl")
public class CLIENTENTITYServiceImpl extends ServiceImplBase
<CLIENTENTITYMapper, CLIENTENTITY> implements CLIENTENTITYService{


    @Resource
    private CLIENTENTITYMapper cliententityMapper;

    @Override
    public boolean create(CLIENTENTITY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(CLIENTENTITY et){
        this.beforeUpdate(et);
		this.cliententityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(CLIENTENTITY et){
        this.cliententityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(CLIENTENTITY et){
		return et.getCliententityid();
    }
    @Override
    public boolean getDraft(CLIENTENTITY et)  {
            return true;
    }







    @Override
	public List<CLIENTENTITY> listDefault(CLIENTENTITYSearchFilter filter) {
		return cliententityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<CLIENTENTITY> searchDefault(CLIENTENTITYSearchFilter filter) {
		return cliententityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return cliententityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return cliententityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return cliententityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return cliententityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<CLIENTENTITY> selectPermission(QueryWrapper<CLIENTENTITY> selectCond) {
        return cliententityMapper.selectPermission(new CLIENTENTITYSearchFilter().getPage(),selectCond);
    }

 }


