package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.Testentity1;
import com.ibz.demo.testb.service.dto.Testentity1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Testentity1] 服务对象接口
 */
public interface Testentity1Service extends IService<Testentity1>{

	boolean checkKey(Testentity1 et);
	boolean update(Testentity1 et);
	Testentity1 get(Testentity1 et);
	boolean remove(Testentity1 et);
	boolean create(Testentity1 et);
	boolean getDraft(Testentity1 et);
    List<Testentity1> listScopeTest(Testentity1SearchFilter filter) ;
	Page<Testentity1> searchScopeTest(Testentity1SearchFilter filter);
    List<Testentity1> listDEFAULT2(Testentity1SearchFilter filter) ;
	Page<Testentity1> searchDEFAULT2(Testentity1SearchFilter filter);
    List<Testentity1> listDefault(Testentity1SearchFilter filter) ;
	Page<Testentity1> searchDefault(Testentity1SearchFilter filter);
	/**	 实体[Testentity1] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
	String getMajState(Testentity1 et);
}
