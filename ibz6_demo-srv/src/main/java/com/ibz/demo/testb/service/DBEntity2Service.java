package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.DBEntity2;
import com.ibz.demo.testb.service.dto.DBEntity2SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[DBEntity2] 服务对象接口
 */
public interface DBEntity2Service extends IService<DBEntity2>{

	boolean checkKey(DBEntity2 et);
	boolean getDraft(DBEntity2 et);
	boolean remove(DBEntity2 et);
	boolean create(DBEntity2 et);
	DBEntity2 get(DBEntity2 et);
	boolean update(DBEntity2 et);
    List<DBEntity2> listDefault(DBEntity2SearchFilter filter) ;
	Page<DBEntity2> searchDefault(DBEntity2SearchFilter filter);
	/**	 实体[DBEntity2] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
