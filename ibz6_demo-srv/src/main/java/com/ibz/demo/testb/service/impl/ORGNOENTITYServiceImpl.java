
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.ORGNOENTITYMapper;
import com.ibz.demo.testb.domain.ORGNOENTITY;
import com.ibz.demo.testb.service.ORGNOENTITYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.ORGNOENTITYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ORGNOENTITY] 服务对象接口实现
 */
@Service(value="ORGNOENTITYServiceImpl")
public class ORGNOENTITYServiceImpl extends ServiceImplBase
<ORGNOENTITYMapper, ORGNOENTITY> implements ORGNOENTITYService{


    @Resource
    private ORGNOENTITYMapper orgnoentityMapper;

    @Override
    public boolean create(ORGNOENTITY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(ORGNOENTITY et){
        this.beforeUpdate(et);
		this.orgnoentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ORGNOENTITY et){
        this.orgnoentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ORGNOENTITY et){
		return et.getOrgnoentityid();
    }
    @Override
    public boolean getDraft(ORGNOENTITY et)  {
            return true;
    }







    @Override
	public List<ORGNOENTITY> listDefault(ORGNOENTITYSearchFilter filter) {
		return orgnoentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ORGNOENTITY> searchDefault(ORGNOENTITYSearchFilter filter) {
		return orgnoentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return orgnoentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return orgnoentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return orgnoentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return orgnoentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ORGNOENTITY> selectPermission(QueryWrapper<ORGNOENTITY> selectCond) {
        return orgnoentityMapper.selectPermission(new ORGNOENTITYSearchFilter().getPage(),selectCond);
    }

 }


