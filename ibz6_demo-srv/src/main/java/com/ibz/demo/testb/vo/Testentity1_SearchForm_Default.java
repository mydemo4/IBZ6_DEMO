package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import com.ibz.demo.testb.service.dto.Testentity1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.StringUtils;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Testentity1_SearchForm_Default{

    private DataObj srfparentdata;

    private String n_security1id_eq;
    private String n_testentity1name_like;
    private Page page;
    private Page getPage(){
        if(this.page==null)
            this.page=new Page(1,20,true);
        return this.page;
    }
    public  void fromTestentity1SearchFilter(Testentity1SearchFilter sourceSearchFilter)  {
        this.setN_security1id_eq(sourceSearchFilter.getN_security1id_eq());
        this.setN_testentity1name_like(sourceSearchFilter.getN_testentity1name_like());
        this.setPage(sourceSearchFilter.getPage());
	}
    public  Testentity1SearchFilter toTestentity1SearchFilter()  {
        Testentity1SearchFilter targetSearchFilter=new Testentity1SearchFilter();
        if(targetSearchFilter.getN_security1id_eq()==null)
            targetSearchFilter.setN_security1id_eq(this.getN_security1id_eq());
        if(targetSearchFilter.getN_testentity1name_like()==null)
            targetSearchFilter.setN_testentity1name_like(this.getN_testentity1name_like());
        targetSearchFilter.setPage(this.getPage());
        return targetSearchFilter;
	}

    //输出获取外键值方法-用于通过parentdata关系填充外键值id
    public String getN_security1id_eq()
    {
        if(StringUtils.isEmpty(n_security1id_eq) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_TESTENTITY1_SECURITY1_SECURITY1ID"))
                this.n_security1id_eq=this.getSrfparentdata().getStringValue("srfparentkey");
        }
                return this.n_security1id_eq;
    }
}
