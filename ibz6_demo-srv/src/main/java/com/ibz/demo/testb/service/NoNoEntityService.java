package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.NoNoEntity;
import com.ibz.demo.testb.service.dto.NoNoEntitySearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[NoNoEntity] 服务对象接口
 */
public interface NoNoEntityService extends IService<NoNoEntity>{

	boolean create(NoNoEntity et);
	boolean update(NoNoEntity et);
	boolean getDraft(NoNoEntity et);
	boolean remove(NoNoEntity et);
	boolean checkKey(NoNoEntity et);
	NoNoEntity get(NoNoEntity et);
    List<NoNoEntity> listDefault(NoNoEntitySearchFilter filter) ;
	Page<NoNoEntity> searchDefault(NoNoEntitySearchFilter filter);
	/**	 实体[NoNoEntity] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
