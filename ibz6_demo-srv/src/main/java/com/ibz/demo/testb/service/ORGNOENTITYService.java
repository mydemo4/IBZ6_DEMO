package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.ORGNOENTITY;
import com.ibz.demo.testb.service.dto.ORGNOENTITYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[ORGNOENTITY] 服务对象接口
 */
public interface ORGNOENTITYService extends IService<ORGNOENTITY>{

	boolean create(ORGNOENTITY et);
	boolean getDraft(ORGNOENTITY et);
	boolean remove(ORGNOENTITY et);
	boolean checkKey(ORGNOENTITY et);
	ORGNOENTITY get(ORGNOENTITY et);
	boolean update(ORGNOENTITY et);
    List<ORGNOENTITY> listDefault(ORGNOENTITYSearchFilter filter) ;
	Page<ORGNOENTITY> searchDefault(ORGNOENTITYSearchFilter filter);
	/**	 实体[ORGNOENTITY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
