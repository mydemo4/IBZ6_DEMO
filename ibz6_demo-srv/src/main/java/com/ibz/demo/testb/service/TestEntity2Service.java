package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.TestEntity2;
import com.ibz.demo.testb.service.dto.TestEntity2SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[TestEntity2] 服务对象接口
 */
public interface TestEntity2Service extends IService<TestEntity2>{

	boolean update(TestEntity2 et);
	boolean checkKey(TestEntity2 et);
	boolean create(TestEntity2 et);
	boolean remove(TestEntity2 et);
	TestEntity2 get(TestEntity2 et);
	boolean getDraft(TestEntity2 et);
    List<TestEntity2> listDefault(TestEntity2SearchFilter filter) ;
	Page<TestEntity2> searchDefault(TestEntity2SearchFilter filter);
	/**	 实体[TestEntity2] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
