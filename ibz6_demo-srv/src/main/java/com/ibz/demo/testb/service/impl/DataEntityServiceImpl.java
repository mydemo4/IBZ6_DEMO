
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.DataEntityMapper;
import com.ibz.demo.testb.domain.DataEntity;
import com.ibz.demo.testb.service.DataEntityService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.DataEntitySearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[DataEntity] 服务对象接口实现
 */
@Service(value="DataEntityServiceImpl")
public class DataEntityServiceImpl extends ServiceImplBase
<DataEntityMapper, DataEntity> implements DataEntityService{


    @Resource
    private DataEntityMapper dataentityMapper;

    @Override
    public boolean create(DataEntity et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(DataEntity et){
        this.beforeUpdate(et);
		this.dataentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(DataEntity et){
        this.dataentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(DataEntity et){
		return et.getDataentityid();
    }
    @Override
    public boolean getDraft(DataEntity et)  {
            return true;
    }







    @Override
	public List<DataEntity> listDefault(DataEntitySearchFilter filter) {
		return dataentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<DataEntity> searchDefault(DataEntitySearchFilter filter) {
		return dataentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
	public List<DataEntity> listSelectDEAll(DataEntitySearchFilter filter) {
		return dataentityMapper.searchSelectDEAll(filter,filter.getSelectCond());
	}
    @Override
	public Page<DataEntity> searchSelectDEAll(DataEntitySearchFilter filter) {
		return dataentityMapper.searchSelectDEAll(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return dataentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return dataentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return dataentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return dataentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<DataEntity> selectPermission(QueryWrapper<DataEntity> selectCond) {
        return dataentityMapper.selectPermission(new DataEntitySearchFilter().getPage(),selectCond);
    }

 }


