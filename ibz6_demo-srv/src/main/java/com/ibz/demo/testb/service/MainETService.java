package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.MainET;
import com.ibz.demo.testb.service.dto.MainETSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[MainET] 服务对象接口
 */
public interface MainETService extends IService<MainET>{

	boolean remove(MainET et);
	boolean create(MainET et);
	boolean checkKey(MainET et);
	boolean getDraft(MainET et);
	boolean update(MainET et);
	MainET get(MainET et);
    List<MainET> listDefault(MainETSearchFilter filter) ;
	Page<MainET> searchDefault(MainETSearchFilter filter);
	/**	 实体[MainET] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
