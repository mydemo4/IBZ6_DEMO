package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.USER_LQY;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class USER_LQYSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<USER_LQY> selectCond;
	public USER_LQYSearchFilter(){
		this.page =new Page<USER_LQY>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<USER_LQY>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_USER_LQY_CLIENTENTITY_CLIENTENTITYID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_cliententityid_eq("NA");
            } else {
                this.setN_cliententityid_eq(strParentkey);
            }
        }
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_USER_LQY_ORG_ORGID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_orgid_eq("NA");
            } else {
                this.setN_orgid_eq(strParentkey);
            }
        }
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_USER_LQY_ORGDEPT_ORGDEPTID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_orgdeptid_eq("NA");
            } else {
                this.setN_orgdeptid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("user_lqyname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_user_lqyname_like;//[用户2名称]
	public void setN_user_lqyname_like(String n_user_lqyname_like) {
        this.n_user_lqyname_like = n_user_lqyname_like;
        if(!StringUtils.isEmpty(this.n_user_lqyname_like)){
            this.selectCond.like("user_lqyname", n_user_lqyname_like);
        }
    }
	private String n_cliententityid_eq;//[远程调用实体标识]
	public void setN_cliententityid_eq(String n_cliententityid_eq) {
        this.n_cliententityid_eq = n_cliententityid_eq;
        if(!StringUtils.isEmpty(this.n_cliententityid_eq)){
            this.selectCond.eq("cliententityid", n_cliententityid_eq);
        }
    }
	private String n_orgid_eq;//[组织标识2]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!StringUtils.isEmpty(this.n_orgid_eq)){
            this.selectCond.eq("orgid", n_orgid_eq);
        }
    }
	private String n_orgdeptid_eq;//[部门标识]
	public void setN_orgdeptid_eq(String n_orgdeptid_eq) {
        this.n_orgdeptid_eq = n_orgdeptid_eq;
        if(!StringUtils.isEmpty(this.n_orgdeptid_eq)){
            this.selectCond.eq("orgdeptid", n_orgdeptid_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
