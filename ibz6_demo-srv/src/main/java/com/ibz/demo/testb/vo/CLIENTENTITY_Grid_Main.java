package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.testb.domain.CLIENTENTITY;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.springframework.cglib.beans.BeanMap;
import com.alibaba.fastjson.JSONObject;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CLIENTENTITY_Grid_Main{
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
    private String cliententityname;
    private String srfmajortext;
    private String srfdataaccaction;
    private String srfkey;
    public void fromCLIENTENTITY(CLIENTENTITY sourceEntity)  {
         this.setUpdateman(sourceEntity.getUpdateman());
         this.setUpdatedate(sourceEntity.getUpdatedate());
         this.setCliententityname(sourceEntity.getCliententityname());
         this.setSrfmajortext(sourceEntity.getCliententityname());
         this.setSrfdataaccaction(sourceEntity.getCliententityid());
         this.setSrfkey(sourceEntity.getCliententityid());
	}
	public static Page<CLIENTENTITY_Grid_Main> fromCLIENTENTITY(Page<CLIENTENTITY> sourcePage)  {
        if(sourcePage==null)
            return null;
        Page<CLIENTENTITY_Grid_Main> targetpage=new Page<CLIENTENTITY_Grid_Main>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<CLIENTENTITY_Grid_Main> records=new ArrayList<CLIENTENTITY_Grid_Main>();
        for(CLIENTENTITY source:sourcePage.getRecords()) {
            CLIENTENTITY_Grid_Main target=new CLIENTENTITY_Grid_Main();
            target.fromCLIENTENTITY(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
    /**
     * 表格模型集合
     * @return
     */
    public static List<Map<String,String>> getGridColumnModels() {
        List columnModelList =new ArrayList();
        columnModelList.add(new HashMap(){ { put("cliententityname","远程调用实体名称"); }});
        columnModelList.add(new HashMap(){ { put("updateman","更新人"); }});
        columnModelList.add(new HashMap(){ { put("updatedate","更新时间"); }});
        return columnModelList;
    }
    /**
     *将entity转成map
     * @param bean
     * @param <T>
     * @return
     */
    public <T> Map<String,Object> beanToMap(T bean){
        Map map =new HashMap<String,Object>();
        if(bean!=null){
            BeanMap beanMap=BeanMap.create(bean);
            for(Object obj:beanMap.keySet()){
                map.put(obj+"",beanMap.get(obj));
            }
        }
        return map;
    }
    /**
     * 将entity转成map
     * @param searchResult
     * @return
     */
    public static List<Map<String, Object>> pageToListDatas(Page<CLIENTENTITY> searchResult) {
        if(searchResult==null)
            return null;
        List<Map<String, Object>> records=new ArrayList<>();
        for(CLIENTENTITY source:searchResult.getRecords()) {
            CLIENTENTITY_Grid_Main target=new CLIENTENTITY_Grid_Main();
            records.add(target.beanToMap(source));
        }
        return records;
    }
    /**
     * 输出表格数据导出文件url
     * @param sourcePage
     * @param downloadurl
     * @return
     */
    public static Page<JSONObject> getResultPage(Page<CLIENTENTITY> sourcePage,String downloadurl)  {
        if(sourcePage==null)
            return null;
        Page<JSONObject> targetpage=new Page<JSONObject>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<JSONObject> records=new ArrayList<JSONObject>();
        JSONObject obj =new JSONObject();
        obj.put("downloadurl",downloadurl);
        records.add(obj);
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
