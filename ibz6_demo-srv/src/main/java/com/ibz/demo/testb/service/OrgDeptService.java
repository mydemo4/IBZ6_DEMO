package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.OrgDept;
import com.ibz.demo.testb.service.dto.OrgDeptSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.Collection;

/**
 * 实体[OrgDept] 服务对象接口
 */
public interface OrgDeptService {

    boolean checkKey(OrgDept et);
    boolean create(OrgDept et);
    boolean save(OrgDept et);
    boolean update(OrgDept et);
        OrgDept get(OrgDept et);
    boolean getDraft(OrgDept et);
    boolean remove(OrgDept et);
    List<OrgDept> listDefault(OrgDeptSearchFilter filter) ;
	Page<OrgDept> searchDefault(OrgDeptSearchFilter filter);
    OrgDept getById(Serializable id) throws Exception;
    boolean saveBatch(Collection<OrgDept> entityList, int batchSize);
}
