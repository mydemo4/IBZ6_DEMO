
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.TestEntity3Mapper;
import com.ibz.demo.testb.domain.TestEntity3;
import com.ibz.demo.testb.service.TestEntity3Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.TestEntity3SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[TestEntity3] 服务对象接口实现
 */
@Service(value="TestEntity3ServiceImpl")
public class TestEntity3ServiceImpl extends ServiceImplBase
<TestEntity3Mapper, TestEntity3> implements TestEntity3Service{


    @Resource
    private TestEntity3Mapper testentity3Mapper;

    @Override
    public boolean create(TestEntity3 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(TestEntity3 et){
        this.beforeUpdate(et);
		this.testentity3Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(TestEntity3 et){
        this.testentity3Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(TestEntity3 et){
		return et.getTestentity3id();
    }
    @Override
    public boolean getDraft(TestEntity3 et)  {
            return true;
    }







    @Override
	public List<TestEntity3> listDefault(TestEntity3SearchFilter filter) {
		return testentity3Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<TestEntity3> searchDefault(TestEntity3SearchFilter filter) {
		return testentity3Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return testentity3Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return testentity3Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return testentity3Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return testentity3Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<TestEntity3> selectPermission(QueryWrapper<TestEntity3> selectCond) {
        return testentity3Mapper.selectPermission(new TestEntity3SearchFilter().getPage(),selectCond);
    }

 }


