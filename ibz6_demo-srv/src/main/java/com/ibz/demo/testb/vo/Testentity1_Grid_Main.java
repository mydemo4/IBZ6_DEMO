package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.ibz.demo.testb.domain.Testentity1;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import org.springframework.cglib.beans.BeanMap;
import com.alibaba.fastjson.JSONObject;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Testentity1_Grid_Main{
    private String security1id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp datascopetest02;
    private Integer datascopetest01;
    private String testentity1name;
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updatedate;
    private String srfmstag;
    private String srfmajortext;
    private String srfdataaccaction;
    private String srfkey;
    public void fromTestentity1(Testentity1 sourceEntity)  {
         this.setSecurity1id(sourceEntity.getSecurity1id());
         this.setDatascopetest02(sourceEntity.getDatascopetest02());
         this.setDatascopetest01(sourceEntity.getDatascopetest01());
         this.setTestentity1name(sourceEntity.getTestentity1name());
         this.setUpdateman(sourceEntity.getUpdateman());
         this.setUpdatedate(sourceEntity.getUpdatedate());
         this.setSrfmajortext(sourceEntity.getTestentity1name());
         this.setSrfdataaccaction(sourceEntity.getTestentity1id());
         this.setSrfkey(sourceEntity.getTestentity1id());
	}
	public static Page<Testentity1_Grid_Main> fromTestentity1(Page<Testentity1> sourcePage)  {
        if(sourcePage==null)
            return null;
        Page<Testentity1_Grid_Main> targetpage=new Page<Testentity1_Grid_Main>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<Testentity1_Grid_Main> records=new ArrayList<Testentity1_Grid_Main>();
        for(Testentity1 source:sourcePage.getRecords()) {
            Testentity1_Grid_Main target=new Testentity1_Grid_Main();
            target.fromTestentity1(source);
            records.add(target);
        }
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
    /**
     * 表格模型集合
     * @return
     */
    public static List<Map<String,String>> getGridColumnModels() {
        List columnModelList =new ArrayList();
        columnModelList.add(new HashMap(){ { put("testentity1name","权限测试实体1名称"); }});
        columnModelList.add(new HashMap(){ { put("updateman","更新人"); }});
        columnModelList.add(new HashMap(){ { put("updatedate","更新时间"); }});
        columnModelList.add(new HashMap(){ { put("datascopetest01","数据范围测试01"); }});
        columnModelList.add(new HashMap(){ { put("datascopetest02","数据范围测试02"); }});
        return columnModelList;
    }
    /**
     *将entity转成map
     * @param bean
     * @param <T>
     * @return
     */
    public <T> Map<String,Object> beanToMap(T bean){
        Map map =new HashMap<String,Object>();
        if(bean!=null){
            BeanMap beanMap=BeanMap.create(bean);
            for(Object obj:beanMap.keySet()){
                map.put(obj+"",beanMap.get(obj));
            }
        }
        return map;
    }
    /**
     * 将entity转成map
     * @param searchResult
     * @return
     */
    public static List<Map<String, Object>> pageToListDatas(Page<Testentity1> searchResult) {
        if(searchResult==null)
            return null;
        List<Map<String, Object>> records=new ArrayList<>();
        for(Testentity1 source:searchResult.getRecords()) {
            Testentity1_Grid_Main target=new Testentity1_Grid_Main();
            records.add(target.beanToMap(source));
        }
        return records;
    }
    /**
     * 输出表格数据导出文件url
     * @param sourcePage
     * @param downloadurl
     * @return
     */
    public static Page<JSONObject> getResultPage(Page<Testentity1> sourcePage,String downloadurl)  {
        if(sourcePage==null)
            return null;
        Page<JSONObject> targetpage=new Page<JSONObject>(sourcePage.getCurrent(),sourcePage.getSize(),sourcePage.getTotal(),sourcePage.isSearchCount());
        List<JSONObject> records=new ArrayList<JSONObject>();
        JSONObject obj =new JSONObject();
        obj.put("downloadurl",downloadurl);
        records.add(obj);
        targetpage.setAsc(sourcePage.ascs());
        targetpage.setDesc(sourcePage.descs());
        targetpage.setOptimizeCountSql(sourcePage.optimizeCountSql());
        targetpage.setRecords(records);
        return targetpage;
    }
}
