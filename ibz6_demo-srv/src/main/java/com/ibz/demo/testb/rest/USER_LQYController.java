package com.ibz.demo.testb.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.testb.service.USER_LQYService;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import com.ibz.demo.testb.domain.USER_LQY;
import com.ibz.demo.testb.service.dto.USER_LQYSearchFilter;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;
import java.io.IOException;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.RedirectResult;
import javax.validation.constraints.NotBlank;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.access.prepost.PreAuthorize;
import com.ibz.demo.ibizutil.security.DataAccessUtil;

@RestController
public class USER_LQYController{
    @Autowired
    DataAccessUtil dataAccessUtil;
    @Autowired
    private USER_LQYService user_lqyService;
    /**
     * 获取服务对象
    */
    protected USER_LQYService getService(){
        return this.user_lqyService;
    }
    /**
     * 重定向视图
     * @param srfkey
     * @return
     */
    @GetMapping(value="/security/testb/user_lqy/redirectview/getmodel")
    public ResponseEntity<RedirectResult>securityredirectviewGet(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey) {
        RedirectResult redirectResult = new RedirectResult();
        return ResponseEntity.ok(redirectResult);
    }
	/**
	 * 用于权限校验
	 * @return
	 */
	public USER_LQY getEntity(){
		return new USER_LQY();
	}
}


