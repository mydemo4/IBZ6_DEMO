package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.beans.BeanMap;
import java.util.*;
import java.io.Serializable;
import javax.annotation.Resource;
import com.ibz.demo.ibizutil.center.AbstractCenter;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import com.github.javaparser.utils.Log;
import org.springframework.http.HttpMethod;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.domain.OrgDept;
import com.ibz.demo.testb.service.OrgDeptService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.OrgDeptSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[OrgDept] 服务对象接口实现
 */
@Service(value="OrgDeptServiceImpl")
public class OrgDeptServiceImpl extends AbstractCenter implements OrgDeptService{

    public boolean checkKey(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/orgdept/{orgdeptid}/checkkey",et.getOrgdeptid()),de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean create(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI("/orgdept",de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean save(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI("/orgdept/save",de,et.getClass(),HttpMethod.POST);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean update(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI("/orgdept",de,et.getClass(),HttpMethod.PUT);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public OrgDept get(OrgDept et){
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
        OrgDept result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/orgdept/{orgdeptid}",et.getOrgdeptid()),de,et.getClass(),HttpMethod.GET);
        if(result!=null){
            BeanCopier copier=BeanCopier.create(result.getClass(),et.getClass(), false);
            copier.copy(result,et,null);
        }
        return et;
    }
    public boolean getDraft(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI(getRestPath("http://172.16.103.29:8082/rest/orgrestapi/orgdept/{orgdeptid}/getdraft",et.getOrgdeptid()),de,et.getClass(),HttpMethod.GET);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public boolean remove(OrgDept et){
        this.beforeCreate(et);
        boolean f = false;
        JSONObject de = new JSONObject();
        try{
            de = (JSONObject) JSON.toJSON(et);
        }catch (Exception e){
            Log.error("参数异常："+e);
            e.printStackTrace();
            throw e;
        }
                OrgDept result = callServiceAPI("/orgdept",de,et.getClass(),HttpMethod.DELETE);
        if(result!=null){
            f=true;
        }
        return f;


    }
    public List<OrgDept> listDefault(OrgDeptSearchFilter filter){
        JSONObject jsonfilter =new JSONObject();
        BeanMap beanMap = BeanMap.create(filter);
        jsonfilter.putAll(beanMap);
        jsonfilter.remove("datacontext");
        jsonfilter.remove("permissionCond");
        jsonfilter.remove("selectCond");
                        OrgDept[] arr = callServiceAPI("http://172.16.103.29:8082/rest/orgrestapi/orgdept/search-cur_org",jsonfilter,OrgDept[].class,HttpMethod.POST);
        List<OrgDept> list = Arrays.asList(arr);
		return list;
    }
	public Page<OrgDept> searchDefault(OrgDeptSearchFilter filter){
	    JSONObject jsonfilter =new JSONObject();
        BeanMap beanMap = BeanMap.create(filter);
        jsonfilter.putAll(beanMap);
        jsonfilter.remove("datacontext");
        jsonfilter.remove("permissionCond");
        jsonfilter.remove("selectCond");
        Page<OrgDept> pages = new Page<>();
        OrgDept[] arr = callServiceAPI("http://172.16.103.29:8082/rest/orgrestapi/orgdept/search-cur_org",jsonfilter,OrgDept[].class,HttpMethod.POST);
		List<OrgDept> list = Arrays.asList(arr);
        pages.setRecords(list);
		return pages;
	}
    public OrgDept getById(Serializable id) throws Exception {
        if(id==null){
            Log.error("参数异常："+id);
            throw new Exception();
        }
        OrgDept et = new OrgDept();
        et.setOrgdeptid(id.toString());
        return get(et);
    }

    public boolean saveBatch(Collection<OrgDept> entityList, int batchSize){
        return false;
    }
    protected void beforeCreate(OrgDept et){

    }
    protected void beforeUpdate(OrgDept et){

    }
}



