
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.ORGDEPTENTITYMapper;
import com.ibz.demo.testb.domain.ORGDEPTENTITY;
import com.ibz.demo.testb.service.ORGDEPTENTITYService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.ORGDEPTENTITYSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[ORGDEPTENTITY] 服务对象接口实现
 */
@Service(value="ORGDEPTENTITYServiceImpl")
public class ORGDEPTENTITYServiceImpl extends ServiceImplBase
<ORGDEPTENTITYMapper, ORGDEPTENTITY> implements ORGDEPTENTITYService{


    @Resource
    private ORGDEPTENTITYMapper orgdeptentityMapper;

    @Override
    public boolean create(ORGDEPTENTITY et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(ORGDEPTENTITY et){
        this.beforeUpdate(et);
		this.orgdeptentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(ORGDEPTENTITY et){
        this.orgdeptentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(ORGDEPTENTITY et){
		return et.getOrgdeptentityid();
    }
    @Override
    public boolean getDraft(ORGDEPTENTITY et)  {
            return true;
    }







    @Override
	public List<ORGDEPTENTITY> listDefault(ORGDEPTENTITYSearchFilter filter) {
		return orgdeptentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<ORGDEPTENTITY> searchDefault(ORGDEPTENTITYSearchFilter filter) {
		return orgdeptentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return orgdeptentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return orgdeptentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return orgdeptentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return orgdeptentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<ORGDEPTENTITY> selectPermission(QueryWrapper<ORGDEPTENTITY> selectCond) {
        return orgdeptentityMapper.selectPermission(new ORGDEPTENTITYSearchFilter().getPage(),selectCond);
    }

 }


