package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.UniRes;
import com.ibz.demo.testb.service.dto.UniResSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[UniRes] 服务对象接口
 */
public interface UniResService extends IService<UniRes>{

	UniRes get(UniRes et);
	boolean remove(UniRes et);
	boolean create(UniRes et);
	boolean update(UniRes et);
	boolean getDraft(UniRes et);
	boolean checkKey(UniRes et);
    List<UniRes> listDefault(UniResSearchFilter filter) ;
	Page<UniRes> searchDefault(UniResSearchFilter filter);
    List<UniRes> listSelectUniresAll(UniResSearchFilter filter) ;
	Page<UniRes> searchSelectUniresAll(UniResSearchFilter filter);
	/**	 实体[UniRes] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
