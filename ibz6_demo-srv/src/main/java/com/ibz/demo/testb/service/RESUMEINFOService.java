package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.RESUMEINFO;
import com.ibz.demo.testb.service.dto.RESUMEINFOSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[RESUMEINFO] 服务对象接口
 */
public interface RESUMEINFOService extends IService<RESUMEINFO>{

	boolean update(RESUMEINFO et);
	boolean checkKey(RESUMEINFO et);
	boolean create(RESUMEINFO et);
	boolean remove(RESUMEINFO et);
	boolean getDraft(RESUMEINFO et);
	RESUMEINFO get(RESUMEINFO et);
    List<RESUMEINFO> listDefault(RESUMEINFOSearchFilter filter) ;
	Page<RESUMEINFO> searchDefault(RESUMEINFOSearchFilter filter);
	/**	 实体[RESUMEINFO] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
