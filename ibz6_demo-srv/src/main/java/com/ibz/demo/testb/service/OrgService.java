package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.Org;
import com.ibz.demo.testb.service.dto.OrgSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.Collection;

/**
 * 实体[Org] 服务对象接口
 */
public interface OrgService {

    boolean update(Org et);
    boolean checkKey(Org et);
    boolean save(Org et);
        Org get(Org et);
    boolean remove(Org et);
    boolean create(Org et);
    boolean getDraft(Org et);
    List<Org> listDefault(OrgSearchFilter filter) ;
	Page<Org> searchDefault(OrgSearchFilter filter);
    Org getById(Serializable id) throws Exception;
    boolean saveBatch(Collection<Org> entityList, int batchSize);
}
