package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.DBEntity1;
import com.ibz.demo.testb.service.dto.DBEntity1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[DBEntity1] 服务对象接口
 */
public interface DBEntity1Service extends IService<DBEntity1>{

	boolean remove(DBEntity1 et);
	boolean checkKey(DBEntity1 et);
	boolean update(DBEntity1 et);
	boolean create(DBEntity1 et);
	boolean getDraft(DBEntity1 et);
	DBEntity1 testFunc1(DBEntity1 et);
	DBEntity1 get(DBEntity1 et);
    List<DBEntity1> listDefault(DBEntity1SearchFilter filter) ;
	Page<DBEntity1> searchDefault(DBEntity1SearchFilter filter);
	/**	 实体[DBEntity1] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
