
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.DBEntity2Mapper;
import com.ibz.demo.testb.domain.DBEntity2;
import com.ibz.demo.testb.service.DBEntity2Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.DBEntity2SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.DBEntity1Service;

/**
 * 实体[DBEntity2] 服务对象接口实现
 */
@Service(value="DBEntity2ServiceImpl")
public class DBEntity2ServiceImpl extends ServiceImplBase
<DBEntity2Mapper, DBEntity2> implements DBEntity2Service{


    @Autowired
    private DBEntity1Service dbentity1Service;
    @Resource
    private DBEntity2Mapper dbentity2Mapper;

    @Override
    public boolean create(DBEntity2 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(DBEntity2 et){
        this.beforeUpdate(et);
		this.dbentity2Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(DBEntity2 et){
        this.dbentity2Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(DBEntity2 et){
		return et.getDbentity2id();
    }
    @Override
    public boolean getDraft(DBEntity2 et)  {
            return true;
    }







    @Override
	public List<DBEntity2> listDefault(DBEntity2SearchFilter filter) {
		return dbentity2Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<DBEntity2> searchDefault(DBEntity2SearchFilter filter) {
		return dbentity2Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return dbentity2Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return dbentity2Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return dbentity2Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return dbentity2Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<DBEntity2> selectPermission(QueryWrapper<DBEntity2> selectCond) {
        return dbentity2Mapper.selectPermission(new DBEntity2SearchFilter().getPage(),selectCond);
    }

 }


