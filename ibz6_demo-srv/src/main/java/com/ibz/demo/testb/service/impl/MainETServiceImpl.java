
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.MainETMapper;
import com.ibz.demo.testb.domain.MainET;
import com.ibz.demo.testb.service.MainETService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.MainETSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[MainET] 服务对象接口实现
 */
@Service(value="MainETServiceImpl")
public class MainETServiceImpl extends ServiceImplBase
<MainETMapper, MainET> implements MainETService{


    @Resource
    private MainETMapper mainetMapper;

    @Override
    public boolean create(MainET et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(MainET et){
        this.beforeUpdate(et);
		this.mainetMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(MainET et){
        this.mainetMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(MainET et){
		return et.getMainetid();
    }
    @Override
    public boolean getDraft(MainET et)  {
            return true;
    }







    @Override
	public List<MainET> listDefault(MainETSearchFilter filter) {
		return mainetMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<MainET> searchDefault(MainETSearchFilter filter) {
		return mainetMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return mainetMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return mainetMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return mainetMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return mainetMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<MainET> selectPermission(QueryWrapper<MainET> selectCond) {
        return mainetMapper.selectPermission(new MainETSearchFilter().getPage(),selectCond);
    }

 }


