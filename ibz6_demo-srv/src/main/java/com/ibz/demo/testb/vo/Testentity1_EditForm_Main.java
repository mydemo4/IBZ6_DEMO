package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.testb.domain.Testentity1;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Testentity1_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String testentity1id;
    @JsonIgnore
    private String testentity1name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private Integer datascopetest01;
    @JsonIgnore
    private Timestamp datascopetest02;
    @JsonIgnore
    private String security1id;
    @JsonIgnore
    private String security1name;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[权限测试实体1标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return testentity1id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String testentity1id){
        this.testentity1id = testentity1id;
    }
    @Size(min = 0, max = 200, message = "[权限测试实体1名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return testentity1name;
    }
    @Size(min = 0, max = 200, message = "[权限测试实体1名称]长度必须在[200]以内!")
    @JsonProperty(value = "testentity1name")
    public String getTestentity1name(){
        return testentity1name;
    }
    @JsonProperty(value = "testentity1name")
    public void setTestentity1name(String testentity1name){
        this.testentity1name = testentity1name;
    }
    @JsonProperty(value = "datascopetest01")
    public Integer getDatascopetest01(){
        return datascopetest01;
    }
    @JsonProperty(value = "datascopetest01")
    public void setDatascopetest01(Integer datascopetest01){
        this.datascopetest01 = datascopetest01;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "datascopetest02")
    public Timestamp getDatascopetest02(){
        return datascopetest02;
    }
    @JsonProperty(value = "datascopetest02")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setDatascopetest02(Timestamp datascopetest02){
        this.datascopetest02 = datascopetest02;
    }
    @Size(min = 0, max = 100, message = "[权限测试实体1标识]长度必须在[100]以内!")
    @JsonProperty(value = "security1id")
    public String getSecurity1id(){
        //输出获取外键值方法-用于通过parentdata关系填充外键值id
        if(StringUtils.isEmpty(security1id) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_TESTENTITY1_SECURITY1_SECURITY1ID"))
                this.security1id=this.getSrfparentdata().getStringValue("srfparentkey");
        }
        return security1id;
    }
    @JsonProperty(value = "security1id")
    public void setSecurity1id(String security1id){
        this.security1id = security1id;
    }
    @Size(min = 0, max = 200, message = "[ac]长度必须在[200]以内!")
    @JsonProperty(value = "security1name")
    public String getSecurity1name(){
        return security1name;
    }
    @JsonProperty(value = "security1name")
    public void setSecurity1name(String security1name){
        this.security1name = security1name;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[权限测试实体1标识]长度必须在[100]以内!")
    @JsonProperty(value = "testentity1id")
    public String getTestentity1id(){
        return testentity1id;
    }
    @JsonProperty(value = "testentity1id")
    public void setTestentity1id(String testentity1id){
        this.testentity1id = testentity1id;
    }
    public  void fromTestentity1(Testentity1 sourceEntity)  {
        this.fromTestentity1(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromTestentity1(Testentity1 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(Testentity1.class, Testentity1_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  Testentity1 toTestentity1()  {
        Testentity1 targetEntity =new Testentity1();
        BeanCopier copier=BeanCopier.create(Testentity1_EditForm_Main.class, Testentity1.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
