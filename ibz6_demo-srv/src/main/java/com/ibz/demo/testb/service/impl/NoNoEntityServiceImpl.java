
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.NoNoEntityMapper;
import com.ibz.demo.testb.domain.NoNoEntity;
import com.ibz.demo.testb.service.NoNoEntityService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.NoNoEntitySearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[NoNoEntity] 服务对象接口实现
 */
@Service(value="NoNoEntityServiceImpl")
public class NoNoEntityServiceImpl extends ServiceImplBase
<NoNoEntityMapper, NoNoEntity> implements NoNoEntityService{


    @Resource
    private NoNoEntityMapper nonoentityMapper;

    @Override
    public boolean create(NoNoEntity et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(NoNoEntity et){
        this.beforeUpdate(et);
		this.nonoentityMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(NoNoEntity et){
        this.nonoentityMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(NoNoEntity et){
		return et.getNonoentityid();
    }
    @Override
    public boolean getDraft(NoNoEntity et)  {
            return true;
    }







    @Override
	public List<NoNoEntity> listDefault(NoNoEntitySearchFilter filter) {
		return nonoentityMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<NoNoEntity> searchDefault(NoNoEntitySearchFilter filter) {
		return nonoentityMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return nonoentityMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return nonoentityMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return nonoentityMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return nonoentityMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<NoNoEntity> selectPermission(QueryWrapper<NoNoEntity> selectCond) {
        return nonoentityMapper.selectPermission(new NoNoEntitySearchFilter().getPage(),selectCond);
    }

 }


