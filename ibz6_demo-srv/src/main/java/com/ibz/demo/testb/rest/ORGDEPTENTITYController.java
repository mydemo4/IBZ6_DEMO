package com.ibz.demo.testb.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.testb.service.ORGDEPTENTITYService;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import com.ibz.demo.testb.domain.ORGDEPTENTITY;
import com.ibz.demo.testb.service.dto.ORGDEPTENTITYSearchFilter;
import com.ibz.demo.testb.vo.*;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;
import java.io.IOException;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.RedirectResult;
import javax.validation.constraints.NotBlank;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.access.prepost.PreAuthorize;
import com.ibz.demo.ibizutil.security.DataAccessUtil;

@RestController
public class ORGDEPTENTITYController{
    @Autowired
    DataAccessUtil dataAccessUtil;
    @Autowired
    private ORGDEPTENTITYService orgdeptentityService;
    /**
     * 获取服务对象
    */
    protected ORGDEPTENTITYService getService(){
        return this.orgdeptentityService;
    }

        @PreAuthorize("hasPermission(#form.srfkey,'UPDATE',this.getEntity())")
        @PostMapping(value="/security/testb/orgdeptentity/maineditform/update")
        public ResponseEntity<ORGDEPTENTITY_EditForm_Main> securitymainEditFormUpdate(@Validated @RequestBody ORGDEPTENTITY_EditForm_Main form){
            ORGDEPTENTITY entity =form.toORGDEPTENTITY();
            this.getService().get(entity);//获取entity完整数据
            BeanCopier copier=BeanCopier.create(form.getClass(),entity.getClass(), false);//vo数据覆盖do数据
            copier.copy(form,entity,null);//执行覆盖操作
            getService().update(entity);
            form.fromORGDEPTENTITY(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'DELETE',this.getEntity())")
    @GetMapping(value="/security/testb/orgdeptentity/maineditform/remove")
    public ResponseEntity<ORGDEPTENTITY_EditForm_Main> securitymainEditFormRemove(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        ORGDEPTENTITY_EditForm_Main form=new ORGDEPTENTITY_EditForm_Main();
        ORGDEPTENTITY entity = new ORGDEPTENTITY();
        entity.setOrgdeptentityid(srfkey);
        getService().remove(entity);
        form.setSrfuf("0");
        form.fromORGDEPTENTITY(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/orgdeptentity/maineditform/getdraft")
        public ResponseEntity<ORGDEPTENTITY_EditForm_Main> securitymainEditFormGetDraft(@RequestBody ORGDEPTENTITY_EditForm_Main form){
        if(!StringUtils.isEmpty(form.getSrfsourcekey()))
        {
            ORGDEPTENTITY sourceEntity =new ORGDEPTENTITY();
            sourceEntity.setOrgdeptentityid(form.getSrfsourcekey());
            this.getService().get(sourceEntity);

            ORGDEPTENTITY targetEntity =new ORGDEPTENTITY();
            sourceEntity.copyTo(targetEntity);
            form.fromORGDEPTENTITY(targetEntity,false);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
         }
            ORGDEPTENTITY entity =form.toORGDEPTENTITY();
            getService().getDraft(entity);
            form.fromORGDEPTENTITY(entity);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'READ',this.getEntity())")
    @GetMapping(value="/security/testb/orgdeptentity/maineditform/get")
    public ResponseEntity<ORGDEPTENTITY_EditForm_Main> securitymainEditFormGet(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        ORGDEPTENTITY_EditForm_Main form=new ORGDEPTENTITY_EditForm_Main();
        ORGDEPTENTITY entity = new ORGDEPTENTITY();
        entity.setOrgdeptentityid(srfkey);
        getService().get(entity);
        form.setSrfuf("1");
        form.fromORGDEPTENTITY(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/orgdeptentity/maineditform/create")
        public ResponseEntity<ORGDEPTENTITY_EditForm_Main> securitymainEditFormCreate(@Validated @RequestBody ORGDEPTENTITY_EditForm_Main form){
            ORGDEPTENTITY entity =form.toORGDEPTENTITY();
            getService().create(entity);
            form.fromORGDEPTENTITY(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/update")
    public ORGDEPTENTITY securitymainGridUpdate(@Validated ORGDEPTENTITY mainItem){
        return mainItem;
    }
    @PreAuthorize("hasPermission('DELETE',{this.getEntity(),#args})")
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/remove")
    public ResponseEntity<ORGDEPTENTITY> securitymainGridRemove(@Validated @RequestBody Map args){
        ORGDEPTENTITY entity =new ORGDEPTENTITY();
        if ( !StringUtils.isEmpty(args.get("srfkeys"))) {
            String srfkeys=args.get("srfkeys").toString();
            String srfkeyArr[] =srfkeys.split(";");
            for(String srfkey : srfkeyArr)
            {
                if(!StringUtils.isEmpty(srfkey)){
                entity.setOrgdeptentityid(srfkey);
                this.getService().remove(entity);
                }
            }
        }
        return ResponseEntity.ok().body(entity);
    }
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/getdraft")
    public ORGDEPTENTITY securitymainGridGetDraft(@Validated ORGDEPTENTITY mainItem){
        return mainItem;
    }
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/create")
    public ORGDEPTENTITY securitymainGridCreate(@Validated ORGDEPTENTITY mainItem){
        return mainItem;
    }
    @org.springframework.beans.factory.annotation.Value("${ibiz.filePath}")
    private String strFilePath;
    @Autowired
    private com.ibz.demo.ibizutil.service.IBZFILEService ibzfileService;
    /**
     * [main]表格数据导出
     * @param searchFilter
     * @return
     * @throws IOException
     * @throws
     */
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/exportdata/searchdefault")
    public ResponseEntity<Page<JSONObject>> maingridExportDataSearchDefault(@Validated @RequestBody ORGDEPTENTITYSearchFilter searchFilter) throws IOException, jxl.write.WriteException {
        String fileid=com.baomidou.mybatisplus.core.toolkit.IdWorker.get32UUID();
        String localPath=securityExportDataInit(fileid);//输出文件相对路径
        Page<ORGDEPTENTITY> searchResult = this.getService().searchDefault(searchFilter);//1.查询表格数据
        List<Map<String,Object>> datas=ORGDEPTENTITY_Grid_Main.pageToListDatas(searchResult);//2.将数据转换成list
        List<Map<String,String>> colnums=ORGDEPTENTITY_Grid_Main.getGridColumnModels();//3.获取表格列头
        java.io.File outputFile=com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().output(strFilePath+localPath,colnums,datas,new ORGDEPTENTITY().getDictField(),new ORGDEPTENTITY().getDateField());//4.生成导出文件
        com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().saveFileData(outputFile,localPath,fileid,ibzfileService); //5.保存file表记录
        String strDownloadUrl =String.format("ibizutil/download/"+fileid);//6.回传文件路径给前台
        Page<JSONObject> resultObj=ORGDEPTENTITY_Grid_Main.getResultPage(searchResult,strDownloadUrl);//7.获取输出对象
            return ResponseEntity.ok().body(resultObj);
    }
    /**
     * 表格数据导出
     * @param fileid
     * @return
     */
    private String securityExportDataInit(String fileid) {
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
        String filepath=dateFormat.format(new java.util.Date())+ java.io.File.separator;
        java.text.SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
        String strTempFileName = fileid+"-"+dateFormat2.format(new java.util.Date())+".xls";
        java.io.File file =new java.io.File(strFilePath+filepath);
        if(!file.exists())
           file.mkdirs();
        return filepath+strTempFileName;
    }
    @PreAuthorize("hasPermission('READ','ORGDEPTENTITY')")
    @PostMapping(value="/security/testb/orgdeptentity/maingrid/searchdefault")
    public ResponseEntity<Page<ORGDEPTENTITY_Grid_Main>> securitymainGridSearchDefault(@Validated @RequestBody ORGDEPTENTITYSearchFilter searchFilter){
            dataAccessUtil.setPermissionCond(searchFilter, new ORGDEPTENTITY(), "READ");
            Page<ORGDEPTENTITY> searchResult = this.getService().searchDefault(searchFilter);
            Page<ORGDEPTENTITY_Grid_Main> searchResult_vo_data =ORGDEPTENTITY_Grid_Main.fromORGDEPTENTITY(searchResult);
                    return ResponseEntity.ok().body(searchResult_vo_data);
    }
    @PostMapping(value="/security/testb/orgdeptentity/defaultsearchform/load")
    public void securitydefaultSearchFormLoad(){

    }
    @PostMapping(value="/security/testb/orgdeptentity/defaultsearchform/loaddraft")
    public ResponseEntity<ORGDEPTENTITY_SearchForm_Default> securitydefaultSearchFormLoadDraft(@Validated @RequestBody ORGDEPTENTITY_SearchForm_Default searchform){
            ORGDEPTENTITYSearchFilter searchfilter =searchform.toORGDEPTENTITYSearchFilter();
        searchform.fromORGDEPTENTITYSearchFilter(searchfilter);
        return ResponseEntity.ok().body(searchform);
    }
    @PostMapping(value="/security/testb/orgdeptentity/defaultsearchform/search")
    public void securitydefaultSearchFormSearch(){

    }
	/**
	 * 用于权限校验
	 * @return
	 */
	public ORGDEPTENTITY getEntity(){
		return new ORGDEPTENTITY();
	}
}


