package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.DBEntity3;
import com.ibz.demo.testb.service.dto.DBEntity3SearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface DBEntity3Mapper extends BaseMapper<DBEntity3>{

    List<DBEntity3> searchDefault(@Param("srf") DBEntity3SearchFilter searchfilter,@Param("ew") Wrapper<DBEntity3> wrapper) ;
    Page<DBEntity3> searchDefault(IPage page, @Param("srf") DBEntity3SearchFilter searchfilter, @Param("ew") Wrapper<DBEntity3> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'DBEntity3:'+#p0")
    DBEntity3 selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity3:'+#p0.dbentity3id")
    int insert(DBEntity3 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity3:'+#p0.dbentity3id")
    int updateById(@Param(Constants.ENTITY) DBEntity3 entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'DBEntity3:'+#p0")
    int deleteById(Serializable id);
    Page<DBEntity3> selectPermission(IPage page,@Param("pw") Wrapper<DBEntity3> wrapper) ;

    //直接sql查询。
    List<DBEntity3> executeRawSql(@Param("sql")String sql);
}
