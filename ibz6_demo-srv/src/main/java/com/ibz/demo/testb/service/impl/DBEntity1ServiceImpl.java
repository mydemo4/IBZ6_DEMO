
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.DBEntity1Mapper;
import com.ibz.demo.testb.domain.DBEntity1;
import com.ibz.demo.testb.service.DBEntity1Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.DBEntity1SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[DBEntity1] 服务对象接口实现
 */
@Service(value="DBEntity1ServiceImpl")
public class DBEntity1ServiceImpl extends ServiceImplBase
<DBEntity1Mapper, DBEntity1> implements DBEntity1Service{


    @Resource
    private DBEntity1Mapper dbentity1Mapper;

    @Override
    public boolean create(DBEntity1 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(DBEntity1 et){
        this.beforeUpdate(et);
		this.dbentity1Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(DBEntity1 et){
        this.dbentity1Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(DBEntity1 et){
		return et.getDbentity1id();
    }
    @Override
    public boolean getDraft(DBEntity1 et)  {
            return true;
    }





    @Override
    public DBEntity1 testFunc1(DBEntity1 et)  {
		return et;
    }



    @Override
	public List<DBEntity1> listDefault(DBEntity1SearchFilter filter) {
		return dbentity1Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<DBEntity1> searchDefault(DBEntity1SearchFilter filter) {
		return dbentity1Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return dbentity1Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return dbentity1Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return dbentity1Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return dbentity1Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<DBEntity1> selectPermission(QueryWrapper<DBEntity1> selectCond) {
        return dbentity1Mapper.selectPermission(new DBEntity1SearchFilter().getPage(),selectCond);
    }

 }


