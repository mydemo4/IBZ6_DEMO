
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.DBEntity3Mapper;
import com.ibz.demo.testb.domain.DBEntity3;
import com.ibz.demo.testb.service.DBEntity3Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.DBEntity3SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.DBEntity1Service;

/**
 * 实体[DBEntity3] 服务对象接口实现
 */
@Service(value="DBEntity3ServiceImpl")
public class DBEntity3ServiceImpl extends ServiceImplBase
<DBEntity3Mapper, DBEntity3> implements DBEntity3Service{


    @Autowired
    private DBEntity1Service dbentity1Service;
    @Resource
    private DBEntity3Mapper dbentity3Mapper;

    @Override
    public boolean create(DBEntity3 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(DBEntity3 et){
        this.beforeUpdate(et);
		this.dbentity3Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(DBEntity3 et){
        this.dbentity3Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(DBEntity3 et){
		return et.getDbentity3id();
    }
    @Override
    public boolean getDraft(DBEntity3 et)  {
            return true;
    }







    @Override
	public List<DBEntity3> listDefault(DBEntity3SearchFilter filter) {
		return dbentity3Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<DBEntity3> searchDefault(DBEntity3SearchFilter filter) {
		return dbentity3Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return dbentity3Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return dbentity3Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return dbentity3Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return dbentity3Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<DBEntity3> selectPermission(QueryWrapper<DBEntity3> selectCond) {
        return dbentity3Mapper.selectPermission(new DBEntity3SearchFilter().getPage(),selectCond);
    }

 }


