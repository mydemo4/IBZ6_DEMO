package com.ibz.demo.testb.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.testb.service.Security1Service;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import com.ibz.demo.testb.domain.Security1;
import com.ibz.demo.testb.service.dto.Security1SearchFilter;
import com.ibz.demo.testb.vo.*;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;
import java.io.IOException;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.RedirectResult;
import javax.validation.constraints.NotBlank;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.access.prepost.PreAuthorize;
import com.ibz.demo.ibizutil.security.DataAccessUtil;

@RestController
public class Security1Controller{
    @Autowired
    DataAccessUtil dataAccessUtil;
    @Autowired
    private Security1Service security1Service;
    /**
     * 获取服务对象
    */
    protected Security1Service getService(){
        return this.security1Service;
    }
    @PostMapping(value="/security/testb/security1/maingrid/update")
    public Security1 securitymainGridUpdate(@Validated Security1 mainItem){
        return mainItem;
    }
    @PreAuthorize("hasPermission('DELETE',{this.getEntity(),#args})")
    @PostMapping(value="/security/testb/security1/maingrid/remove")
    public ResponseEntity<Security1> securitymainGridRemove(@Validated @RequestBody Map args){
        Security1 entity =new Security1();
        if ( !StringUtils.isEmpty(args.get("srfkeys"))) {
            String srfkeys=args.get("srfkeys").toString();
            String srfkeyArr[] =srfkeys.split(";");
            for(String srfkey : srfkeyArr)
            {
                if(!StringUtils.isEmpty(srfkey)){
                entity.setSecurity1id(srfkey);
                this.getService().remove(entity);
                }
            }
        }
        return ResponseEntity.ok().body(entity);
    }
    @PostMapping(value="/security/testb/security1/maingrid/getdraft")
    public Security1 securitymainGridGetDraft(@Validated Security1 mainItem){
        return mainItem;
    }
    @PostMapping(value="/security/testb/security1/maingrid/create")
    public Security1 securitymainGridCreate(@Validated Security1 mainItem){
        return mainItem;
    }
    @org.springframework.beans.factory.annotation.Value("${ibiz.filePath}")
    private String strFilePath;
    @Autowired
    private com.ibz.demo.ibizutil.service.IBZFILEService ibzfileService;
    /**
     * [main]表格数据导出
     * @param searchFilter
     * @return
     * @throws IOException
     * @throws
     */
    @PostMapping(value="/security/testb/security1/maingrid/exportdata/searchdefault")
    public ResponseEntity<Page<JSONObject>> maingridExportDataSearchDefault(@Validated @RequestBody Security1SearchFilter searchFilter) throws IOException, jxl.write.WriteException {
        String fileid=com.baomidou.mybatisplus.core.toolkit.IdWorker.get32UUID();
        String localPath=securityExportDataInit(fileid);//输出文件相对路径
        Page<Security1> searchResult = this.getService().searchDefault(searchFilter);//1.查询表格数据
        List<Map<String,Object>> datas=Security1_Grid_Main.pageToListDatas(searchResult);//2.将数据转换成list
        List<Map<String,String>> colnums=Security1_Grid_Main.getGridColumnModels();//3.获取表格列头
        java.io.File outputFile=com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().output(strFilePath+localPath,colnums,datas,new Security1().getDictField(),new Security1().getDateField());//4.生成导出文件
        com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().saveFileData(outputFile,localPath,fileid,ibzfileService); //5.保存file表记录
        String strDownloadUrl =String.format("ibizutil/download/"+fileid);//6.回传文件路径给前台
        Page<JSONObject> resultObj=Security1_Grid_Main.getResultPage(searchResult,strDownloadUrl);//7.获取输出对象
            return ResponseEntity.ok().body(resultObj);
    }
    /**
     * 表格数据导出
     * @param fileid
     * @return
     */
    private String securityExportDataInit(String fileid) {
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
        String filepath=dateFormat.format(new java.util.Date())+ java.io.File.separator;
        java.text.SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
        String strTempFileName = fileid+"-"+dateFormat2.format(new java.util.Date())+".xls";
        java.io.File file =new java.io.File(strFilePath+filepath);
        if(!file.exists())
           file.mkdirs();
        return filepath+strTempFileName;
    }
    @PreAuthorize("hasPermission('READ','Security1')")
    @PostMapping(value="/security/testb/security1/maingrid/searchdefault")
    public ResponseEntity<Page<Security1_Grid_Main>> securitymainGridSearchDefault(@Validated @RequestBody Security1SearchFilter searchFilter){
            dataAccessUtil.setPermissionCond(searchFilter, new Security1(), "READ");
            Page<Security1> searchResult = this.getService().searchDefault(searchFilter);
            Page<Security1_Grid_Main> searchResult_vo_data =Security1_Grid_Main.fromSecurity1(searchResult);
                    return ResponseEntity.ok().body(searchResult_vo_data);
    }
    @PostMapping(value="/security/testb/security1/defaultsearchform/load")
    public void securitydefaultSearchFormLoad(){

    }
    @PostMapping(value="/security/testb/security1/defaultsearchform/loaddraft")
    public ResponseEntity<Security1_SearchForm_Default> securitydefaultSearchFormLoadDraft(@Validated @RequestBody Security1_SearchForm_Default searchform){
            Security1SearchFilter searchfilter =searchform.toSecurity1SearchFilter();
        searchform.fromSecurity1SearchFilter(searchfilter);
        return ResponseEntity.ok().body(searchform);
    }
    @PostMapping(value="/security/testb/security1/defaultsearchform/search")
    public void securitydefaultSearchFormSearch(){

    }

        @PreAuthorize("hasPermission(#form.srfkey,'UPDATE',this.getEntity())")
        @PostMapping(value="/security/testb/security1/maineditform/update")
        public ResponseEntity<Security1_EditForm_Main> securitymainEditFormUpdate(@Validated @RequestBody Security1_EditForm_Main form){
            Security1 entity =form.toSecurity1();
            this.getService().get(entity);//获取entity完整数据
            BeanCopier copier=BeanCopier.create(form.getClass(),entity.getClass(), false);//vo数据覆盖do数据
            copier.copy(form,entity,null);//执行覆盖操作
            getService().update(entity);
            form.fromSecurity1(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'DELETE',this.getEntity())")
    @GetMapping(value="/security/testb/security1/maineditform/remove")
    public ResponseEntity<Security1_EditForm_Main> securitymainEditFormRemove(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        Security1_EditForm_Main form=new Security1_EditForm_Main();
        Security1 entity = new Security1();
        entity.setSecurity1id(srfkey);
        getService().remove(entity);
        form.setSrfuf("0");
        form.fromSecurity1(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/security1/maineditform/getdraft")
        public ResponseEntity<Security1_EditForm_Main> securitymainEditFormGetDraft(@RequestBody Security1_EditForm_Main form){
        if(!StringUtils.isEmpty(form.getSrfsourcekey()))
        {
            Security1 sourceEntity =new Security1();
            sourceEntity.setSecurity1id(form.getSrfsourcekey());
            this.getService().get(sourceEntity);

            Security1 targetEntity =new Security1();
            sourceEntity.copyTo(targetEntity);
            form.fromSecurity1(targetEntity,false);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
         }
            Security1 entity =form.toSecurity1();
            getService().getDraft(entity);
            form.fromSecurity1(entity);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'READ',this.getEntity())")
    @GetMapping(value="/security/testb/security1/maineditform/get")
    public ResponseEntity<Security1_EditForm_Main> securitymainEditFormGet(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        Security1_EditForm_Main form=new Security1_EditForm_Main();
        Security1 entity = new Security1();
        entity.setSecurity1id(srfkey);
        getService().get(entity);
        form.setSrfuf("1");
        form.fromSecurity1(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/security1/maineditform/create")
        public ResponseEntity<Security1_EditForm_Main> securitymainEditFormCreate(@Validated @RequestBody Security1_EditForm_Main form){
            Security1 entity =form.toSecurity1();
            getService().create(entity);
            form.fromSecurity1(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
	/**
	 * 用于权限校验
	 * @return
	 */
	public Security1 getEntity(){
		return new Security1();
	}
}


