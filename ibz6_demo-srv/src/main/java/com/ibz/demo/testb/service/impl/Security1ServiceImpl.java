
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.Security1Mapper;
import com.ibz.demo.testb.domain.Security1;
import com.ibz.demo.testb.service.Security1Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.Security1SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[Security1] 服务对象接口实现
 */
@Service(value="Security1ServiceImpl")
public class Security1ServiceImpl extends ServiceImplBase
<Security1Mapper, Security1> implements Security1Service{


    @Resource
    private Security1Mapper security1Mapper;

    @Override
    public boolean create(Security1 et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(Security1 et){
        this.beforeUpdate(et);
		this.security1Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(Security1 et){
        this.security1Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(Security1 et){
		return et.getSecurity1id();
    }
    @Override
    public boolean getDraft(Security1 et)  {
            return true;
    }







    @Override
	public List<Security1> listDefault(Security1SearchFilter filter) {
		return security1Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<Security1> searchDefault(Security1SearchFilter filter) {
		return security1Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return security1Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return security1Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return security1Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return security1Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<Security1> selectPermission(QueryWrapper<Security1> selectCond) {
        return security1Mapper.selectPermission(new Security1SearchFilter().getPage(),selectCond);
    }

 }


