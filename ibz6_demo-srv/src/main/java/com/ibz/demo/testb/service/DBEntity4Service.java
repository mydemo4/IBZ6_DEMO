package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.DBEntity4;
import com.ibz.demo.testb.service.dto.DBEntity4SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[DBEntity4] 服务对象接口
 */
public interface DBEntity4Service extends IService<DBEntity4>{

	boolean checkKey(DBEntity4 et);
	boolean remove(DBEntity4 et);
	boolean create(DBEntity4 et);
	boolean update(DBEntity4 et);
	DBEntity4 get(DBEntity4 et);
	boolean getDraft(DBEntity4 et);
    List<DBEntity4> listDefault(DBEntity4SearchFilter filter) ;
	Page<DBEntity4> searchDefault(DBEntity4SearchFilter filter);
	/**	 实体[DBEntity4] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
