package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.Testentity1;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Testentity1SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<Testentity1> selectCond;
	public Testentity1SearchFilter(){
		this.page =new Page<Testentity1>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<Testentity1>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_TESTENTITY1_SECURITY1_SECURITY1ID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_security1id_eq("NA");
            } else {
                this.setN_security1id_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("testentity1name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_testentity1name_like;//[权限测试实体1名称]
	public void setN_testentity1name_like(String n_testentity1name_like) {
        this.n_testentity1name_like = n_testentity1name_like;
        if(!StringUtils.isEmpty(this.n_testentity1name_like)){
            this.selectCond.like("testentity1name", n_testentity1name_like);
        }
    }
	private String n_security1id_eq;//[权限测试实体1标识]
	public void setN_security1id_eq(String n_security1id_eq) {
        this.n_security1id_eq = n_security1id_eq;
        if(!StringUtils.isEmpty(this.n_security1id_eq)){
            this.selectCond.eq("security1id", n_security1id_eq);
        }
    }
	private String n_security1name_eq;//[AC]
	public void setN_security1name_eq(String n_security1name_eq) {
        this.n_security1name_eq = n_security1name_eq;
        if(!StringUtils.isEmpty(this.n_security1name_eq)){
            this.selectCond.eq("security1name", n_security1name_eq);
        }
    }
	private String n_security1name_like;//[AC]
	public void setN_security1name_like(String n_security1name_like) {
        this.n_security1name_like = n_security1name_like;
        if(!StringUtils.isEmpty(this.n_security1name_like)){
            this.selectCond.like("security1name", n_security1name_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
