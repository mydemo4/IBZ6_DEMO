package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.SubET;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubETSearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<SubET> selectCond;
	public SubETSearchFilter(){
		this.page =new Page<SubET>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<SubET>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_SUBET_MAINET_MAINETID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_mainetid_eq("NA");
            } else {
                this.setN_mainetid_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("subetname",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_subetname_like;//[测试从实体名称]
	public void setN_subetname_like(String n_subetname_like) {
        this.n_subetname_like = n_subetname_like;
        if(!StringUtils.isEmpty(this.n_subetname_like)){
            this.selectCond.like("subetname", n_subetname_like);
        }
    }
	private String n_mainetid_eq;//[测试主实体标识]
	public void setN_mainetid_eq(String n_mainetid_eq) {
        this.n_mainetid_eq = n_mainetid_eq;
        if(!StringUtils.isEmpty(this.n_mainetid_eq)){
            this.selectCond.eq("mainetid", n_mainetid_eq);
        }
    }
	private String n_mainetname_eq;//[测试主实体名称]
	public void setN_mainetname_eq(String n_mainetname_eq) {
        this.n_mainetname_eq = n_mainetname_eq;
        if(!StringUtils.isEmpty(this.n_mainetname_eq)){
            this.selectCond.eq("mainetname", n_mainetname_eq);
        }
    }
	private String n_mainetname_like;//[测试主实体名称]
	public void setN_mainetname_like(String n_mainetname_like) {
        this.n_mainetname_like = n_mainetname_like;
        if(!StringUtils.isEmpty(this.n_mainetname_like)){
            this.selectCond.like("mainetname", n_mainetname_like);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
