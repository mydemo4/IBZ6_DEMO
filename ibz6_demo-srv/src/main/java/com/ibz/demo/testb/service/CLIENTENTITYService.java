package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.CLIENTENTITY;
import com.ibz.demo.testb.service.dto.CLIENTENTITYSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[CLIENTENTITY] 服务对象接口
 */
public interface CLIENTENTITYService extends IService<CLIENTENTITY>{

	boolean update(CLIENTENTITY et);
	boolean getDraft(CLIENTENTITY et);
	boolean create(CLIENTENTITY et);
	boolean checkKey(CLIENTENTITY et);
	CLIENTENTITY get(CLIENTENTITY et);
	boolean remove(CLIENTENTITY et);
    List<CLIENTENTITY> listDefault(CLIENTENTITYSearchFilter filter) ;
	Page<CLIENTENTITY> searchDefault(CLIENTENTITYSearchFilter filter);
	/**	 实体[CLIENTENTITY] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
