package com.ibz.demo.testb.service;

import com.ibz.demo.testb.domain.Security1;
import com.ibz.demo.testb.service.dto.Security1SearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;
import com.alibaba.fastjson.JSONObject;

/**
 * 实体[Security1] 服务对象接口
 */
public interface Security1Service extends IService<Security1>{

	Security1 get(Security1 et);
	boolean create(Security1 et);
	boolean remove(Security1 et);
	boolean checkKey(Security1 et);
	boolean getDraft(Security1 et);
	boolean update(Security1 et);
    List<Security1> listDefault(Security1SearchFilter filter) ;
	Page<Security1> searchDefault(Security1SearchFilter filter);
	/**	 实体[Security1] 自定义数据查询  	**/
	List<JSONObject> select(String sql, Map param);
    boolean execute(String sql, Map param);
}
