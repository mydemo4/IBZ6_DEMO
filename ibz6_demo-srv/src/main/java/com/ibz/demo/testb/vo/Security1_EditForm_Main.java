package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.testb.domain.Security1;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Security1_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String security1id;
    @JsonIgnore
    private String security1name;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonProperty
    private String formitem;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[权限测试实体1标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return security1id;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String security1id){
        this.security1id = security1id;
    }
    @Size(min = 0, max = 200, message = "[权限测试实体1名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return security1name;
    }
    @Size(min = 0, max = 200, message = "[权限测试实体1名称]长度必须在[200]以内!")
    @JsonProperty(value = "security1name")
    public String getSecurity1name(){
        return security1name;
    }
    @JsonProperty(value = "security1name")
    public void setSecurity1name(String security1name){
        this.security1name = security1name;
    }
    @Size(min = 0, max = 100, message = "[权限测试实体1标识]长度必须在[100]以内!")
    @JsonProperty(value = "security1id")
    public String getSecurity1id(){
        return security1id;
    }
    @JsonProperty(value = "security1id")
    public void setSecurity1id(String security1id){
        this.security1id = security1id;
    }
    public  void fromSecurity1(Security1 sourceEntity)  {
        this.fromSecurity1(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromSecurity1(Security1 sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(Security1.class, Security1_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  Security1 toSecurity1()  {
        Security1 targetEntity =new Security1();
        BeanCopier copier=BeanCopier.create(Security1_EditForm_Main.class, Security1.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
