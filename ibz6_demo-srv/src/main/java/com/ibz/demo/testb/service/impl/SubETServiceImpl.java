
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.SubETMapper;
import com.ibz.demo.testb.domain.SubET;
import com.ibz.demo.testb.service.SubETService;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.SubETSearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.testb.service.MainETService;

/**
 * 实体[SubET] 服务对象接口实现
 */
@Service(value="SubETServiceImpl")
public class SubETServiceImpl extends ServiceImplBase
<SubETMapper, SubET> implements SubETService{


    @Autowired
    private MainETService mainetService;
    @Resource
    private SubETMapper subetMapper;

    @Override
    public boolean create(SubET et) {
        this.beforeCreate(et);
         return super.create(et);
    }
    @Override
    public boolean update(SubET et){
        this.beforeUpdate(et);
		this.subetMapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(SubET et){
        this.subetMapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(SubET et){
		return et.getSubetid();
    }
    @Override
    public boolean getDraft(SubET et)  {
        //加载草稿时，如果外键附加文本对应的属性为空，则从数据库查询对应数据加载。
        if((!StringUtils.isEmpty(et.getMainetid())) && et.getMainetname() == null){
                try {
                    et.setMainetname(mainetService.getById(et.getMainetid()).getMainetname());
                }catch (Exception ex){}
            }
            return true;
    }







    @Override
	public List<SubET> listDefault(SubETSearchFilter filter) {
		return subetMapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<SubET> searchDefault(SubETSearchFilter filter) {
		return subetMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return subetMapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return subetMapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return subetMapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return subetMapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<SubET> selectPermission(QueryWrapper<SubET> selectCond) {
        return subetMapper.selectPermission(new SubETSearchFilter().getPage(),selectCond);
    }

 }


