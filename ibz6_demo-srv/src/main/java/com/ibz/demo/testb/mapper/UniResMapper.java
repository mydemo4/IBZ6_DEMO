package com.ibz.demo.testb.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.*;
import com.ibz.demo.testb.domain.UniRes;
import com.ibz.demo.testb.service.dto.UniResSearchFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface UniResMapper extends BaseMapper<UniRes>{

    List<UniRes> searchDefault(@Param("srf") UniResSearchFilter searchfilter,@Param("ew") Wrapper<UniRes> wrapper) ;
    Page<UniRes> searchDefault(IPage page, @Param("srf") UniResSearchFilter searchfilter, @Param("ew") Wrapper<UniRes> wrapper) ;
    List<UniRes> searchSelectUniresAll(@Param("srf") UniResSearchFilter searchfilter,@Param("ew") Wrapper<UniRes> wrapper) ;
    Page<UniRes> searchSelectUniresAll(IPage page, @Param("srf") UniResSearchFilter searchfilter, @Param("ew") Wrapper<UniRes> wrapper) ;
     /**
      * 自定义查询SQL，当前仅支持对象为自己的
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义更新SQL
      * @param sql
      * @return
      */
     @Update("${sql}")
     boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义插入SQL
      * @param sql
      * @return
      */
     @Insert("${sql}")
     boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);
     /**
      * 自定义删除SQL
      * @param sql
      * @return
      */
     @Delete("${sql}")
     boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);
    @Override
    @Cacheable( value="IBZ6_DEMO_entity",key = "'UniRes:'+#p0")
    UniRes selectById(Serializable id);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'UniRes:'+#p0.uniresid")
    int insert(UniRes entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'UniRes:'+#p0.uniresid")
    int updateById(@Param(Constants.ENTITY) UniRes entity);
    @Override
    @CacheEvict( value="IBZ6_DEMO_entity",key = "'UniRes:'+#p0")
    int deleteById(Serializable id);
    Page<UniRes> selectPermission(IPage page,@Param("pw") Wrapper<UniRes> wrapper) ;

    //直接sql查询。
    List<UniRes> executeRawSql(@Param("sql")String sql);
}
