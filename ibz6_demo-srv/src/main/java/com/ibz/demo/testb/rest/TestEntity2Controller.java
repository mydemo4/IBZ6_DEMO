package com.ibz.demo.testb.rest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.ibz.demo.ibizutil.domain.ActionResult;
import com.ibz.demo.testb.service.TestEntity2Service;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import com.ibz.demo.testb.domain.TestEntity2;
import com.ibz.demo.testb.service.dto.TestEntity2SearchFilter;
import com.ibz.demo.testb.vo.*;
import org.springframework.cglib.beans.BeanGenerator;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;
import java.io.IOException;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import org.springframework.validation.annotation.Validated;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import com.ibz.demo.ibizutil.domain.RedirectResult;
import javax.validation.constraints.NotBlank;
import com.ibz.demo.ibizutil.domain.AutoCompleteItem;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.access.prepost.PreAuthorize;
import com.ibz.demo.ibizutil.security.DataAccessUtil;

@RestController
public class TestEntity2Controller{
    @Autowired
    DataAccessUtil dataAccessUtil;
    @Autowired
    private TestEntity2Service testentity2Service;
    /**
     * 获取服务对象
    */
    protected TestEntity2Service getService(){
        return this.testentity2Service;
    }
    @PostMapping(value="/security/testb/testentity2/maingrid/update")
    public TestEntity2 securitymainGridUpdate(@Validated TestEntity2 mainItem){
        return mainItem;
    }
    @PreAuthorize("hasPermission('DELETE',{this.getEntity(),#args})")
    @PostMapping(value="/security/testb/testentity2/maingrid/remove")
    public ResponseEntity<TestEntity2> securitymainGridRemove(@Validated @RequestBody Map args){
        TestEntity2 entity =new TestEntity2();
        if ( !StringUtils.isEmpty(args.get("srfkeys"))) {
            String srfkeys=args.get("srfkeys").toString();
            String srfkeyArr[] =srfkeys.split(";");
            for(String srfkey : srfkeyArr)
            {
                if(!StringUtils.isEmpty(srfkey)){
                entity.setTestentity2id(srfkey);
                this.getService().remove(entity);
                }
            }
        }
        return ResponseEntity.ok().body(entity);
    }
    @PostMapping(value="/security/testb/testentity2/maingrid/getdraft")
    public TestEntity2 securitymainGridGetDraft(@Validated TestEntity2 mainItem){
        return mainItem;
    }
    @PostMapping(value="/security/testb/testentity2/maingrid/create")
    public TestEntity2 securitymainGridCreate(@Validated TestEntity2 mainItem){
        return mainItem;
    }
    @org.springframework.beans.factory.annotation.Value("${ibiz.filePath}")
    private String strFilePath;
    @Autowired
    private com.ibz.demo.ibizutil.service.IBZFILEService ibzfileService;
    /**
     * [main]表格数据导出
     * @param searchFilter
     * @return
     * @throws IOException
     * @throws
     */
    @PostMapping(value="/security/testb/testentity2/maingrid/exportdata/searchdefault")
    public ResponseEntity<Page<JSONObject>> maingridExportDataSearchDefault(@Validated @RequestBody TestEntity2SearchFilter searchFilter) throws IOException, jxl.write.WriteException {
        String fileid=com.baomidou.mybatisplus.core.toolkit.IdWorker.get32UUID();
        String localPath=securityExportDataInit(fileid);//输出文件相对路径
        Page<TestEntity2> searchResult = this.getService().searchDefault(searchFilter);//1.查询表格数据
        List<Map<String,Object>> datas=TestEntity2_Grid_Main.pageToListDatas(searchResult);//2.将数据转换成list
        List<Map<String,String>> colnums=TestEntity2_Grid_Main.getGridColumnModels();//3.获取表格列头
        java.io.File outputFile=com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().output(strFilePath+localPath,colnums,datas,new TestEntity2().getDictField(),new TestEntity2().getDateField());//4.生成导出文件
        com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().saveFileData(outputFile,localPath,fileid,ibzfileService); //5.保存file表记录
        String strDownloadUrl =String.format("ibizutil/download/"+fileid);//6.回传文件路径给前台
        Page<JSONObject> resultObj=TestEntity2_Grid_Main.getResultPage(searchResult,strDownloadUrl);//7.获取输出对象
            return ResponseEntity.ok().body(resultObj);
    }
    /**
     * 表格数据导出
     * @param fileid
     * @return
     */
    private String securityExportDataInit(String fileid) {
        java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
        String filepath=dateFormat.format(new java.util.Date())+ java.io.File.separator;
        java.text.SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
        String strTempFileName = fileid+"-"+dateFormat2.format(new java.util.Date())+".xls";
        java.io.File file =new java.io.File(strFilePath+filepath);
        if(!file.exists())
           file.mkdirs();
        return filepath+strTempFileName;
    }
    @PreAuthorize("hasPermission('READ','TestEntity2')")
    @PostMapping(value="/security/testb/testentity2/maingrid/searchdefault")
    public ResponseEntity<Page<TestEntity2_Grid_Main>> securitymainGridSearchDefault(@Validated @RequestBody TestEntity2SearchFilter searchFilter){
            dataAccessUtil.setPermissionCond(searchFilter, new TestEntity2(), "READ");
            Page<TestEntity2> searchResult = this.getService().searchDefault(searchFilter);
            Page<TestEntity2_Grid_Main> searchResult_vo_data =TestEntity2_Grid_Main.fromTestEntity2(searchResult);
                    return ResponseEntity.ok().body(searchResult_vo_data);
    }
    @PostMapping(value="/security/testb/testentity2/defaultsearchform/load")
    public void securitydefaultSearchFormLoad(){

    }
    @PostMapping(value="/security/testb/testentity2/defaultsearchform/loaddraft")
    public ResponseEntity<TestEntity2_SearchForm_Default> securitydefaultSearchFormLoadDraft(@Validated @RequestBody TestEntity2_SearchForm_Default searchform){
            TestEntity2SearchFilter searchfilter =searchform.toTestEntity2SearchFilter();
        searchform.fromTestEntity2SearchFilter(searchfilter);
        return ResponseEntity.ok().body(searchform);
    }
    @PostMapping(value="/security/testb/testentity2/defaultsearchform/search")
    public void securitydefaultSearchFormSearch(){

    }

        @PreAuthorize("hasPermission(#form.srfkey,'UPDATE',this.getEntity())")
        @PostMapping(value="/security/testb/testentity2/maineditform/update")
        public ResponseEntity<TestEntity2_EditForm_Main> securitymainEditFormUpdate(@Validated @RequestBody TestEntity2_EditForm_Main form){
            TestEntity2 entity =form.toTestEntity2();
            this.getService().get(entity);//获取entity完整数据
            BeanCopier copier=BeanCopier.create(form.getClass(),entity.getClass(), false);//vo数据覆盖do数据
            copier.copy(form,entity,null);//执行覆盖操作
            getService().update(entity);
            form.fromTestEntity2(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'DELETE',this.getEntity())")
    @GetMapping(value="/security/testb/testentity2/maineditform/remove")
    public ResponseEntity<TestEntity2_EditForm_Main> securitymainEditFormRemove(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        TestEntity2_EditForm_Main form=new TestEntity2_EditForm_Main();
        TestEntity2 entity = new TestEntity2();
        entity.setTestentity2id(srfkey);
        getService().remove(entity);
        form.setSrfuf("0");
        form.fromTestEntity2(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/testentity2/maineditform/getdraft")
        public ResponseEntity<TestEntity2_EditForm_Main> securitymainEditFormGetDraft(@RequestBody TestEntity2_EditForm_Main form){
        if(!StringUtils.isEmpty(form.getSrfsourcekey()))
        {
            TestEntity2 sourceEntity =new TestEntity2();
            sourceEntity.setTestentity2id(form.getSrfsourcekey());
            this.getService().get(sourceEntity);

            TestEntity2 targetEntity =new TestEntity2();
            sourceEntity.copyTo(targetEntity);
            form.fromTestEntity2(targetEntity,false);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
         }
            TestEntity2 entity =form.toTestEntity2();
            getService().getDraft(entity);
            form.fromTestEntity2(entity);
            form.setSrfuf("0");
            return ResponseEntity.ok().body(form);
        }
    @PreAuthorize("hasPermission(#srfkey,'READ',this.getEntity())")
    @GetMapping(value="/security/testb/testentity2/maineditform/get")
    public ResponseEntity<TestEntity2_EditForm_Main> securitymainEditFormGet(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey")String srfkey){
        TestEntity2_EditForm_Main form=new TestEntity2_EditForm_Main();
        TestEntity2 entity = new TestEntity2();
        entity.setTestentity2id(srfkey);
        getService().get(entity);
        form.setSrfuf("1");
        form.fromTestEntity2(entity);
        return ResponseEntity.ok().body(form);
    }
        @PreAuthorize("hasPermission('','CREATE',this.getEntity())")
        @PostMapping(value="/security/testb/testentity2/maineditform/create")
        public ResponseEntity<TestEntity2_EditForm_Main> securitymainEditFormCreate(@Validated @RequestBody TestEntity2_EditForm_Main form){
            TestEntity2 entity =form.toTestEntity2();
            getService().create(entity);
            form.fromTestEntity2(entity);
            form.setSrfuf("1");
            return ResponseEntity.ok().body(form);
        }
    @Autowired
    private com.ibz.demo.testb.service.Testentity1Service testentity1Service;
    @PreAuthorize("hasPermission('READ','Testentity1')")
    @PostMapping(value="/security/testb/testentity2/maineditform/ac/ac")
    public ResponseEntity<List<AutoCompleteItem>> securitymainEditForm_ac_ac(@RequestBody TestEntity2_EditForm_Main form){
        List<AutoCompleteItem> list =new ArrayList<>();
        com.ibz.demo.testb.service.dto.Testentity1SearchFilter searchFilter = new com.ibz.demo.testb.service.dto.Testentity1SearchFilter();
        dataAccessUtil.setPermissionCond(searchFilter, new com.ibz.demo.testb.domain.Testentity1(), "READ");
        if(!StringUtils.isEmpty(form.getSrfparentdata()))
            searchFilter.setSrfparentdata(form.getSrfparentdata());
        TestEntity2 entity =form.toTestEntity2();
        String queryField=entity.getAc();
        if(!StringUtils.isEmpty(queryField))
            searchFilter.setN_testentity1name_like(queryField);
        Page<com.ibz.demo.testb.domain.Testentity1> searchResult = testentity1Service.searchDefault(searchFilter);
        for(com.ibz.demo.testb.domain.Testentity1 entity2 :searchResult.getRecords()) {
            AutoCompleteItem acItem =new AutoCompleteItem();
            acItem.setValue(entity2.getTestentity1id());
            acItem.setText(entity2.getTestentity1name());
            acItem.setRealtext(entity2.getTestentity1name());
            list.add(acItem);
        }
        return ResponseEntity.ok().body(list);
    }
	/**
	 * 用于权限校验
	 * @return
	 */
	public TestEntity2 getEntity(){
		return new TestEntity2();
	}
}


