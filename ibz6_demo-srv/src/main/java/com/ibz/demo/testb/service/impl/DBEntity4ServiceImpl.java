
package com.ibz.demo.testb.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.testb.mapper.DBEntity4Mapper;
import com.ibz.demo.testb.domain.DBEntity4;
import com.ibz.demo.testb.service.DBEntity4Service;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.testb.service.dto.DBEntity4SearchFilter;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import java.util.UUID;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.math.BigInteger;
import com.ibz.demo.ibizutil.annotation.PreField;
import java.lang.reflect.Field;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 实体[DBEntity4] 服务对象接口实现
 */
@Service(value="DBEntity4ServiceImpl")
public class DBEntity4ServiceImpl extends ServiceImplBase
<DBEntity4Mapper, DBEntity4> implements DBEntity4Service{


    @Resource
    private DBEntity4Mapper dbentity4Mapper;

    @Override
    public boolean create(DBEntity4 et) {
        this.beforeCreate(et);
        //如果设置了有效标记，创建时默认设置为1。
        et.setEnable(1);
         return super.create(et);
    }
    @Override
    public boolean update(DBEntity4 et){
        this.beforeUpdate(et);
		this.dbentity4Mapper.updateById(et);
		this.get(et);
		return true;

    }

    @Override
    public boolean remove(DBEntity4 et){
        this.dbentity4Mapper.deleteById(et);
        return true;
    }

    @Override
    public Object getEntityKey(DBEntity4 et){
		return et.getDbentity4id();
    }
    @Override
    public boolean getDraft(DBEntity4 et)  {
            return true;
    }







    @Override
	public List<DBEntity4> listDefault(DBEntity4SearchFilter filter) {
		return dbentity4Mapper.searchDefault(filter,filter.getSelectCond());
	}
    @Override
	public Page<DBEntity4> searchDefault(DBEntity4SearchFilter filter) {
		return dbentity4Mapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
	}
    @Override
    public  List<JSONObject> select(String sql, Map param){
       return dbentity4Mapper.selectBySQL(sql,param);
    }
    @Override
    public boolean execute(String sql, Map param) {
         if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return dbentity4Mapper.insertBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return dbentity4Mapper.updateBySQL(sql, param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return dbentity4Mapper.deleteBySQL(sql, param);
        }
        throw new BadRequestAlertException("暂未支持的SQL语法",null,null);
    }
    @Override
    public Page<DBEntity4> selectPermission(QueryWrapper<DBEntity4> selectCond) {
        return dbentity4Mapper.selectPermission(new DBEntity4SearchFilter().getPage(),selectCond);
    }

 }


