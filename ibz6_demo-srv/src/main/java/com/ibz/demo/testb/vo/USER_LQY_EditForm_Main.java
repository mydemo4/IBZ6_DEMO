package com.ibz.demo.testb.vo;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.util.StringUtils;
import com.ibz.demo.testb.domain.USER_LQY;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import java.util.Date;
import org.springframework.cglib.beans.BeanCopier;
import javax.validation.constraints.Size;
import java.util.UUID;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import java.math.BigInteger;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.ibz.demo.ibizutil.domain.DataObj;
import java.util.HashSet;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class USER_LQY_EditForm_Main{

    @JsonProperty(access=Access.WRITE_ONLY)
    private DataObj srfparentdata;
    @JsonIgnore
    private Timestamp updatedate;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srforikey;
    @JsonIgnore
    private String user_lqyid;
    @JsonIgnore
    private String user_lqyname;
    @JsonIgnore
    private String srftempmode;
    @JsonProperty
    private String srfuf;
    @JsonProperty
    private String srfdeid;
    @JsonProperty(access=Access.WRITE_ONLY)
    private String srfsourcekey;
    @JsonIgnore
    private String orgdeptid;
    @JsonIgnore
    private String orgname;
    @JsonIgnore
    private String orgdeptname;
    @JsonIgnore
    private String orgid;
    @JsonIgnore
    private String createman;
    @JsonIgnore
    private Timestamp createdate;
    @JsonIgnore
    private String updateman;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "srfupdatedate")
    public Timestamp getSrfupdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "srfupdatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setSrfupdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[用户2标识]长度必须在[100]以内!")
    @JsonProperty(value = "srfkey")
    public String getSrfkey(){
        return user_lqyid;
    }
    @JsonProperty(value = "srfkey")
    public void setSrfkey(String user_lqyid){
        this.user_lqyid = user_lqyid;
    }
    @Size(min = 0, max = 200, message = "[用户2名称]长度必须在[200]以内!")
    @JsonProperty(value = "srfmajortext")
    public String getSrfmajortext(){
        return user_lqyname;
    }
    @Size(min = 0, max = 200, message = "[用户2名称]长度必须在[200]以内!")
    @JsonProperty(value = "user_lqyname")
    public String getUser_lqyname(){
        return user_lqyname;
    }
    @JsonProperty(value = "user_lqyname")
    public void setUser_lqyname(String user_lqyname){
        this.user_lqyname = user_lqyname;
    }
    @Size(min = 0, max = 100, message = "[部门标识]长度必须在[100]以内!")
    @JsonProperty(value = "orgdeptid")
    public String getOrgdeptid(){
        //输出获取外键值方法-用于通过parentdata关系填充外键值id
        if(StringUtils.isEmpty(orgdeptid) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_USER_LQY_ORGDEPT_ORGDEPTID"))
                this.orgdeptid=this.getSrfparentdata().getStringValue("srfparentkey");
        }
        return orgdeptid;
    }
    @JsonProperty(value = "orgdeptid")
    public void setOrgdeptid(String orgdeptid){
        this.orgdeptid = orgdeptid;
    }
    @Size(min = 0, max = 100, message = "[组织]长度必须在[100]以内!")
    @JsonProperty(value = "orgname")
    public String getOrgname(){
        return orgname;
    }
    @JsonProperty(value = "orgname")
    public void setOrgname(String orgname){
        this.orgname = orgname;
    }
    @Size(min = 0, max = 100, message = "[部门]长度必须在[100]以内!")
    @JsonProperty(value = "orgdeptname")
    public String getOrgdeptname(){
        return orgdeptname;
    }
    @JsonProperty(value = "orgdeptname")
    public void setOrgdeptname(String orgdeptname){
        this.orgdeptname = orgdeptname;
    }
    @Size(min = 0, max = 100, message = "[组织标识2]长度必须在[100]以内!")
    @JsonProperty(value = "orgid")
    public String getOrgid(){
        //输出获取外键值方法-用于通过parentdata关系填充外键值id
        if(StringUtils.isEmpty(orgid) && this.getSrfparentdata() != null)
        {
            if(this.getSrfparentdata().containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_USER_LQY_ORG_ORGID"))
                this.orgid=this.getSrfparentdata().getStringValue("srfparentkey");
        }
        return orgid;
    }
    @JsonProperty(value = "orgid")
    public void setOrgid(String orgid){
        this.orgid = orgid;
    }
    @Size(min = 0, max = 60, message = "[建立人]长度必须在[60]以内!")
    @JsonProperty(value = "createman")
    public String getCreateman(){
        return createman;
    }
    @JsonProperty(value = "createman")
    public void setCreateman(String createman){
        this.createman = createman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "createdate")
    public Timestamp getCreatedate(){
        return createdate;
    }
    @JsonProperty(value = "createdate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setCreatedate(Timestamp createdate){
        this.createdate = createdate;
    }
    @Size(min = 0, max = 60, message = "[更新人]长度必须在[60]以内!")
    @JsonProperty(value = "updateman")
    public String getUpdateman(){
        return updateman;
    }
    @JsonProperty(value = "updateman")
    public void setUpdateman(String updateman){
        this.updateman = updateman;
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JsonProperty(value = "updatedate")
    public Timestamp getUpdatedate(){
        return updatedate;
    }
    @JsonProperty(value = "updatedate")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public void setUpdatedate(Timestamp updatedate){
        this.updatedate = updatedate;
    }
    @Size(min = 0, max = 100, message = "[用户2标识]长度必须在[100]以内!")
    @JsonProperty(value = "user_lqyid")
    public String getUser_lqyid(){
        return user_lqyid;
    }
    @JsonProperty(value = "user_lqyid")
    public void setUser_lqyid(String user_lqyid){
        this.user_lqyid = user_lqyid;
    }
    public  void fromUSER_LQY(USER_LQY sourceEntity)  {
        this.fromUSER_LQY(sourceEntity,true);
    }
     /**
	 * do转换为vo
	 * @param sourceEntity do数据对象
	 * @return
	 */
    public  void fromUSER_LQY(USER_LQY sourceEntity,boolean reset)  {
      BeanCopier copier=BeanCopier.create(USER_LQY.class, USER_LQY_EditForm_Main.class, false);
      copier.copy(sourceEntity,this,  null);
      this.toString();
	}
    /**
	 * vo转换为do
	 * @param
	 * @return
	 */
    public  USER_LQY toUSER_LQY()  {
        USER_LQY targetEntity =new USER_LQY();
        BeanCopier copier=BeanCopier.create(USER_LQY_EditForm_Main.class, USER_LQY.class, false);
        copier.copy(this, targetEntity, null);
        return targetEntity;
	}
}
