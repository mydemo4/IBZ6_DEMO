package com.ibz.demo.testb.service.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.ParentData;
import com.ibz.demo.testb.domain.DBEntity3;
import org.springframework.util.StringUtils;
import lombok.Data;
import java.util.Map;
import java.sql.Timestamp;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ibz.demo.ibizutil.domain.DataObj;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DBEntity3SearchFilter extends SearchFilterBase {

	private String query;
	private Page page;
	private QueryWrapper<DBEntity3> selectCond;
	public DBEntity3SearchFilter(){
		this.page =new Page<DBEntity3>(1,Short.MAX_VALUE);
		this.selectCond=new QueryWrapper<DBEntity3>();
	}
	/**
	 * 设定自定义查询条件，在原有SQL基础上追加该SQL
	 */
	public void setCustomCond(String sql)
	{
		this.selectCond.apply(sql);
	}
	/**
	 * 获取父数据
	 * @param srfparentdata
	 */
	public void setSrfparentdata(DataObj srfparentdata) {
		this.srfparentdata = srfparentdata;
		String strParentkey=this.getSrfparentdata().getStringValue("srfparentkey");
		if(this.srfparentdata.containsKey("srfparentmode")&& this.getSrfparentdata().getStringValue("srfparentmode").equals("DER1N_DBENTITY3_DBENTITY1_DBENTITY1ID")) {
            if(StringUtils.isEmpty(strParentkey)){
			    this.setN_dbentity1id_eq("NA");
            } else {
                this.setN_dbentity1id_eq(strParentkey);
            }
        }
	}
	/**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
			this.selectCond.or().like("dbentity3name",query);
		 }
	}
	/**
	 * 输出实体搜索项
	 */
	private String n_dbentity3name_like;//[数据库实体3名称]
	public void setN_dbentity3name_like(String n_dbentity3name_like) {
        this.n_dbentity3name_like = n_dbentity3name_like;
        if(!StringUtils.isEmpty(this.n_dbentity3name_like)){
            this.selectCond.like("dbentity3name", n_dbentity3name_like);
        }
    }
	private String n_dbentity1id_eq;//[数据库实体1标识]
	public void setN_dbentity1id_eq(String n_dbentity1id_eq) {
        this.n_dbentity1id_eq = n_dbentity1id_eq;
        if(!StringUtils.isEmpty(this.n_dbentity1id_eq)){
            this.selectCond.eq("dbentity1id", n_dbentity1id_eq);
        }
    }


	/**
	 * 获取实体搜索条件，用于权限
	 * @return
	 */
	@Override
	public QueryWrapper getPermissionCond() {
		return this.selectCond;
	}

}
