package com.ibz.demo.api.restsysrest1.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.testb.domain.DBEntity1;
import com.ibz.demo.testb.service.DBEntity1Service;
import com.ibz.demo.testb.service.dto.DBEntity1SearchFilter;
import com.ibz.demo.testb.domain.DBEntity2;
import com.ibz.demo.testb.service.DBEntity2Service;
import com.ibz.demo.testb.service.dto.DBEntity2SearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.restsysrest1.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"DBEntity1" })
@Slf4j
@RestController
@RequestMapping("/rest/restsysrest1/dbentity1")
public class RestSysRest1DBEntity1Resource {

    @Autowired
    private DBEntity1Service dbentity1Service;

    @Autowired
    private DBEntity2Service dbentity2Service;

	@ApiOperation(value = "测试方法1", tags = {"DBEntity1" },  notes = "测试方法1")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/testfunc1")
	public ResponseEntity<DBEntity1DTO> testfunc1DBEntity1(@PathVariable String dbentity1id,@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.testFunc1(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "CheckKey", tags = {"DBEntity1" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/checkkey")
	public ResponseEntity<DBEntity1DTO> checkkeyDBEntity1(@PathVariable String dbentity1id,@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"DBEntity1" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<DBEntity1DTO>> searchDBEntity1(@RequestBody DBEntity1SearchFilter searchFilter) {
	    DBEntity1DTO dto = new DBEntity1DTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(dbentity1Service.searchDefault(searchFilter)));
	}
	@ApiOperation(value = "Update", tags = {"DBEntity1" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{dbentity1id}")
	public ResponseEntity<DBEntity1DTO> updateDBEntity1(@PathVariable String dbentity1id,@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"DBEntity1" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity1id}/getdraft")
	public ResponseEntity<DBEntity1DTO> getdraftDBEntity1(@PathVariable String dbentity1id,@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"DBEntity1" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<DBEntity1DTO> createDBEntity1(@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		dbentity1Service.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"DBEntity1" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{dbentity1id}")
	public ResponseEntity<Boolean> removeDBEntity1(@PathVariable String dbentity1id) {
		DBEntity1DTO dto = new DBEntity1DTO();
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
        Boolean f = dbentity1Service.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Get", tags = {"DBEntity1" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity1id}")
	public ResponseEntity<DBEntity1DTO> getDBEntity1(@PathVariable String dbentity1id) {
		DBEntity1DTO dto = new DBEntity1DTO();
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"DBEntity1" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/save")
	public ResponseEntity<DBEntity1DTO> saveDBEntity1(@PathVariable String dbentity1id,@RequestBody DBEntity1DTO dto) {
		DBEntity1 po = dto.toPO();
		po.setDbentity1id(dbentity1id);
		dbentity1Service.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "CheckKey", tags = {"DBEntity1" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/dbentity2/{dbentity2id}/checkkey")
	public ResponseEntity<DBEntity2DTO> checkkeyDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"DBEntity1" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{dbentity1id}/dbentity2/{dbentity2id}")
	public ResponseEntity<Boolean> removeDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id) {
		DBEntity2DTO dto = new DBEntity2DTO();
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
        Boolean f = dbentity2Service.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Create", tags = {"DBEntity1" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/dbentity2")
	public ResponseEntity<DBEntity2DTO> createDBEntity2(@PathVariable String dbentity1id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		dbentity2Service.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"DBEntity1" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{dbentity1id}/dbentity2/{dbentity2id}")
	public ResponseEntity<DBEntity2DTO> updateDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"DBEntity1" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/dbentity2/{dbentity2id}/save")
	public ResponseEntity<DBEntity2DTO> saveDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Get", tags = {"DBEntity1" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity1id}/dbentity2/{dbentity2id}")
	public ResponseEntity<DBEntity2DTO> getDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id) {
		DBEntity2DTO dto = new DBEntity2DTO();
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"DBEntity1" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/{dbentity1id}/dbentity2/search")
	public ResponseEntity<List<DBEntity2DTO>> searchDBEntity2(@RequestBody DBEntity2SearchFilter searchFilter) {
	    DBEntity2DTO dto = new DBEntity2DTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(dbentity2Service.searchDefault(searchFilter)));
	}
	@ApiOperation(value = "GetDraft", tags = {"DBEntity1" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity1id}/dbentity2/{dbentity2id}/getdraft")
	public ResponseEntity<DBEntity2DTO> getdraftDBEntity2(@PathVariable String dbentity1id,@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

}
