package com.ibz.demo.api.testservice.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.ysmk.valuerule.anno.accountmx.*;
import com.ibz.demo.ysmk.domain.AccountMX;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class AccountMXDTO {

    private String createman;

    private Timestamp createdate;

    private String accountmxname;

    private String accountmxid;

    private String updateman;

    private Timestamp updatedate;

    private String accountid;

    private String accountname;

    private String use;

    private Double je;

    private Timestamp bxsj;


    public AccountMX toPO() {
        AccountMX po = new AccountMX();
        BeanCopier copier=BeanCopier.create(AccountMXDTO.class, AccountMX.class, false);
        copier.copy(this, po, null);
        po.setAccountmxid(null);
        return po;
    }

    public void fromPO(AccountMX po) {
        BeanCopier copier=BeanCopier.create(AccountMX.class, AccountMXDTO.class, false);
        copier.copy(po, this, null);
    }

    public List<AccountMXDTO> fromPOPage(Page<AccountMX> poPage)   {
        if(poPage == null)
            return null;
        //Page<AccountMXDTO> dtoPage=new Page<AccountMXDTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<AccountMXDTO> dtos=new ArrayList<AccountMXDTO>();
        for(AccountMX po : poPage.getRecords()) {
            AccountMXDTO dto = new AccountMXDTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
