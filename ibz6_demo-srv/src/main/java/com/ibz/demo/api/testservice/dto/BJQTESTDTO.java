package com.ibz.demo.api.testservice.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.ysmk.valuerule.anno.bjqtest.*;
import com.ibz.demo.ysmk.domain.BJQTEST;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class BJQTESTDTO {

    private String bjqtestname;

    private String bjqtestid;

    private String createman;

    private Timestamp createdate;

    private String updateman;

    private Timestamp updatedate;

    private String input;

    private String bujinqi;

    private String dxlb;

    private String dsjxz;

    private String dhwb;

    private String kgbj;

    private String mmk;

    private String sjxzq;

    private String sjxz;

    private String tpxzd;

    private String tpxzdx;

    private String wjsc;

    private String wjscd;

    private String xllbd;

    private String xllbdx;


    public BJQTEST toPO() {
        BJQTEST po = new BJQTEST();
        BeanCopier copier=BeanCopier.create(BJQTESTDTO.class, BJQTEST.class, false);
        copier.copy(this, po, null);
        po.setBjqtestid(null);
        return po;
    }

    public void fromPO(BJQTEST po) {
        BeanCopier copier=BeanCopier.create(BJQTEST.class, BJQTESTDTO.class, false);
        copier.copy(po, this, null);
    }

    public List<BJQTESTDTO> fromPOPage(Page<BJQTEST> poPage)   {
        if(poPage == null)
            return null;
        //Page<BJQTESTDTO> dtoPage=new Page<BJQTESTDTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<BJQTESTDTO> dtos=new ArrayList<BJQTESTDTO>();
        for(BJQTEST po : poPage.getRecords()) {
            BJQTESTDTO dto = new BJQTESTDTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
