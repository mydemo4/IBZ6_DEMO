package com.ibz.demo.api.restsysrest1.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.testb.domain.DBEntity2;
import com.ibz.demo.testb.service.DBEntity2Service;
import com.ibz.demo.testb.service.dto.DBEntity2SearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.restsysrest1.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"DBEntity2" })
@Slf4j
@RestController
@RequestMapping("/rest/restsysrest1/dbentity2")
public class RestSysRest1DBEntity2Resource {

    @Autowired
    private DBEntity2Service dbentity2Service;

	@ApiOperation(value = "CheckKey", tags = {"DBEntity2" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity2id}/checkkey")
	public ResponseEntity<DBEntity2DTO> checkkeyDBEntity2(@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"DBEntity2" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{dbentity2id}")
	public ResponseEntity<Boolean> removeDBEntity2(@PathVariable String dbentity2id) {
		DBEntity2DTO dto = new DBEntity2DTO();
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
        Boolean f = dbentity2Service.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Create", tags = {"DBEntity2" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<DBEntity2DTO> createDBEntity2(@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		dbentity2Service.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"DBEntity2" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{dbentity2id}")
	public ResponseEntity<DBEntity2DTO> updateDBEntity2(@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"DBEntity2" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity2id}/save")
	public ResponseEntity<DBEntity2DTO> saveDBEntity2(@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Get", tags = {"DBEntity2" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity2id}")
	public ResponseEntity<DBEntity2DTO> getDBEntity2(@PathVariable String dbentity2id) {
		DBEntity2DTO dto = new DBEntity2DTO();
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"DBEntity2" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<DBEntity2DTO>> searchDBEntity2(@RequestBody DBEntity2SearchFilter searchFilter) {
	    DBEntity2DTO dto = new DBEntity2DTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(dbentity2Service.searchDefault(searchFilter)));
	}
	@ApiOperation(value = "GetDraft", tags = {"DBEntity2" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity2id}/getdraft")
	public ResponseEntity<DBEntity2DTO> getdraftDBEntity2(@PathVariable String dbentity2id,@RequestBody DBEntity2DTO dto) {
		DBEntity2 po = dto.toPO();
		po.setDbentity2id(dbentity2id);
		dbentity2Service.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

}
