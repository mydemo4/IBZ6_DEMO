package com.ibz.demo.api.testservice.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.ysmk.domain.BJQTEST;
import com.ibz.demo.ysmk.service.BJQTESTService;
import com.ibz.demo.ysmk.service.dto.BJQTESTSearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.testservice.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"BJQTEST" })
@Slf4j
@RestController
@RequestMapping("/rest/testservice/bjqtest")
public class testServiceBJQTESTResource {

    @Autowired
    private BJQTESTService bjqtestService;

	@ApiOperation(value = "Remove", tags = {"BJQTEST" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{bjqtestid}")
	public ResponseEntity<Boolean> removeBJQTEST(@PathVariable String bjqtestid) {
		BJQTESTDTO dto = new BJQTESTDTO();
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
        Boolean f = bjqtestService.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Create", tags = {"BJQTEST" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<BJQTESTDTO> createBJQTEST(@RequestBody BJQTESTDTO dto) {
		BJQTEST po = dto.toPO();
		bjqtestService.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"BJQTEST" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{bjqtestid}")
	public ResponseEntity<BJQTESTDTO> updateBJQTEST(@PathVariable String bjqtestid,@RequestBody BJQTESTDTO dto) {
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
		bjqtestService.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Get", tags = {"BJQTEST" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{bjqtestid}")
	public ResponseEntity<BJQTESTDTO> getBJQTEST(@PathVariable String bjqtestid) {
		BJQTESTDTO dto = new BJQTESTDTO();
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
		bjqtestService.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"BJQTEST" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{bjqtestid}/save")
	public ResponseEntity<BJQTESTDTO> saveBJQTEST(@PathVariable String bjqtestid,@RequestBody BJQTESTDTO dto) {
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
		bjqtestService.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "CheckKey", tags = {"BJQTEST" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{bjqtestid}/checkkey")
	public ResponseEntity<BJQTESTDTO> checkkeyBJQTEST(@PathVariable String bjqtestid,@RequestBody BJQTESTDTO dto) {
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
		bjqtestService.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"BJQTEST" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{bjqtestid}/getdraft")
	public ResponseEntity<BJQTESTDTO> getdraftBJQTEST(@PathVariable String bjqtestid,@RequestBody BJQTESTDTO dto) {
		BJQTEST po = dto.toPO();
		po.setBjqtestid(bjqtestid);
		bjqtestService.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"BJQTEST" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<BJQTESTDTO>> searchBJQTEST(@RequestBody BJQTESTSearchFilter searchFilter) {
	    BJQTESTDTO dto = new BJQTESTDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(bjqtestService.searchDefault(searchFilter)));
	}
}
