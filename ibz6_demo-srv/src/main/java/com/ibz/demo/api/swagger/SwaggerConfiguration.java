package com.ibz.demo.api.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("DEFAULT")
                .pathMapping("/")
                .apiInfo(
						new ApiInfoBuilder()
						.title("DEFAULT")
						.build()
                    )
                .select()
                .paths(or(regex("/rest/.*")))
                .build()
                ;
    }

	@Bean
	public Docket restsysrest1Docket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("REST接口1")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("REST接口1")
						.version("1")
						.build()
                    )
				.select()
				.paths(or(regex("/rest/restsysrest1/.*")))
				.build()
				;
	}
	@Bean
	public Docket restsysrest3Docket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("REST接口3")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("REST接口3")
						.version("1")
						.build()
                    )
				.select()
				.paths(or(regex("/rest/restsysrest3/.*")))
				.build()
				;
	}
	@Bean
	public Docket restsysrest2Docket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("REST接口2")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("REST接口2")
						.version("1")
						.build()
                    )
				.select()
				.paths(or(regex("/rest/restsysrest2/.*")))
				.build()
				;
	}
	@Bean
	public Docket testserviceDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("测试移动端服务接口")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("测试移动端服务接口")
						.version("1")
						.build()
                    )
				.select()
				.paths(or(regex("/rest/testservice/.*")))
				.build()
				;
	}

}
