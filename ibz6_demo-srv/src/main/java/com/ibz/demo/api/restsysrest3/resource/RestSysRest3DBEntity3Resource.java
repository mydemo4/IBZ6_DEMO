package com.ibz.demo.api.restsysrest3.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.testb.domain.DBEntity3;
import com.ibz.demo.testb.service.DBEntity3Service;
import com.ibz.demo.testb.service.dto.DBEntity3SearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.restsysrest3.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"DBEntity3" })
@Slf4j
@RestController
@RequestMapping("/rest/restsysrest3/dbentity3")
public class RestSysRest3DBEntity3Resource {

    @Autowired
    private DBEntity3Service dbentity3Service;

	@ApiOperation(value = "Save", tags = {"DBEntity3" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity3id}/save")
	public ResponseEntity<DBEntity3DTO> saveDBEntity3(@PathVariable String dbentity3id,@RequestBody DBEntity3DTO dto) {
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
		dbentity3Service.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Get", tags = {"DBEntity3" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity3id}")
	public ResponseEntity<DBEntity3DTO> getDBEntity3(@PathVariable String dbentity3id) {
		DBEntity3DTO dto = new DBEntity3DTO();
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
		dbentity3Service.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"DBEntity3" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<DBEntity3DTO> createDBEntity3(@RequestBody DBEntity3DTO dto) {
		DBEntity3 po = dto.toPO();
		dbentity3Service.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"DBEntity3" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{dbentity3id}/getdraft")
	public ResponseEntity<DBEntity3DTO> getdraftDBEntity3(@PathVariable String dbentity3id,@RequestBody DBEntity3DTO dto) {
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
		dbentity3Service.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"DBEntity3" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{dbentity3id}")
	public ResponseEntity<Boolean> removeDBEntity3(@PathVariable String dbentity3id) {
		DBEntity3DTO dto = new DBEntity3DTO();
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
        Boolean f = dbentity3Service.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Update", tags = {"DBEntity3" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{dbentity3id}")
	public ResponseEntity<DBEntity3DTO> updateDBEntity3(@PathVariable String dbentity3id,@RequestBody DBEntity3DTO dto) {
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
		dbentity3Service.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "CheckKey", tags = {"DBEntity3" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{dbentity3id}/checkkey")
	public ResponseEntity<DBEntity3DTO> checkkeyDBEntity3(@PathVariable String dbentity3id,@RequestBody DBEntity3DTO dto) {
		DBEntity3 po = dto.toPO();
		po.setDbentity3id(dbentity3id);
		dbentity3Service.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"DBEntity3" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<DBEntity3DTO>> searchDBEntity3(@RequestBody DBEntity3SearchFilter searchFilter) {
	    DBEntity3DTO dto = new DBEntity3DTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(dbentity3Service.searchDefault(searchFilter)));
	}
}
