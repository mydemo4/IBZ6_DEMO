package com.ibz.demo.api.restsysrest1.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.testb.valuerule.anno.dbentity1.*;
import com.ibz.demo.testb.domain.DBEntity1;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class DBEntity1DTO {

    private String dbentity1id;

    private String createman;

    private String dbentity1name;

    private Integer enable;

    private String updateman;

    private Timestamp createdate;

    private Timestamp updatedate;

	List <DBEntity2DTO> dbentity2DTOs;


    public DBEntity1 toPO() {
        DBEntity1 po = new DBEntity1();
        BeanCopier copier=BeanCopier.create(DBEntity1DTO.class, DBEntity1.class, false);
        copier.copy(this, po, null);
        po.setDbentity1id(null);
        return po;
    }

    public void fromPO(DBEntity1 po) {
        BeanCopier copier=BeanCopier.create(DBEntity1.class, DBEntity1DTO.class, false);
        copier.copy(po, this, null);
    }

    public List<DBEntity1DTO> fromPOPage(Page<DBEntity1> poPage)   {
        if(poPage == null)
            return null;
        //Page<DBEntity1DTO> dtoPage=new Page<DBEntity1DTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<DBEntity1DTO> dtos=new ArrayList<DBEntity1DTO>();
        for(DBEntity1 po : poPage.getRecords()) {
            DBEntity1DTO dto = new DBEntity1DTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
