package com.ibz.demo.api.testservice.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.ysmk.domain.Account;
import com.ibz.demo.ysmk.service.AccountService;
import com.ibz.demo.ysmk.service.dto.AccountSearchFilter;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.ibz.demo.ysmk.service.AccountMXService;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.testservice.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"Account" })
@Slf4j
@RestController
@RequestMapping("/rest/testservice/account")
public class testServiceAccountResource {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountMXService accountmxService;

	@ApiOperation(value = "CheckKey", tags = {"Account" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{accountid}/checkkey")
	public ResponseEntity<AccountDTO> checkkeyAccount(@PathVariable String accountid,@RequestBody AccountDTO dto) {
		Account po = dto.toPO();
		po.setAccountid(accountid);
		accountService.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"Account" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{accountid}")
	public ResponseEntity<Boolean> removeAccount(@PathVariable String accountid) {
		AccountDTO dto = new AccountDTO();
		Account po = dto.toPO();
		po.setAccountid(accountid);
        Boolean f = accountService.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Save", tags = {"Account" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{accountid}/save")
	public ResponseEntity<AccountDTO> saveAccount(@PathVariable String accountid,@RequestBody AccountDTO dto) {
		Account po = dto.toPO();
		po.setAccountid(accountid);
		accountService.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"Account" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{accountid}/getdraft")
	public ResponseEntity<AccountDTO> getdraftAccount(@PathVariable String accountid,@RequestBody AccountDTO dto) {
		Account po = dto.toPO();
		po.setAccountid(accountid);
		accountService.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"Account" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO dto) {
		Account po = dto.toPO();
		accountService.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Get", tags = {"Account" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{accountid}")
	public ResponseEntity<AccountDTO> getAccount(@PathVariable String accountid) {
		AccountDTO dto = new AccountDTO();
		Account po = dto.toPO();
		po.setAccountid(accountid);
		accountService.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"Account" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{accountid}")
	public ResponseEntity<AccountDTO> updateAccount(@PathVariable String accountid,@RequestBody AccountDTO dto) {
		Account po = dto.toPO();
		po.setAccountid(accountid);
		accountService.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"Account" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<AccountDTO>> searchAccount(@RequestBody AccountSearchFilter searchFilter) {
	    AccountDTO dto = new AccountDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(accountService.searchDefault(searchFilter)));
	}
	@ApiOperation(value = "Get", tags = {"Account" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{accountid}/accountmx/{accountmxid}")
	public ResponseEntity<AccountMXDTO> getAccountMX(@PathVariable String accountid,@PathVariable String accountmxid) {
		AccountMXDTO dto = new AccountMXDTO();
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"Account" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{accountid}/accountmx/{accountmxid}")
	public ResponseEntity<AccountMXDTO> updateAccountMX(@PathVariable String accountid,@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"Account" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="/{accountid}/accountmx")
	public ResponseEntity<AccountMXDTO> createAccountMX(@PathVariable String accountid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		accountmxService.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"Account" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{accountid}/accountmx/{accountmxid}/getdraft")
	public ResponseEntity<AccountMXDTO> getdraftAccountMX(@PathVariable String accountid,@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"Account" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{accountid}/accountmx/{accountmxid}/save")
	public ResponseEntity<AccountMXDTO> saveAccountMX(@PathVariable String accountid,@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"Account" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{accountid}/accountmx/{accountmxid}")
	public ResponseEntity<Boolean> removeAccountMX(@PathVariable String accountid,@PathVariable String accountmxid) {
		AccountMXDTO dto = new AccountMXDTO();
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
        Boolean f = accountmxService.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "CheckKey", tags = {"Account" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{accountid}/accountmx/{accountmxid}/checkkey")
	public ResponseEntity<AccountMXDTO> checkkeyAccountMX(@PathVariable String accountid,@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"Account" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/{accountid}/accountmx/search")
	public ResponseEntity<List<AccountMXDTO>> searchAccountMX(@RequestBody AccountMXSearchFilter searchFilter) {
	    AccountMXDTO dto = new AccountMXDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(accountmxService.searchDefault(searchFilter)));
	}
}
