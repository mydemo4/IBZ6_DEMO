package com.ibz.demo.api.restsysrest3.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.testb.valuerule.anno.dbentity3.*;
import com.ibz.demo.testb.domain.DBEntity3;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class DBEntity3DTO {

    private String dbentity3id;

    private String dbentity3name;

    private Timestamp updatedate;

    private String createman;

    private Integer enable;

    private Timestamp createdate;

    private String updateman;

    private String dbentity1id;


    public DBEntity3 toPO() {
        DBEntity3 po = new DBEntity3();
        BeanCopier copier=BeanCopier.create(DBEntity3DTO.class, DBEntity3.class, false);
        copier.copy(this, po, null);
        po.setDbentity3id(null);
        return po;
    }

    public void fromPO(DBEntity3 po) {
        BeanCopier copier=BeanCopier.create(DBEntity3.class, DBEntity3DTO.class, false);
        copier.copy(po, this, null);
    }

    public List<DBEntity3DTO> fromPOPage(Page<DBEntity3> poPage)   {
        if(poPage == null)
            return null;
        //Page<DBEntity3DTO> dtoPage=new Page<DBEntity3DTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<DBEntity3DTO> dtos=new ArrayList<DBEntity3DTO>();
        for(DBEntity3 po : poPage.getRecords()) {
            DBEntity3DTO dto = new DBEntity3DTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
