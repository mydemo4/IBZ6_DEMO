package com.ibz.demo.api.testservice.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.ysmk.valuerule.anno.account.*;
import com.ibz.demo.ysmk.domain.Account;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class AccountDTO {

    private String accountid;

    private String accountname;

    private Timestamp createdate;

    private String createman;

    private Timestamp updatedate;

    private String updateman;

    private String bxxzlb;

    private String bxdx;

    private Double je;

    private Timestamp bxdate;

	List <AccountMXDTO> accountmxDTOs;


    public Account toPO() {
        Account po = new Account();
        BeanCopier copier=BeanCopier.create(AccountDTO.class, Account.class, false);
        copier.copy(this, po, null);
        po.setAccountid(null);
        return po;
    }

    public void fromPO(Account po) {
        BeanCopier copier=BeanCopier.create(Account.class, AccountDTO.class, false);
        copier.copy(po, this, null);
    }

    public List<AccountDTO> fromPOPage(Page<Account> poPage)   {
        if(poPage == null)
            return null;
        //Page<AccountDTO> dtoPage=new Page<AccountDTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<AccountDTO> dtos=new ArrayList<AccountDTO>();
        for(Account po : poPage.getRecords()) {
            AccountDTO dto = new AccountDTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
