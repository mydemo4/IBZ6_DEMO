package com.ibz.demo.api.testservice.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.ysmk.domain.Department;
import com.ibz.demo.ysmk.service.DepartmentService;
import com.ibz.demo.ysmk.service.dto.DepartmentSearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.testservice.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"Department" })
@Slf4j
@RestController
@RequestMapping("/rest/testservice/department")
public class testServiceDepartmentResource {

    @Autowired
    private DepartmentService departmentService;

	@ApiOperation(value = "Get", tags = {"Department" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{departmentid}")
	public ResponseEntity<DepartmentDTO> getDepartment(@PathVariable String departmentid) {
		DepartmentDTO dto = new DepartmentDTO();
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
		departmentService.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"Department" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{departmentid}")
	public ResponseEntity<DepartmentDTO> updateDepartment(@PathVariable String departmentid,@RequestBody DepartmentDTO dto) {
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
		departmentService.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"Department" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{departmentid}")
	public ResponseEntity<Boolean> removeDepartment(@PathVariable String departmentid) {
		DepartmentDTO dto = new DepartmentDTO();
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
        Boolean f = departmentService.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "Save", tags = {"Department" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{departmentid}/save")
	public ResponseEntity<DepartmentDTO> saveDepartment(@PathVariable String departmentid,@RequestBody DepartmentDTO dto) {
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
		departmentService.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"Department" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<DepartmentDTO> createDepartment(@RequestBody DepartmentDTO dto) {
		Department po = dto.toPO();
		departmentService.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"Department" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{departmentid}/getdraft")
	public ResponseEntity<DepartmentDTO> getdraftDepartment(@PathVariable String departmentid,@RequestBody DepartmentDTO dto) {
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
		departmentService.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "CheckKey", tags = {"Department" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{departmentid}/checkkey")
	public ResponseEntity<DepartmentDTO> checkkeyDepartment(@PathVariable String departmentid,@RequestBody DepartmentDTO dto) {
		Department po = dto.toPO();
		po.setDepartmentid(departmentid);
		departmentService.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "获取工程部", tags = {"Department" } ,notes = "获取工程部")
    @RequestMapping(method= RequestMethod.POST , value="/search-engineering")
	public ResponseEntity<List<DepartmentDTO>> searchDepartmentEngineering(@RequestBody DepartmentSearchFilter searchFilter) {
	    DepartmentDTO dto = new DepartmentDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(departmentService.searchEngineering(searchFilter)));
	}
	@ApiOperation(value = "获取Management", tags = {"Department" } ,notes = "获取Management")
    @RequestMapping(method= RequestMethod.POST , value="/search-management")
	public ResponseEntity<List<DepartmentDTO>> searchDepartmentManagement(@RequestBody DepartmentSearchFilter searchFilter) {
	    DepartmentDTO dto = new DepartmentDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(departmentService.searchManagement(searchFilter)));
	}
	@ApiOperation(value = "获取DEFAULT", tags = {"Department" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<DepartmentDTO>> searchDepartment(@RequestBody DepartmentSearchFilter searchFilter) {
	    DepartmentDTO dto = new DepartmentDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(departmentService.searchDefault(searchFilter)));
	}
	@ApiOperation(value = "获取业务部", tags = {"Department" } ,notes = "获取业务部")
    @RequestMapping(method= RequestMethod.POST , value="/search-business")
	public ResponseEntity<List<DepartmentDTO>> searchDepartmentBusiness(@RequestBody DepartmentSearchFilter searchFilter) {
	    DepartmentDTO dto = new DepartmentDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(departmentService.searchBusiness(searchFilter)));
	}
}
