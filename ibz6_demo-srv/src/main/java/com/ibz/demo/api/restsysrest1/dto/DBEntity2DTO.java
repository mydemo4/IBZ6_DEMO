package com.ibz.demo.api.restsysrest1.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.testb.valuerule.anno.dbentity2.*;
import com.ibz.demo.testb.domain.DBEntity2;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class DBEntity2DTO {

    private Integer enable;

    private String dbentity2id;

    private String dbentity2name;

    private Timestamp updatedate;

    private String createman;

    private String updateman;

    private Timestamp createdate;

    private String dbentity1id;


    public DBEntity2 toPO() {
        DBEntity2 po = new DBEntity2();
        BeanCopier copier=BeanCopier.create(DBEntity2DTO.class, DBEntity2.class, false);
        copier.copy(this, po, null);
        po.setDbentity2id(null);
        return po;
    }

    public void fromPO(DBEntity2 po) {
        BeanCopier copier=BeanCopier.create(DBEntity2.class, DBEntity2DTO.class, false);
        copier.copy(po, this, null);
    }

    public List<DBEntity2DTO> fromPOPage(Page<DBEntity2> poPage)   {
        if(poPage == null)
            return null;
        //Page<DBEntity2DTO> dtoPage=new Page<DBEntity2DTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<DBEntity2DTO> dtos=new ArrayList<DBEntity2DTO>();
        for(DBEntity2 po : poPage.getRecords()) {
            DBEntity2DTO dto = new DBEntity2DTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
