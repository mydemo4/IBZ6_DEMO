package com.ibz.demo.api.testservice.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import com.ibz.demo.ysmk.domain.AccountMX;
import com.ibz.demo.ysmk.service.AccountMXService;
import com.ibz.demo.ysmk.service.dto.AccountMXSearchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import com.ibz.demo.api.testservice.dto.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = {"AccountMX" })
@Slf4j
@RestController
@RequestMapping("/rest/testservice/accountmx")
public class testServiceAccountMXResource {

    @Autowired
    private AccountMXService accountmxService;

	@ApiOperation(value = "Get", tags = {"AccountMX" },  notes = "Get")
	@RequestMapping(method= RequestMethod.GET , value="/{accountmxid}")
	public ResponseEntity<AccountMXDTO> getAccountMX(@PathVariable String accountmxid) {
		AccountMXDTO dto = new AccountMXDTO();
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.get(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Update", tags = {"AccountMX" },  notes = "Update")
	@RequestMapping(method= RequestMethod.PUT , value="/{accountmxid}")
	public ResponseEntity<AccountMXDTO> updateAccountMX(@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.update(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Create", tags = {"AccountMX" },  notes = "Create")
	@RequestMapping(method= RequestMethod.POST , value="")
	public ResponseEntity<AccountMXDTO> createAccountMX(@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		accountmxService.create(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "GetDraft", tags = {"AccountMX" },  notes = "GetDraft")
	@RequestMapping(method= RequestMethod.GET , value="/{accountmxid}/getdraft")
	public ResponseEntity<AccountMXDTO> getdraftAccountMX(@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.getDraft(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
	}

	@ApiOperation(value = "Save", tags = {"AccountMX" },  notes = "Save")
	@RequestMapping(method= RequestMethod.POST , value="/{accountmxid}/save")
	public ResponseEntity<AccountMXDTO> saveAccountMX(@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.save(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "Remove", tags = {"AccountMX" },  notes = "Remove")
	@RequestMapping(method= RequestMethod.DELETE , value="/{accountmxid}")
	public ResponseEntity<Boolean> removeAccountMX(@PathVariable String accountmxid) {
		AccountMXDTO dto = new AccountMXDTO();
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
        Boolean f = accountmxService.remove(po);
        if(f){
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(f);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(f);
		}
	}

	@ApiOperation(value = "CheckKey", tags = {"AccountMX" },  notes = "CheckKey")
	@RequestMapping(method= RequestMethod.POST , value="/{accountmxid}/checkkey")
	public ResponseEntity<AccountMXDTO> checkkeyAccountMX(@PathVariable String accountmxid,@RequestBody AccountMXDTO dto) {
		AccountMX po = dto.toPO();
		po.setAccountmxid(accountmxid);
		accountmxService.checkKey(po);
		dto.fromPO(po);
		return ResponseEntity.status(HttpStatus.CREATED).body(dto);
	}

	@ApiOperation(value = "获取DEFAULT", tags = {"AccountMX" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/search")
	public ResponseEntity<List<AccountMXDTO>> searchAccountMX(@RequestBody AccountMXSearchFilter searchFilter) {
	    AccountMXDTO dto = new AccountMXDTO();
	    return ResponseEntity.status(HttpStatus.CREATED).body(dto.fromPOPage(accountmxService.searchDefault(searchFilter)));
	}
}
