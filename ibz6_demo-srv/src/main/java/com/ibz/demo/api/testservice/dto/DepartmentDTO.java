package com.ibz.demo.api.testservice.dto;

import java.util.ArrayList;
import java.util.List;
//import com.ibz.demo.ysmk.valuerule.anno.department.*;
import com.ibz.demo.ysmk.domain.Department;
import lombok.Data;
import java.sql.Timestamp;
import org.springframework.cglib.beans.BeanCopier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

@Data
public class DepartmentDTO {

    private String departmentname;

    private String departmentid;

    private Timestamp createdate;

    private String createman;

    private String updateman;

    private Timestamp updatedate;

    private String type;

    private String srfmajortext;


    public Department toPO() {
        Department po = new Department();
        BeanCopier copier=BeanCopier.create(DepartmentDTO.class, Department.class, false);
        copier.copy(this, po, null);
        po.setDepartmentid(null);
        return po;
    }

    public void fromPO(Department po) {
        BeanCopier copier=BeanCopier.create(Department.class, DepartmentDTO.class, false);
        copier.copy(po, this, null);
    }

    public List<DepartmentDTO> fromPOPage(Page<Department> poPage)   {
        if(poPage == null)
            return null;
        //Page<DepartmentDTO> dtoPage=new Page<DepartmentDTO>(poPage.getCurrent(), poPage.getSize(), poPage.getTotal(), poPage.isSearchCount());
        List<DepartmentDTO> dtos=new ArrayList<DepartmentDTO>();
        for(Department po : poPage.getRecords()) {
            DepartmentDTO dto = new DepartmentDTO();
            dto.fromPO(po);
            dtos.add(dto);
        }
        //dtoPage.setAsc(poPage.ascs());
        //dtoPage.setDesc(poPage.descs());
        //dtoPage.setOptimizeCountSql(poPage.optimizeCountSql());
        //dtoPage.setRecords(dtos);
        return dtos;
    }

}
