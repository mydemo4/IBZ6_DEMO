package com.ibz.demo.ibizutil.cache.utils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

/**
 * 缓存操作工具类(接口）
 */
public interface ICacheHelper {
    /**
     * 从cache中获取数据。
     */
    @Cacheable(value = "IBZ6_DEMO_cacheHelper",key = "'cache:'+#p0")
    JSONObject get(String key);

    /**
     * 增量添加到缓存，不删除原有数据。格式如下
     * {
     *     ....                    //已有的数据。
     *     jsonkey: jsonValue     //添加内容
     * }
     * @param key           缓存key
     * @param jsonKey       缓存标签
     * @param jsonValue     添加到缓存的数据内容。
     */
    void add(String key, String jsonKey, Object jsonValue);

    /**
     * 保存json到缓存，如果有对应缓存,替换原有缓存。
     */
    @CachePut(value = "IBZ6_DEMO_cacheHelper",key = "'cache:'+#p0")
    JSONObject put(String key, JSONObject json);

    /**
     * 清除缓存数据
     */
    @CacheEvict(value = "IBZ6_DEMO_cacheHelper",key = "'cache:'+#p0")
    void clear(String key);
}

