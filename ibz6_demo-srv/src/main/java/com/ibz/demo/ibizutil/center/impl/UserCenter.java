package com.ibz.demo.ibizutil.center.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.cache.utils.ICacheHelper;
import com.ibz.demo.ibizutil.center.AbstractCenter;
import com.ibz.demo.ibizutil.center.IUser;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 远程用户数据存储中心（调用器）
 */
@Slf4j
@Component
public class UserCenter extends AbstractCenter implements IUser {
    @Value("${ibiz.center.user.baseURL}")
    private String baseURL;
    @Value("${ibiz.center.user.rest.getaccount}")
    private String getAccoutURI;
    @Value("${ibiz.center.user.rest.getuserinfo}")
    private String getUserInfoURI;
    @Value("${ibiz.center.user.rest.org.porg}")
    private String getPorgURI;
    @Value("${ibiz.center.user.rest.org.sorg}")
    private String getSorgURI;
    @Value("${ibiz.center.user.rest.orgdept.porgdept}")
    private String getPorgDeptURI;
    @Value("${ibiz.center.user.rest.orgdept.sorgdept}")
    private String getSorgDeptURI;
    @Autowired
    private ICacheHelper cacheHelper;

    @Override
    public JSONObject getUserInfo(JSONObject params) {
        log.debug("尝试获取用户综合信息,参数：" + params);
        //查询本地缓存中，是否已有用户信息。
        String loginname = params.getString("loginname");
        JSONObject userInfo = getLocalUserInfo(loginname);
        if (userInfo.isEmpty()) {
            //如果本地没有缓存，请求远端。
            userInfo = getRemoteUserInfo(params);
            //存入缓存中。
            cacheHelper.add(loginname, "userInfo", userInfo);
        }
        log.debug("获取的用户综合信息：" + userInfo);
        return userInfo;
    }

    private boolean isSuperUser(JSONObject userAccountResp) {
        String issuperuser = userAccountResp.getString("issuperuser");
        return "1".equals(issuperuser);
    }

    private JSONObject formatUser(JSONObject userAccountResp) {
        JSONObject userObject = new JSONObject();
        userObject.put("username", userAccountResp.getString("username"));
        userObject.put("loginname", userAccountResp.getString("loginaccountname"));
        userObject.put("userid", userAccountResp.getString("userid"));
        userObject.put("issuperuser", userAccountResp.getString("issuperuser"));
        return userObject;
    }

    /**
     * 从远端获取用户信息。
     *
     * @return
     */
    private JSONObject getRemoteUserInfo(JSONObject params) {
        log.debug("尝试远程查询用户信息，参数：" + params);
        //查询用户账号信息
        JSONObject userAccountResp = getUserAccountInfo(params);
        JSONObject contextUser = new JSONObject();
        //如果是超级管理员，不查询用户组织相关数据。
        if (!isSuperUser(userAccountResp)) {
            String curorgId = userAccountResp.getString("orgid");
            String curorgdeptId = userAccountResp.getString("orgdeptid");
            //查询当前组织的上下级组织
            contextUser.put("org", getOrg(curorgId));
            //查询当前部门的上下级部门
            contextUser.put("orgdept", getOrgDept(curorgdeptId));
        }
        contextUser.put("user", formatUser(userAccountResp));
        log.debug("远程查询的用户信息结果：" + contextUser);
        return contextUser;
    }

    /**
     * 获取本地缓存的用户信息。
     *
     * @return
     */
    private JSONObject getLocalUserInfo(String loginname) {
        String subKey = "userInfo";
        JSONObject cache = cacheHelper.get(loginname);
        if (cache == null || !cache.containsKey(subKey)) {
            return new JSONObject();
        } else {
            return cache.getJSONObject(subKey);
        }
    }

    /**
     * 查询用户登录账号相关信息
     *
     * @param params 登录名 loginacountname
     * @return issuperadmin   是否超级管理员（1或者0）
     */
    private JSONObject getUserAccountInfo(JSONObject params) {
        String loginname = params.getString("loginname");
        JSONObject paramMap = new JSONObject();
        JSONObject parentdata = new JSONObject();
        parentdata.put("loginaccountname", loginname);
        paramMap.put("srfparentdata", parentdata);
        System.out.println(paramMap);
        JSONArray resp = callServiceAPI(baseURL + getAccoutURI, paramMap, JSONArray.class);
        log.info("用户中心[账号数据]返回数据：" + resp);
        if (resp == null || resp.size() == 0) {
            throw new BadRequestAlertException("用户中心无法找到[账号数据]，请联系管理员。", null, null);
        }
        return resp.getJSONObject(0);
    }

    /**
     * 查询用户当前组织、部门信息。
     *
     * @param params 登录名 loginname
     * @return
     */
    private JSONObject getUserOrg(JSONObject params) {
        String orgdeptusername = params.getString("loginname");
        JSONObject parentdata = new JSONObject();
        parentdata.put("orgdeptusername", orgdeptusername);
        JSONObject paramMap = new JSONObject();
        paramMap.put("srfparentdata", parentdata);
        JSONArray resp = callServiceAPI(baseURL + getUserInfoURI, paramMap, JSONArray.class);
        log.info("用户中心[组织数据]原始返回数据：" + resp);
        if (resp == null || resp.size() == 0) {
            throw new BadRequestAlertException("用户中心无法找到[组织数据]，请联系管理员。", null, null);
        }
        return resp.getJSONObject(0);
    }

    /**
     * 获取上下级组织信息（标识）。
     *
     * @param curOrgId
     * @return
     */
    private JSONObject getOrg(String curOrgId) {
        JSONObject org = new JSONObject();
        org.put("curorg", curOrgId);
        org.put("porg", getOrgDeptList("orgid", curOrgId, getPorgURI));
        org.put("sorg", getOrgDeptList("orgid", curOrgId, getSorgURI));
        log.info("组织：" + org);
        return org;
    }

    /**
     * 获取上下级部门信息（标识）。
     *
     * @param curOrgdeptId
     * @return
     */
    private JSONObject getOrgDept(String curOrgdeptId) {
        JSONObject orgDept = new JSONObject();
        orgDept.put("curorgdept", curOrgdeptId);
        orgDept.put("porgdept", getOrgDeptList("orgdeptid", curOrgdeptId, getPorgDeptURI));
        orgDept.put("sorgdept", getOrgDeptList("orgdeptid", curOrgdeptId, getSorgDeptURI));
        log.info("部门信息：" + orgDept);
        return orgDept;
    }

    private List<String> getOrgDeptList(String key, String searchId, String relPath) {
        JSONArray resp = callServiceAPI(baseURL + getRestPath(relPath, searchId), null, JSONArray.class);
        List<String> list = jsonArrayToList(resp, key);
        return list;
    }

    private List<String> jsonArrayToList(JSONArray array, String key) {
        List<String> idList = new ArrayList<>();
        if (array == null || array.size() == 0)
            return idList;
        for (int i = 0; i < array.size(); i++) {
            idList.add(array.getJSONObject(i).getString(key));
        }
        return idList;
    }

    @Override
    public void syncData(String relPath, String pathid, JSONObject paramMap) {
        callServiceAPI(baseURL + getRestPath(relPath, pathid), paramMap, JSONObject.class);
    }
}