package com.ibz.demo.ibizutil.service;

import javax.annotation.Resource;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.ibz.demo.ibizutil.mapper.IBZUSERMapper;
import com.ibz.demo.ibizutil.domain.IBZUSER;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBZUSER] 服务对象接口实现
 */
public  interface IBZUSERService extends IService<IBZUSER> {
    @Cacheable( value="IBZ6_DEMO_users",key = "'getByUsername:'+#p0")
    AuthenticationUser getByUsername(String username);
    @CacheEvict( value="IBZ6_DEMO_users",key = "'getByUsername:'+#p0")
    void resetByUsername(String username);

    AuthenticationUser createUserDetails(IBZUSER user);

    }