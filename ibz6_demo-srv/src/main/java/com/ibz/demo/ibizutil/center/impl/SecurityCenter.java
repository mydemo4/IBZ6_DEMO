package com.ibz.demo.ibizutil.center.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.cache.utils.ICacheHelper;
import com.ibz.demo.ibizutil.center.AbstractCenter;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 用户权限中心（调用）
 */
@Slf4j
@Component
public class SecurityCenter extends AbstractCenter implements ISecurity {
    @Value("${ibiz.center.security.baseURL}")
    private String baseURL;
    @Value("${ibiz.center.security.rest.unires}")
    private String uniresURI;
    @Value("${ibiz.center.security.rest.grid}")
    private String gridURI;
    @Value("${ibiz.center.security.rest.form}")
    private String formURI;
    @Value("${ibiz.center.security.rest.ability}")
    private String abilityURI;
    @Autowired
    private ICacheHelper cacheHelper;

    /**
     * 获取用户权限
     *
     * @param loginname
     * @return
     */
    @Override
    public JSONArray getUniRes(String loginname) {
        log.debug("尝试获取统一资源：" + loginname);
        String userid = AuthenticationUser.getAuthenticationUser().getUserid();
        JSONObject paramMap = prepareUniResParams(userid);
        JSONObject cacheJSON = cacheHelper.get(loginname);
        JSONArray resp;
        if (cacheJSON == null || !cacheJSON.containsKey("unires")) {
            resp = callServiceAPI(baseURL + uniresURI, paramMap, JSONArray.class);
            cacheHelper.add(loginname, "unires", resp);
        } else {
            resp = cacheJSON.getJSONArray("unires");
        }
        log.debug("统一资源获取:" + resp);
        return resp;
    }

    @Override
    public boolean hasGridPermission(JSONObject paramMap) {
        return hasPermission(paramMap, gridURI);
    }

    @Override
    public boolean hasFormPermission(JSONObject paramMap) {
        return hasPermission(paramMap, formURI);
    }

    /**
     * 判断是否有权限（优先查看是否有缓存记录; 没有记录，再远程请求）
     * case1: 缓存中有权限记录，返回对应结果 true/false
     * case2: 缓存中没有记录，请求权限接口鉴权,再把返回结果存入缓存中。
     *
     * @param paramMap
     * @param path
     * @return true/false
     */
    private boolean hasPermission(JSONObject paramMap, String path) {
        log.debug("尝试获取权限，权限参数：" + paramMap);
        Boolean hasPermissionLocal = hasPermissionLocal(paramMap);
        if (hasPermissionLocal != null) {
            return hasPermissionLocal;
        } else {
            hasPermissionLocal = hasRemotePermission(paramMap, path);
            addToLocalPermission(paramMap, hasPermissionLocal, true);
        }
        log.debug("获取权限结果：" + hasPermissionLocal);
        return hasPermissionLocal;

    }

    private boolean hasRemotePermission(JSONObject paramMap, String path) {
        log.debug("尝试请求远端权限:" + path + "，权限参数：" + paramMap);
        paramMap.put("systemid", getSystemid());
        JSONObject resp = callServiceAPI(baseURL + path, paramMap, JSONObject.class);
        boolean hasPermission = false;
        if (resp != null && resp.size() != 0) {
            hasPermission = resp.getBoolean("hasPermission");
        }
        log.debug("返回的远端权限结果：" + hasPermission);
        return hasPermission;
    }

    /**
     * 获取用户数据能力列表。
     *
     * @param paramMap
     * @return
     */
    @Override
    public JSONArray getDataAbilities(JSONObject paramMap) {
        log.debug("尝试获取数据能力列表，权限参数：" + paramMap);
        JSONArray abilities = getLocalAbilities(paramMap);
        //如果本地缓存中没有数据能力记录，请求远端权限中心。
        if (abilities == null || abilities.size() == 0) {
            paramMap.put("systemid", getSystemid());
            abilities = callServiceAPI(baseURL + abilityURI, paramMap, JSONArray.class);
            addToLocalPermission(paramMap, abilities, false);
        }
        log.debug("返回的获取数据能力列表：" + abilities);
        return abilities;
    }
    private JSONArray getLocalAbilities(JSONObject params) {
        String loginname = AuthenticationUser.getAuthenticationUser().getLoginname();
        JSONObject permissions = cacheHelper.get(loginname);
        if (permissions == null) {
            permissions = new JSONObject();
        }
        JSONArray abilities = permissions.getJSONArray("abilities_" + getKey(params));
        return abilities;
    }

    private void addToLocalPermission(JSONObject params, Object hasPermission, boolean isPermission) {
        String loginname = AuthenticationUser.getAuthenticationUser().getLoginname();
        //缓存中取出之前的查询结果。
        JSONObject permissions = cacheHelper.get(loginname);
        if (permissions == null) {
            permissions = new JSONObject();
        }
        String key = (isPermission ? "" : "abilities_") + getKey(params);
        log.debug("添加到缓存：" + permissions);
        cacheHelper.add(loginname, key, hasPermission);
    }

    /**
     * 查询本地缓存中的权限
     *
     * @param params
     * @return true 有权限，false没权限，null没有对应数据（需要远程调用查询）。
     */
    private Boolean hasPermissionLocal(JSONObject params) {
        String loginname = AuthenticationUser.getAuthenticationUser().getLoginname();
        //缓存中取出之前的查询结果。
        JSONObject permissions = cacheHelper.get(loginname);
        if (permissions != null && permissions.containsKey(getKey(params))) {
            return permissions.getBoolean(getKey(params));
        }
        return null;
    }

    private String getKey(JSONObject params) {
        String entity = params.getString("entity");
        String action = params.getString("action");
        String owner = params.getString("owner");
        return entity + "_" + action + "_" + owner;
    }

    private JSONObject prepareUniResParams(String userid) {
        JSONObject parentdata = new JSONObject();
        parentdata.put("userid", userid);
        parentdata.put("sysid", getSystemid());
        JSONObject paramMap = new JSONObject();
        paramMap.put("srfparentdata", parentdata);
        return paramMap;
    }
}
