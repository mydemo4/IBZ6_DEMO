package com.ibz.demo.ibizutil.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ibz.demo.ibizutil.domain.IBZDataAudit;
import com.ibz.demo.ibizutil.mapper.IBZDataAuditMapper;
import org.springframework.stereotype.Service;


/**
 * 实体[DataAudit] 服务对象接口实现
 */
@Service
public class IBZDataAuditServiceImpl extends ServiceImpl<IBZDataAuditMapper, IBZDataAudit> implements IBZDataAuditService {

}