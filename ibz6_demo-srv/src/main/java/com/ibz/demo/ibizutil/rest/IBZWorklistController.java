package com.ibz.demo.ibizutil.rest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.ibz.demo.ibizutil.service.RemoteWFServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.Worklist;
import com.ibz.demo.ibizutil.service.IBZWorklistService;
import com.ibz.demo.ibizutil.domain.WorklistSearchFilter;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.vo.*;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping({
"/security/ibizutil/worklist/",
})
public class IBZWorklistController{

    @Autowired
	private IBZWorklistService worklistService;
	/**
	 * 获取服务对象
	*/
	protected IBZWorklistService getService(){
		return this.worklistService;
	}
	@PostMapping(value="maingrid/update")
	public Worklist mainGridUpdate(@Validated Worklist mainItem){
		return mainItem;
	}
	@PostMapping(value="maingrid/remove")
	public ResponseEntity<Worklist> mainGridRemove(@Validated @RequestBody Map args){
	    Worklist entity =new Worklist();
	    if ( !StringUtils.isEmpty(args.get("srfkeys"))) {
			String srfkeys=args.get("srfkeys").toString();
			String srfkeyArr[] =srfkeys.split(";");
			for(String srfkey : srfkeyArr)
			{
				if(!StringUtils.isEmpty(srfkey)){
				entity.setWorklistid(srfkey);
				this.getService().remove(entity);
                }
            }
        }
		return ResponseEntity.ok().body(entity);
	}
	public Worklist mainGridGet(String worklistid){
		return null;
	}
	@PostMapping(value="maingrid/getdraft")
	public Worklist mainGridGetDraft(@Validated Worklist mainItem){
		return mainItem;
	}
	@PostMapping(value="maingrid/create")
	public Worklist mainGridCreate(@Validated Worklist mainItem){
		return mainItem;
	}
	@Value("${ibiz.filePath:/app/file/}")
    private String strFilePath;
	@Autowired
    private com.ibz.demo.ibizutil.service.IBZFILEService ibzfileService;
	/**
	 * [main]表格数据导出
	 * @param searchFilter
	 * @return
	 * @throws
	 * @throws
	 */
	@PostMapping(value="maingrid/exportdata/searchmy")
	public ResponseEntity<Page<JSONObject>> maingridExportDataSearchMy(@Validated @RequestBody WorklistSearchFilter searchFilter) throws IOException, jxl.write.WriteException {
		String fileid=com.baomidou.mybatisplus.core.toolkit.IdWorker.get32UUID();
		String localPath=ExportDataInit(fileid);//输出文件相对路径
		Page<Worklist> searchResult = this.getService().searchMy(searchFilter);//1.查询表格数据
		List<Map<String,Object>> datas=Worklist_Grid_Main.pageToListDatas(searchResult);//2.将数据转换成list
		List<Map<String,String>> colnums=Worklist_Grid_Main.getGridColumnModels();//3.获取表格列头
        java.io.File outputFile=com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().output(strFilePath+localPath,colnums,datas,new Worklist().getDictField(),new Worklist().getDateField());//4.生成导出文件
		com.ibz.demo.ibizutil.helper.DEDataExportHelper.getInstance().saveFileData(outputFile,localPath,fileid,ibzfileService); //5.保存file表记录
		String strDownloadUrl =String.format("ibizutil/download/"+fileid);//6.回传文件路径给前台
        Page<JSONObject> resultObj=Worklist_Grid_Main.getResultPage(searchResult,strDownloadUrl);//7.获取输出对象
			return ResponseEntity.ok().body(resultObj);
	}
	/**
	 * 表格数据导出
	 * @param fileid
	 * @return
	 */
	private String ExportDataInit(String fileid) {
		java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
        String filepath=dateFormat.format(new java.util.Date())+ java.io.File.separator;
        java.text.SimpleDateFormat dateFormat2 = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
		String strTempFileName = fileid+"-"+dateFormat2.format(new java.util.Date())+".xls";
		java.io.File file =new java.io.File(strFilePath+filepath);
		if(!file.exists())
           file.mkdirs();
		return filepath+strTempFileName;
	}

	@Autowired
	RemoteWFServiceImpl wfServiceImpl ;

	@PostMapping(value="maingrid/searchmy")
	public ResponseEntity<Page<Worklist_Grid_Main>> mainGridSearchMy(@Validated @RequestBody WorklistSearchFilter searchFilter) throws IOException {
			Page<Worklist> searchResult = new Page<Worklist>();
			searchResult.setRecords(new ArrayList<Worklist>());
			JSONObject result = wfServiceImpl.findTaskByUser(AuthenticationUser.getAuthenticationUser().getUserid());
			JSONArray data = result.getJSONArray("data");
			for(int i=0;i<data.size();i++){
				JSONObject task = data.getJSONObject(i);
				Worklist worklist = new Worklist() ;
				worklist.setWorklistid(task.getString("id"));
				worklist.setWorklistname(wfServiceImpl.getTaskVariableValue(task,"majortext"));
				worklist.setStep(task.getString("name"));
				worklist.setStarttime(new SimpleDateFormat("yyyy-MM-dd").format(task.getDate("createTime")));
				worklist.setStartuserid(wfServiceImpl.getTaskVariableValue(task,"startuserid"));
				worklist.setStartusername(wfServiceImpl.getTaskVariableValue(task,"startusername"));
				worklist.setBusinesskey(wfServiceImpl.getTaskVariableValue(task,"businessKey"));
				worklist.setWorkflowname(wfServiceImpl.getTaskVariableValue(task,"logicName"));
				searchResult.getRecords().add(worklist) ;
			}
			Page<Worklist_Grid_Main> searchResult_vo_data =Worklist_Grid_Main.fromWorklist(searchResult);
					return ResponseEntity.ok().body(searchResult_vo_data);
	}

	@PostMapping(value="defaultsearchform/load")
	public void defaultSearchFormLoad(){

	}
   	@PostMapping(value="defaultsearchform/loaddraft")
	public ResponseEntity<Worklist_SearchForm_Default> defaultSearchFormLoadDraft(@Validated @RequestBody Worklist_SearchForm_Default searchform){

			WorklistSearchFilter searchfilter =searchform.toWorklistSearchFilter();
			searchform.fromWorklistSearchFilter(searchfilter);
			return ResponseEntity.ok().body(searchform);
	}
	@PostMapping(value="defaultsearchform/search")
	public void defaultSearchFormSearch(){

	}

	@GetMapping(value = "/redirectview/getmodel")
	public ResponseEntity<JSONObject> wfdesign(@Validated @NotBlank(message = "srfkey不允许为空") @RequestParam("srfkey") String srfkey) throws IOException {
		JSONObject ret = new JSONObject();

		JSONObject task= wfServiceImpl.findTask(srfkey) ;
		if(task==null) {
			ret.put("ret", "903") ;
			ret.put("errorInfo", "查询待办错误，当前用户无次待办操作。") ;
			return ResponseEntity.ok().body(ret);
		}
		JSONObject instance = wfServiceImpl.findInstance(task.getString("processInstanceId")) ;

		JSONObject params = new JSONObject();
		params.put("srfkey", instance.getString("businessKey")) ;
		params.put("taskID",srfkey);
		ret.put("viewparams", params);
		ret.put("url", "");

		JSONObject definition = wfServiceImpl.findProcessDefinitions(instance.getString("processDefinitionId"));
        if("testflowdemo".equals(definition.getString("key"))){
                ret.put("viewmodule", "common");
                ret.put("viewtag", "237370ca5f92952c893ac670f66bb762");
                ret.put("viewname", "WorkFlowDemoMobWFEditView");
                ret.put("title", "工作流演示");
                ret.put("openmode", "");
                ret.put("width", "0");
                ret.put("height", "0");
        }
        if("resumeread".equals(definition.getString("key"))){
        }
		return ResponseEntity.ok().body(ret);
	}

 }
