package com.ibz.demo.ibizutil.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.domain.EntityBase;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.helper.HttpUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.beans.factory.annotation.Value;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Primary
public class RemoteWFServiceImpl {

    private Log log = LogFactory.getLog(RemoteWFServiceImpl.class);

    @Value("${ibiz.flowableApiUrl:\"\"}")
    private String baseUrl;

    /**
     * 获取请求头信息
     * @return
     */
    public JSONObject getHeader(){
        JSONObject header = new  JSONObject();
        header.put("Content-Type","application/json");
        String token= null;
        try {
            Base64 base64 = new Base64();
            token = base64.encodeToString("admin:test".getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        header.put("Authorization","Basic "+ token);
        return header;
    }

    /**
     * 获取请求头信息
     * @return
     */
    public JSONObject getHeader(String username,String password){
        JSONObject header = new JSONObject();
        header.put("Content-Type","application/json");
        String token= null;
        try {
            Base64 base64 = new Base64();
            token = base64.encodeToString((username+":"+password).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        header.put("Authorization","Basic "+ token);
        return header;
    }

    /**
     * 查询待办任务
     * @param user
     * @return
     */
    public JSONObject findTaskByUser(String user) throws IOException {
        return findTaskByUser(user,null,null);
    }

    /**
     * 查询待办任务
     * @param user
     * @return
     */
   public JSONObject findTaskByUser(String user,Integer startNo,Integer pageSize) throws IOException {
       return findTaskByUserAndDefinitionKey(user,null,null,null,null);
    }

        /**
     * 查询单条待办任务
     * @return
     */
    public JSONObject findSingleTaskByUserAndDefinitionKey(String user,String workflowKey,String businessKey) throws IOException {
        JSONObject instances = findInstance(workflowKey,businessKey);
        if(instances.getJSONArray("data").size()==0){
            return null;
        }
        JSONObject tasks = findTaskByInstance(instances.getJSONArray("data").getJSONObject(0).getString("id"),user);
        if(tasks.getJSONArray("data").size()>0){
            return tasks.getJSONArray("data").getJSONObject(0);
        }
        return null;
    }


        /**
     * 查询待办任务
     * @param workflowKey
     * @param user
     * @return
     */
    public JSONObject findTaskByUserAndDefinitionKey(String user,String workflowKey) throws IOException {
        return findTaskByUserAndDefinitionKey(user,workflowKey,null,null,null);
    }

    /**
     * 查询待办任务
     * @param workflowKey
     * @param user
     * @return
     */
    public JSONObject findTaskByUserAndDefinitionKey(String user,String workflowKey,Integer startNo,Integer pageSize) throws IOException {
        return findTaskByUserAndDefinitionKey(user,workflowKey,null,startNo,pageSize);
    }

    /**
     * 查询待办任务
     * @param workflowKey
     * @param user
     * @return
     */
    public JSONObject findTaskByUserAndDefinitionKey(String user,String workflowKey,String stepName) throws IOException {
        return findTaskByUserAndDefinitionKey(user,workflowKey,stepName,null,null);
    }

        /**
     * 查询待办任务
     * @param workflowKey
     * @param user
     * @return
     */
     public JSONObject findTaskByUserAndDefinitionKey(String user,String workflowKey,String stepName,Integer startNo,Integer pageSize) throws IOException {
        if(startNo==null){
            startNo = 0;
        }
        if(pageSize==null){
            pageSize = 20;
        }
        String url = baseUrl+ "/query/tasks";
        JSONObject params = new JSONObject();
        params.put("candidateOrAssigned",user);
        params.put("includeProcessVariables",true);
        if(workflowKey!=null&&!workflowKey.isEmpty()){
            params.put("processDefinitionKey",workflowKey);
        }
        if(stepName!=null&&!stepName.isEmpty()){
            params.put("name",stepName);
        }
        params.put("sort","createTime");
        params.put("order","asc");
        params.put("start",startNo);
        params.put("size",pageSize);
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 获取待办任务数
     * @param user
     * @param workflowKey
     * @param stepName
     * @return
     * @throws IOException
     */
    public Integer getTaskCount(String user,String workflowKey,String stepName) throws IOException {
        String url = baseUrl+ "/query/tasks";
        JSONObject params = new JSONObject();
        params.put("candidateOrAssigned",user);
        if(workflowKey!=null&&!workflowKey.isEmpty()){
            params.put("processDefinitionKey",workflowKey);
        }
        if(stepName!=null&&!stepName.isEmpty()){
            params.put("name",stepName);
        }
        params.put("start",0);
        params.put("size",1);
        JSONObject result = JSONObject.parseObject(HttpUtils.post(url,getHeader(),params));
        return result.getInteger("total");
    }

    /**
     * 查找任务
     * @return
     */
    public JSONObject findTask(String taskId) throws IOException {
        String url = baseUrl+ "/runtime/tasks/"+taskId;
        String result = HttpUtils.get(url,getHeader(),null);
        return  JSONObject.parseObject(result);
    }

    /**
     * 查找任务
     * @return
     */
    public JSONObject findTaskByInstance(String instanceID,String user) throws IOException {
        String url = baseUrl+ "/query/tasks";
        JSONObject params = new JSONObject();
        params.put("processInstanceId",instanceID);
        params.put("candidateOrAssigned",user);
        params.put("sort","createTime");
        params.put("order","desc");
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }


    /**
     * 查找实例
     * @param instanceId
     * @return
     */
    public JSONObject findInstance(String instanceId) throws IOException {
        String url = baseUrl+ "/runtime/process-instances/"+instanceId;
        String result = HttpUtils.get(url,getHeader(),null);
        return JSONObject.parseObject(result);
    }

    /**
     * 查找实例
     * @return
     */
    public JSONObject findInstance(String workflowKey,String businessKey) throws IOException {
        String url = baseUrl+ "/runtime/process-instances";
        JSONObject params = new JSONObject();
        params.put("processDefinitionKey",workflowKey);
        params.put("businessKey",businessKey);
        params.put("sort","startTime");
        params.put("order","desc");
        String result = HttpUtils.get(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 检查用户是否存在
     * @return
     */
    public boolean checkUserExist(String userID) throws UnsupportedEncodingException {
        String url = baseUrl+ "/identity/users/"+userID;
        try {
            HttpUtils.get(url,getHeader(),null);
        } catch (HttpClientErrorException e) {
            if(e.getStatusCode()== HttpStatus.NOT_FOUND){
                return false;
            }
        }
        return true;
    }
    /**
     * 创建用户
     * @param userID
     * @param userName
     * @return
     */
    public JSONObject createUser(String userID,String userName,String email,String password) throws IOException {
        String url = baseUrl+ "/identity/users";
        JSONObject params = new JSONObject();
        params.put("id",userID);
        params.put("firstName",userName);
        params.put("email",email);
        params.put("password",password);
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 获取用户名称
     * @param userID
     * @return
     */
    public String getUserNameByUserID(String userID) throws UnsupportedEncodingException {
        if(userID == null && "".equals(userID)){
            return "";
        }
        String url = baseUrl+ "/identity/users/" + userID;
        String result = HttpUtils.get(url,getHeader(),null);
        JSONObject obj =  JSONObject.parseObject(result);
        String realName = "";
        if(obj.get("firstName")!=null){
            realName += obj.getString("firstName");
        }
        if(obj.get("lastName")!=null){
            realName += obj.getString("firstName");
        }
        return realName;
    }

    /**
     * 查找历史实例
     * @return
     */
    public JSONObject findHisInstance(String workflowKey,String businessKey) throws IOException {
        String url = baseUrl+ "/query/historic-process-instances";
        JSONObject params = new JSONObject();
        params.put("processDefinitionKey",workflowKey);
        params.put("businessKey",businessKey);
        params.put("sort","startTime");
        params.put("order","desc");
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 查询流程实例
     * @param inistanceID
     * @return
     * @throws UnsupportedEncodingException
     */
    public JSONObject findInstanceById(String inistanceID) throws UnsupportedEncodingException {
        String url = baseUrl+ "/runtime/process-instances/" + inistanceID;
        String result = HttpUtils.get(url,getHeader(),null);
        return JSONObject.parseObject(result);
    }

    /**
     * 查询流程实例
     * @return
     * @throws UnsupportedEncodingException
     */
    public JSONObject findInstanceByBusinessKey(String workflowKey,String businessKey) throws UnsupportedEncodingException {
        String url = baseUrl+ "/runtime/process-instances";
        JSONObject paramMap = new JSONObject();
        paramMap.put("processDefinitionKey",workflowKey);
        paramMap.put("businessKey",businessKey);
        String result = HttpUtils.get(url,getHeader(),paramMap);
        return JSONObject.parseObject(result);
    }

    /**
     * 查找流程定义
     * @param definitionID
     * @return
     */
    public JSONObject findProcessDefinitions(String definitionID) throws IOException {
        String url = baseUrl+ "/repository/process-definitions/"+definitionID;
        String result = HttpUtils.get(url,getHeader(),null);
        return JSONObject.parseObject(result);
    }

    /**
     * 查找BPMN模型
     * @param definitionID
     * @return
     */
    public JSONObject findBPMNModel(String definitionID) throws IOException {
        String url = baseUrl+ "/repository/process-definitions/"+definitionID+"/model";
        String result = HttpUtils.get(url,getHeader(),null);
        return JSONObject.parseObject(result);
    }

    /**
     * 查历史活动实例
     * @param hisInstanceID
     * @return
     */
    public JSONObject findHisActivityInstance(String hisInstanceID) throws IOException {
        String url = baseUrl+ "/query/historic-activity-instances";
        JSONObject params = new JSONObject();
        params.put("processInstanceId",hisInstanceID);
        params.put("sort","startTime");
        params.put("order","asc");
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 查任务
     * @param instanceId
     * @return
     */
    public JSONObject findActiveTaskByInstance(String instanceId) throws IOException {
        String url = baseUrl+ "/query/tasks";
        JSONObject params = new JSONObject();
        params.put("processInstanceId",instanceId);
        params.put("active",true);
        params.put("sort","createTime");
        params.put("order","desc");
        String result = HttpUtils.post(url,getHeader(),params);
        return JSONObject.parseObject(result);
    }

    /**
     * 获取流程实例运行图
     *
     * @return
     */
    public InputStream getProcessInstanceDiagram(String instanceID) throws IOException {
        String url = baseUrl+ "/runtime/process-instances/"+instanceID+"/diagram";
        return HttpUtils.requestInputStream(url,getHeader(),null);
    }

    /**
     * 查询任务操作
     * @param workflowKey
     * @param businessKey
     * @param user
     * @return
     */
    public List<JSONObject> findTaskLinkByUser(String workflowKey, String businessKey , String user) throws IOException {
        List<JSONObject> links = new ArrayList<JSONObject>() ;
        JSONArray ins = findInstance(workflowKey,businessKey).getJSONArray("data");
        JSONObject processInstance = null;
        if (ins.size() > 0) {
            processInstance = ins.getJSONObject(0);
        }
        if (processInstance != null) {
            JSONArray taskList  = findActiveTaskByInstance(processInstance.getString("id")).getJSONArray("data");
            if (taskList.size() > 0) {
                String strResourceTaskf =taskList.getJSONObject(0).getString("taskDefinitionKey");
                JSONObject bpmModel = findBPMNModel(processInstance.getString("processDefinitionId"));
                JSONObject flowElements = bpmModel.getJSONArray("processes").getJSONObject(0).getJSONObject("flowElementMap");
                for(String key : flowElements.keySet()){
                    if(strResourceTaskf.equals(key)){
                        JSONObject flowElement = flowElements.getJSONObject(key);
                        JSONArray outGoingFlows = flowElement.getJSONArray("outgoingFlows");
                        for(int i=0;i<outGoingFlows.size();i++){
                            links.add(outGoingFlows.getJSONObject(i));
                        }
                    }
                }
            }
        }
        return links ;
    }

        /**
     *
     * @param taskID
     * @return
     * @throws IOException
     */
    public List<JSONObject> findTaskLinkByTask(String taskID) throws IOException {
        List<JSONObject> links = new ArrayList<JSONObject>();
        JSONObject task = findTask(taskID);
        JSONObject processInstance = findInstanceById(task.getString("processInstanceId"));
        String strResourceTaskf =task.getString("taskDefinitionKey");
        JSONObject bpmModel = findBPMNModel(processInstance.getString("processDefinitionId"));
        JSONObject flowElements = bpmModel.getJSONArray("processes").getJSONObject(0).getJSONObject("flowElementMap");
        for(String key : flowElements.keySet()){
            if(strResourceTaskf.equals(key)){
                JSONObject flowElement = flowElements.getJSONObject(key);
                JSONArray outGoingFlows = flowElement.getJSONArray("outgoingFlows");
                for(int i=0;i<outGoingFlows.size();i++){
                    links.add(outGoingFlows.getJSONObject(i));
                }
            }
        }
        return links ;
    }

    /**
     * 删除实例
     * @param instanceID
     */
    public void deleteProcessInstance(String instanceID) throws IOException {
        String url = baseUrl+ "/runtime/process-instances/"+instanceID;
        HttpUtils.delete(url,getHeader(),null);
    }

        /**
     * 执行操作
     * @param action
     * @throws Exception
     */
     public void execTaskByTaskID(String taskID,String action,EntityBase entity,JSONObject variables,JSONObject transientVariables) throws Exception {
        String url = baseUrl+ "/runtime/tasks/"+taskID;
        if(variables == null){
            variables = new JSONObject();
        }
        JSONArray arr = new JSONArray();
        for(String key : variables.keySet()){
            JSONObject obj = new JSONObject();
            obj.put("name",key);
            obj.put("value",variables.get(key));
            arr.add(obj);
        }
        if(transientVariables == null){
            transientVariables = new JSONObject();
        }
        transientVariables.put("activedata",(JSONObject) JSON.toJSON(entity));
        transientVariables.put("action",action);
        for(String key : transientVariables.keySet()){
            JSONObject obj = new JSONObject();
            obj.put("name",key);
            obj.put("value",transientVariables.get(key));
            obj.put("scope", "local");
            arr.add(obj);
        }
        JSONObject params = new JSONObject();
        params.put("variables",arr);
        params.put("action","complete");
        HttpUtils.post(url,getHeader(),params);
    }


   /**
     * 通过DifinitionKey启动流程
     * @param businessKey
     * @return
     */
    public JSONObject startByDifinitionKey(String workflowKey,String businessKey,AuthenticationUser user,EntityBase entity,JSONObject variables,JSONObject transientVariables ) throws Exception {
        JSONArray ins = findInstance(workflowKey,businessKey).getJSONArray("data");
        if(ins.size()>0){
            throw new Exception("数据在流程中,无法重复启动");
        }
        if(!checkUserExist(user.getUserid())){
            createUser(user.getUserid(),user.getUsername(),user.getEmail(),user.getPassword());
        }
        String url = baseUrl+ "/runtime/process-instances";
        if(variables == null){
            variables = new JSONObject();
        }
        variables.put("businessKey",businessKey);
        variables.put("startuserid",user.getUserid());
        variables.put("startusername",user.getPersonname());
        JSONArray arr = new JSONArray();
        for(String key : variables.keySet()){
            JSONObject obj = new JSONObject();
            obj.put("name",key);
            obj.put("value",variables.get(key));
            arr.add(obj);
        }
        if(transientVariables == null){
            transientVariables = new JSONObject();
        }
        transientVariables.put("activedata",(JSONObject) JSON.toJSON(entity));
        for(String key : transientVariables.keySet()){
            JSONObject obj = new JSONObject();
            obj.put("name",key);
            obj.put("value",transientVariables.get(key));
            obj.put("scope", "local");
            arr.add(obj);
        }
        JSONObject params = new JSONObject();
        params.put("variables",arr);
        params.put("processDefinitionKey",workflowKey);
        params.put("businessKey",businessKey);
        params.put("returnVariables",true);
        params.put("tenantId","");
        String result = HttpUtils.post(url,getHeader(user.getUserid(),user.getPassword()),params);
        return JSONObject.parseObject(result);
    }

    public String getTaskVariableValue(JSONObject task,String key){
        if(task.getJSONArray("variables").size()<=0){
            return "";
        }
        JSONArray variables = task.getJSONArray("variables");
        for(int i=0;i<variables.size();i++){
            JSONObject vari = variables.getJSONObject(i);
            if(key.equals(vari.getString("name"))){
                return vari.getString("value");
            }
        }
        return "";
    }
}
