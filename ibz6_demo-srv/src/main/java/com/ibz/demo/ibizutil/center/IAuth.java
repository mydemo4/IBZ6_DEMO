package com.ibz.demo.ibizutil.center;

import com.alibaba.fastjson.JSONObject;

/**
 * 认证功能调用接口
 */
public interface IAuth {
    /**
     * 认证
     * @param params
     * @return
     */
    JSONObject validate(JSONObject params);

    /**
     * 登录
     * @param params
     * @return
     */
    JSONObject login(JSONObject params);
}
