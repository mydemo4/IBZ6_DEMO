package com.ibz.demo.ibizutil.center.impl;

import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.center.AbstractCenter;
import com.ibz.demo.ibizutil.center.IAuth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 远端用户认证中心（调用）
 * 登录、检查是否在线等。
 */
@Slf4j
@Component
public class AuthCenter extends AbstractCenter implements IAuth {
    @Value("${ibiz.center.auth.baseURL}")
    private String baseURL;
    @Value("${ibiz.center.auth.validate}")
    private String validateURI;
    @Value("${ibiz.center.auth.login:/login}")
    private String loginURI;
    @Value("${ibiz.systemid}")
    private String systemid;
    /**
     * 用户登录验证、token验证。
     *
     * @param params Authentication:token。
     * @return
     */
    @Override
    public JSONObject validate(JSONObject params) {
        return callServiceAPI(baseURL + validateURI, params, JSONObject.class);
    }

    @Override
    public JSONObject login(JSONObject params) {
        params.put("systemid", systemid);
        return callServiceAPI(baseURL + loginURI, params, JSONObject.class);
    }
}
