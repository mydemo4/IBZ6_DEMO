package com.ibz.demo.ibizutil.cache.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 缓存操作工具类(实现）
 */
@Slf4j
@Component
public class CacheHelper implements ICacheHelper {
    @Autowired
    private ICacheHelper cacheHelper;

    @Override
    public JSONObject get(String key) {
        return null;
    }

    @Override
    public void add(String key, String jsonKey, Object jsonValue) {
        JSONObject cacheJSON = cacheHelper.get(key);
        if(cacheJSON==null){
            cacheJSON = new JSONObject();
        }
        cacheJSON.put(jsonKey, jsonValue);
        cacheHelper.put(key, cacheJSON);
    }

    @Override
    public JSONObject put(String key, JSONObject json) {
        log.debug("缓存中的数据："+json);
        return json;
    }

    @Override
    public void clear(String key) {

    }
}
