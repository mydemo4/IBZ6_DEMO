package com.ibz.demo.ibizutil.security;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.SpringContextHolder;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ibizutil.center.IUser;
import com.ibz.demo.ibizutil.domain.EntityBase;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;

/**
 * spring security 权限管理类
 * 重写权限控制方法
 */
@Component
public class AuthPermissionEvaluator implements PermissionEvaluator {

    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验

    private String orgfield = "orgfield";
    private String orgsecfield = "orgsecfield";
    private String createmanfield = "createmanfield";
    private String updatemanfield = "updatemanfield";

    @Autowired
    private ISecurity securityCenter;
    @Autowired
    private IUser userCenter;

    /**
     * 表格权限检查 ：用于检查当前用户是否拥有表格数据的读取、删除权限
     *
     * @param authentication
     * @param obj_action     表格行为，如：[READ,DELETE]
     * @param grid_param     表格参数，如：当前表格所处实体(EntityName)、表格删除的数据主键(srfkeys)
     * @return true/false true则允许当前行为，false拒绝行为
     */
    @Override
    public boolean hasPermission(Authentication authentication, Object obj_action, Object grid_param) {
        String action = "";
        String entityName;
        boolean isPermission = false;

        if (AuthenticationUser.getAuthenticationUser().getSuperuser() == 1 || !enableRemoteValid)
            return true;  //系统没开启权限、超级管理员 两种情况不进行权限检查

        if (obj_action instanceof String)
            action = (String) obj_action;

        if (StringUtils.isEmpty(action))
            return false;

        if (action.equals("DELETE")) {
            if (grid_param instanceof ArrayList) {
                List param_list = (ArrayList) grid_param;
                EntityBase cur_entity = (EntityBase) param_list.get(0);
                Map<String, String> srfkeys = (Map<String, String>) param_list.get(1);
                entityName = cur_entity.getClass().getSimpleName();
                ServiceImplBase service = SpringContextHolder.getBean(String.format("%s%s", entityName, "ServiceImpl"));//获取当前实体service
                Map<String, String> permissionField = getPermissionField(cur_entity);//获取系统预置属性列表
                if (StringUtils.isEmpty(permissionField))//如果没配置系统预置属性，则不进行权限检查
                    return true;
                JSONArray data_owner = getOwnerList_grid(service, srfkeys, permissionField);//获取所有要删除数据的归属
                JSONObject paramMap = preparePostParam(entityName, action, data_owner);//准备请求参数
                isPermission = securityCenter.hasGridPermission(paramMap);//通过权限中心校验用户权限，获取校验结果
            }
        } else {
            entityName = (String) grid_param;
            JSONObject paramMap = preparePostParam(entityName, action, null);
            isPermission = securityCenter.hasGridPermission(paramMap);//读权限
        }
        return isPermission;
    }

    /**
     * 表单权限检查 ：用于检查当前用户是否拥有表单的新建、编辑、删除权限
     *
     * @param authentication
     * @param srfkey         当前操作数据的主键
     * @param action         当前操作行为：如：[READ、UPDATE、DELETE]
     * @param cur_entity     当前操作的实体对象
     * @return true/false true则允许当前行为，false拒绝行为
     */
    @Override
    public boolean hasPermission(Authentication authentication, Serializable srfkey, String action, Object cur_entity) {

        boolean isPermission;
        EntityBase entityBase = null;
        if (AuthenticationUser.getAuthenticationUser().getSuperuser() == 1 || !enableRemoteValid)
            return true;  //系统没开启权限、超级管理员 两种情况不进行权限检查

        if (cur_entity instanceof EntityBase)
            entityBase = (EntityBase) cur_entity;

        if (StringUtils.isEmpty(entityBase))
            return false;
        try {
            String entityName = entityBase.getClass().getSimpleName();
            ServiceImplBase service = SpringContextHolder.getBean(String.format("%s%s", entityName, "ServiceImpl"));
            Map<String, String> permissionField = getPermissionField(entityBase);
            if (StringUtils.isEmpty(permissionField))//如果没配置系统预置属性，则不进行权限检查
                return true;
            if (action.equals("CREATE")) {//表单新建
                JSONObject paramMap = preparePostParam(entityName, action, null);
                isPermission = securityCenter.hasFormPermission(paramMap);//通过权限中心校验用户权限，获取校验结果
            } else {//表单编辑
                EntityBase entity = (EntityBase) service.getById(srfkey);
                JSONObject data_owner = getOwnerList(permissionField, entity);//获取当前数据归属
                JSONObject paramMap = preparePostParam(entityName, action, data_owner);
                isPermission = securityCenter.hasFormPermission(paramMap);//通过权限中心校验用户权限，获取校验结果
            }
        } catch (Exception e) {
            throw new BadRequestAlertException("权限检查出现异常，异常原因为" + e, "", "");
        }
        return isPermission;
    }

    /**
     * 计算表格所删除的数据的归属
     *
     * @param service         当前实体的服务对象
     * @param delete_keys     当前删除数据的主键
     * @param permissionField 系统预置属性[组织、部门]
     * @return
     */
    private JSONArray getOwnerList_grid(ServiceImplBase service, Map<String, String> delete_keys, Map<String, String> permissionField) {
        JSONArray result = new JSONArray();
        if (!delete_keys.containsKey("srfkeys"))
            return result;

        String srfkey = delete_keys.get("srfkeys");
        if (StringUtils.isEmpty(srfkey))
            return result;

        List<String> delete_keys_list = Arrays.asList(srfkey.split(";"));
        Collection<EntityBase> delete_entities = service.listByIds(delete_keys_list);
        for (EntityBase entity : delete_entities) {
            JSONObject data_owner = getOwnerList(permissionField, entity);
            result.add(data_owner);
        }
        return result;
    }

    /**
     * 准备请求参数
     *
     * @param entityName 当前实体名称
     * @param action     当前操作行为
     * @param dataOwner  当前操作数据的归属
     * @return
     */
    private JSONObject preparePostParam(String entityName, String action, Object dataOwner) {
        String userId = AuthenticationUser.getAuthenticationUser().getUserid();
        JSONObject paramMap = new JSONObject();
        paramMap.put("entity", entityName);
        paramMap.put("action", action);
        paramMap.put("userid", userId);
        paramMap.put("owner", dataOwner);
        return paramMap;
    }

    /**
     * 计算数据归属（表单、表格共用）
     *
     * @param permissionField 预置数据集合
     * @param entity          当前实体数据对象
     * @return
     */
    private JSONObject getOwnerList(Map<String, String> permissionField, EntityBase entity) {
        JSONObject owner_obj = new JSONObject();
        try {
            if (StringUtils.isEmpty(permissionField))
                throw new BadRequestAlertException("未能获取到当前实体预置的权限字段属性，请检查是否已经配置该属性！", "", "");

            AuthenticationUser authenticationUser = AuthenticationUser.getAuthenticationUser();
            Object userid = AuthenticationUser.getAuthenticationUser().getUserid();//当前用户标识


            JSONObject userInfo = AuthenticationUser.getAuthenticationUser().getUserInfo();
            if (userInfo == null) {
                JSONObject params = new JSONObject();
                params.put("loginname", authenticationUser.getLoginname());
                userInfo = userCenter.getUserInfo(params);
            }
            if (StringUtils.isEmpty(userInfo))
                throw new BadRequestAlertException("未能获取到当前用户的信息，请联系管理员！", "", "");


            //如果设置了组织字段，生成对应的权限条件附加字段。
            if (permissionField.containsKey(orgfield)) {
                JSONObject orgInfo = userInfo.getJSONObject("org");
                JSONArray porgList = orgInfo.getJSONArray("porg");//上级组织
                JSONArray sorgList = orgInfo.getJSONArray("sorg");//下级组织
                String cur_org = orgInfo.getString("curorg");//当前组织

                Object cur_org_value = entity.get(permissionField.get(orgfield));
                if (porgList != null && porgList.contains(cur_org_value)) {//上级单位
                    owner_obj.put("orgdr", "PORG");
                }
                if (sorgList != null && sorgList.contains(cur_org_value)) {//下级单位
                    owner_obj.put("orgdr", "SORG");
                }
                if (cur_org != null && cur_org.equals(cur_org_value)) {//当前单位
                    owner_obj.put("orgdr", "CURORG");
                }
            }

            //如果设置了部门字段，生成对应的条权限条件附加字段。
            if (permissionField.containsKey(orgsecfield)) {
                Object cur_orgsec_value = entity.get(permissionField.get(orgsecfield));

                JSONObject org_dept_info = userInfo.getJSONObject("orgdept");
                JSONArray porgdeptlist = org_dept_info.getJSONArray("porgdept");//上级部门
                JSONArray sorgdeptlist = org_dept_info.getJSONArray("sorgdept");//下级部门
                String cur_dept = org_dept_info.getString("curorgdept");//当前部门

                if (porgdeptlist != null && porgdeptlist.contains(cur_orgsec_value)) {//上级部门
                    owner_obj.put("deptdr", "PDEPT");
                }
                if (sorgdeptlist != null && sorgdeptlist.contains(cur_orgsec_value)) {//下级部门
                    owner_obj.put("deptdr", "SDEPT");
                }
                if (cur_dept != null && cur_dept.equals(cur_orgsec_value)) {//当前部门
                    owner_obj.put("deptdr", "CURDEPT");
                }
            }

            JSONArray userdr = new JSONArray();
            //如果设置了创建人字段，生成对应的条权限条件附加字段。
            if (permissionField.containsKey(createmanfield)) {
                Object cur_createman_value = entity.get(permissionField.get(createmanfield));
                if (userid.equals(cur_createman_value)) {//创建人
                    userdr.add("CREATEMAN");
                }

            }
            //如果设置了更新人字段，生成对应的条权限条件附加字段。
            if (permissionField.containsKey(updatemanfield)) {
                Object cur_updateman_value = entity.get(permissionField.get(updatemanfield));
                if (userid.equals(cur_updateman_value)) {//更新人
                    userdr.add("UPDATEMAN");
                }
            }

            if (owner_obj.size() == 0 && userdr.size() == 0) {
                owner_obj.put("other", true);//当前数据归属于其他单位
            } else {
                owner_obj.put("other", false); //当前数据属于本级或者上下级。
            }
            owner_obj.put("userdr", userdr);

        } catch (Exception e) {
            throw new BadRequestAlertException("执行权限检查发生异常，异常原因为" + e, "", "");
        }
        return owner_obj;
    }

    /**
     * 获取平台配置的实体属性中，系统预置权限字段
     * 获取实体权限字段 orgid/orgsecid
     *
     * @param entityBase
     * @return
     */
    private Map<String, String> getPermissionField(EntityBase entityBase) {
        Map<String, String> permissionFiled = new HashMap<>();
        String orgField = "";  //组织权限默认值
        String orgsecField = ""; //部门权限默认值
        String createmanField = "";//创建人默认值
        String updatemanField = "";//更新人默认值
        Map<Field, PreField> preFields = entityBase.SearchPreField(); //从缓存中获取当前类预置属性
        for (Map.Entry<Field, PreField> entry : preFields.entrySet()) {
            Field prefield = entry.getKey();//获取注解字段
            PreField fieldAnnotation = entry.getValue();//获取注解值
            PredefinedType prefieldType = fieldAnnotation.preType();
            //用户配置系统预置属性-组织机构标识
            if (prefieldType == PredefinedType.ORGID) {
                orgField = prefield.getName();
                permissionFiled.put("orgfield", orgField);
            }
            //用户配置系统预置属性-部门标识
            if (prefieldType == PredefinedType.ORGSECTORID) {
                orgsecField = prefield.getName();
                permissionFiled.put("orgsecfield", orgsecField);
            }

            if (prefieldType == PredefinedType.CREATEMAN) {
                createmanField = prefield.getName();
                permissionFiled.put("createmanfield", createmanField);
            }

            if (prefieldType == PredefinedType.UPDATEMAN) {
                updatemanField = prefield.getName();
                permissionFiled.put("updatemanfield", updatemanField);
            }

        }
        return permissionFiled;
    }

}