package com.ibz.demo.ibizutil.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/*
 * 菜单项过滤工具类
 *
 * */
@Component
public class MenuAppMenuHelper {

    @Autowired
    private ISecurity securityCenter;

    /**
     * 过滤菜单
     *
     * @param jsonNode 本项目的菜单数据
     */
    public JsonNode menuFilter(JsonNode jsonNode) throws Exception {
        String loginname = AuthenticationUser.getAuthenticationUser().getLoginname();
        //从权限中心获取统一资源
        JSONArray uniRespermissions = securityCenter.getUniRes(loginname);
        JSONObject jsonNode1 = JSONObject.parseObject(jsonNode.toString());
        //转为jsonobject进行过滤
        parseRoot(jsonNode1, uniRespermissions);
        ObjectMapper mapper = new ObjectMapper();
        //JSONObject转JsonNode
        jsonNode = mapper.readTree(jsonNode1.toString());
        return jsonNode;
    }

    /**
     * 解析顶层菜单节点
     *
     * @param rootNode    菜单根节点
     * @param permissions 菜单授权列表
     */
    private static void parseRoot(JSONObject rootNode, JSONArray permissions) {
        if (rootNode.containsKey("items")) {
            JSONArray rootNodeArr = rootNode.getJSONArray("items");
            parseNode(rootNodeArr, permissions);
        }
    }

    /**
     * 解析子节点
     *
     * @param nodeArr     菜单数组
     * @param permissions 菜单授权列表
     */
    private static void parseNode(JSONArray nodeArr, JSONArray permissions) {
        List<JSONObject> nPermissions = new ArrayList<JSONObject>();//无权限节点列表
        for (int i = 0; i < nodeArr.size(); i++) {
            JSONObject rootNodeObj = nodeArr.getJSONObject(i);
            if (rootNodeObj.containsKey("items")) { //判断是否有子节点
                if (!hasMenuPermission(rootNodeObj, permissions)) {
                    nPermissions.add(rootNodeObj);//不在授权列表中，则将当前节点添加到无权限列表中
                }

                JSONArray rootNodeArr = rootNodeObj.getJSONArray("items");
                parseNode(rootNodeArr, permissions);//递归检查子节点
            } else {
                if (!hasMenuPermission(rootNodeObj, permissions)) //统一资源授权判断
                    nPermissions.add(rootNodeObj);
            }
        }
        clearNpermissions(nodeArr, nPermissions);//清除菜单中无权限列表中的节点
    }

    /**
     * 清除菜单中无权限列表中的节点
     *
     * @param nodeArr      菜单列表
     * @param nPermissions 无权限菜单列表
     */
    private static void clearNpermissions(JSONArray nodeArr, List<JSONObject> nPermissions) {
        for (JSONObject nPermission : nPermissions) {
            if (nodeArr.contains(nPermission)) {
                nodeArr.remove(nPermission);
            }
        }
    }

    /**
     * 判断节点是否有前端展示权限。
     * <p>
     * case1:没有设置统一资源标记，"uniResId":"" ---true
     * case2: uniResId, 用户没有任何统一资源权限  ---false
     * case3:"uniResId":"uniResId" && 用户在UniRes权限集合中，有对应uniTagName权限。--true
     *
     * @param rootNodeObj 菜单节点
     * @return
     */
    private static boolean hasMenuPermission(JSONObject rootNodeObj, JSONArray uniResPermissions) {
        String uniResId = rootNodeObj.getString("resourceid");
        if (StringUtils.isEmpty(uniResId)) {//case1:
            return true;
        }
        if (uniResPermissions == null) {    //case2:
            return false;
        }

        for (int i = 0; i < uniResPermissions.size(); i++) {    // case3
            if (matchSingleUniResPermission(uniResPermissions.get(i), uniResId)) {
                return true;
            }
        }
        return false;
    }

    private static boolean matchSingleUniResPermission(Object uniRes, String resourceTag) {
        String uniResName = ((JSONObject) JSON.toJSON(uniRes)).getString("uniresid");
        if (resourceTag.equals(uniResName)) {
            return true;
        }
        return false;
    }
}
