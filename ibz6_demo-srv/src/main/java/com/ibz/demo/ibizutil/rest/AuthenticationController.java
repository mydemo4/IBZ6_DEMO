package com.ibz.demo.ibizutil.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.center.IAuth;
import com.ibz.demo.ibizutil.center.IUser;
import com.ibz.demo.ibizutil.cache.utils.ICacheHelper;
import com.ibz.demo.ibizutil.errors.BadRequestAlertException;
import com.ibz.demo.ibizutil.security.AuthTokenUtil;
import com.ibz.demo.ibizutil.security.AuthenticationInfo;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import com.ibz.demo.ibizutil.security.AuthorizationLogin;
import com.ibz.demo.ibizutil.service.IBZUSERService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.util.DigestUtils;

@Slf4j
@RestController
@RequestMapping("/")
public class AuthenticationController
{
    @Value("${ibiz.auth.pwencry:false}")
    private boolean pwencry;
    @Autowired
    private AuthTokenUtil jwtTokenUtil;
    @Autowired
    @Qualifier("AuthenticationUserService")
    private UserDetailsService userDetailsService;
    @Autowired
    private IBZUSERService userService;
    @Autowired
    private IAuth authCenter;
    @Autowired
    private IUser userCenter;
    @Autowired
    private ICacheHelper cacheHelper;
    @Value("${ibiz.token.header}")
    private String tokenHeader;
    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验

    @PostMapping(value = "${ibiz.auth.path:ibizutil/login}")
    public ResponseEntity<AuthenticationInfo> login(@Validated @RequestBody AuthorizationLogin authorizationLogin){
        if(enableRemoteValid){
            return remoteLoginValid(authorizationLogin);
        }
        else{
            return localLoginValid(authorizationLogin);
        }
    }

    @GetMapping(value = "${ibiz.auth.account:ibizutil/account}")
    public ResponseEntity<AuthenticationUser> getUserInfo(){
        UserDetails userDetails = (UserDetails)  SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AuthenticationUser authuserdetail;
        if(userDetails==null){
            throw new BadRequestAlertException("未能获取用户信息",null,null);
        }
        else if(userDetails instanceof AuthenticationUser ) {
            authuserdetail= (AuthenticationUser)userDetails;
        }
        else {
            authuserdetail= (AuthenticationUser)userDetailsService.loadUserByUsername(userDetails.getUsername());
        }
        return ResponseEntity.ok().body(authuserdetail);
    }
    /**
    * 远程登录认证
    * @param authorizationLogin
    * @return
    */
    private ResponseEntity<AuthenticationInfo> remoteLoginValid(AuthorizationLogin authorizationLogin){
        //清理登录账号相关的所有远程调用缓存
        cacheHelper.clear(authorizationLogin.getLoginname());
        //认证中心登录
        JSONObject loginResult = authCenter.login((JSONObject) JSON.toJSON(authorizationLogin));
        //调用用户中心获取用户信息。
        AuthenticationInfo userInfo = getUserInfo(authorizationLogin.getLoginname(), loginResult.getString(tokenHeader));
        log.info("登录成功，返回的用户信息："+userInfo);
        return ResponseEntity.ok().body(userInfo);
    }


    /**
    * 本地登录认证
    * @param authorizationLogin
    * @return
    */
    private ResponseEntity<AuthenticationInfo> localLoginValid(AuthorizationLogin authorizationLogin){
        userService.resetByUsername(authorizationLogin.getUsername());
        final AuthenticationUser authuserdetail = (AuthenticationUser) userDetailsService.loadUserByUsername(authorizationLogin.getUsername());
        String password=authorizationLogin.getPassword();
        if(pwencry){
            password= DigestUtils.md5DigestAsHex(authorizationLogin.getPassword().getBytes());
        }
        if(!authuserdetail.getPassword().equals( password )){
            throw new BadRequestAlertException("用户名密码错误",null,null);
        }
        // 生成令牌
        final String token = jwtTokenUtil.generateToken(authuserdetail);
        // 返回 token
        return ResponseEntity.ok().body(new AuthenticationInfo(token,authuserdetail));
    }

    /**
     * 登录成功后，从用户中心获取用户信息。
     * 用于前端页面渲染。
     * @param loginname 登录名
     * @param activeToken   认证中心获得的token
     * @return  token，用户中心。
     */
    private AuthenticationInfo getUserInfo(String loginname, String activeToken) {
        JSONObject params = new JSONObject();
        params.put("loginname", loginname);
        AuthenticationUser authUserDetails = new AuthenticationUser();
        authUserDetails.setToken(activeToken);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(authUserDetails, null, authUserDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //从用户中心获取用户信息
        JSONObject userInfo = userCenter.getUserInfo(params);
        authUserDetails.setUserInfo(userInfo);
        AuthenticationUser userDetails = AuthenticationUser.getAuthenticationUser();
        return new AuthenticationInfo(activeToken, userDetails);
    }
}
