package com.ibz.demo.ibizutil.valuerule.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class SysValueRule {
    //系统内置值规则名、生成方法名Map
    public static Map<String, String> ruleMap;

    public static boolean validateByName(String name, String value) throws Exception {
        String methodname = ruleMap.get(name);
        Method method = SysValueRule.class.getDeclaredMethod(methodname, String.class);
        boolean isValid = (boolean) method.invoke(new SysValueRule(), value);
        return isValid;

    }

    /**
     * 值规则名与方法对照表。
     */
    static {
        ruleMap = new HashMap<>();
        ruleMap.put("浮点数", "match_0");
        ruleMap.put("正整数", "match_1");
        ruleMap.put("email地址", "match_2");
        ruleMap.put("非负整数（正整数 + 0）", "match_3");
        ruleMap.put("由数字和26个英文字母组成的字符串", "match_4");
        ruleMap.put("由26个英文字母的大写组成的字符串", "match_5");
        ruleMap.put("由数字、26个英文字母或者下划线组成的字符串", "match_6");
        ruleMap.put("非正浮点数（负浮点数 + 0）", "match_7");
        ruleMap.put("代码名称", "match_8");
        ruleMap.put("非正整数（负整数 + 0）", "match_9");
        ruleMap.put("正浮点数", "match_10");
        ruleMap.put("整数", "match_11");
        ruleMap.put("由26个英文字母组成的字符串", "match_12");
        ruleMap.put("由26个英文字母的小写组成的字符串", "match_13");
        ruleMap.put("负整数", "match_14");
        ruleMap.put("负浮点数", "match_15");
        ruleMap.put("文件名称", "match_16");
        ruleMap.put("非负浮点数（正浮点数 + 0）", "match_17");
        ruleMap.put("URL", "match_18");
    }

    /**
     * 系统值规则名称：浮点数
     * 值规则信息：内容必须为浮点数
     */
    private static boolean match_0(String value){
        //(-?\d+)(\.\d+)?
        String reg = "(-?\\d+)(\\.\\d+)?";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：正整数
     * 值规则信息：内容必须为正整数
     */
    private static boolean match_1(String value){
        //[0-9]*[1-9][0-9]*
        String reg = "[0-9]*[1-9][0-9]*";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：email地址
     * 值规则信息：内容必须为电子邮件格式
     */
    private static boolean match_2(String value){
        //[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+
        String reg = "[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：非负整数（正整数 + 0）
     * 值规则信息：内容必须为非负整数（正整数 + 0）
     */
    private static boolean match_3(String value){
        //\d+
        String reg = "\\d+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：由数字和26个英文字母组成的字符串
     * 值规则信息：内容必须由数字和26个英文字母组成的字符串
     */
    private static boolean match_4(String value){
        //[A-Za-z0-9]+
        String reg = "[A-Za-z0-9]+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：由26个英文字母的大写组成的字符串
     * 值规则信息：内容必须由26个英文字母的大写组成的字符串
     */
    private static boolean match_5(String value){
        //[A-Z]+
        String reg = "[A-Z]+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：由数字、26个英文字母或者下划线组成的字符串
     * 值规则信息：内容必须由数字、26个英文字母或者下划线组成的字符串
     */
    private static boolean match_6(String value){
        //\w+
        String reg = "\\w+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：非正浮点数（负浮点数 + 0）
     * 值规则信息：内容必须为非正浮点数（负浮点数 + 0）
     */
    private static boolean match_7(String value){
        //((-\d+(\.\d+)?)|(0+(\.0+)?))+
        String reg = "((-\\d+(\\.\\d+)?)|(0+(\\.0+)?))+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：代码名称
     * 值规则信息：内容必须为字符及数字组成，且开始必须为字符。
     */
    private static boolean match_8(String value){
        //[a-zA-Z_$][a-zA-Z0-9_$]*
        String reg = "[a-zA-Z_$][a-zA-Z0-9_$]*";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：非正整数（负整数 + 0）
     * 值规则信息：内容必须为正整数（负整数 + 0）
     */
    private static boolean match_9(String value){
        //((-\d+)|(0+))+
        String reg = "((-\\d+)|(0+))+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：正浮点数
     * 值规则信息：内容必须为正浮点数
     */
    private static boolean match_10(String value){
        //(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))+
        String reg = "(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：整数
     * 值规则信息：内容必须为整数
     */
    private static boolean match_11(String value){
        //-?\d+
        String reg = "-?\\d+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：由26个英文字母组成的字符串
     * 值规则信息：内容必须为26个英文字母组成的字符串
     */
    private static boolean match_12(String value){
        //[A-Za-z]+
        String reg = "[A-Za-z]+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：由26个英文字母的小写组成的字符串
     * 值规则信息：内容必须为26个英文字母的小写组成的字符串
     */
    private static boolean match_13(String value){
        //[a-z]+
        String reg = "[a-z]+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：负整数
     * 值规则信息：内容必须为负整数
     */
    private static boolean match_14(String value){
        //-[0-9]*[1-9][0-9]*
        String reg = "-[0-9]*[1-9][0-9]*";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：负浮点数
     * 值规则信息：内容必须为负浮点数
     */
    private static boolean match_15(String value){
        //(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))+
        String reg = "(-(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*)))+";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：文件名称
     * 值规则信息：内容不能包括/、\、?、*、<、>、:
     */
    private static boolean match_16(String value){
        //[^\\\/\:\*\?\"\<\>\|]+(\[^\\\/\:\*\?\"\<\>\|]+)*
        String reg = "[^\\\\\\/\\:\\*\\?\\\"\\<\\>\\|]+(\\[^\\\\\\/\\:\\*\\?\\\"\\<\\>\\|]+)*";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：非负浮点数（正浮点数 + 0）
     * 值规则信息：内容必须为非负浮点数（正浮点数 + 0）
     */
    private static boolean match_17(String value){
        //\d+(\.\d+)?
        String reg = "\\d+(\\.\\d+)?";
        return value.matches(reg);
    }

    /**
     * 系统值规则名称：URL
     * 值规则信息：内容必须为有效URL格式
     */
    private static boolean match_18(String value){
        //[a-zA-z]+://(\w+(-\w+)*)(\.(\w+(-\w+)*))*(\?\S*)?
        String reg = "[a-zA-z]+://(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*(\\?\\S*)?";
        return value.matches(reg);
    }

}
