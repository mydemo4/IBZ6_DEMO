package com.ibz.demo.ibizutil.center;

import com.alibaba.fastjson.JSONObject;

/**
 * 用户信息调用接口
 */
public interface IUser {
    /**
     * 获取用户信息、账号信息、组织部门信息、上下级信息。
     * @param params
     * @return
     */
    JSONObject getUserInfo(JSONObject params);

    /**
     * 同步管理员账号到用户中心。
     * @param relPath
     * @param pathid
     * @param paramMap
     */
    void syncData(String relPath, String pathid, JSONObject paramMap);

}
