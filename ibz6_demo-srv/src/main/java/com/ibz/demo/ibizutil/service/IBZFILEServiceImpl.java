package com.ibz.demo.ibizutil.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ibz.demo.ibizutil.domain.IBZFILE;
import com.ibz.demo.ibizutil.mapper.IBZFILEMapper;
import org.springframework.stereotype.Service;

/**
 * 实体[IBZFILE] 服务对象接口
 */
@Service
public class IBZFILEServiceImpl extends ServiceImpl<IBZFILEMapper, IBZFILE> implements IBZFILEService
{

}