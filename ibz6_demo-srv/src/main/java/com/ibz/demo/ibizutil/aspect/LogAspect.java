package com.ibz.demo.ibizutil.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 单体系统全局日志切面类
 * 用于查看每个Module下的方法执行结果。
 *
 * @author nancy
 * @date 2020-3-20
 */
@Slf4j
@Aspect
@Component
public class LogAspect {
    @Pointcut(
          "execution(public * com.ibz.demo.testb..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.YSMK..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.xiuyaoyao..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.SLMK..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.wf..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.V6Model..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.security..*.*(..)) ||" + 
              "execution(public * com.ibz.demo.V5Model..*.*(..))"
    )
    private void pointCut() {
    }

    @Around(value = "pointCut()")
    public Object aroundLog(ProceedingJoinPoint joinPoint) throws Throwable {
        //设置日志位置，aop类-->切入点位置
        String className = joinPoint.getSignature().getDeclaringTypeName();
        Field nameField = log.getClass().getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(log,className);
        nameField.setAccessible(false);

        //设置日志中执行的方法签名：类名.方法名（参数类型1，参数类型2...)
        String methodSignature = getMethodSignature(joinPoint);

        //监控返回值、参数、执行时间
        Object[] params = joinPoint.getArgs();
        Object returnedValue = null;
        long start = System.currentTimeMillis();

        log.info("【方法】{}启动", methodSignature);
        log.info("【传入参数】：{}", params == null ? "null" : JSONObject.toJSON(params).toString());

        //0代表正常结束，1抛出了异常，2准正常结束
        int status = 0;
        String errormsg = null;
        try {
            returnedValue = joinPoint.proceed();
            return returnedValue;
        } catch (Throwable throwable) {
            errormsg = throwable.getMessage();
            if (throwable instanceof RuntimeException) {
                status = 1;
                throwable.printStackTrace();
            } else {
                status = 2;
            }
            throw throwable;

        } finally {
            //形成本地日志信息
            String returnedStr = returnedValue == null ? "null" : JSON.toJSONString(returnedValue);
            long timeCost = System.currentTimeMillis() - start;
            switch (status) {
                case 0:
                    log.info("【方法】{}正常结束",methodSignature);
                    log.debug("【返回值】为{}，【耗时】{}ms",returnedStr, timeCost);

                    break;
                case 1:
                    log.error("【方法】{}发生了异常，【异常信息】为{}", methodSignature, errormsg);
                    break;
                case 2:
                    log.info("【方法】{}准正常结束, 抛出了检查异常", methodSignature);
                    log.debug("【异常信息】为{}，【耗时】{}ms", errormsg, timeCost);
                    break;
                default:
            }
        }
    }
    private String getMethodSignature(ProceedingJoinPoint joinPoint){
        String methodSignature = joinPoint.getSignature().toShortString();
        Object[] params = joinPoint.getArgs();

        String[] paramtypes = new String[params.length];
        for(int i=0;i<params.length;i++){
            paramtypes[i] = params[i].getClass().getSimpleName();
        }
        return methodSignature.replace("..",String.join(",",paramtypes));
    }
}
