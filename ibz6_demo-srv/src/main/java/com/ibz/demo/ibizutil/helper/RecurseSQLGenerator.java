package com.ibz.demo.ibizutil.helper;

/**
 * 有限次递归sql生成器。
 */
public class RecurseSQLGenerator {

    /**
     * 递归查询所有节点，格式见文件尾部。
     *
     * @param s     基础语句  select t#.orgid from t_org t# where
     * @param loop  递归查询层级, 1:只查询当前子节点，不进行递归。
     *              仅支持1-5层设置，超过范围，会设置为1或者5。
     * @param value 根节点标识
     * @param keycolumn 主键列名
     * @return
     */
    public static String getChilderenQuerySqL(String s, int loop, String value,String keycolumn) {
        if (loop <= 0) {
            loop = 1;
        } else if (loop > 5) {
            loop = 5;
        }

        String[] sqls = new String[loop];
        for (int i = 0; i < loop; i++) {
            sqls[i] = getSql(s, i + 1, value,keycolumn);
        }

        return unionsql(sqls);
    }

    public static String getSql(String s, int loop, String value,String keyColumn) {
        if (loop == 1) {
            return String.format(s.replace("#", "1") + "t1.p%s = '%s'",keyColumn, value);
        } else {
            return String.format(s.replace("#", String.valueOf(loop)) + " EXISTS (" + getSql(s, loop - 1, value,keyColumn) + " and t%s.p%s = t%s.%s)",loop,keyColumn,loop-1,keyColumn);
        }
    }

    public static String unionsql(String... s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length; i++) {
            builder.append(String.format("( %s )", s[i]));
            if (i == s.length - 1) {
                break;
            }
            builder.append(" union ");
        }
        return builder.toString();
    }

}

/*
(select t1.orgid from t_org t1
where t1.porgid = 'a1'
)
union(
select t2.orgid from t_org t2 where
EXISTS (select t1.orgid from t_org t1
where t1.porgid = 'a1' and t2.porgid = t1.orgid)
)
union(
select t3.orgid from t_org t3 where
EXISTS (
select t2.orgid from t_org t2 where
EXISTS (select t1.orgid from t_org t1
where t1.porgid = 'a1' and t2.porgid = t1.orgid)
and t3.porgid = t2.orgid)
)
union(
select t4.orgid from t_org t4 where
EXISTS	(
select t3.orgid from t_org t3 where
EXISTS (
select t2.orgid from t_org t2 where
EXISTS (select t1.orgid from t_org t1
where t1.porgid = 'a1' and t2.porgid = t1.orgid)
and t3.porgid = t2.orgid)
and t4.porgid = t3.orgid)
)
union(
select t5.orgid from t_org t5 where
EXISTS(
select t4.orgid from t_org t4 where
EXISTS	(
select t3.orgid from t_org t3 where
EXISTS (
select t2.orgid from t_org t2 where
EXISTS (select t1.orgid from t_org t1
where t1.porgid = 'a1' and t2.porgid = t1.orgid)
and t3.porgid = t2.orgid)
and t4.porgid = t3.orgid)
and t5.porgid = t4.orgid)
)
*/
