package com.ibz.demo.ibizutil.center;

import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.helper.HttpUtils;
import com.ibz.demo.ibizutil.security.AuthenticationUser;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Data
public abstract class AbstractCenter {
    @Value("${ibiz.systemid}")
    private String systemid;
    @Value("${ibiz.appid}")
    private String appid;
    @Value("${ibiz.token.header}")
    private String tokenHeader;
    @Value("${ibiz.token.prefix}")
    private String tokenPrefix;

    public <T> T callServiceAPI(String path, JSONObject paramMap, Class<T> clazz) {
        return callServiceAPI(path, paramMap, clazz, HttpMethod.POST);
    }

    /**
     * 调用远程serviceAPI
     *
     * @param path     绝对路径，http://ip:port/rest地址
     * @param paramMap 传递参数。
     * @param clazz    指定返回类型
     * @param method   请求类型
     * @param <T>      指定返回类型
     * @return
     */
    public <T> T callServiceAPI(String path, JSONObject paramMap, Class<T> clazz, HttpMethod method) {

        RestTemplate restTemplate = HttpUtils.getRestTemplate();
        if (paramMap == null) {
            paramMap = new JSONObject();
        }
        HttpHeaders headers = getHttpHeader(paramMap);
        HttpEntity<String> formEntity = new HttpEntity<>(paramMap.toString(), headers);
        ResponseEntity<T> resp = restTemplate.exchange(path, method, formEntity, clazz);
        return resp.getBody();
    }

    /**
     * 解析RESTful路径为可访问字符串
     *
     * @param path       eg: http://ip:port/rest/entity1/{id1}/entity2/{id2}
     * @param replaceIds 替换的id,变长参数。
     * @return 可访问地址，eg: http://ip:port/rest/entity1/1/entity2/2
     */
    public static String getRestPath(String path, String... replaceIds) {
        String regex = "\\{([^}]*)\\}";
        for (String replaceId : replaceIds) {
            path = path.replaceFirst(regex, replaceId);
        }
        return path;
    }

    /**
     * 获得请求头。
     * token - 从security容器中获取
     * systemid、appid - 从yml文件中读取。
     *
     * @return
     */
    private HttpHeaders getHttpHeader(JSONObject paramMap) {
        String activeToken = AuthenticationUser.getAuthenticationUser().getToken();
        activeToken = (activeToken == null ? paramMap.getString(tokenHeader) : activeToken);
        HttpHeaders headers = new HttpHeaders();
        if (activeToken != null) {
            headers.add(tokenHeader, tokenPrefix + activeToken);
        }
        headers.add("Content-Type", "application/json");
        headers.add("systemid", systemid);
        headers.add("appid", appid);
        return headers;
    }
}
