package com.ibz.demo.ibizutil.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ibz.demo.ibizutil.domain.IBZDataAudit;


/**
 * 实体[DataAudit] 服务对象接口
 */
public interface IBZDataAuditService extends IService<IBZDataAudit>{

}