package com.ibz.demo.ibizutil.center;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 安全功能调用接口
 */
public interface ISecurity {
    /**
     * 统一资源
     *
     * @param loginname 登录账号
     * @return  统一资源列表
     */
    JSONArray getUniRes(String loginname);

    /**
     * 表格权限校验
     *
     * @param paramMap
     * @return
     */
    boolean hasGridPermission(JSONObject paramMap);

    /**
     * 表单权限校验
     *
     * @param paramMap
     * @return
     */
    boolean hasFormPermission(JSONObject paramMap);

    /**
     * 获取表格（READ）数据能力
     *
     * @param paramMap
     * @return
     */
    JSONArray getDataAbilities(JSONObject paramMap);
}
