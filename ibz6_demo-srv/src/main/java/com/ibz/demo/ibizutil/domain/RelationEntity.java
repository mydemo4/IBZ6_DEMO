package com.ibz.demo.ibizutil.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Map;


/**
 * 关系实体数据对象
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RelationEntity extends EntityBase implements Serializable{

    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private Map<String,String> srfparentdata;
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private String srfkeys;
}