package com.ibz.demo.ibizutil.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ibz.demo.ibizutil.domain.Worklist;
import com.ibz.demo.ibizutil.domain.WorklistSearchFilter;
import com.ibz.demo.ibizutil.mapper.IBZWorkFlowMapper;
import com.ibz.demo.ibizutil.service.ServiceImplBase;
import com.ibz.demo.ibizutil.service.IBZWorklistService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class IBZWorklistServiceImpl extends ServiceImplBase<IBZWorkFlowMapper, Worklist> implements IBZWorklistService {


    @Resource
    private IBZWorkFlowMapper worklistMapper;


    @Override
    public boolean create(Worklist et) {
        this.beforeCreate(et);
        return super.create(et);
    }
    @Override
    public boolean update(Worklist et){
        this.beforeUpdate(et);
        this.worklistMapper.updateOne(et);
        this.get(et);
        return true;
    }

    public Object getEntityKey(Worklist et){
        return et.getWorklistid();
    }

    @Override
    public boolean getDraft(Worklist et)  {
        return true;
    }

    public List<Worklist> listMy(WorklistSearchFilter filter) {
        return worklistMapper.searchMy(filter,filter.getSelectCond());
    }

    public Page<Worklist> searchMy(WorklistSearchFilter filter) {
        return worklistMapper.searchMy(filter.getPage(),filter,filter.getSelectCond());
    }


    public List<Worklist> listDefault(WorklistSearchFilter filter) {
        return worklistMapper.searchDefault(filter,filter.getSelectCond());
    }

    public Page<Worklist> searchDefault(WorklistSearchFilter filter) {
        return worklistMapper.searchDefault(filter.getPage(),filter,filter.getSelectCond());
    }

    public  List<Map<String,Object>> selectRow(String sql){
        return worklistMapper.selectRow(sql);
    }

}