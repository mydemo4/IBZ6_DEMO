package com.ibz.demo.ibizutil.security;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ibz.demo.ibizutil.helper.RecurseSQLGenerator;
import com.ibz.demo.ibizutil.annotation.PreField;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ibizutil.center.IUser;
import com.ibz.demo.ibizutil.domain.EntityBase;
import com.ibz.demo.ibizutil.enums.PredefinedType;
import com.ibz.demo.ibizutil.service.SearchFilterBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
/**
 * 表格权限处理类，从权限中心获取当前用户权限，
 * 拼接SQL，过滤出权限外的数据
 */
@Component
public class DataAccessUtil {
    @Autowired
    ISecurity securityCenter;
    @Autowired
    private IUser userCenter;
    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验

    /**
     * 设置当前用户的权限
     *
     * @param searchfilter
     * @param entityBase
     * @param action
     */
    public void setPermissionCond(SearchFilterBase searchfilter, EntityBase entityBase, String action) {

        if (AuthenticationUser.getAuthenticationUser().getSuperuser() == 1 || !enableRemoteValid)//超级管理员则不进行权限校验
            return;

        String entityName = entityBase.getClass().getSimpleName();
        JSONObject paramMap = preparePostParam(entityName, action);
        JSONArray permissionList = securityCenter.getDataAbilities(paramMap);//到权限中心获取当前用户的权限列表
        Map<String, String> permissionField = getPermissionField(entityBase);//获取预置属性
        String permissionSQL = getPermissionSQL(permissionList, permissionField);//根据权限列表拼接权限条件
        fillSeachfilter(searchfilter, permissionSQL);//将QueryWrapper设置到searchfilter中，过滤出权限外的数据
    }

    /**
     * 从权限中心获取当前用户数据能力，并将多个数据能力按照权限类型进行合并
     *
     * @param permissionList  权限中心数据能力列表
     * @param permissionField 预置属性字段
     * @return
     */
    private String getPermissionSQL(JSONArray permissionList, Map<String, String> permissionField) {
        StringBuffer permissionSQL = new StringBuffer();
        String nPermissionSQL = "1<>1";//当用户无权限时拼接的条件
        String resultCond;

        for (int i = 0; i < permissionList.size(); i++) {//数据对象能力集合
            String tempCond;
            JSONObject permissionObj = permissionList.getJSONObject(i);
            tempCond = appendSQLByUserRoleData(permissionObj, permissionField);//单个数据对象能力
            if (!StringUtils.isEmpty(tempCond)) {
                permissionSQL.append(String.format("OR (%s)", tempCond));
            }
        }
        resultCond = permissionSQL.toString();
        if (StringUtils.isEmpty(resultCond))
            resultCond = nPermissionSQL;
        else
            resultCond = parseResult(permissionSQL, "OR");

        return resultCond;
    }

    /**
     * 输出单个数据对象能力Sql
     *
     * @param permissionObj
     * @param permissionField
     * @return
     */
    private String appendSQLByUserRoleData(JSONObject permissionObj, Map<String, String> permissionField) {
        String resultCond;
        StringBuffer permissionSQL = new StringBuffer();
        if (permissionObj.containsKey("isalldata")) {//判断是否为全部数据，若为全部数据，则不在进行权限拼接
            String str_isalldata = permissionObj.getString("isalldata");
            if (str_isalldata.equals("1")) {
                resultCond = " 1=1 ";
                return resultCond;
            }
        }
        for (Map.Entry<String, Object> permissionData : permissionObj.entrySet()) {
            String tempCond = "";
            String permissionKey = permissionData.getKey();
            String permissionValue = String.valueOf(permissionData.getValue());
            if (StringUtils.isEmpty(permissionValue))
                continue;
            if (permissionKey.equalsIgnoreCase("userdr") || permissionKey.equalsIgnoreCase("orgdr") || permissionKey.equalsIgnoreCase("deptdr")) {//本单位
                tempCond = appendSQLByPreDataType(permissionValue, permissionField);
            } else if (permissionKey.equalsIgnoreCase("condcontent")) {
                tempCond = appendSQLByDataRange(permissionValue);
            }
            if (!StringUtils.isEmpty(tempCond)) {
                permissionSQL.append(String.format("AND %s", tempCond));
            }
        }

        resultCond = parseResult(permissionSQL, "AND");
        return resultCond;
    }

    /**
     * 根据数据范围输出Sql
     *
     * @param str_cond_array
     * @return
     */
    private String appendSQLByDataRange(String str_cond_array) {
        String resultCond = "";
        StringBuffer permissionSQL = new StringBuffer();
        if (StringUtils.isEmpty(str_cond_array))
            return resultCond;
        JSONArray cond_array = JSONArray.parseArray(str_cond_array);
        for (int a = 0; a < cond_array.size(); a++) {
            String cond = cond_array.getString(a);
            if (!StringUtils.isEmpty(cond)) {
                permissionSQL.append(String.format("AND %s ", cond));
            }
        }
        resultCond = parseResult(permissionSQL, "AND");
        System.out.println("根据数据范围输出Sql:---------" + resultCond);
        return resultCond;
    }

    /**
     * 输出预置数据类型对应的Sql
     *
     * @param str_permissionValue
     * @param permissionField
     * @return
     */
    private String appendSQLByPreDataType(String str_permissionValue, Map<String, String> permissionField) {
        StringBuffer resultCond = new StringBuffer();
        if (StringUtils.isEmpty(str_permissionValue))
            return "";

        String[] permissions = str_permissionValue.split(";");


        for (String permissionValue : permissions) {
            StringBuffer permissionSQL = new StringBuffer();
            if (StringUtils.isEmpty(permissionValue))
                return resultCond.toString();

            AuthenticationUser authenticationUser = AuthenticationUser.getAuthenticationUser();
            JSONObject userInfo = authenticationUser.getUserInfo();
            if (userInfo == null) {
                JSONObject params = new JSONObject();
                params.put("loginname", authenticationUser.getLoginname());
                userInfo = userCenter.getUserInfo(params);
            }

            permissionSQL.append("OR");

            //如果当前实体预置了组织字段，添加对应的权限过滤。
            if (permissionField.containsKey("orgfield")) {
                String orgField = permissionField.get("orgfield");      //数据库判断中数据库字段，组织
                JSONObject orgObject = userInfo.getJSONObject("org");
                if (orgObject != null) {
                    String org = orgObject.getString("curorg");
                    JSONArray orgParent = orgObject.getJSONArray("porg");
                    JSONArray orgChild = orgObject.getJSONArray("sorg");

                    if (org != null && permissionValue.equalsIgnoreCase("curorg")) {//本单位
                        permissionSQL.append(String.format(" t1.%s ='%s' ", orgField, org));
                    }
                    if (orgParent != null && permissionValue.equalsIgnoreCase("sorg")) {//下级单位
                        permissionSQL.append(String.format(" t1.%s in(%s) ", orgField, formatStringArr(orgChild)));
                    }
                    if (orgChild != null && permissionValue.equalsIgnoreCase("porg")) {//上级单位
                        permissionSQL.append(String.format(" t1.%s in(%s) ", orgField, formatStringArr(orgParent)));
                    }
                }
            }

            //如果当前实体预置了部门字段，添加对应的权限过滤
            if (permissionField.containsKey("orgsecfield")) {
                String orgDeptField = permissionField.get("orgsecfield");//数据库判断中数据库字段，部门
                JSONObject orgDeptObject = userInfo.getJSONObject("orgdept");
                if (orgDeptObject != null) {
                    String orgdept = orgDeptObject.getString("curorgdept");
                    JSONArray orgDeptParent = orgDeptObject.getJSONArray("porgdept");
                    JSONArray orgDeptChild = orgDeptObject.getJSONArray("sorgdept");
                    if (orgdept != null && permissionValue.equalsIgnoreCase("curdept")) {//本部门
                        permissionSQL.append(String.format(" t1.%s = '%s' ", orgDeptField, orgdept));
                    }
                    if (orgDeptParent != null && permissionValue.equalsIgnoreCase("sdept")) {//下级部门
                        permissionSQL.append(String.format(" t1.%s in (%s) ", orgDeptField, formatStringArr(orgDeptChild)));
                    }
                    if (orgDeptChild != null && permissionValue.equalsIgnoreCase("pdept")) {//上级部门
                        permissionSQL.append(String.format(" t1.%s in (%s) ", orgDeptField, formatStringArr(orgDeptParent)));
                    }
                }
            }


            if (permissionValue.equalsIgnoreCase("createman")) {//建立人
                permissionSQL.append(String.format(" t1.createman='%s' ", AuthenticationUser.getAuthenticationUser().getUserid()));
            }
            if (permissionValue.equalsIgnoreCase("updateman")) {//更新人
                permissionSQL.append(String.format(" t1.updateman='%s' ", AuthenticationUser.getAuthenticationUser().getUserid()));
            }

            if (!StringUtils.isEmpty(parseResult(permissionSQL, "OR"))) {
                resultCond = resultCond.append(permissionSQL);
            }
        }
        if (!StringUtils.isEmpty(parseResult(resultCond, "OR"))) {
            return String.format("(%s)", parseResult(resultCond, "OR"));
        }

        return resultCond.toString();
    }

    /**
     * 表格拼接权限条件，过滤出权限数据
     *
     * @param targetDomainObject
     * @param permissionSQL
     * @throws Exception
     */
    private void fillSeachfilter(Object targetDomainObject, String permissionSQL) {
        if (targetDomainObject instanceof SearchFilterBase) {
            SearchFilterBase searchFilterBase = (SearchFilterBase) targetDomainObject;
            QueryWrapper queryWrapper = searchFilterBase.getPermissionCond();
            queryWrapper.apply(permissionSQL);
        }
    }

    /**
     * 转换[a,b]格式字符串到 'a','b'格式
     *
     * @return
     */
    private String formatStringArr(JSONArray array) {
        String[] arr = array.toArray(new String[array.size()]);
        return "'" + String.join("','", arr) + "'";
    }

    /**
     * 获取平台配置的实体属性中，系统预置权限字段
     * 获取实体权限字段 orgid/orgsecid
     *
     * @param entityBase
     * @return
     */
    private Map<String, String> getPermissionField(EntityBase entityBase) {
        Map<String, String> permissionFiled = new HashMap<>();
        String orgField = "";  //组织权限默认值
        String orgsecField = ""; //部门权限默认值
        Map<Field, PreField> preFields = entityBase.SearchPreField(); //从缓存中获取当前类预置属性
        //寻找实体权限属性
        for (Map.Entry<Field, PreField> entry : preFields.entrySet()) {
            Field prefield = entry.getKey();//获取注解字段
            PreField fieldAnnotation = entry.getValue();//获取注解值
            PredefinedType prefieldType = fieldAnnotation.preType();

            //用户配置系统预置属性-组织机构标识
            if (prefieldType == PredefinedType.ORGID) {
                orgField = prefield.getName();
                permissionFiled.put("orgfield", orgField);
            }
            //用户配置系统预置属性-部门标识
            if (prefieldType == PredefinedType.ORGSECTORID) {
                orgsecField = prefield.getName();
                permissionFiled.put("orgsecfield", orgsecField);
            }

        }
        return permissionFiled;
    }

    /**
     * 准备请求参数
     *
     * @param entityid
     * @param action
     * @return
     */
    private JSONObject preparePostParam(String entityid, String action) {
        String userid = AuthenticationUser.getAuthenticationUser().getUserid();
        JSONObject paramMap = new JSONObject();
        paramMap.put("entity", entityid);
        paramMap.put("action", action);
        paramMap.put("userid", userid);
        return paramMap;
    }


    /**
     * 格式转换
     *
     * @param cond
     * @param operator
     * @return
     */
    private String parseResult(StringBuffer cond, String operator) {
        String resultCond = cond.toString();
        if (resultCond.startsWith(operator))
            resultCond = resultCond.replaceFirst(operator, "");
        if (resultCond.endsWith(operator))
            resultCond = resultCond.substring(0, resultCond.lastIndexOf(operator));
        return resultCond;
    }

    /**
     * 检查当前登录用户是否存在上下级组织或者部门数据
     *
     * @param permissionValue 当前用户权限表达式
     * @param orgObject       组织信息
     * @param orgDeptObject   部门信息
     * @return
     */
    private boolean checkNullValue(String permissionValue, JSONObject orgObject, JSONObject orgDeptObject) {

        if (!(permissionValue.equalsIgnoreCase("porg") || permissionValue.equalsIgnoreCase("sorg") ||
                permissionValue.equalsIgnoreCase("pdept") || permissionValue.equalsIgnoreCase("sdept")))
            return false;

        JSONArray tempValue = null;
        boolean flag = false;
        if (permissionValue.equalsIgnoreCase("porg") || permissionValue.equalsIgnoreCase("sorg")) {
            tempValue = orgObject.getJSONArray(permissionValue.toLowerCase());
        } else if (permissionValue.equalsIgnoreCase("pdept") || permissionValue.equalsIgnoreCase("sdept")) {
            tempValue = orgDeptObject.getJSONArray(permissionValue.toLowerCase());
        }
        if (StringUtils.isEmpty(tempValue))
            return true;
        if (tempValue.size() == 0)
            return true;
        return flag;
    }
}