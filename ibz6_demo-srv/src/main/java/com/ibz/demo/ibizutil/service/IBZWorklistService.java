package com.ibz.demo.ibizutil.service;

import com.ibz.demo.ibizutil.domain.Worklist;
import com.ibz.demo.ibizutil.domain.WorklistSearchFilter;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;

/**
 * 实体[Worklist] 服务对象接口
 */
public interface IBZWorklistService extends IService<Worklist>{

	boolean update(Worklist et);
	boolean create(Worklist et);
	Worklist get(Worklist et);
	boolean checkKey(Worklist et);
	boolean getDraft(Worklist et);
	boolean remove(Worklist et);
    List<Worklist> listMy(WorklistSearchFilter filter) ;
	Page<Worklist> searchMy(WorklistSearchFilter filter);
    List<Worklist> listDefault(WorklistSearchFilter filter) ;
	Page<Worklist> searchDefault(WorklistSearchFilter filter);
	/**	 实体[Worklist] 自定义数据查询  	**/
	List<Map<String,Object>> selectRow(String sql);
 }