package com.ibz.demo.ibizutil.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ibz.demo.ibizutil.domain.IBZDataAudit;

public interface IBZDataAuditMapper extends BaseMapper<IBZDataAudit> {

}