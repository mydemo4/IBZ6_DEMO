package com.ibz.demo.ibizutil.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ibz.demo.ibizutil.domain.IBZFILE;
import com.ibz.demo.ibizutil.mapper.IBZFILEMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBZFILE] 服务对象接口
 */
public interface IBZFILEService extends IService<IBZFILE>
{
}