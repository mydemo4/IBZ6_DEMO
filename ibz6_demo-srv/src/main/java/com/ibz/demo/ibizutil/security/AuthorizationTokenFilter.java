package com.ibz.demo.ibizutil.security;

import com.alibaba.fastjson.JSONObject;
import com.ibz.demo.ibizutil.center.IAuth;
import com.ibz.demo.ibizutil.center.ISecurity;
import com.ibz.demo.ibizutil.center.IUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.security.core.userdetails.UserDetails;
import io.jsonwebtoken.ExpiredJwtException;

@Slf4j
@Component
public class AuthorizationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private IAuth authCenter;
    @Autowired
    private IUser userCenter;
    private final UserDetailsService userDetailsService;
    private final AuthTokenUtil authTokenUtil;
    @Value("${ibiz.token.header}")
    private String tokenHeader;
    @Value("${ibiz.token.prefix}")
    private String tokenPrefix;
    @Value("${ibiz.auth.path:/ibizutil/login}")
    private String loginPath;
    @Value("${ibiz.enableRemoteValid:false}")
    boolean enableRemoteValid;  //是否开启权限校验

    public AuthorizationTokenFilter(@Qualifier("AuthenticationUserService") UserDetailsService userDetailsService, AuthTokenUtil authTokenUtil, @Value("${ibiz.token.header}") String tokenHeader) {
        this.userDetailsService = userDetailsService;
        this.authTokenUtil = authTokenUtil;
        this.tokenHeader = tokenHeader;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if(enableRemoteValid){
            remoteLoginValid(request,response,chain);
        }
        else{
            localLoginValid(request,response,chain);
        }
    }

    /**
     * 远程登录认证
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    public void remoteLoginValid(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        JSONObject authResp = null;
        String authToken = null;
        JSONObject tokenJSON = new JSONObject();
        final String requestHeader = request.getHeader(this.tokenHeader);
        if (requestHeader != null && requestHeader.startsWith(tokenPrefix) && !loginPath.equals(request.getRequestURI())) {
            authToken = requestHeader.substring(7);
            tokenJSON.put(tokenHeader, authToken);
            //认证中心验证token
            authResp = authCenter.validate(tokenJSON);
        }
        if (authResp != null && SecurityContextHolder.getContext().getAuthentication() == null ) {
            //校验通过，构造授权用户
            AuthenticationUser userDetails = new AuthenticationUser();
            userDetails.setToken(authToken);
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            //从用户中心获取用户信息
            JSONObject userInfo = userCenter.getUserInfo(authResp);
            userDetails.setUserInfo(userInfo);
        }
        chain.doFilter(request, response);
    }

    /**
     *本地登录认证
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    public void localLoginValid(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        final String requestHeader = request.getHeader(this.tokenHeader);

        String username = null;
        String authToken = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                username = authTokenUtil.getUsernameFromToken(authToken);
            } catch (ExpiredJwtException e) {
                log.error(e.getMessage());
            }
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            // It is not compelling necessary to load the use details from the database. You could also store the information
            // in the token and read it from it. It's up to you ;)
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

            // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
            // the database compellingly. Again it's up to you ;)
            if (authTokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                log.info("authorizated user '{}', setting security context", username);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }
}
