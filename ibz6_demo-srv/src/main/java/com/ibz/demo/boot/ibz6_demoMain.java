package com.ibz.demo.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.ibz.demo.config.liquibase.MasterProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication(exclude = {
 org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
	    org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientAutoConfiguration.class,
	    org.springframework.boot.autoconfigure.security.oauth2.OAuth2AutoConfiguration.class,
		DruidDataSourceAutoConfigure.class
	})
@ComponentScans({
@ComponentScan(basePackages = {"com.ibz.demo"}

				),
})
@MapperScan("com.ibz.demo.*.mapper")
@EnableConfigurationProperties({MasterProperties.class})
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@EnableFeignClients(basePackages = {"com.ibz.demo" })
//@EnableEurekaClient
public class ibz6_demoMain extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(ibz6_demoMain.class);
                builder.run(args) ;
	}

}
