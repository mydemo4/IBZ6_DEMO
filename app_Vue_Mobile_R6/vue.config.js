const path = require('path');
const os = require('os');

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    publicPath: './',
    // 去除 map 文件
    productionSourceMap: false,
    outputDir:"../ibz6_demo-srv/target/classes/META-INF/resources",
    devServer: {
        host: '0.0.0.0',
        port: 8111,
        compress: true,
        disableHostCheck: true,
        proxy: "http://localhost:8080",
        historyApiFallback: {
            rewrites: [
                // { from: /^\/appindex$/, to: '/appindex.html' },
            ]
        }
    }
}